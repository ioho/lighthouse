/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento  maintenance:enable

git pull origin master

composer install --no-dev --ignore-platform-reqs

rm -rf pub/static/*

rm -rf generated/*

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento deploy:mode:set production --skip-compilation

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento set:up

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento setup:di:compile

date

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento  setup:static-content:deploy it_IT en_US  -f

php bin/magento setup:static-content:deploy en_US -f

date

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento  indexer:reindex

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento  cache:enable

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento c:c

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento  c:f

/usr/bin/php7.4 /var/www/www.lightech.it/htdocs/bin/magento  maintenance:disable

date
