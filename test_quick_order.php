<?php

ini_set('display_errors',1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
ini_set('max_execution_time', 144000);
ini_set("memory_limit", '16384M');

use Magento\Framework\App\Bootstrap;

require __DIR__ . '/./app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);

$obj = $bootstrap->getObjectManager();

$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

echo 'START';

$quote = $obj->get('Mageplaza\QuickOrder\Cron\GenerateJsonFile')->execute();

echo 'FINISH';

?>
