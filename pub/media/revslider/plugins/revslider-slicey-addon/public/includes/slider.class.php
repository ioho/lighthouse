<?php
/* 
 * @author    ThemePunch <info@themepunch.com>
 * @link      http://www.themepunch.com/
 * @copyright 2017 ThemePunch
*/

require_once(RS_SLICEY_PLUGIN_PATH . 'framework/slider.front.class.php');

use Nwdthemes\Revslider\Helper\Framework;
use Nwdthemes\Revslider\Model\Revslider\RevSliderSlider;

class RsSliceySliderFront extends RsAddonSliceySliderFront {
	
	protected static $_Version,
					 $_PluginUrl, 
					 $_PluginTitle;
					 
	public function __construct(
	    $_version,
        $_pluginUrl,
        $_pluginTitle,
        $_isAdmin = false,
        Framework $frameworkHelper,
        RevSliderSlider $revSliderSlider
    ) {

	    parent::__construct($frameworkHelper, $revSliderSlider);
		
		static::$_Version     = $_version;
		static::$_PluginUrl   = $_pluginUrl;
		static::$_PluginTitle = $_pluginTitle;
		
		if(!$_isAdmin) {
		
			parent::enqueueScripts();
			
		}
		else {
		
			parent::enqueuePreview();
			
		}
		
		parent::writeInitScript();
		
	}
	
}