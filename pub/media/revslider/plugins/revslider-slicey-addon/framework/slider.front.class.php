<?php
/* 
 * @author    ThemePunch <info@themepunch.com>
 * @link      http://www.themepunch.com/
 * @copyright 2017 ThemePunch
*/

use Nwdthemes\Revslider\Model\Revslider\Framework\RevSliderFunctions;

class RsAddonSliceySliderFront {

    protected $_frameworkHelper;
    protected $_revSliderSlider;

    public function __construct(
        Nwdthemes\Revslider\Helper\Framework $frameworkHelper,
        Nwdthemes\Revslider\Model\Revslider\RevSliderSlider $revSliderSlider
    ) {
        $this->_frameworkHelper = $frameworkHelper;
        $this->_revSliderSlider = $revSliderSlider;
    }

	protected function enqueueScripts() {

		$this->_frameworkHelper->add_action('revslider_slide_initByData', array($this, 'enqueue_scripts'), 10, 1);
		
	}
	
	protected function enqueuePreview() {

        $this->_frameworkHelper->add_action('revslider_preview_slider_head', array($this, 'enqueue_preview'));
		
	}
	
	protected function writeInitScript() {
        $this->_frameworkHelper->add_action('revslider_frontend_require', array($this, 'add_required_scripts'), 10, 2);
        $this->_frameworkHelper->add_action('revslider_fe_javascript_output', array($this, 'write_init_script'), 10, 2);
	}

    public function enqueue_scripts($_record) {

        if(empty($_record)) return $_record;

        $_params = RevSliderFunctions::getVal($_record, 'params', false);
        $_sliderId = RevSliderFunctions::getVal($_record, 'slider_id', false);

        if(empty($_params) || empty($_sliderId)) return $_record;

        $_params = json_decode($_params);
        if(empty($_params)) return $_record;

        $_slider = $this->_revSliderSlider;
        $_slider->initByID($_sliderId);

        if(empty($_slider)) return $_record;

        $_settings = $_slider->getParams();
        if(empty($_settings)) return $_record;

        $_enabled = RevSliderFunctions::getVal($_settings, static::$_PluginTitle . '_enabled', false) == 'true';
        if(empty($_enabled)) return $_record;

		$_handle       = 'rs-' . static::$_PluginTitle . '-front';
		$_base         = static::$_PluginUrl . 'public/assets/';

        $this->_frameworkHelper->wp_enqueue_script(
			$_handle,
			$_base . 'js/revolution.addon.' . static::$_PluginTitle . '.min.js', 
			array('jquery', 'revmin'), 
			static::$_Version,
            true
        );

        $this->_frameworkHelper->remove_action('revslider_slide_initByData', array($this, 'enqueue_scripts'), 10);
        return $_record;
    }
	
	public function enqueue_preview() {
		
		$_base = static::$_PluginUrl . 'public/assets/';
		
		?>
		<script type="text/javascript" src="<?php echo $_base . 'js/revolution.addon.' . static::$_PluginTitle . '.min.js'; ?>"></script>
		<?php
		
	}

	public function write_init_script($_slider, $_id) {
		
		$_enabled = $_slider->getParam('slicey_enabled', false) == 'true';
		$_id = $_slider->getID();
		
		if($_enabled) {
	
			echo                  "\n";
			echo '                RevSliderSliceyAddon(jQuery, revapi' . $_id . ');'."\n";

		}
		
	}

    public function add_required_scripts($slider) {
        if ($slider->getParam('slicey_enabled', false) == 'true') {
            $require = static::$_PluginUrl . 'public/assets/js/revolution.addon.' . static::$_PluginTitle . '.min.js';
            echo ", '{$require}'";
        }
    }

}