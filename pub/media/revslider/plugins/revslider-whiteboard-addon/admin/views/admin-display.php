<div id="rev_addon_whiteboard_settings_slideout" class="rs-sbs-slideout-wrapper" style="display:none">
	<div class="rs-sbs-header">
		<div class="rs-sbs-step"><i class="eg-icon-cog"></i></div>
		<div class="rs-sbs-title"><?php echo __('Configure Whiteboard'); ?></div>
		<div class="rs-sbs-close"><i class="eg-icon-cancel"></i></div>
	</div>
	<div class="tp-clearfix"></div>
	<div class="rs-sbs-slideout-inner">

	<!-- Start Settings -->
		<h3 class="tp-steps wb"><span>1</span> <?php echo __('"Add-ons" Tab'); ?></h3>
		<img src="<?php echo WHITEBOARD_PLUGIN_URL . "admin/assets/images/tutorial1.jpg"; ?>">
		<div class="wb-featuretext"><?php echo __('Right after installing the Whiteboard add-on, you are ready for action!'); ?></div>

		<h3 class="tp-steps wb"><span>2</span> <?php echo __('Select "Whiteboard"'); ?></h3>
		<img src="<?php echo WHITEBOARD_PLUGIN_URL . "admin/assets/images/tutorial2.jpg"; ?>">
		<div class="wb-featuretext"><?php echo __('Select a layer you would like to add the Whiteboard effect to, and configure the options.'); ?></div>

		<h3 class="tp-steps wb"><span>3</span> <?php echo __('"Draw" Layers'); ?></h3>
		<img src="<?php echo WHITEBOARD_PLUGIN_URL . "admin/assets/images/tutorial3.jpg"; ?>">
		<div class="wb-featuretext"><?php echo __('The "Draw" setting allows you to let layers appear like they are being drawn. You can configure "Jitter" and hand movement to create your desired effect!'); ?></div>

		<h3 class="tp-steps wb"><span>4</span> <?php echo __('"Write" Text'); ?></h3>
		<img src="<?php echo WHITEBOARD_PLUGIN_URL . "admin/assets/images/tutorial4.jpg"; ?>">
		<div class="wb-featuretext"><?php echo __('The "Write" setting is only available for text layers and creates a great illusion of your texts being written by hand! Similar to the "Draw" effect you can configure the animation to your liking.'); ?></div>

		<h3 class="tp-steps wb"><span>5</span> <?php echo __('"Move" Layers'); ?></h3>
		<img src="<?php echo WHITEBOARD_PLUGIN_URL . "admin/assets/images/tutorial5.jpg"; ?>">
		<div class="wb-featuretext"><?php echo __('Using the "Move" setting you can move-in or reveal any layer. Another cool effect for a kinetic animated whiteboard!'); ?></div>

		<h3 class="tp-steps wb"><span>6</span> <?php echo __('Advanced Options'); ?></h3>
		<img src="<?php echo WHITEBOARD_PLUGIN_URL . "admin/assets/images/tutorial6.jpg"; ?>">
		<div class="wb-featuretext"><?php echo __('In the slider settings you will also find a "Whiteboard" tab, on the right. This is where all the default settings and advanced goodies are found!'); ?></div>
	<!-- End Settings -->
	</div>
</div>
<script type="text/javascript">
    require(['<?php echo WHITEBOARD_PLUGIN_URL; ?>admin/assets/js/rev_addon_dash-admin.min.js'], function() {});
</script>