<?php
/* 
 * @author    ThemePunch <info@themepunch.com>
 * @link      http://www.themepunch.com/
 * @copyright 2017 ThemePunch
*/

use \Nwdthemes\Revslider\Helper\Data;

require_once(RS_SLICEY_PLUGIN_PATH . 'framework/base.class.php');

class RsSliceyBase extends RsAddOnSliceyBase {
	
	protected static $_PluginPath    = RS_SLICEY_PLUGIN_PATH,
					 $_PluginUrl     = RS_SLICEY_PLUGIN_URL,
					 $_PluginTitle   = 'slicey',
				     $_FilePath      = __FILE__,
                     $_Version       = '1.0.2';
	
	public function __construct(
        \Nwdthemes\Revslider\Helper\Framework $frameworkHelper,
        \Nwdthemes\Revslider\Model\Revslider\RevSliderSlider $revSliderSlider
    ) {

	    parent::__construct($frameworkHelper, $revSliderSlider);
		
		// check to make sure all requirements are met
		$notice = $this->systemsCheck();
		if($notice) {
			
			require_once(RS_SLICEY_PLUGIN_PATH . 'framework/notices.class.php');
			
			new RsAddOnSliceyNotice($notice, static::$_PluginTitle, $frameworkHelper);
			return;
			
		}
		
		parent::loadClasses();

	}

}