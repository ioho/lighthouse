<?php
/* 
 * @author    ThemePunch <info@themepunch.com>
 * @link      http://www.themepunch.com/
 * @copyright 2017 ThemePunch
*/

use Nwdthemes\Revslider\Helper\Data;
use Nwdthemes\Revslider\Helper\Framework;
use Nwdthemes\Revslider\Model\Revslider\RevSliderGlobals;
use Nwdthemes\Revslider\Model\Revslider\RevSliderSlider;

class RsAddOnSliceyBase {

    const MINIMUM_VERSION = '5.4.6';

    protected $_frameworkHelper;
    protected $_revSliderSlider;

    public function __construct(
        Framework $frameworkHelper,
        RevSliderSlider $revSliderSlider
    ) {
        $this->_frameworkHelper = $frameworkHelper;
        $this->_revSliderSlider = $revSliderSlider;
    }

	protected function systemsCheck() {
		
		if(!version_compare(RevSliderGlobals::SLIDER_REVISION, RsAddOnSliceyBase::MINIMUM_VERSION, '>=')) {
		
			return 'add_notice_version';
		
		}
		else if($this->_frameworkHelper->get_option('revslider-valid', 'false') == 'false') {
		
			 return 'add_notice_activation';
		
		}
		
		return false;
		
	}
	
	protected function loadClasses() {

		$isAdmin = $this->_frameworkHelper->is_admin();
		
		if($isAdmin) {
			
			//handle update process, this uses the typical ThemePunch server process
			require_once(static::$_PluginPath . 'admin/includes/update.class.php');
			$update_admin = new RevAddOnSliceyUpdate(static::$_Version, $this->_frameworkHelper);

            $this->_frameworkHelper->add_filter('pre_set_site_transient_update_plugins', array($update_admin, 'set_update_transient'));
            $this->_frameworkHelper->add_filter('plugins_api', array($update_admin, 'set_updates_api_results'), 10, 3);
			
			// Add-Ons page
            $this->_frameworkHelper->add_filter('rev_addon_dash_slideouts', array($this, 'addons_page_content'));
			
			// admin CSS/JS
            $this->_frameworkHelper->add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));
			
			require_once(static::$_PluginPath . 'admin/includes/slider.class.php');
			require_once(static::$_PluginPath . 'admin/includes/slide.class.php');
			
			// admin init
			new RsSliceySliderAdmin(static::$_PluginTitle, static::$_Version, $this->_frameworkHelper);
			new RsSliceySlideAdmin(static::$_PluginTitle, static::$_PluginPath, $this->_frameworkHelper);
			
		}
		
		/* 
		frontend scripts always enqueued for admin previews
		*/
		require_once(static::$_PluginPath . 'public/includes/slider.class.php');
		require_once(static::$_PluginPath . 'public/includes/slide.class.php');
		
		new RsSliceySliderFront(static::$_Version, static::$_PluginUrl, static::$_PluginTitle, $isAdmin, $this->_frameworkHelper, $this->_revSliderSlider);
		new RsSliceySlideFront(static::$_PluginTitle, $this->_frameworkHelper);
		
	}
	
	// AddOn's page slideout panel
	public function addons_page_content() {
		
		include_once(static::$_PluginPath . 'admin/views/admin-display.php');
		
	}
	
	// load admin scripts
	public function enqueue_admin_scripts($hook) {

		if($hook === 'toplevel_page_revslider' || $hook === 'slider-revolution_page_rev_addon') {
			
			if(!isset(Data::$_GET['page'])) return;
			
			$page = Data::$_GET['page'];
			if($page !== 'revslider' && $page !== 'rev_addon') return;
			
			$_handle = 'rs-' . static::$_PluginTitle . '-admin';
			$_base   = static::$_PluginUrl . 'admin/assets/';
			
			switch($page) {
				
				case 'revslider':
				
					if(isset(Data::$_GET['view']) && Data::$_GET['view'] === 'slide' && isset(Data::$_GET['id']) && strpos(Data::$_GET['id'], 'static') === false) {

                        $this->_frameworkHelper->wp_enqueue_style($_handle, $_base . 'css/' . static::$_PluginTitle . '-slide-admin.css', array(), static::$_Version);
                        $this->_frameworkHelper->wp_enqueue_script($_handle, $_base . 'js/' . static::$_PluginTitle . '-slide-admin.min.js', array('jquery'), static::$_Version, true);
						
					}
				
				break;
				
				case 'rev_addon':

                    $this->_frameworkHelper->wp_enqueue_style($_handle, $_base . 'css/' . static::$_PluginTitle . '-dash-admin.css', array(), static::$_Version);
                    $this->_frameworkHelper->wp_enqueue_script($_handle, $_base . 'js/' . static::$_PluginTitle . '-dash-admin.min.js', array('jquery'), static::$_Version, true);

				break;
				
			}
			
		}
		
	}

}