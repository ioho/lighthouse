<?php
/* 
 * @author    ThemePunch <info@themepunch.com>
 * @link      http://www.themepunch.com/
 * @copyright 2017 ThemePunch
*/
 
 /*
 
	create fake "Add-On" block to test this widget
	http://pastebin.com/J0wB676U
 
 */
 
?>

<div id="rev_addon_slicey_settings_slideout" class="rs-sbs-slideout-wrapper" style="display:none">

	<div class="rs-sbs-header">
		<div class="rs-sbs-step"><i class="eg-icon-cog"></i></div>
		<div class="rs-sbs-title"><?php echo __('How to use the Slicey Add-On'); ?></div>
		<div class="rs-sbs-close"><i class="eg-icon-cancel"></i></div>
	</div>
	
	<div class="tp-clearfix"></div>
	
	<div class="rs-sbs-slideout-inner">
		
		<h3><span>1</span> <?php echo __('"Enable" Slicey for the <a href="https://cdntphome-themepunchgbr.netdna-ssl.com/wp-content/uploads/2016/12/particles-5.jpg" target="_blank">Slider Settings</a>'); ?></h3>
		<img src="<?php echo RS_SLICEY_PLUGIN_URL . "admin/assets/images/tutorial0.jpg"; ?>">

		<h3><span>2</span> <?php echo __('"Add" some Slicey Layers to your <a href="https://cdntphome-themepunchgbr.netdna-ssl.com/wp-content/uploads/2016/10/slide-settings.jpg" target="_blank">Slides</a>'); ?></h3>
		<img src="<?php echo RS_SLICEY_PLUGIN_URL . "admin/assets/images/tutorial1.jpg"; ?>">

		<h3><span>3</span> <?php echo __('"Adjust" the Slicey Layer Settings'); ?></h3>
		<img src="<?php echo RS_SLICEY_PLUGIN_URL . "admin/assets/images/tutorial2.jpg"; ?>">
		<br><br>
		<img src="<?php echo RS_SLICEY_PLUGIN_URL . "admin/assets/images/tutorial3.jpg"; ?>">
		<br><br>
		<img src="<?php echo RS_SLICEY_PLUGIN_URL . "admin/assets/images/tutorial4.jpg"; ?>">
		
		<h3><span>4</span> <?php echo __('"Preview" the Slide to see the Effect'); ?></h3>
		<img src="<?php echo RS_SLICEY_PLUGIN_URL . "admin/assets/images/tutorial5.jpg"; ?>">
		
	</div>
</div>
<script type="text/javascript">
    require(['<?php echo RS_SLICEY_PLUGIN_URL; ?>admin/assets/js/slicey-dash-admin.min.js'], function() {});
</script>