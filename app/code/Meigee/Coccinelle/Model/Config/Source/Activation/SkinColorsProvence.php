<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class SkinColorsProvence implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'yellow', 'label' => __('Yellow')],
			  ['value' => 'light_blue', 'label' => __('Light Blue')],
			  ['value' => 'beige', 'label' => __('Beige')],
			  ['value' => 'pink', 'label' => __('Pink')]
		];
    }
}
