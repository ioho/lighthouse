<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class SkinColorsBurgundy implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'white', 'label' => __('White')],
			  ['value' => 'beige', 'label' => __('Beige')],
			  ['value' => 'white2', 'label' => __('White 2')],
			  ['value' => 'beige2', 'label' => __('Beige 2')]
		];
    }
}
