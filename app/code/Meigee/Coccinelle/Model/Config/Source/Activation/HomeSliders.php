<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class HomeSliders implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'coccinelle_header_slider', 'label' => __('Default slider')],
			  ['value' => 'coccinelle_berry_2_header_slider', 'label' => __('Berry 2 Slider')],
			  ['value' => 'coccinelle_berry_3_header_slider', 'label' => __('Berry 3 Slider')],
			  ['value' => 'coccinelle_normandy_header_slider', 'label' => __('Normandy Slider')],
			  ['value' => 'coccinelle_normandy_2_header_slider', 'label' => __('Normandy 2 Slider')],
			  ['value' => 'coccinelle_normandy_3_slider', 'label' => __('Normandy 3 Slider')],
			  ['value' => 'coccinelle_normandy_4_slider', 'label' => __('Normandy 4 Slider')],
			  ['value' => 'coccinelle_champagne_header_slider', 'label' => __('Champagne Slider')],
			  ['value' => 'coccinelle_champagne_2_header_slider', 'label' => __('Champagne 2 Slider')],
			  ['value' => 'coccinelle_champagne_3_header_slider', 'label' => __('Champagne 3 Slider')],
			  ['value' => 'coccinelle_champagne_4_slider', 'label' => __('Champagne 4 Slider')],
			  ['value' => 'coccinelle_brittany_slider', 'label' => __('Brittany Slider')],
			  ['value' => 'coccinelle_burgundy_header_slider', 'label' => __('Burgundy Slider')]
		];
    }
}
