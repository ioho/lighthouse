<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class SkinColorsBerry implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'black', 'label' => __('Black')],
			  ['value' => 'crimson', 'label' => __('Crimson')],
			  ['value' => 'purple', 'label' => __('Purple')],
			  ['value' => 'yellow', 'label' => __('Yellow')]
		];
    }
}
