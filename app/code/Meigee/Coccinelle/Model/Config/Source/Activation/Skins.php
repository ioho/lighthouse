<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class Skins implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'skin_berry', 'label' => __('Skin 1 (Berry)')],
			  ['value' => 'skin_brittany', 'label' => __('Skin 2 (Brittany)')],
			  ['value' => 'skin_champagne', 'label' => __('Skin 3 (Champagne)')],
			  ['value' => 'skin_lorraine', 'label' => __('Skin 4 (Lorraine)')],
			  ['value' => 'skin_normandy', 'label' => __('Skin 5 (Normandy)')],
			  ['value' => 'skin_provence', 'label' => __('Skin 6 (Provence)')],
			  ['value' => 'skin_burgundy', 'label' => __('Skin 7 (Burgundy)')]
		];
    }
}
