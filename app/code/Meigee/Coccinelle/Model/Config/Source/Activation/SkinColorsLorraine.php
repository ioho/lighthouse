<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class SkinColorsLorraine implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			['value' => 'black1', 'label' => __('Black with Blue')],
			['value' => 'black2', 'label' => __('Black with Orange')],
			['value' => 'purple', 'label' => __('Purple')],
			['value' => 'burgundy', 'label' => __('Burgundy')]
		];
    }
}
