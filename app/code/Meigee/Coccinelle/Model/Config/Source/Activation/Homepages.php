<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class Homepages implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			['value' => 'home', 'label' => __('Coccinelle - Responsive Ready Magento theme')],
			['value' => 'coccinelle-berry-2-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Berry 2)')],
			['value' => 'coccinelle-berry-3-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Berry 3)')],
			['value' => 'coccinelle-berry-4-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Berry 4)')],
			['value' => 'coccinelle-normandy-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Normandy)')],
			['value' => 'coccinelle-normandy-2-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Normandy 2)')],
			['value' => 'coccinelle-normandy-3-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Normandy 3)')],
			['value' => 'coccinelle-normandy-4-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Normandy 4)')],
			['value' => 'coccinelle-champagne-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Champagne)')],
			['value' => 'coccinelle-champagne-2-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Champagne 2)')],
			['value' => 'coccinelle-champagne-3-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Champagne 3)')],
			['value' => 'coccinelle-champagne-4-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Champagne 4)')],
			['value' => 'coccinelle-brittany-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Brittany)')],
			['value' => 'coccinelle-brittany-2-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Brittany 2)')],
			['value' => 'coccinelle-brittany-3-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Brittany 3)')],
			['value' => 'coccinelle-brittany-4-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Brittany 4)')],
			['value' => 'coccinelle-burgundy-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Burgundy)')],
			['value' => 'coccinelle-burgundy-2-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Burgundy 2)')],
			['value' => 'coccinelle-burgundy-3-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Burgundy 3)')],
			['value' => 'coccinelle-burgundy-4-home', 'label' => __('Coccinelle - Responsive Ready Magento theme (Burgundy 4)')]
		];
    }
}
