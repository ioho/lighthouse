<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class SkinColorsNormandy implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'light_blue', 'label' => __('Light Blue')],
			  ['value' => 'beige', 'label' => __('Beige')],
			  ['value' => 'crimson', 'label' => __('Crimson')],
			  ['value' => 'red', 'label' => __('Red')]
		];
    }
}
