<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class SkinColorsChampagne implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'blue', 'label' => __('Blue')],
			  ['value' => 'crimson', 'label' => __('Crimson')],
			  ['value' => 'yellow1', 'label' => __('Yellow with dark Header')],
			  ['value' => 'yellow2', 'label' => __('Yellow')],
		];
    }
}
