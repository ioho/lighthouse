<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class Footers implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'coccinelle_footer', 'label' => __('Default footer')],
			  ['value' => 'coccinelle_normandy_footer', 'label' => __('Normandy Footer')],
			  ['value' => 'coccinelle_champagne_footer', 'label' => __('Champagne Footer')],
			  ['value' => 'coccinelle_brittany_footer', 'label' => __('Brittany Footer')],
			  ['value' => 'coccinelle_burgundy_footer', 'label' => __('Burgundy Footer')]
		];
    }
}
