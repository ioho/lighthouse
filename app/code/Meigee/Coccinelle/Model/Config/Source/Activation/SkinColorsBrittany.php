<?php
namespace Meigee\Coccinelle\Model\Config\Source\Activation;

class SkinColorsBrittany implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'orange', 'label' => __('Orange')],
			  ['value' => 'yellow', 'label' => __('Yellow')],
			  ['value' => 'light_blue', 'label' => __('Light Blue')],
			  ['value' => 'black', 'label' => __('Black')]
		];
    }
}
