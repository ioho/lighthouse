<?php
namespace Meigee\Coccinelle\Model\Config\Source;

class ProductsGridHover implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('Default Hover')],
            ['value' => 2, 'label' => __('Hover #2')],
            ['value' => 3, 'label' => __('Hover #3')]
        ];
    }
}
