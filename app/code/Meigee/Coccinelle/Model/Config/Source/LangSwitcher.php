<?php
namespace Meigee\Coccinelle\Model\Config\Source;

class LangSwitcher implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'language_select', 'label' => __('Select Box'), 'img' => 'Meigee_Coccinelle::images/language_select.png'],
			  ['value' => 'language_flags', 'label' => __('Flags'), 'img' => 'Meigee_Coccinelle::images/language_flags.png'],
			  ['value' => 'language_list', 'label' => __('List'), 'img' => 'Meigee_Coccinelle::images/language_list.png'],
		];
    }
}
