<?php
namespace Meigee\Coccinelle\Model\Config\Source;

class CurrencySwitcherTemplate implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
		return [
			  ['value' => 'currency_select', 'label' => __('Select Box'), 'img' => 'Meigee_Coccinelle::images/currency_select.png'],
			  ['value' => 'currency_images', 'label' => __('List'), 'img' => 'Meigee_Coccinelle::images/currency_images.png']
		];
    }
}
