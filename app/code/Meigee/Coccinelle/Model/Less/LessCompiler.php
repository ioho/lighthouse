<?php
namespace Meigee\Coccinelle\Model\Less;
use Meigee\Coccinelle\Model\Less\Lessc;

class LessCompiler {
	
	const DS = DIRECTORY_SEPARATOR;
	
	public function objectManager()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		return $objectManager;
	}
	
	public function compile($file, $output, $filename)
	{
		$_activation = $this->objectManager()->create('\Meigee\Coccinelle\Block\Frontend\ThemeActivation');
		$_messageManager = $this->objectManager()->create('\Magento\Framework\Message\ManagerInterface');
		$status = false;
		$less = $this->objectManager()->create('\Meigee\Coccinelle\Model\Less\Lessc');
		try {
			$less->compileFile($file, $output);
			$status = true;
		}
		catch(\Exception $e){
			$message = $e->getMessage();
			$_messageManager->addError($message);
        }

		if($status) $_activation->moveFileToCssDir($output, $filename);
	}
	
	public function movetoTmpFolder($file, $folder, $skin, $color)
	{
		$_messageManager = $this->objectManager()->create('\Magento\Framework\Message\ManagerInterface');
		$status = false;
		try {
			$tmpFolder = $folder.self::DS.'tmp';
			$this->deleteTmpFolder($tmpFolder);
			if(!file_exists($tmpFolder)) {
				mkdir($tmpFolder, 0777);
			}
			$skinFolder = $folder.self::DS.$skin;
			$skinColor = $skinFolder.self::DS.$color.'.less';
			copy($file, $tmpFolder.self::DS.$skin.'.less');
			copy($skinColor, $tmpFolder.self::DS.'colors.less');
			$status = true;
		}
		catch(\Exception $e){
			$message = $e->getMessage();
			$_messageManager->addError($message);
        }
		if($status) $this->compile($tmpFolder.self::DS.$skin.'.less', $tmpFolder.self::DS.$skin.'_'.$color.'.css', $skin.'_'.$color.'.css');
	}
	
	public function deleteTmpFolder($folder)
	{
		if (! is_dir($folder)) {
			throw new InvalidArgumentException("$folder must be a directory");
		}
		if (substr($folder, strlen($folder) - 1, 1) != '/') {
			$folder .= '/';
		}
		$files = glob($folder . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($folder);
	}
	
}