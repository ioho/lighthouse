<?php 
namespace Meigee\Coccinelle\Model;

class cssGenerate
{
	
	public function __construct(\Meigee\Coccinelle\Block\Frontend\CustomDesign $customDesign)
	{
		$this->_customDesign = $customDesign;
    }

	public function issetVar($var)
	{
		if(isset($var) && $var != null) {
			return true;
		}
		return false;
	}

	public function cssData( $variables, $selector, $properties )
	{
		$newData = '';

		foreach ( $variables as $index => $variable)
		{
			$property = $properties[$index];

			if ( isset( $variable ) && !empty( $variable ) )
			{
				$newData .= PHP_EOL.$selector.'{'.$property.': '.$variable.'}';
			}

		}

		return $newData;
	}

	public function getDesignValues( $sectionId )
	{
		// General options
		$cssData = '/***** New Css styles *****/';

	    // General Tab
		$general_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_bg');
		$general_bg_container = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_bg_container');
		$elements_border = $this->_customDesign->getConfigData($sectionId, 'general_options', 'elements_border');
		$general_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_color');
		$title_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'title_color');
		$links_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'links_color_d');
		$links_color_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'links_color_h');
		$links_color_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'links_color_a');
		$links_color_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'links_color_f');
		// Prices
		$regular_price = $this->_customDesign->getConfigData($sectionId, 'general_options', 'regular_price');
		$special_price = $this->_customDesign->getConfigData($sectionId, 'general_options', 'special_price');
		$old_price = $this->_customDesign->getConfigData($sectionId, 'general_options', 'old_price');
		$sold_out = $this->_customDesign->getConfigData($sectionId, 'general_options', 'sold_out');
		// Breadcrumbs
		$breadcrumbs_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'breadcrumbs_color_d');
		$breadcrumbs_color_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'breadcrumbs_color_h');
		$breadcrumbs_color_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'breadcrumbs_color_a');
		$breadcrumbs_color_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'breadcrumbs_color_f');
		// Form Elements
		$general_input_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_input_color');
		$general_input_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_input_bg');
		$general_input_border = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_input_border');
		$general_input_color_focused = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_input_color_focused');
		$general_input_bg_focused = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_input_bg_focused');
		$general_input_border_focused = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_input_border_focused');
		$general_select_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_select_color');
		$general_select_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_select_bg');
		// Reviews design
		$reviews_rating_general_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'reviews_rating_general_color');
		$reviews_rating_active_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'reviews_rating_active_color');
		$reviews_rating_links_color_hover = $this->_customDesign->getConfigData($sectionId, 'general_options', 'reviews_rating_links_color_hover');

		// Buttons Tab
		// Button Type 1
		$general_btn1_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_color_d');
		$general_btn1_color_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_color_h');
		$general_btn1_color_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_color_a');
		$general_btn1_color_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_color_f');
		$general_btn1_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_bg_d');
		$general_btn1_bg_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_bg_h');
		$general_btn1_bg_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_bg_a');
		$general_btn1_bg_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_bg_f');
		$general_btn1_border = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_border_d');
		$general_btn1_border_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_border_h');
		$general_btn1_border_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_border_a');
		$general_btn1_border_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn1_border_f');
		// Button Type 2
		$general_btn2_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_color_d');
		$general_btn2_color_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_color_h');
		$general_btn2_color_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_color_a');
		$general_btn2_color_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_color_f');
		$general_btn2_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_bg_d');
		$general_btn2_bg_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_bg_h');
		$general_btn2_bg_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_bg_a');
		$general_btn2_bg_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_bg_f');
		$general_btn2_border = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_border_d');
		$general_btn2_border_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_border_h');
		$general_btn2_border_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_border_a');
		$general_btn2_border_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_btn2_border_f');
		// Slider arrows
		$general_slider_btns_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_slider_btns_color_d');
		$general_slider_btns_color_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_slider_btns_color_h');
		$general_slider_btns_color_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_slider_btns_color_a');
		$general_slider_btns_color_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_slider_btns_color_f');

		// Home Page Tab
		// Subscribe block
		$general_home_subscribe_title_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_title_color');
		$general_home_subscribe_text_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_text_color');
		$general_home_subscribe_input_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_input_bg');
		$general_home_subscribe_input_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_input_color');
		$general_home_subscribe_input_border = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_input_border');
		$general_home_subscribe_input_bg_focused = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_input_bg_focused');
		$general_home_subscribe_input_color_focused = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_input_color_focused');
		$general_home_subscribe_input_border_focused = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_input_border_focused');
		$general_home_subscribe_btn_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_btn_bg_d');
		$general_home_subscribe_btn_bg_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_btn_bg_h');
		$general_home_subscribe_btn_bg_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_btn_bg_a');
		$general_home_subscribe_btn_bg_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_btn_bg_f');
		$general_home_subscribe_btn_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_btn_color_d');
		$general_home_subscribe_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_btn_color_h');
		$general_home_subscribe_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_btn_color_a');
		$general_home_subscribe_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_subscribe_btn_color_f');
		// Home Slider
		$general_home_slider_text_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_text_color');
		$general_home_slider_arrows_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_arrows_color_d');
		$general_home_slider_arrows_color_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_arrows_color_h');
		$general_home_slider_arrows_color_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_arrows_color_a');
		$general_home_slider_arrows_color_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_arrows_color_f');
		$general_home_slider_arrows_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_arrows_bg_d');
		$general_home_slider_arrows_bg_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_arrows_bg_h');
		$general_home_slider_arrows_bg_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_arrows_bg_a');
		$general_home_slider_arrows_bg_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_arrows_bg_f');
		$general_home_slider_dots_border = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_dots_border');
		$general_home_slider_dot_active_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_slider_dot_active_color');
		// Home Shop By
		$general_home_shop_by_container_bg = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_shop_by_container_bg_d');
		$general_home_shop_by_container_bg_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_shop_by_container_bg_h');
		$general_home_shop_by_container_bg_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_shop_by_container_bg_a');
		$general_home_shop_by_container_bg_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_shop_by_container_bg_f');
		$general_home_shop_by_text_color = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_shop_by_text_color_d');
		$general_home_shop_by_text_color_h = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_shop_by_text_color_h');
		$general_home_shop_by_text_color_a = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_shop_by_text_color_a');
		$general_home_shop_by_text_color_f = $this->_customDesign->getConfigData($sectionId, 'general_options', 'general_home_shop_by_text_color_f');

	    // General Tab
		$header_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_bg');
		$header_top_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_top_bg');
		$header_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_color');
		$header_border = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_border');

		// Search
		$header_search_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search_bg');
		$header_search_border = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search_border');
		$header_search_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search_color');
		$header_search_btn_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search_btn_color_d');
		$header_search_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search_btn_color_h');
		$header_search_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search_btn_color_a');
		$header_search_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search_btn_color_f');
		// Header Search (Type 2)
		$header_search2_btn_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search2_btn_color_d');
		$header_search2_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search2_btn_color_h');
		$header_search2_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search2_btn_color_a');
		$header_search2_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_search2_btn_color_f');

		// Account/Options
		// Header Account/Options Button
		$header_options_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_color_d');
		$header_options_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_color_h');
		$header_options_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_color_a');
		$header_options_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_color_f');
		// Header Account/Options dropdown
		$header_options_dropdown_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_bg');
		$header_options_dropdown_links_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_links_color_d');
		$header_options_dropdown_links_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_links_color_h');
		$header_options_dropdown_links_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_links_color_a');
		$header_options_dropdown_links_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_links_color_f');
		$header_options_dropdown_labels_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_labels_color');
		$header_options_dropdown_links_border = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_links_border');
		$header_options_dropdown_select_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_select_bg');
		$header_options_dropdown_select_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_options_dropdown_select_color');

		// Wishlist
		// Header Wishlist Button
		$header_wishlist_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_wishlist_color_d');
		$header_wishlist_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_wishlist_color_h');
		$header_wishlist_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_wishlist_color_a');
		$header_wishlist_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_wishlist_color_f');
		$header_wishlist_qty_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_wishlist_qty_bg');
		$header_wishlist_qty_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_wishlist_qty_color');
		
		// Cart
		// Header Cart Button
		$header_cart_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_color_d');
		$header_cart_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_color_h');
		$header_cart_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_color_a');
		$header_cart_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_color_f');
		$header_cart_qty_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_qty_color');
		$header_cart_qty_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_qty_bg');

		// Header Cart dropdown
		$header_cart_cheackout_block_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_cheackout_block_bg');
		$header_cart_cheackout_block_label_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_cheackout_block_label_color');
		$header_cart_cheackout_block_subtotal_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_cheackout_block_subtotal_color');
		$header_cart_dropdown_checkout_btn_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_checkout_btn_bg_d');
		$header_cart_dropdown_checkout_btn_bg_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_checkout_btn_bg_h');
		$header_cart_dropdown_checkout_btn_bg_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_checkout_btn_bg_a');
		$header_cart_dropdown_checkout_btn_bg_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_checkout_btn_bg_f');
		$header_cart_dropdown_checkout_btn_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_checkout_btn_color_d');
		$header_cart_dropdown_checkout_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_checkout_btn_color_h');
		$header_cart_dropdown_checkout_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_checkout_btn_color_a');
		$header_cart_dropdown_checkout_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_checkout_btn_color_f');


		$header_cart_dropdown_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_bg');
		$header_cart_dropdown_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_color');
		$header_cart_dropdown_actions_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_actions_color_d');
		$header_cart_dropdown_actions_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_actions_color_h');
		$header_cart_dropdown_actions_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_actions_color_a');
		$header_cart_dropdown_actions_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_actions_color_f');
		$header_cart_dropdown_product_price_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_product_price_color');

		// Header Cart dropdown "Cart" button
		$header_cart_dropdown_cart_btn_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_bg_d');
		$header_cart_dropdown_cart_btn_bg_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_bg_h');
		$header_cart_dropdown_cart_btn_bg_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_bg_a');
		$header_cart_dropdown_cart_btn_bg_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_bg_f');
		$header_cart_dropdown_cart_btn_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_color_d');
		$header_cart_dropdown_cart_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_color_h');
		$header_cart_dropdown_cart_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_color_a');
		$header_cart_dropdown_cart_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_color_f');
		$header_cart_dropdown_cart_btn_border = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_border_d');
		$header_cart_dropdown_cart_btn_border_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_border_h');
		$header_cart_dropdown_cart_btn_border_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_border_a');
		$header_cart_dropdown_cart_btn_border_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_cart_dropdown_cart_btn_border_f');

		// Social links
		$header_socials_bg = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_socials_bg_d');
		$header_socials_bg_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_socials_bg_h');
		$header_socials_bg_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_socials_bg_a');
		$header_socials_bg_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_socials_bg_f');
		$header_socials_color = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_socials_color_d');
		$header_socials_color_h = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_socials_color_h');
		$header_socials_color_a = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_socials_color_a');
		$header_socials_color_f = $this->_customDesign->getConfigData($sectionId, 'header_tabs', 'header_socials_color_f');

	    // Default Dropdown
		$header_default_menu_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_default_menu_bg');
		$header_menu_item_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_menu_item_bg_d');
		$header_menu_item_bg_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_menu_item_bg_h');
		$header_menu_item_bg_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_menu_item_bg_a');
		$header_menu_item_bg_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_menu_item_bg_f');
		$header_menu_item_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_menu_item_color_d');
		$header_menu_item_color_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_menu_item_color_h');
		$header_menu_item_color_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_menu_item_color_a');
		$header_menu_item_color_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_menu_item_color_f');

		$mega_menu_general_menu_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_bg');
		$mega_menu_general_menu_item_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_item_bg_d');
		$mega_menu_general_menu_item_bg_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_item_bg_h');
		$mega_menu_general_menu_item_bg_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_item_bg_a');
		$mega_menu_general_menu_item_bg_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_item_bg_f');
		$mega_menu_general_menu_item_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_item_color_d');
		$mega_menu_general_menu_item_color_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_item_color_h');
		$mega_menu_general_menu_item_color_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_item_color_a');
		$mega_menu_general_menu_item_color_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'mega_menu_general_menu_item_color_f');
		$header_mega_menu_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_bg');
		$header_mega_menu_links_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_links_color_d');
		$header_mega_menu_links_color_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_links_color_h');
		$header_mega_menu_links_color_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_links_color_a');
		$header_mega_menu_links_color_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_links_color_f');
		$header_mega_menu_bold_links_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_bold_links_color_d');
		$header_mega_menu_bold_links_color_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_bold_links_color_h');
		$header_mega_menu_bold_links_color_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_bold_links_color_a');
		$header_mega_menu_bold_links_color_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_bold_links_color_f');
		$header_mega_menu_vtab_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_color_d');
		$header_mega_menu_vtab_color_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_color_h');
		$header_mega_menu_vtab_color_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_color_a');
		$header_mega_menu_vtab_color_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_color_f');
		$header_mega_menu_vtab_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_bg_d');
		$header_mega_menu_vtab_bg_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_bg_h');
		$header_mega_menu_vtab_bg_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_bg_a');
		$header_mega_menu_vtab_bg_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_bg_f');
		$header_mega_menu_vtab_border = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_vtab_border');
		$header_mega_menu_htab_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_htab_color_d');
		$header_mega_menu_htab_color_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_htab_color_h');
		$header_mega_menu_htab_color_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_htab_color_a');
		$header_mega_menu_htab_color_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_htab_color_f');
		$header_mega_menu_htab_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_htab_bg_d');
		$header_mega_menu_htab_bg_h = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_htab_bg_h');
		$header_mega_menu_htab_bg_a = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_htab_bg_a');
		$header_mega_menu_htab_bg_f = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_mega_menu_htab_bg_f');

		$header_label_one_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_label_one_bg');
		$header_label_one_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_label_one_color');
		$header_label_two_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_label_two_bg');
		$header_label_two_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_label_two_color');
		$header_label_three_bg = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_label_three_bg');
		$header_label_three_color = $this->_customDesign->getConfigData($sectionId, 'mega_menu_tabs', 'header_label_three_color');

	    // General Tab
		$footer_bg = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_bg');
		$footer_color = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_color');
		$footer_border = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_border');
		$footer_title_color = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_title_color');
		$footer_links_color_hover = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_links_color_hover');

		// Social links
		$footer_socials_color = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_socials_color_d');
		$footer_socials_color_h = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_socials_color_h');
		$footer_socials_color_a = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_socials_color_a');
		$footer_socials_color_f = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_socials_color_f');
		$footer_socials_bg = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_socials_bg_d');
		$footer_socials_bg_h = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_socials_bg_h');
		$footer_socials_bg_a = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_socials_bg_a');
		$footer_socials_bg_f = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_socials_bg_f');

		// Subscribe block
		$footer_subscribe_title_color = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_title_color');
		$footer_subscribe_text_color = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_text_color');
		$footer_subscribe_input_bg = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_input_bg');
		$footer_subscribe_input_color = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_input_color');
		$footer_subscribe_input_border = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_input_border');
		$footer_subscribe_input_bg_focused = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_input_bg_focused');
		$footer_subscribe_input_color_focused = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_input_color_focused');
		$footer_subscribe_input_border_focused = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_input_border_focused');
		$footer_subscribe_btn_bg = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_btn_bg_d');
		$footer_subscribe_btn_bg_h = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_btn_bg_h');
		$footer_subscribe_btn_bg_a = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_btn_bg_a');
		$footer_subscribe_btn_bg_f = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_btn_bg_f');
		$footer_subscribe_btn_color = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_btn_color_d');
		$footer_subscribe_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_btn_color_h');
		$footer_subscribe_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_btn_color_a');
		$footer_subscribe_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'footer_tabs', 'footer_subscribe_btn_color_f');

	    // Product Hover Buttons
	    $product_name_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_name_color_d');
		$product_name_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_name_color_h');
		$product_name_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_name_color_a');
		$product_name_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_name_color_f');
	    $product_sold_name_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_sold_name_color_d');
		$product_sold_name_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_sold_name_color_h');
		$product_sold_name_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_sold_name_color_a');
		$product_sold_name_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_sold_name_color_f');
		$product_wishlist_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_wishlist_color_d');
		$product_wishlist_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_wishlist_color_h');
		$product_wishlist_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_wishlist_color_a');
		$product_wishlist_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_wishlist_color_f');
		$product_bottom_btns_bg = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_bottom_btns_bg_d');
		$product_bottom_btns_bg_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_bottom_btns_bg_h');
		$product_bottom_btns_bg_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_bottom_btns_bg_a');
		$product_bottom_btns_bg_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_bottom_btns_bg_f');
		$product_bottom_btns_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_bottom_btns_color_d');
		$product_bottom_btns_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_bottom_btns_color_h');
		$product_bottom_btns_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_bottom_btns_color_a');
		$product_bottom_btns_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'product_bottom_btns_color_f');

		// Toolbar
        $toolbar_sorter_btn_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_color_d');
		$toolbar_sorter_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_color_h');
		$toolbar_sorter_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_color_a');
		$toolbar_sorter_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_color_f');
		$toolbar_sorter_btn_bg = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_bg_d');
		$toolbar_sorter_btn_bg_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_bg_h');
		$toolbar_sorter_btn_bg_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_bg_a');
		$toolbar_sorter_btn_bg_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_bg_f');
		$toolbar_sorter_btn_border = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_border_d');
		$toolbar_sorter_btn_border_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_border_h');
		$toolbar_sorter_btn_border_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_border_a');
		$toolbar_sorter_btn_border_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_sorter_btn_border_f');
		$toolbar_switcher_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_color_d');
		$toolbar_switcher_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_color_h');
		$toolbar_switcher_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_color_a');
		$toolbar_switcher_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_color_f');
		$toolbar_switcher_bg = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_bg_d');
		$toolbar_switcher_bg_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_bg_h');
		$toolbar_switcher_bg_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_bg_a');
		$toolbar_switcher_bg_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_bg_f');
		$toolbar_switcher_border = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_border_d');
		$toolbar_switcher_border_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_border_h');
		$toolbar_switcher_border_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_border_a');
		$toolbar_switcher_border_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_switcher_border_f');
		$general_pagination_item_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'general_pagination_item_color_d');
		$general_pagination_item_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'general_pagination_item_color_h');
		$general_pagination_item_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'general_pagination_item_color_a');
		$general_pagination_item_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'general_pagination_item_color_f');
		$general_pagination_item_bg = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'general_pagination_item_bg_d');
		$general_pagination_item_bg_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'general_pagination_item_bg_h');
		$general_pagination_item_bg_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'general_pagination_item_bg_a');
		$general_pagination_item_bg_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'general_pagination_item_bg_f');

		//Toolbar Filters
		$toolbar_filter_btn_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_btn_color');
		$toolbar_filter_current_text_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_text_color');
		$toolbar_filter_current_bg = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_bg');
		$toolbar_filter_current_border = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_border');

		$toolbar_filter_current_remove_btn_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_remove_btn_color_d');
		$toolbar_filter_current_remove_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_remove_btn_color_h');
		$toolbar_filter_current_remove_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_remove_btn_color_a');
		$toolbar_filter_current_remove_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_remove_btn_color_f');
		$toolbar_filter_current_remove_btn_bg = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_remove_btn_bg_d');
		$toolbar_filter_current_remove_btn_bg_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_remove_btn_bg_h');
		$toolbar_filter_current_remove_btn_bg_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_remove_btn_bg_a');
		$toolbar_filter_current_remove_btn_bg_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_remove_btn_bg_f');
		$toolbar_filter_current_clear_btn_color = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_clear_btn_color_d');
		$toolbar_filter_current_clear_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_clear_btn_color_h');
		$toolbar_filter_current_clear_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_clear_btn_color_a');
		$toolbar_filter_current_clear_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_clear_btn_color_f');
		$toolbar_filter_current_clear_btn_bg = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_clear_btn_bg_d');
		$toolbar_filter_current_clear_btn_bg_h = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_clear_btn_bg_h');
		$toolbar_filter_current_clear_btn_bg_a = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_clear_btn_bg_a');
		$toolbar_filter_current_clear_btn_bg_f = $this->_customDesign->getConfigData($sectionId, 'category_tabs', 'toolbar_filter_current_clear_btn_bg_f');

	    // General Tab
		$cat_sidebar_border = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'cat_sidebar_border');
		$cat_sidebar_title_color = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'cat_sidebar_title_color');
		$cat_sidebar_color = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'cat_sidebar_color');
		$cat_sidebar_links_color_hover = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'cat_sidebar_links_color_hover');

		// Sidebar Blocks
		// Shop-By block design
		$meigee_sidebar_blocks_shopby_title_color = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_title_color');
		$meigee_sidebar_blocks_shopby_subtitle_color = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_subtitle_color');
		$meigee_sidebar_blocks_shopby_links_color = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_links_color_d');
		$meigee_sidebar_blocks_shopby_links_color_h = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_links_color_h');
		$meigee_sidebar_blocks_shopby_links_color_a = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_links_color_a');
		$meigee_sidebar_blocks_shopby_links_color_f = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_links_color_f');
		$meigee_sidebar_blocks_shopby_swatches_color = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_color_d');
		$meigee_sidebar_blocks_shopby_swatches_color_h = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_color_h');
		$meigee_sidebar_blocks_shopby_swatches_color_a = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_color_a');
		$meigee_sidebar_blocks_shopby_swatches_color_f = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_color_f');
		$meigee_sidebar_blocks_shopby_swatches_bg = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_bg_d');
		$meigee_sidebar_blocks_shopby_swatches_bg_h = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_bg_h');
		$meigee_sidebar_blocks_shopby_swatches_bg_a = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_bg_a');
		$meigee_sidebar_blocks_shopby_swatches_bg_f = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_bg_f');
		$meigee_sidebar_blocks_shopby_swatches_border = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_border_d');
		$meigee_sidebar_blocks_shopby_swatches_border_h = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_border_h');
		$meigee_sidebar_blocks_shopby_swatches_border_a = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_border_a');
		$meigee_sidebar_blocks_shopby_swatches_border_f = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_border_f');
		$meigee_sidebar_blocks_shopby_swatches_shadow = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_shadow_d');
		$meigee_sidebar_blocks_shopby_swatches_shadow_h = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_shadow_h');
		$meigee_sidebar_blocks_shopby_swatches_shadow_a = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_shadow_a');
		$meigee_sidebar_blocks_shopby_swatches_shadow_f = $this->_customDesign->getConfigData($sectionId, 'sidebar_tabs', 'meigee_sidebar_blocks_shopby_swatches_shadow_f');

	    // General Tab
		$productpage_info_name_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_name_color');
		$product_general_price_regular = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'product_general_price_regular');
		$product_general_price_special = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'product_general_price_special');
		$product_general_price_old = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'product_general_price_old');

		// Add-to links
		$productpage_info_add_links_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_add_links_color_d');
		$productpage_info_add_links_color_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_add_links_color_h');
		$productpage_info_add_links_color_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_add_links_color_a');
		$productpage_info_add_links_color_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_add_links_color_f');
		$productpage_info_add_links_bg = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_add_links_bg_d');
		$productpage_info_add_links_bg_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_add_links_bg_h');
		$productpage_info_add_links_bg_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_add_links_bg_a');
		$productpage_info_add_links_bg_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_add_links_bg_f');

		// Product labels
		$general_product_label_sale_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'general_product_label_sale_color');
		$general_product_label_sale_bg = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'general_product_label_sale_bg');
		$general_product_label_new_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'general_product_label_new_color');
		$general_product_label_new_bg = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'general_product_label_new_bg');

		// Add to Cart Button
		$productpage_info_cart_btn_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_cart_btn_color_d');
		$productpage_info_cart_btn_color_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_cart_btn_color_h');
		$productpage_info_cart_btn_color_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_cart_btn_color_a');
		$productpage_info_cart_btn_color_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_cart_btn_color_f');
		$productpage_info_cart_btn_bg = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_cart_btn_bg_d');
		$productpage_info_cart_btn_bg_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_cart_btn_bg_h');
		$productpage_info_cart_btn_bg_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_cart_btn_bg_a');
		$productpage_info_cart_btn_bg_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_cart_btn_bg_f');
		// Products Options
		$productpage_options_label_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_options_label_color');
		$productpage_options_size_chart_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_options_size_chart_color_d');
		$productpage_options_size_chart_color_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_options_size_chart_color_h');
		$productpage_options_size_chart_color_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_options_size_chart_color_a');
		$productpage_options_size_chart_color_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_options_size_chart_color_f');

		// Product quantity
		$productpage_info_qty_bg = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_bg');
		$productpage_info_qty_input_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_input_color');
		$productpage_info_qty_btns_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_btns_color_d');
		$productpage_info_qty_btns_color_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_btns_color_h');
		$productpage_info_qty_btns_color_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_btns_color_a');
		$productpage_info_qty_btns_color_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_btns_color_f');
		$productpage_info_qty_input_btns_bg = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_input_btns_bg_d');
		$productpage_info_qty_input_btns_bg_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_input_btns_bg_h');
		$productpage_info_qty_input_btns_bg_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_input_btns_bg_a');
		$productpage_info_qty_input_btns_bg_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_info_qty_input_btns_bg_f');
		$productpage_add_to_cart_free_shipping_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_add_to_cart_free_shipping_color');

		// Collateral Block
		$productpage_tabs_border = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_tabs_border');
		$productpage_tabs_head_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_tabs_head_color_d');
		$productpage_tabs_head_color_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_tabs_head_color_h');
		$productpage_tabs_head_color_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_tabs_head_color_a');
		$productpage_tabs_head_color_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_tabs_head_color_f');

		// Reviews Design
		// Product Customer Reviews
		$productpage_reviews_bg = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_bg');
		$productpage_reviews_border = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_border');
		$productpage_reviews_rating_general_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_rating_general_color');
		$productpage_reviews_rating_active_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_rating_active_color');
		$productpage_reviews_title_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_title_color');
		$productpage_reviews_text_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_text_color');
		$productpage_reviews_link_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_link_color_d');
		$productpage_reviews_link_color_h = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_link_color_h');
		$productpage_reviews_link_color_a = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_link_color_a');
		$productpage_reviews_link_color_f = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_link_color_f');
		$productpage_reviews_date_color = $this->_customDesign->getConfigData($sectionId, 'product_tabs', 'productpage_reviews_date_color');


		if($this->issetVar($general_bg)) {
			$cssData .= PHP_EOL.
			'html body.checkout-cart-index.wide-layout,
			 html body.checkout-cart-index.boxed-layout,
			 html body.wide-layout,
			 html body.boxed-layout {background-color: '.$general_bg.';}';
		}

		if($this->issetVar($general_bg_container)) {
			$cssData .= PHP_EOL.
			'html body.boxed-layout #maincontent.page-main,
			 html body.boxed-layout .breadcrumbs-wrapper .container,
			 html body.boxed-layout.cms-index-index #maincontent .box-content,
			 html body.boxed-layout .page-wrapper > div.breadcrumbs,
			 html body.boxed-layout .page-header.header-5 .top-padding .container::before,
			 html body.boxed-layout .page-header.header-5 .top-block .container,
			 html body.boxed-layout .page-header.header-5 .middle-block .container,
			 .cart-container .cart-summary .totals-wrapper .checkout-methods-items::before,
			 .cart-container .cart-summary .totals-wrapper .checkout-methods-items::after,
			 html body.boxed-layout .page-header.header-5 .menu-wrapper .container { background-color: '.$general_bg_container.'}';
		}

		if($this->issetVar($elements_border)) {
			$cssData .= PHP_EOL.
			'.toolbar,
			 .product.info.detailed .product.data.items .additional-attributes-wrapper .table tbody,
			 .amazon-validate-container .block .block-title,
			 .login-container .block .block-title,
			 body.account .column.main .block:not(.widget) .block-title,
			 body.account .column.main .block.block-dashboard-info .block-content .box,
			 body.account .column.main .block.block-dashboard-addresses .block-content .box,
			 .block-addresses-list .items.addresses .item.actions .action:after,
			 .account .data.table .col.actions .action:after,
			 [class^="sales-guest-"] .data.table .col.actions .action:after,
			 .sales-guest-view .data.table .col.actions .action:after,
			 .block .box-actions .action:after,
			 .block-collapsible-nav .item .delimiter,
			 .table-wrapper.comparison #product-comparison tbody .cell,
			 .table > tbody > tr > th,
			 .table > tbody > tr > td,
			 .abs-account-blocks .block-title,
			 .account .legend,
			 .form-giftregistry-search .legend,
			 .block-giftregistry-results .block-title,
			 .block-giftregistry-shared-items .block-title,
			 .block-wishlist-search-form .block-title,
			 .block-wishlist-search-results .block-title,
			 .multicheckout .block-title,
			 .multicheckout .block-content .title,
			 .customer-review .review-details .title,
			 .paypal-review .block .block-title,
			 .account .column.main .block:not(.widget) .block-title,
			 .multicheckout .block-title,
			 .magento-rma-guest-returns .column.main .block:not(.widget) .block-title, 
			 [class^="sales-guest-"] .column.main .block:not(.widget) .block-title,
			 .sales-guest-view .column.main .block:not(.widget) .block-title,
			 .toolbar-bottom .toolbar,
			 .page-layout-1column #layered-filter-block:not(.mobile),
			 .page-layout-1column #layered-filter-block.filter-no-options:not(.mobile),
			 .page-layout-1column #layered-filter-block {border-color: '.$elements_border.';}
			 .products-grid .product-item-info .product-reviews-summary .reviews-actions a + a:before,
			 .products-list .product-item-info .product-reviews-summary .reviews-actions a + a:before {
			 	background-color: '.$elements_border.';
			 }';
		}

		if($this->issetVar($general_color)) {
			$cssData .= PHP_EOL.
			'html body,
			.product-info-main .reviews-actions a:not(:hover),
			.product-info-main .product.attribute.overview,
			.product.info.detailed .product.data.items > .item.content,
			.block.review-add #review-form .review-field-ratings .review-field-rating .label,
			.fieldset > .field > .label,
			.fieldset > .fields > .field > .label,
			.fieldset > .legend,
			.product-info-main .product.attribute.sku .value {color: '.$general_color.';}
			.widget-title .subtitle {
				color: '.$general_color.';
				opacity: 0.3;
			}
			.block-compare .counter,
			.block-wishlist .counter,
			.block.review-add #review-form .review-field-ratings .review-field-rating .rating-values,
			.product-info-main .product.attribute.sku .type,
			.product.media .action-collapse {
				color: '.$general_color.';
				opacity: 0.7;
			}';
		}

		if($this->issetVar($title_color)) {
			$cssData .= PHP_EOL.
			'.page-title,
			 .page-title h1 {color: '.$title_color.';}';
		}

		if($this->issetVar($links_color)) {
			$cssData .= PHP_EOL.
			'body a,
			body a.action,
			.products-grid .product-item-info .product-reviews-summary .reviews-actions a,
			.products-list .product-item-info .product-reviews-summary .reviews-actions a,
			.block.related .block-content .block-actions .action.select,
			.block.upsell .block-content .block-actions .action.select {color: '.$links_color.';}';
		}
		if($this->issetVar($links_color_h)) {
			$cssData .= PHP_EOL.
			'body a:hover,
			.block.related .block-content .block-actions .action.select:hover,
			.block.upsell .block-content .block-actions .action.select:hover,
			.cart-container .cart-summary .totals-wrapper .checkout-methods-items .action.multicheckout:hover {color: '.$links_color_h.';}
			body a.action:hover	{background-color: '.$links_color_h.';}';
		}
		if($this->issetVar($links_color_a)) {
			$cssData .= PHP_EOL.
			'body a:active,
			body a:visited,
			.block.related .block-content .block-actions .action.select:active,
			.block.upsell .block-content .block-actions .action.select:active,
			.cart-container .cart-summary .totals-wrapper .checkout-methods-items .action.multicheckout:active {color: '.$links_color_a.';}
			body a.action:active {background-color: '.$links_color_a.';}';
		}
		if($this->issetVar($links_color_f)) {
			$cssData .= PHP_EOL.
			'body a:focus,
			.block.related .block-content .block-actions .action.select:focus,
			.block.upsell .block-content .block-actions .action.select:focus,
			.cart-container .cart-summary .totals-wrapper .checkout-methods-items .action.multicheckout:focus {color: '.$links_color_f.';
			body a.action:focus {background-color: '.$links_color_f.';}}';
		}

		if($this->issetVar($regular_price)) {
			$cssData .= PHP_EOL.
			'.price-container .price {color: '.$regular_price.';}';
		}
		if($this->issetVar($special_price)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .price-box .special-price .price-container .price,
			 .products-list .product-item-info .price-box .special-price .price-container .price {color: '.$special_price.';}';
		}
		if($this->issetVar($old_price)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .price-box .old-price .price-container .price,
			 .products-list .product-item-info .price-box .old-price .price-container .price {color: '.$old_price.';}';
		}
		if($this->issetVar($sold_out)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info.sold-out .price-box .price-container .price,
			 .products-list .product-item-info.sold-out .price-box .price-container .price {color: '.$sold_out.';}';
		}

		if($this->issetVar($breadcrumbs_color)) {
			$cssData .= PHP_EOL.
			'body .breadcrumbs a:visited, 
			 body .breadcrumbs a {color: '.$breadcrumbs_color.';}
			 body .breadcrumbs .item:not(:last-child):after {color: '.$breadcrumbs_color.'; opacity: 0.2;}';
		}
		if($this->issetVar($breadcrumbs_color_h)) {
			$cssData .= PHP_EOL.
			'body .breadcrumbs a:hover {color: '.$breadcrumbs_color_h.';}';
		}
		if($this->issetVar($breadcrumbs_color_a)) {
			$cssData .= PHP_EOL.
			'body .breadcrumbs strong,
			 body .breadcrumbs strong:visited {color: '.$breadcrumbs_color_a.';}';
		}
		if($this->issetVar($breadcrumbs_color_f)) {
			$cssData .= PHP_EOL.
			'body .breadcrumbs a:focus {color: '.$breadcrumbs_color_f.';}';
		}

		if($this->issetVar($general_input_color)) {
			$cssData .= PHP_EOL.
			'input[type="text"],
			input[type="password"],
			input[type="url"],
			input[type="tel"],
			input[type="search"],
			input[type="number"],
			input[type="datetime"],
			input[type="email"],
			textarea {color: '.$general_input_color.';}
			input[type="text"]::-webkit-input-placeholder,
			input[type="password"]::-webkit-input-placeholder,
			input[type="url"]::-webkit-input-placeholder,
			input[type="tel"]::-webkit-input-placeholder,
			input[type="search"]::-webkit-input-placeholder,
			input[type="number"]::-webkit-input-placeholder,
			input[type="datetime"]::-webkit-input-placeholder,
			input[type="email"]::-webkit-input-placeholder,
			textarea::-webkit-input-placeholder {color: '.$general_input_color.';}
			input[type="text"]:-moz-placeholder,
			input[type="password"]:-moz-placeholder,
			input[type="url"]:-moz-placeholder,
			input[type="tel"]:-moz-placeholder,
			input[type="search"]:-moz-placeholder,
			input[type="number"]:-moz-placeholder,
			input[type="datetime"]:-moz-placeholder,
			input[type="email"]:-moz-placeholder,
			textarea:-moz-placeholder {color: '.$general_input_color.';}
			input[type="text"]::-moz-placeholder,
			input[type="password"]::-moz-placeholder,
			input[type="url"]::-moz-placeholder,
			input[type="tel"]::-moz-placeholder,
			input[type="search"]::-moz-placeholder,
			input[type="number"]::-moz-placeholder,
			input[type="datetime"]::-moz-placeholder,
			input[type="email"]::-moz-placeholder,
			textarea::-moz-placeholder {color: '.$general_input_color.';}
			input[type="text"]:-ms-input-placeholder,
			input[type="password"]:-ms-input-placeholder,
			input[type="url"]:-ms-input-placeholder,
			input[type="tel"]:-ms-input-placeholder,
			input[type="search"]:-ms-input-placeholder,
			input[type="number"]:-ms-input-placeholder,
			input[type="datetime"]:-ms-input-placeholder,
			input[type="email"]:-ms-input-placeholder,
			textarea:-ms-input-placeholder {color: '.$general_input_color.';}
			input[type="text"]::placeholder,
			input[type="password"]::placeholder,
			input[type="url"]::placeholder,
			input[type="tel"]::placeholder,
			input[type="search"]::placeholder,
			input[type="number"]::placeholder,
			input[type="datetime"]::placeholder,
			input[type="email"]::placeholder,
			textarea::placeholder {color: '.$general_input_color.';}';
		}
		if($this->issetVar($general_input_bg)) {
			$cssData .= PHP_EOL.
			'input[type="text"],
			input[type="password"],
			input[type="url"],
			input[type="tel"],
			input[type="search"],
			input[type="number"],
			input[type="datetime"],
			input[type="email"],
			textarea {background-color: '.$general_input_bg.';}';
		}
		if($this->issetVar($general_input_border)) {
			$cssData .= PHP_EOL.
			'input[type="text"],
			input[type="password"],
			input[type="url"],
			input[type="tel"],
			input[type="search"],
			input[type="number"],
			input[type="datetime"],
			input[type="email"],
			textarea {border-color: '.$general_input_border.';}';
		}

		if($this->issetVar($general_input_color_focused)) {
			$cssData .= PHP_EOL.
			'input[type="text"]:focus,
			input[type="password"]:focus,
			input[type="url"]:focus,
			input[type="tel"]:focus,
			input[type="search"]:focus,
			input[type="number"]:focus,
			input[type="datetime"]:focus,
			input[type="email"]:focus,
			textarea:focus {color: '.$general_input_color_focused.';}';
		}
		if($this->issetVar($general_input_bg_focused)) {
			$cssData .= PHP_EOL.
			'input[type="text"]:focus,
			input[type="password"]:focus,
			input[type="url"]:focus,
			input[type="tel"]:focus,
			input[type="search"]:focus,
			input[type="number"]:focus,
			input[type="datetime"]:focus,
			input[type="email"]:focus,
			textarea:focus {background-color: '.$general_input_bg_focused.';}';
		}
		if($this->issetVar($general_input_border_focused)) {
			$cssData .= PHP_EOL.
			'input[type="text"]:focus,
			input[type="password"]:focus,
			input[type="url"]:focus,
			input[type="tel"]:focus,
			input[type="search"]:focus,
			input[type="number"]:focus,
			input[type="datetime"]:focus,
			input[type="email"]:focus,
			textarea:focus {border-color: '.$general_input_border_focused.';}';
		}

		if($this->issetVar($general_select_color)) {
			$cssData .= PHP_EOL.
			'select,
			.toolbar select {color: '.$general_select_color.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-options .filter-options-item .filter-options-title {
					color: '.$general_select_color.';
				}
			}';
		}
		if($this->issetVar($general_select_bg)) {
			$cssData .= PHP_EOL.
			'select,
			.toolbar select {background-color: '.$general_select_bg.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-options .filter-options-item .filter-options-title {
					background-color: '.$general_select_bg.';
				}
			}';
		}

		if($this->issetVar($reviews_rating_general_color)) {
			$cssData .= PHP_EOL.
			'.rating-summary .rating-result,
			.block.review-add #review-form .review-field-ratings .review-field-rating .review-control-vote:before {color: '.$reviews_rating_general_color.';}';
		}
		if($this->issetVar($reviews_rating_active_color)) {
			$cssData .= PHP_EOL.
			'.rating-summary .rating-result span,
			.block.review-add #review-form .review-field-ratings .review-field-rating .review-control-vote label:before {color: '.$reviews_rating_active_color.';}';
		}
		if($this->issetVar($reviews_rating_links_color_hover)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .product-reviews-summary .reviews-actions a:hover,
			 .products-list .product-item-info .product-reviews-summary .reviews-actions a:hover {color: '.$reviews_rating_links_color_hover.';}';
		}

		if($this->issetVar($general_btn1_color)) {
			$cssData .= PHP_EOL.
			'body button.action,
			.btn,
			.btn.btn-default,
			.actions-toolbar .primary a.action {color: '.$general_btn1_color.';}';
		}
		if($this->issetVar($general_btn1_bg)) {
			$cssData .= PHP_EOL.
			'body button.action,
			.btn,
			.btn.btn-default,
			.actions-toolbar .primary a.action {background-color: '.$general_btn1_bg.';}';
		}
		if($this->issetVar($general_btn1_border)) {
			$cssData .= PHP_EOL.
			'body button.action,
			.btn,
			.btn.btn-default,
			.actions-toolbar .primary a.action {border-color: '.$general_btn1_border.';}';
		}
		if($this->issetVar($general_btn1_color_h)) {
			$cssData .= PHP_EOL.
			'body button.action:hover,
			.btn:hover,
			.btn.btn-default:hover,
			.actions-toolbar .primary a.action:hover {color: '.$general_btn1_color_h.';}';
		}
		if($this->issetVar($general_btn1_bg_h)) {
			$cssData .= PHP_EOL.
			'body button.action:hover,
			.btn:hover,
			.btn.btn-default:hover,
			.actions-toolbar .primary a.action:hover {background-color: '.$general_btn1_bg_h.';}';
		}
		if($this->issetVar($general_btn1_border_h)) {
			$cssData .= PHP_EOL.
			'body button.action:hover,
			.btn:hover,
			.btn.btn-default:hover,
			.actions-toolbar .primary a.action:hover {border-color: '.$general_btn1_border_h.';}';
		}
		if($this->issetVar($general_btn1_color_a)) {
			$cssData .= PHP_EOL.
			'body button.action:active,
			.btn:active,
			.btn.btn-default:active,
			.actions-toolbar .primary a.action:active,
			body button.action.active,
			.btn.active,
			.btn.btn-default.active,
			.actions-toolbar .primary a.action.active {color: '.$general_btn1_color_a.';}';
		}
		if($this->issetVar($general_btn1_bg_a)) {
			$cssData .= PHP_EOL.
			'body button.action:active,
			.btn:active,
			.btn.btn-default:active,
			.actions-toolbar .primary a.action:active,
			body button.action.active,
			.btn.active,
			.btn.btn-default.active,
			.actions-toolbar .primary a.action.active {background-color: '.$general_btn1_bg_a.';}';
		}
		if($this->issetVar($general_btn1_border_a)) {
			$cssData .= PHP_EOL.
			'body button.action:active,
			.btn:active,
			.btn.btn-default:active,
			.actions-toolbar .primary a.action:active,
			body button.action.active,
			.btn.active,
			.btn.btn-default.active,
			.actions-toolbar .primary a.action.active {border-color: '.$general_btn1_border_a.';}';
		}
		if($this->issetVar($general_btn1_color_f)) {
			$cssData .= PHP_EOL.
			'body button.action:focus,
			.btn:focus,
			.btn.btn-default:focus,
			.actions-toolbar .primary a.action:focus {color: '.$general_btn1_color_f.';}';
		}
		if($this->issetVar($general_btn1_bg_f)) {
			$cssData .= PHP_EOL.
			'body button.action:focus,
			.btn:focus,
			.btn.btn-default:focus,
			.actions-toolbar .primary a.action:focus {background-color: '.$general_btn1_bg_f.';}';
		}
		if($this->issetVar($general_btn1_border_f)) {
			$cssData .= PHP_EOL.
			'body button.action:focus,
			.btn:focus,
			.btn.btn-default:focus,
			.actions-toolbar .primary a.action:focus {border-color: '.$general_btn1_border_f.';}';
		}

		if($this->issetVar($general_btn2_color)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary,
			.btn.btn-primary,
			.btn.btn-default.btn-primary,
			.actions-toolbar .primary a.action.btn-primary,
			body button.action.primary,
			.btn.primary,
			.btn.btn-default.primary,
			.actions-toolbar .primary a.action.primary {color: '.$general_btn2_color.';}';
		}
		if($this->issetVar($general_btn2_bg)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary,
			.btn.btn-primary,
			.btn.btn-default.btn-primary,
			.actions-toolbar .primary a.action.btn-primary,
			body button.action.primary,
			.btn.primary,
			.btn.btn-default.primary,
			.actions-toolbar .primary a.action.primary {background-color: '.$general_btn2_bg.';}';
		}
		if($this->issetVar($general_btn2_border)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary,
			.btn.btn-primary,
			.btn.btn-default.btn-primary,
			.actions-toolbar .primary a.action.btn-primary,
			body button.action.primary,
			.btn.primary,
			.btn.btn-default.primary,
			.actions-toolbar .primary a.action.primary {border-color: '.$general_btn2_border.';}';
		}
		if($this->issetVar($general_btn2_color_h)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:hover,
			.btn.btn-primary:hover,
			.btn.btn-default.btn-primary:hover,
			.actions-toolbar .primary a.action.btn-primary:hover,
			body button.action.primary:hover,
			.btn.primary:hover,
			.btn.btn-default.primary:hover,
			.actions-toolbar .primary a.action.primary:hover {color: '.$general_btn2_color_h.';}';
		}
		if($this->issetVar($general_btn2_bg_h)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:hover,
			.btn.btn-primary:hover,
			.btn.btn-default.btn-primary:hover,
			.actions-toolbar .primary a.action.btn-primary:hover,
			body button.action.primary:hover,
			.btn.primary:hover,
			.btn.btn-default.primary:hover,
			.actions-toolbar .primary a.action.primary:hover {background-color: '.$general_btn2_bg_h.';}';
		}
		if($this->issetVar($general_btn2_border_h)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:hover,
			.btn.btn-primary:hover,
			.btn.btn-default.btn-primary:hover,
			.actions-toolbar .primary a.action.btn-primary:hover,
			body button.action.primary:hover,
			.btn.primary:hover,
			.btn.btn-default.primary:hover,
			.actions-toolbar .primary a.action.primary:hover {border-color: '.$general_btn2_border_h.';}';
		}
		if($this->issetVar($general_btn2_color_a)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:active,
			.btn.btn-primary:active,
			.btn.btn-default.btn-primary:active,
			.actions-toolbar .primary a.action.btn-primary:active,
			body button.action.primary:active,
			.btn.primary:active,
			.btn.btn-default.primary:active,
			.actions-toolbar .primary a.action.primary:active,
			body button.action.btn-primary.active,
			.btn.btn-primary.active,
			.btn.btn-default.btn-primary.active,
			.actions-toolbar .primary a.action.btn-primary.active,
			body button.action.primary.active,
			.btn.primary.active,
			.btn.btn-default.primary.active,
			.actions-toolbar .primary a.action.primary.active {color: '.$general_btn2_color_a.';}';
		}
		if($this->issetVar($general_btn2_bg_a)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:active,
			.btn.btn-primary:active,
			.btn.btn-default.btn-primary:active,
			.actions-toolbar .primary a.action.btn-primary:active,
			body button.action.primary:active,
			.btn.primary:active,
			.btn.btn-default.primary:active,
			.actions-toolbar .primary a.action.primary:active,
			body button.action.btn-primary.active,
			.btn.btn-primary.active,
			.btn.btn-default.btn-primary.active,
			.actions-toolbar .primary a.action.btn-primary.active,
			body button.action.primary.active,
			.btn.primary.active,
			.btn.btn-default.primary.active,
			.actions-toolbar .primary a.action.primary.active {background-color: '.$general_btn2_bg_a.';}';
		}
		if($this->issetVar($general_btn2_border_a)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:active,
			.btn.btn-primary:active,
			.btn.btn-default.btn-primary:active,
			.actions-toolbar .primary a.action.btn-primary:active,
			body button.action.primary:active,
			.btn.primary:active,
			.btn.btn-default.primary:active,
			.actions-toolbar .primary a.action.primary:active,
			body button.action.btn-primary.active,
			.btn.btn-primary.active,
			.btn.btn-default.btn-primary.active,
			.actions-toolbar .primary a.action.btn-primary.active,
			body button.action.primary.active,
			.btn.primary.active,
			.btn.btn-default.primary.active,
			.actions-toolbar .primary a.action.primary.active {border-color: '.$general_btn2_border_a.';}';
		}
		if($this->issetVar($general_btn2_color_f)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:focus,
			.btn.btn-primary:focus,
			.btn.btn-default.btn-primary:focus,
			.actions-toolbar .primary a.action.btn-primary:focus,
			body button.action.primary:focus,
			.btn.primary:focus,
			.btn.btn-default.primary:focus,
			.actions-toolbar .primary a.action.primary:focus {color: '.$general_btn2_color_f.';}';
		}
		if($this->issetVar($general_btn2_bg_f)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:focus,
			.btn.btn-primary:focus,
			.btn.btn-default.btn-primary:focus,
			.actions-toolbar .primary a.action.btn-primary:focus,
			body button.action.primary:focus,
			.btn.primary:focus,
			.btn.btn-default.primary:focus,
			.actions-toolbar .primary a.action.primary:focus {background-color: '.$general_btn2_bg_f.';}';
		}
		if($this->issetVar($general_btn2_border_f)) {
			$cssData .= PHP_EOL.
			'body button.action.btn-primary:focus,
			.btn.btn-primary:focus,
			.btn.btn-default.btn-primary:focus,
			.actions-toolbar .primary a.action.btn-primary:focus,
			body button.action.primary:focus,
			.btn.primary:focus,
			.btn.btn-default.primary:focus,
			.actions-toolbar .primary a.action.primary:focus {border-color: '.$general_btn2_border_f.';}';
		}

		if($this->issetVar($general_slider_btns_color)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-nav div,
			.instagram-slider .meigee-instagram-block .owl-nav div {color: '.$general_slider_btns_color.';}';
		}
		if($this->issetVar($general_slider_btns_color_h)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-nav div:hover,
			.instagram-slider .meigee-instagram-block .owl-nav div:hover {color: '.$general_slider_btns_color_h.';}';
		}
		if($this->issetVar($general_slider_btns_color_a)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-nav div:active,
			.instagram-slider .meigee-instagram-block .owl-nav div:active {color: '.$general_slider_btns_color_a.';}';
		}
		if($this->issetVar($general_slider_btns_color_f)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-nav div:focus,
			.instagram-slider .meigee-instagram-block .owl-nav div:focus {color: '.$general_slider_btns_color_f.';}';
		}

		if($this->issetVar($general_home_subscribe_title_color)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block-title,
			#maincontent .subscribe-block .block-title {color: '.$general_home_subscribe_title_color.';}';
		}
		if($this->issetVar($general_home_subscribe_text_color)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block-subtitle,
			#maincontent .subscribe-block .block-subtitle {color: '.$general_home_subscribe_text_color.';}';
		}
		if($this->issetVar($general_home_subscribe_input_bg)) {
			$cssData .= PHP_EOL.
			'#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input {background-color: '.$general_home_subscribe_input_bg.';}';
		}
		if($this->issetVar($general_home_subscribe_input_color)) {
			$cssData .= PHP_EOL.
			'#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input {color: '.$general_home_subscribe_input_color.';}
			#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input::-webkit-input-placeholder {color: '.$general_home_subscribe_input_color.';}
			#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:-moz-placeholder {color: '.$general_home_subscribe_input_color.';}
			#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input::-moz-placeholder {color: '.$general_home_subscribe_input_color.';}
			#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:-ms-input-placeholder {color: '.$general_home_subscribe_input_color.';}
			#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input::placeholder {color: '.$general_home_subscribe_input_color.';}';
		}
		if($this->issetVar($general_home_subscribe_input_border)) {
			$cssData .= PHP_EOL.
			'#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input {border-color: '.$general_home_subscribe_input_border.';}';
		}
		if($this->issetVar($general_home_subscribe_input_bg_focused)) {
			$cssData .= PHP_EOL.
			'#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:focus {background-color: '.$general_home_subscribe_input_bg_focused.';}';
		}
		if($this->issetVar($general_home_subscribe_input_color_focused)) {
			$cssData .= PHP_EOL.
			'#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:focus {color: '.$general_home_subscribe_input_color_focused.';}';
		}
		if($this->issetVar($general_home_subscribe_input_border_focused)) {
			$cssData .= PHP_EOL.
			'#maincontent .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:focus {border-color: '.$general_home_subscribe_input_border_focused.';}';
		}
		if($this->issetVar($general_home_subscribe_btn_bg)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block.newsletter .action.subscribe,
			#maincontent .subscribe-block .block.newsletter .action.subscribe {background-color: '.$general_home_subscribe_btn_bg.';}';
		}
		if($this->issetVar($general_home_subscribe_btn_bg_h)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block.newsletter .action.subscribe:hover,
			#maincontent .subscribe-block .block.newsletter .action.subscribe:hover {background-color: '.$general_home_subscribe_btn_bg_h.';}';
		}
		if($this->issetVar($general_home_subscribe_btn_bg_a)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block.newsletter .action.subscribe:active,
			#maincontent .subscribe-block .block.newsletter .action.subscribe:active {background-color: '.$general_home_subscribe_btn_bg_a.';}';
		}
		if($this->issetVar($general_home_subscribe_btn_bg_f)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block.newsletter .action.subscribe:focus,
			#maincontent .subscribe-block .block.newsletter .action.subscribe:focus {background-color: '.$general_home_subscribe_btn_bg_f.';}';
		}
		if($this->issetVar($general_home_subscribe_btn_color)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block.newsletter .action.subscribe,
			#maincontent .subscribe-block .block.newsletter .action.subscribe {color: '.$general_home_subscribe_btn_color.';}';
		}
		if($this->issetVar($general_home_subscribe_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block.newsletter .action.subscribe:hover,
			#maincontent .subscribe-block .block.newsletter .action.subscribe:hover {color: '.$general_home_subscribe_btn_color_h.';}';
		}
		if($this->issetVar($general_home_subscribe_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block.newsletter .action.subscribe:active,
			#maincontent .subscribe-block .block.newsletter .action.subscribe:active {color: '.$general_home_subscribe_btn_color_a.';}';
		}
		if($this->issetVar($general_home_subscribe_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.fullwidth-block.subscribe-block .block.newsletter .action.subscribe:focus,
			#maincontent .subscribe-block .block.newsletter .action.subscribe:focus {color: '.$general_home_subscribe_btn_color_f.';}';
		}

		if($this->issetVar($general_home_slider_text_color)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-item .title,
			 #home-slider .owl-item .subtitle,
			 #home-slider .owl-item .text strong,
			 .home-slider-wrapper .slider-counter {color: '.$general_home_slider_text_color.';}
			 #home-slider .owl-item .text span {color: '.$general_home_slider_text_color.'; opacity: 0.4;}';
		}
		if($this->issetVar($general_home_slider_arrows_color)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-nav div {color: '.$general_home_slider_arrows_color.';}';
		}
		if($this->issetVar($general_home_slider_arrows_color_h)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-nav div:hover {color: '.$general_home_slider_arrows_color_h.';}';
		}
		if($this->issetVar($general_home_slider_arrows_color_a)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-nav div:active {color: '.$general_home_slider_arrows_color_a.';}';
		}
		if($this->issetVar($general_home_slider_arrows_color_f)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-nav div:focus {color: '.$general_home_slider_arrows_color_f.';}';
		}
		if($this->issetVar($general_home_slider_arrows_bg)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-nav div {background-color: '.$general_home_slider_arrows_bg.';}';
		}
		if($this->issetVar($general_home_slider_arrows_bg_h)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-nav div:hover {background-color: '.$general_home_slider_arrows_bg_h.';}';
		}
		if($this->issetVar($general_home_slider_arrows_bg_a)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-nav div:active {background-color: '.$general_home_slider_arrows_bg_a.';}';
		}
		if($this->issetVar($general_home_slider_arrows_bg_f)) {
			$cssData .= PHP_EOL.
			'#home-slider .owl-nav div:focus {background-color: '.$general_home_slider_arrows_bg_f.';}';
		}
		if($this->issetVar($general_home_slider_dots_border)) {
			$cssData .= PHP_EOL.
			'#home-slider.owl-theme .owl-dots .owl-dot {border-color: '.$general_home_slider_dots_border.';}';
		}
		if($this->issetVar($general_home_slider_dot_active_color)) {
			$cssData .= PHP_EOL.
			'#home-slider.owl-theme .owl-dots .owl-dot.active,
			#home-slider.owl-theme .owl-dots .owl-dot:hover {
				border-color: '.$general_home_slider_dot_active_color.';
				background-color: '.$general_home_slider_dot_active_color.';
			}';
		}
		if($this->issetVar($general_home_shop_by_container_bg)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-item .slide-container {background-color: '.$general_home_shop_by_container_bg.';}';
		}
		if($this->issetVar($general_home_shop_by_container_bg_h)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-item.active > a:hover .slide-container {background-color: '.$general_home_shop_by_container_bg_h.';}';
		}
		if($this->issetVar($general_home_shop_by_container_bg_a)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-item.active > a:active .slide-container {background-color: '.$general_home_shop_by_container_bg_a.';}';
		}
		if($this->issetVar($general_home_shop_by_container_bg_f)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-item.active > a:focus .slide-container {background-color: '.$general_home_shop_by_container_bg_f.';}';
		}
		if($this->issetVar($general_home_shop_by_text_color)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-item .slide-container {color: '.$general_home_shop_by_text_color.';}
			#shopby-slider .owl-item .slide-container .btn {color: '.$general_home_shop_by_text_color.'; opacity: 0.4;}';
		}
		if($this->issetVar($general_home_shop_by_text_color_h)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-item.active > a:hover .slide-container {color: '.$general_home_shop_by_text_color_h.';}
			#shopby-slider .owl-item.active > a:hover .slide-container .btn {color: '.$general_home_shop_by_text_color_h.'; opacity: 0.4;}';
		}
		if($this->issetVar($general_home_shop_by_text_color_a)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-item.active > a:active .slide-container {color: '.$general_home_shop_by_text_color_a.';}
			#shopby-slider .owl-item.active > a:active .slide-container .btn {color: '.$general_home_shop_by_text_color_a.'; opacity: 0.4;}';
		}
		if($this->issetVar($general_home_shop_by_text_color_f)) {
			$cssData .= PHP_EOL.
			'#shopby-slider .owl-item.active > a:focus .slide-container {color: '.$general_home_shop_by_text_color_f.';}
			#shopby-slider .owl-item.active > a:focus .slide-container .btn {color: '.$general_home_shop_by_text_color_f.'; opacity: 0.4;}';
		}


		// Header
		if($this->issetVar($header_bg)) {
			$cssData .= PHP_EOL.
			'body.wide-layout .page-header .middle-block,
			 body.boxed-layout .page-header.header-5 .middle-block .middle-block-inner,
			 body.boxed-layout .page-header:not(.header-5) .middle-block .container {background: '.$header_bg.'}
			 @media (max-width: 1007px) {
			 	body.boxed-layout .page-header.header-5,
			 	body.boxed-layout .page-header.header-5 .middle-block .container,
			 	body.boxed-layout .page-header.header-5 .middle-block .middle-block-inner {background:'.$header_bg.'}
			 }';
		}
		if($this->issetVar($header_top_bg)) {
			$cssData .= PHP_EOL.
			'body.wide-layout .page-header .top-block,
			 body.boxed-layout .page-header.header-5 .top-block .top-block-inner,
			 body.boxed-layout .page-header:not(.header-5) .top-block .container {background: '.$header_top_bg.';}';
		}
		if($this->issetVar($header_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .welcome,
			.page-header[class*="header-"] .logo-wrapper .nav-toggle,
			.page-header .header-info a {color: '.$header_color.';}
			.page-header .header-info a:not(:last-child):before {background-color: '.$header_color.'; opacity: 0.15;}
			.page-header[class*="header-"] .welcome {color: '.$header_color.'; opacity: 0.75;}
			.page-header[class*="header-"] .logo-wrapper .nav-toggle:hover {background-color: transparent;}
			';
		}
		if($this->issetVar($header_border)) {
			$cssData .= PHP_EOL.
			 'html body.wide-layout .page-header .middle-block,
			 html body.wide-layout .page-header .top-block,
			 html body.boxed-layout .page-header .middle-block .container,
			 html body.boxed-layout .page-header .top-block .container {border-color: '.$header_border.';}
			 @media (min-width: 1008px) {
				 html body.boxed-layout .page-header.header-5 .top-block .top-block-inner,
				 html body.boxed-layout .page-header.header-5 .middle-block .middle-block-inner {
				 	border-bottom: 1px solid '.$header_border.';
				 }
			 }
			 html body.boxed-layout .page-header.header-5 .top-block .container,
			 html body.boxed-layout .page-header.header-5 .middle-block .container {border-bottom: none}';
		}
		if($this->issetVar($header_search_bg)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search .block-content input {background-color: '.$header_search_bg.';}';
		}
		if($this->issetVar($header_search_border)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search .block-content input {border-color: '.$header_search_border.';}';
		}
		if($this->issetVar($header_search_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search .block-content input {color: '.$header_search_color.';}
			 .page-header[class*="header-"] .block-search .block-content input::-webkit-input-placeholder {color: '.$header_search_color.';}
			.page-header[class*="header-"] .block-search .block-content input:-moz-placeholder {color: '.$header_search_color.';}
			.page-header[class*="header-"] .block-search .block-content input::-moz-placeholder {color: '.$header_search_color.';}
			.page-header[class*="header-"] .block-search .block-content input:-ms-input-placeholder {color: '.$header_search_color.';}
			.page-header[class*="header-"] .block-search .block-content input::placeholder {color: '.$header_search_color.';}';
		}
		if($this->issetVar($header_search_btn_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search .block-content .action.search {color: '.$header_search_btn_color.';}';
		}
		if($this->issetVar($header_search_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search .block-content .action.search:hover {color: '.$header_search_btn_color_h.';}';
		}
		if($this->issetVar($header_search_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search .block-content .action.search:active {color: '.$header_search_btn_color_a.';}';
		}
		if($this->issetVar($header_search_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search .block-content .action.search:focus {color: '.$header_search_btn_color_f.';}';
		}

		if($this->issetVar($header_search2_btn_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search.type-2 .block-title {color: '.$header_search2_btn_color.';}';
		}
		if($this->issetVar($header_search2_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search.type-2 .block-title:hover {color: '.$header_search2_btn_color_h.';}';
		}
		if($this->issetVar($header_search2_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search.type-2 .block-title:active {color: '.$header_search2_btn_color_a.';}';
		}
		if($this->issetVar($header_search2_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .block-search.type-2 .block-title:focus {color: '.$header_search2_btn_color_f.';}';
		}

		if($this->issetVar($header_options_color)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-block {color: '.$header_options_color.';}
			.options-wrapper .options-block .currency + .language:before {background-color: '.$header_options_color.'; opacity: 0.1;}';
		}
		if($this->issetVar($header_options_color_h)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-block:hover {color: '.$header_options_color_h.';}
			.options-wrapper .options-block:hover .currency + .language:before {background-color: '.$header_options_color_h.'; opacity: 0.1;}';
		}
		if($this->issetVar($header_options_color_a)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-block.open,
			.options-wrapper .options-block:active {color: '.$header_options_color_a.';}
			.options-wrapper .options-block.open .currency + .language:before,
			.options-wrapper .options-block:active .currency + .language:before {background-color: '.$header_options_color_a.'; opacity: 0.1;}';
		}
		if($this->issetVar($header_options_color_f)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-block:focus {color: '.$header_options_color_f.';}
			.options-wrapper .options-block:focus .currency + .language:before {background-color: '.$header_options_color_f.'; opacity: 0.1;}';
		}
		if($this->issetVar($header_options_dropdown_bg)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown {background-color: '.$header_options_dropdown_bg.';}';
		}
		if($this->issetVar($header_options_dropdown_links_color)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown ul:not(.switcher-dropdown) li a,
			.options-wrapper .options-dropdown ul:not(.switcher-dropdown) li span {color: '.$header_options_dropdown_links_color.';}';
		}
		if($this->issetVar($header_options_dropdown_links_color_h)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown ul:not(.switcher-dropdown) li a:hover {color: '.$header_options_dropdown_links_color_h.';}';
		}
		if($this->issetVar($header_options_dropdown_links_color_a)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown ul:not(.switcher-dropdown) li a:active {color: '.$header_options_dropdown_links_color_a.';}';
		}
		if($this->issetVar($header_options_dropdown_links_color_f)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown ul:not(.switcher-dropdown) li a:focus {color: '.$header_options_dropdown_links_color_f.';}';
		}
		if($this->issetVar($header_options_dropdown_labels_color)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown .label {color: '.$header_options_dropdown_labels_color.';}';
		}
		if($this->issetVar($header_options_dropdown_links_border)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown .switcher + .switcher,
			.options-wrapper .options-dropdown .dropdown-item + .switcher,
			.options-wrapper .options-dropdown .switcher + .dropdown-item,
			.options-wrapper .options-dropdown .dropdown-item + .dropdown-item {border-color: '.$header_options_dropdown_links_border.';}';
		}
		if($this->issetVar($header_options_dropdown_select_bg)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown .switcher .switcher-options .action.toggle,
			.options-wrapper .options-dropdown .dropdown-item .switcher-options .action.toggle {background-color: '.$header_options_dropdown_select_bg.';}';
		}
		if($this->issetVar($header_options_dropdown_select_color)) {
			$cssData .= PHP_EOL.
			'.options-wrapper .options-dropdown .switcher .switcher-options .action.toggle,
			.options-wrapper .options-dropdown .dropdown-item .switcher-options .action.toggle {color: '.$header_options_dropdown_select_color.';}';
		}

		if($this->issetVar($header_wishlist_color)) {
			$cssData .= PHP_EOL.
			'.top-link-wishlist {color: '.$header_wishlist_color.';}';
		}
		if($this->issetVar($header_wishlist_color_h)) {
			$cssData .= PHP_EOL.
			'.top-link-wishlist:hover {color: '.$header_wishlist_color_h.';}';
		}
		if($this->issetVar($header_wishlist_color_a)) {
			$cssData .= PHP_EOL.
			'.top-link-wishlist:active {color: '.$header_wishlist_color_a.';}';
		}
		if($this->issetVar($header_wishlist_color_f)) {
			$cssData .= PHP_EOL.
			'.top-link-wishlist:focus {color: '.$header_wishlist_color_f.';}';
		}
		if($this->issetVar($header_wishlist_qty_bg)) {
			$cssData .= PHP_EOL.
			'.top-link-wishlist .wishlist-counter.counter {background-color: '.$header_wishlist_qty_bg.';}';
		}
		if($this->issetVar($header_wishlist_qty_color)) {
			$cssData .= PHP_EOL.
			'.top-link-wishlist .wishlist-counter.counter {color: '.$header_wishlist_qty_color.';}';
		}

		//Cart
		if($this->issetVar($header_cart_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .action.showcart.title-cart {color: '.$header_cart_color.';}';
		}
		if($this->issetVar($header_cart_color_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .action.showcart.title-cart:hover {color: '.$header_cart_color_h.';}';
		}
		if($this->issetVar($header_cart_color_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .action.showcart.title-cart:active,
			.page-header[class*="header-"] .minicart-wrapper .action.showcart.title-cart.active {color: '.$header_cart_color_a.';}';
		}
		if($this->issetVar($header_cart_color_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .action.showcart.title-cart:focus {color: '.$header_cart_color_f.';}';
		}
		if($this->issetVar($header_cart_qty_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .action.showcart.title-cart .icon-wrapper .counter-number {color: '.$header_cart_qty_color.';}';
		}
		if($this->issetVar($header_cart_qty_bg)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .action.showcart.title-cart .icon-wrapper .counter-number {background-color: '.$header_cart_qty_bg.';}';
		}
		//Cart Dropdown
		if($this->issetVar($header_cart_cheackout_block_bg)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .subtotal,
			.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary {background-color: '.$header_cart_cheackout_block_bg.';}';
		}
		if($this->issetVar($header_cart_cheackout_block_label_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .subtotal .label {color: '.$header_cart_cheackout_block_label_color.';}';
		}
		if($this->issetVar($header_cart_cheackout_block_subtotal_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .subtotal .amount.price-container .price {color: '.$header_cart_cheackout_block_subtotal_color.';}';
		}
		if($this->issetVar($header_cart_dropdown_checkout_btn_bg)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary .btn.checkout {
				background-color: '.$header_cart_dropdown_checkout_btn_bg.';
				border-color: '.$header_cart_dropdown_checkout_btn_bg.';
			}';
		}
		if($this->issetVar($header_cart_dropdown_checkout_btn_bg_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary .btn.checkout:hover {
				background-color: '.$header_cart_dropdown_checkout_btn_bg_h.';
				border-color: '.$header_cart_dropdown_checkout_btn_bg_h.';
			}';
		}
		if($this->issetVar($header_cart_dropdown_checkout_btn_bg_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary .btn.checkout:active {
				background-color: '.$header_cart_dropdown_checkout_btn_bg_a.';
				border-color: '.$header_cart_dropdown_checkout_btn_bg_a.';
			}';
		}
		if($this->issetVar($header_cart_dropdown_checkout_btn_bg_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary .btn.checkout:focus {
				background-color: '.$header_cart_dropdown_checkout_btn_bg_f.';
				border-color: '.$header_cart_dropdown_checkout_btn_bg_f.';
			}';
		}
		if($this->issetVar($header_cart_dropdown_checkout_btn_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary .btn.checkout {color: '.$header_cart_dropdown_checkout_btn_color.';}';
		}
		if($this->issetVar($header_cart_dropdown_checkout_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary .btn.checkout:hover {color: '.$header_cart_dropdown_checkout_btn_color_h.';}';
		}
		if($this->issetVar($header_cart_dropdown_checkout_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary .btn.checkout:active {color: '.$header_cart_dropdown_checkout_btn_color_a.';}';
		}
		if($this->issetVar($header_cart_dropdown_checkout_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) > .primary .btn.checkout:focus {color: '.$header_cart_dropdown_checkout_btn_color_f.';}';
		}
		if($this->issetVar($header_cart_dropdown_bg)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart {background-color: '.$header_cart_dropdown_bg.';}';
		}
		if($this->issetVar($header_cart_dropdown_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart,
			.page-header[class*="header-"] .minicart-wrapper .block-minicart .subtitle,
			.page-header[class*="header-"] .minicart-wrapper .block-minicart .minicart-items-wrapper .product-item .product-item-details .product-item-name a,
			.page-header[class*="header-"] .minicart-wrapper .block-minicart .minicart-items-wrapper .product-item .product-item-details .details-qty .item-qty {
			 	color: '.$header_cart_dropdown_color.';
			 }
			 .page-header[class*="header-"] .minicart-wrapper .block-minicart .minicart-items-wrapper .product-item .product-item-details .details-qty .label,
			 .minicart-items .product .toggle:after {
			 	color: '.$header_cart_dropdown_color.';
			 	opacity: 0.5;
			 }';
		}
		if($this->issetVar($header_cart_dropdown_actions_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .minicart-items-wrapper .product-item .product-item-details .product.actions .action {color: '.$header_cart_dropdown_actions_color.';}';
		}
		if($this->issetVar($header_cart_dropdown_actions_color_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .minicart-items-wrapper .product-item .product-item-details .product.actions .action:hover {color: '.$header_cart_dropdown_actions_color_h.';}';
		}
		if($this->issetVar($header_cart_dropdown_actions_color_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .minicart-items-wrapper .product-item .product-item-details .product.actions .action:active {color: '.$header_cart_dropdown_actions_color_a.';}';
		}
		if($this->issetVar($header_cart_dropdown_actions_color_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .minicart-items-wrapper .product-item .product-item-details .product.actions .action:focus {color: '.$header_cart_dropdown_actions_color_f.';}';
		}
		if($this->issetVar($header_cart_dropdown_product_price_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .minicart-items-wrapper .product-item .product-item-details .price {color: '.$header_cart_dropdown_product_price_color.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_bg)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart {background-color: '.$header_cart_dropdown_cart_btn_bg.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_bg_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:hover {background-color: '.$header_cart_dropdown_cart_btn_bg_h.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_bg_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:active {background-color: '.$header_cart_dropdown_cart_btn_bg_a.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_bg_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:focus {background-color: '.$header_cart_dropdown_cart_btn_bg_f.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart {color: '.$header_cart_dropdown_cart_btn_color.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:hover {color: '.$header_cart_dropdown_cart_btn_color_h.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:active {color: '.$header_cart_dropdown_cart_btn_color_a.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:focus {color: '.$header_cart_dropdown_cart_btn_color_f.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_border)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart {border-color: '.$header_cart_dropdown_cart_btn_border.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_border_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:hover {border-color: '.$header_cart_dropdown_cart_btn_border_h.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_border_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:active {border-color: '.$header_cart_dropdown_cart_btn_border_a.';}';
		}
		if($this->issetVar($header_cart_dropdown_cart_btn_border_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .minicart-wrapper .block-minicart .actions:not(.product) .secondary .btn.viewcart:focus {border-color: '.$header_cart_dropdown_cart_btn_border_f.';}';
		}
		
		// Header Social links
		if($this->issetVar($header_socials_bg)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .social-links li a {background-color: '.$header_socials_bg.';}';
		}
		if($this->issetVar($header_socials_bg_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .social-links li a:hover {background-color: '.$header_socials_bg_h.';}';
		}
		if($this->issetVar($header_socials_bg_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .social-links li a:active {background-color: '.$header_socials_bg_a.';}';
		}
		if($this->issetVar($header_socials_bg_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .social-links li a:focus {background-color: '.$header_socials_bg_f.';}';
		}
		if($this->issetVar($header_socials_color)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .social-links li a {color: '.$header_socials_color.';}';
		}
		if($this->issetVar($header_socials_color_h)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .social-links li a:hover {color: '.$header_socials_color_h.';}';
		}
		if($this->issetVar($header_socials_color_a)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .social-links li a:active {color: '.$header_socials_color_a.';}';
		}
		if($this->issetVar($header_socials_color_f)) {
			$cssData .= PHP_EOL.
			'.page-header[class*="header-"] .social-links li a:focus {color: '.$header_socials_color_f.';}';
		}

		//Mega Menu
		
		if($this->issetVar($mega_menu_general_menu_bg)) {
			$cssData .= PHP_EOL.
			'body.wide-layout .page-header .menu-wrapper,
			body.boxed-layout .menu-wrapper .container,
			body .page-header.header-2:not(.transparent-menu) .menu-wrapper {background: '.$mega_menu_general_menu_bg.';}
			body.wide-layout .menu-wrapper .container,
			body.wide-layout .menu-wrapper .container .menu-inner,
			body.boxed-layout .menu-wrapper .container .menu-inner {background-color: transparent;}
			@media (min-width: 1008px) {
				body.boxed-layout .page-header.header-5 .menu-wrapper .menu-inner {background: '.$mega_menu_general_menu_bg.'}
			}			
			@media (max-width: 1007px) {
				body.boxed-layout .page-header.header-5 .menu-wrapper,
				body.boxed-layout .page-header.header-5 .menu-wrapper .container,
				body.boxed-layout .page-header.header-5 .menu-wrapper .menu-inner {background:transparent}
			}';
		}
		if($this->issetVar($mega_menu_general_menu_item_bg)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top {background-color: '.$mega_menu_general_menu_item_bg.';}';
		}
		if($this->issetVar($mega_menu_general_menu_item_bg_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:hover,
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top.ui-state-focus {background-color: '.$mega_menu_general_menu_item_bg_h.';}';
		}
		if($this->issetVar($mega_menu_general_menu_item_bg_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:active,
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top.active a.level-top,
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top.ui-state-active {background-color: '.$mega_menu_general_menu_item_bg_a.';}';
		}
		if($this->issetVar($mega_menu_general_menu_item_bg_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:focus {background-color: '.$mega_menu_general_menu_item_bg_f.';}';
		}
		if($this->issetVar($mega_menu_general_menu_item_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top {color: '.$mega_menu_general_menu_item_color.';}
			 .header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top::before {background-color: '.$mega_menu_general_menu_item_color.';}';
		}
		if($this->issetVar($mega_menu_general_menu_item_color_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:hover,
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top.ui-state-focus {color: '.$mega_menu_general_menu_item_color_h.';}
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:hover::before {background-color: '.$mega_menu_general_menu_item_color_h.';}';
		}
		if($this->issetVar($mega_menu_general_menu_item_color_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:active,
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top.active a.level-top,
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top.ui-state-active {color: '.$mega_menu_general_menu_item_color_a.';}
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:active::before {background-color: '.$mega_menu_general_menu_item_color_a.';}';
		}
		if($this->issetVar($mega_menu_general_menu_item_color_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:focus {color: '.$mega_menu_general_menu_item_color_f.';}
			.header-wrapper .page-header[class*="header-"] .navbar-default .navbar-collapse.collapse li.level-top a.level-top:focus::before {background-color: '.$mega_menu_general_menu_item_color_f.';}';
		}


		if($this->issetVar($header_default_menu_bg)) {
			$cssData .= PHP_EOL.
			'@media only screen and (min-width: 992px) {
				.header-wrapper .navigation .megamenu li .submenu.default-menu,
				.navigation .level0 .submenu {background-color: '.$header_default_menu_bg.';}
			}';
		}
		if($this->issetVar($header_menu_item_bg)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .submenu.default-menu li a {background-color: '.$header_menu_item_bg.';}';
		}
		if($this->issetVar($header_menu_item_bg_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .submenu.default-menu li a:hover {background-color: '.$header_menu_item_bg_h.';}';
		}
		if($this->issetVar($header_menu_item_bg_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .submenu.default-menu li a:active {background-color: '.$header_menu_item_bg_a.';}';
		}
		if($this->issetVar($header_menu_item_bg_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .submenu.default-menu li a:focus {background-color: '.$header_menu_item_bg_f.';}';
		}
		if($this->issetVar($header_menu_item_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .submenu.default-menu li a {color: '.$header_menu_item_color.';}
			.header-wrapper .navigation .megamenu .nav .default-menu-parent li > a .ui-menu-icon:after {border-left-color: '.$header_menu_item_color.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_menu_item_color_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .submenu.default-menu li a:hover {color: '.$header_menu_item_color_h.';}
			.header-wrapper .navigation .megamenu .nav .default-menu-parent li > a:hover .ui-menu-icon:after {border-left-color: '.$header_menu_item_color_h.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_menu_item_color_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .submenu.default-menu li a:active {color: '.$header_menu_item_color_a.';}
			.header-wrapper .navigation .megamenu .nav .default-menu-parent li > a:active .ui-menu-icon:after {border-left-color: '.$header_menu_item_color_a.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_menu_item_color_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .submenu.default-menu li a:focus {color: '.$header_menu_item_color_f.';}
			.header-wrapper .navigation .megamenu .nav .default-menu-parent li > a:focus .ui-menu-icon:after {border-left-color: '.$header_menu_item_color_f.'; opacity: 0.3;}';
		}

		if($this->issetVar($header_mega_menu_bg)) {
			$cssData .= PHP_EOL.
			'@media only screen and (min-width: 992px) {
				.header-wrapper .navigation .megamenu li .megamenu-wrapper,
				.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu ul.level0 li.level1 > .megamenu-inner {background-color: '.$header_mega_menu_bg.';}
			}';
		}
		if($this->issetVar($header_mega_menu_links_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a {color: '.$header_mega_menu_links_color.' !important;}
			.header-wrapper .megamenu .megamenu-wrapper:not(.tabs-menu) ul.level1 li.parent > a:after,
			.header-wrapper .megamenu .megamenu-wrapper.tabs-menu ul.level2 li.parent > a:after,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a:after,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a:after {border-left-color: '.$header_mega_menu_links_color.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_mega_menu_links_color_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a:hover,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a:hover {color: '.$header_mega_menu_links_color_h.' !important;}
			.header-wrapper .megamenu .megamenu-wrapper:not(.tabs-menu) ul.level1 li.parent > a:hover:after,
			.header-wrapper .megamenu .megamenu-wrapper.tabs-menu ul.level2 li.parent > a:hover:after,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a:hover:after,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a:hover:after {border-left-color: '.$header_mega_menu_links_color.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_mega_menu_links_color_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a:active,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a:active {color: '.$header_mega_menu_links_color_a.' !important;}
			.header-wrapper .megamenu .megamenu-wrapper:not(.tabs-menu) ul.level1 li.parent > a:active:after,
			.header-wrapper .megamenu .megamenu-wrapper.tabs-menu ul.level2 li.parent > a:active:after,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a:active:after,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a:active:after {border-left-color: '.$header_mega_menu_links_color.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_mega_menu_links_color_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a:focus,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a:focus {color: '.$header_mega_menu_links_color_f.' !important;}
			.header-wrapper .megamenu .megamenu-wrapper:not(.tabs-menu) ul.level1 li.parent > a:focus:after,
			.header-wrapper .megamenu .megamenu-wrapper.tabs-menu ul.level2 li.parent > a:focus:after,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a:focus:after,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a:focus:after {border-left-color: '.$header_mega_menu_links_color.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_mega_menu_bold_links_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a[data-bold-link="1"],
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a[data-bold-link="1"] {color: '.$header_mega_menu_bold_links_color.' !important;}';
		}
		if($this->issetVar($header_mega_menu_bold_links_color_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a[data-bold-link="1"]:hover,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a[data-bold-link="1"]:hover {color: '.$header_mega_menu_bold_links_color_h.' !important;}';
		}
		if($this->issetVar($header_mega_menu_bold_links_color_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a[data-bold-link="1"]:active,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a[data-bold-link="1"]:active {color: '.$header_mega_menu_bold_links_color_a.' !important;}';
		}
		if($this->issetVar($header_mega_menu_bold_links_color_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper li.level1 a[data-bold-link="1"]:focus,
			.header-wrapper .navbar-default .navbar-collapse.collapse li.level-top .megamenu-wrapper ul.level1 a[data-bold-link="1"]:focus {color: '.$header_mega_menu_bold_links_color_f.' !important;}';
		}
		if($this->issetVar($header_mega_menu_vtab_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical ul.level0 li.level1 > a {color: '.$header_mega_menu_vtab_color.' !important;}
			.header-wrapper .megamenu .megamenu-wrapper.tabs-menu.vertical li.level1.parent > a:after {border-left-color: '.$header_mega_menu_vtab_color.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_mega_menu_vtab_color_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical ul.level0 li.level1 > a:hover {color: '.$header_mega_menu_vtab_color_h.' !important;}
			.header-wrapper .megamenu .megamenu-wrapper.tabs-menu.vertical li.level1.parent > a:hover:after {border-left-color: '.$header_mega_menu_vtab_color_h.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_mega_menu_vtab_color_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical ul.level0 li.level1:hover > a:not(:hover) {color: '.$header_mega_menu_vtab_color_a.' !important;}
			.header-wrapper .megamenu .megamenu-wrapper.tabs-menu.vertical li.level1.parent:hover > a:not(:hover):after {border-left-color: '.$header_mega_menu_vtab_color_a.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_mega_menu_vtab_color_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical ul.level0 li.level1 > a:focus {color: '.$header_mega_menu_vtab_color_f.' !important;}
			.header-wrapper .megamenu .megamenu-wrapper.tabs-menu.vertical li.level1.parent > a:focus:after {border-left-color: '.$header_mega_menu_vtab_color_f.'; opacity: 0.3;}';
		}
		if($this->issetVar($header_mega_menu_vtab_bg)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical ul.level0 li.level1 > a {background-color: '.$header_mega_menu_vtab_bg.';}';
		}
		if($this->issetVar($header_mega_menu_vtab_bg_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical ul.level0 li.level1 > a:hover {background-color: '.$header_mega_menu_vtab_bg_h.';}';
		}
		if($this->issetVar($header_mega_menu_vtab_bg_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical ul.level0 li.level1:hover > a:not(:hover) {background-color: '.$header_mega_menu_vtab_bg_a.';}';
		}
		if($this->issetVar($header_mega_menu_vtab_bg_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical ul.level0 li.level1 > a:focus {background-color: '.$header_mega_menu_vtab_bg_f.';}';
		}
		if($this->issetVar($header_mega_menu_vtab_border)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.vertical:before {border-color: '.$header_mega_menu_vtab_border.';}';
		}
		
		if($this->issetVar($header_mega_menu_htab_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.horizontal ul.level0 li.level1 > a {color: '.$header_mega_menu_htab_color.' !important;}';
		}
		if($this->issetVar($header_mega_menu_htab_color_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.horizontal ul.level0 li.level1 > a:hover {color: '.$header_mega_menu_htab_color_h.' !important;}';
		}
		if($this->issetVar($header_mega_menu_htab_color_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.horizontal ul.level0 li.level1 > a:active {color: '.$header_mega_menu_htab_color_a.' !important;}';
		}
		if($this->issetVar($header_mega_menu_htab_color_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.horizontal ul.level0 li.level1 > a:focus {color: '.$header_mega_menu_htab_color_f.' !important;}';
		}
		if($this->issetVar($header_mega_menu_htab_bg)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu:not(.vertical):before {background-color: '.$header_mega_menu_htab_bg.';}';
		}
		if($this->issetVar($header_mega_menu_htab_bg_h)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.horizontal ul.level0 li.level1 > a:hover {background-color: '.$header_mega_menu_htab_bg_h.';}';
		}
		if($this->issetVar($header_mega_menu_htab_bg_a)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.horizontal ul.level0 li.level1 > a:active {background-color: '.$header_mega_menu_htab_bg_a.';}';
		}
		if($this->issetVar($header_mega_menu_htab_bg_f)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li .megamenu-wrapper.tabs-menu.horizontal ul.level0 li.level1 > a:focus {background-color: '.$header_mega_menu_htab_bg_f.';}';
		}

		// Menu Labels
		if($this->issetVar($header_label_one_bg)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li a .category-label.label-one {background-color: '.$header_label_one_bg.';}';
		}
		if($this->issetVar($header_label_one_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li a .category-label.label-one {color: '.$header_label_one_color.';}';
		}
		if($this->issetVar($header_label_two_bg)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li a .category-label.label-two {background-color: '.$header_label_two_bg.';}';
		}
		if($this->issetVar($header_label_two_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li a .category-label.label-two {color: '.$header_label_two_color.';}';
		}
		if($this->issetVar($header_label_three_bg)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li a .category-label.label-three {background-color: '.$header_label_three_bg.';}';
		}
		if($this->issetVar($header_label_three_color)) {
			$cssData .= PHP_EOL.
			'.header-wrapper .navigation .megamenu li a .category-label.label-three {color: '.$header_label_three_color.';}';
		}

		//Footer
		if($this->issetVar($footer_bg)) {
			$cssData .= PHP_EOL.
			'html body.wide-layout .footer .footer-top,
			html body.wide-layout .footer .footer-middle,
			html body.wide-layout .footer .footer-bottom,
			html body.boxed-layout .footer .footer-top .container,
			html body.boxed-layout .footer .footer-middle .container,
			html body.boxed-layout .footer .footer-bottom .container {background-color: '.$footer_bg.';}';
		}
		if($this->issetVar($footer_color)) {
			$cssData .= PHP_EOL.
			'.footer {color: '.$footer_color.';}';
		}
		if($this->issetVar($footer_color)) {
			$cssData .= PHP_EOL.
			'.footer,
			.footer .switcher-store .switcher-options {color: '.$footer_color.';}
			.footer .contacts-block div:not(.accordion-content) span.title {color: '.$footer_color.'; opacity: 0.66;}';
		}
		if($this->issetVar($footer_border)) {
			$cssData .= PHP_EOL.
			'.wide-layout .footer .footer-top,
			.wide-layout .footer .footer-middle,
			.wide-layout .footer .footer-bottom,
			.boxed-layout .footer .footer-top .container,
			.boxed-layout .footer .footer-middle .container,
			.boxed-layout .footer .footer-bottom .container {border-color: '.$footer_border.';}
			.footer .switcher-store .switcher-options {background-color: '.$footer_border.'}
			@media only screen and (max-width: 991px) {
				.accordion-list #customer-reviews:not(:first-of-type) .accordion-title,
				.accordion-list .accordion-item:not(:first-of-type) .accordion-title {border-color: '.$footer_border.';}
			}';
		}
		if($this->issetVar($footer_title_color)) {
			$cssData .= PHP_EOL.
			'.footer .footer-block-title,
			.footer .contacts-block i,
			.footer .contacts-block div:not(.accordion-content) span.phone,
			.accordion-list.footer .accordion-item .accordion-title {color: '.$footer_title_color.';}
			@media only screen and (max-width: 991px) {
				.accordion-list #customer-reviews .accordion-title,
				.accordion-list .accordion-item .accordion-title {color: '.$footer_title_color.';}
			}';
		}
		if($this->issetVar($footer_links_color_hover)) {
			$cssData .= PHP_EOL.
			'.footer a:hover {color: '.$footer_links_color_hover.';}';
		}
		if($this->issetVar($footer_socials_color)) {
			$cssData .= PHP_EOL.
			'.footer .social-links li a {color: '.$footer_socials_color.';}';
		}
		if($this->issetVar($footer_socials_color_h)) {
			$cssData .= PHP_EOL.
			'.footer .social-links li a:hover {color: '.$footer_socials_color_h.';}';
		}
		if($this->issetVar($footer_socials_color_a)) {
			$cssData .= PHP_EOL.
			'.footer .social-links li a:active {color: '.$footer_socials_color_a.';}';
		}
		if($this->issetVar($footer_socials_color_f)) {
			$cssData .= PHP_EOL.
			'.footer .social-links li a:focus {color: '.$footer_socials_color_f.';}';
		}
		if($this->issetVar($footer_socials_bg)) {
			$cssData .= PHP_EOL.
			'.footer .social-links li a {background-color: '.$footer_socials_bg.';}';
		}
		if($this->issetVar($footer_socials_bg_h)) {
			$cssData .= PHP_EOL.
			'.footer .social-links li a:hover {background-color: '.$footer_socials_bg_h.';}';
		}
		if($this->issetVar($footer_socials_bg_a)) {
			$cssData .= PHP_EOL.
			'.footer .social-links li a:active {background-color: '.$footer_socials_bg_a.';}';
		}
		if($this->issetVar($footer_socials_bg_f)) {
			$cssData .= PHP_EOL.
			'.footer .social-links li a:focus {background-color: '.$footer_socials_bg_f.';}';
		}

		if($this->issetVar($footer_subscribe_title_color)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .footer-block-title,
			 .accordion-list.footer .accordion-item.subscribe-block .accordion-title.footer-block-title {color: '.$footer_subscribe_title_color.';}';
		}

		if($this->issetVar($footer_subscribe_text_color)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block-title {color: '.$footer_subscribe_text_color.';}';
		}
		if($this->issetVar($footer_subscribe_input_bg)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input {background-color: '.$footer_subscribe_input_bg.';}';
		}
		if($this->issetVar($footer_subscribe_input_color)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input {color: '.$footer_subscribe_input_color.';}
			.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input::-webkit-input-placeholder {color: '.$footer_subscribe_input_color.';}
			.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:-moz-placeholder {color: '.$footer_subscribe_input_color.';}
			.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input::-moz-placeholder {color: '.$footer_subscribe_input_color.';}
			.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:-ms-input-placeholder {color: '.$footer_subscribe_input_color.';}
			.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input::placeholder {color: '.$footer_subscribe_input_color.';}';
		}
		if($this->issetVar($footer_subscribe_input_border)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input {border-color: '.$footer_subscribe_input_border.';}';
		}
		if($this->issetVar($footer_subscribe_input_bg_focused)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:focus {background-color: '.$footer_subscribe_input_bg_focused.';}';
		}
		if($this->issetVar($footer_subscribe_input_color_focused)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:focus {color: '.$footer_subscribe_input_color_focused.';}';
		}
		if($this->issetVar($footer_subscribe_input_border_focused)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .input-wrapper input:focus {border-color: '.$footer_subscribe_input_border_focused.';}';
		}
		if($this->issetVar($footer_subscribe_btn_bg)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .action.subscribe {background-color: '.$footer_subscribe_btn_bg.';}';
		}
		if($this->issetVar($footer_subscribe_btn_bg_h)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .action.subscribe:hover {background-color: '.$footer_subscribe_btn_bg_h.';}';
		}
		if($this->issetVar($footer_subscribe_btn_bg_a)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .action.subscribe:active {background-color: '.$footer_subscribe_btn_bg_a.';}';
		}
		if($this->issetVar($footer_subscribe_btn_bg_f)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .action.subscribe:focus {background-color: '.$footer_subscribe_btn_bg_f.';}';
		}
		if($this->issetVar($footer_subscribe_btn_color)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .action.subscribe {color: '.$footer_subscribe_btn_color.';}';
		}
		if($this->issetVar($footer_subscribe_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .action.subscribe:hover {color: '.$footer_subscribe_btn_color_h.';}';
		}
		if($this->issetVar($footer_subscribe_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .action.subscribe:active {color: '.$footer_subscribe_btn_color_a.';}';
		}
		if($this->issetVar($footer_subscribe_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.footer .subscribe-block .block.newsletter .content .field.newsletter .action.subscribe:focus {color: '.$footer_subscribe_btn_color_f.';}';
		}

		// Category Page
		if($this->issetVar($product_name_color)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-name > a,
			.products-list .product-item-name > a,
			.products-grid .product.name a > a,
			.products-list .product.name a > a {color: '.$product_name_color.';}';
		}
		if($this->issetVar($product_name_color_h)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-name > a:hover,
			.products-list .product-item-name > a:hover,
			.products-grid .product.name a > a:hover,
			.products-list .product.name a > a:hover {color: '.$product_name_color_h.';}';
		}
		if($this->issetVar($product_name_color_a)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-name > a:active,
			.products-list .product-item-name > a:active,
			.products-grid .product.name a > a:active,
			.products-list .product.name a > a:active {color: '.$product_name_color_a.';}';
		}
		if($this->issetVar($product_name_color_f)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-name > a:focus,
			.products-list .product-item-name > a:focus,
			.products-grid .product.name a > a:focus,
			.products-list .product.name a > a:focus {color: '.$product_name_color_f.';}';
		}

		if($this->issetVar($product_sold_name_color)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info.sold-out .product-item-name > a,
			.products-list .product-item-info.sold-out .product-item-name > a {color: '.$product_sold_name_color.';}';
		}
		if($this->issetVar($product_sold_name_color_h)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info.sold-out .product-item-name > a:hover,
			.products-list .product-item-info.sold-out .product-item-name > a:hover {color: '.$product_sold_name_color_h.';}';
		}
		if($this->issetVar($product_sold_name_color_a)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info.sold-out .product-item-name > a:active,
			.products-list .product-item-info.sold-out .product-item-name > a:active {color: '.$product_sold_name_color_a.';}';
		}
		if($this->issetVar($product_sold_name_color_f)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info.sold-out .product-item-name > a:focus,
			.products-list .product-item-info.sold-out .product-item-name > a:focus {color: '.$product_sold_name_color_f.';}';
		}

		if($this->issetVar($product_wishlist_color)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .image-wrapper .add-to-links a.action.towishlist,
			.products-list .product-item-info .image-wrapper .add-to-links a.action.towishlist {color: '.$product_wishlist_color.';}';
		}
		if($this->issetVar($product_wishlist_color_h)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .image-wrapper .add-to-links a.action.towishlist:hover,
			.products-list .product-item-info .image-wrapper .add-to-links a.action.towishlist:hover {color: '.$product_wishlist_color_h.';}';
		}
		if($this->issetVar($product_wishlist_color_a)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .image-wrapper .add-to-links a.action.towishlist:active,
			.products-list .product-item-info .image-wrapper .add-to-links a.action.towishlist:active {color: '.$product_wishlist_color_a.';}';
		}
		if($this->issetVar($product_wishlist_color_f)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .image-wrapper .add-to-links a.action.towishlist:focus,
			.products-list .product-item-info .image-wrapper .add-to-links a.action.towishlist:focus {color: '.$product_wishlist_color_f.';}';
		}



		if($this->issetVar($product_bottom_btns_bg)) {
			$cssData .= PHP_EOL.
			'.products-grid .mobile-action-buttons .add-to-links a.action,
			.large-product .lightbox-button,
			.large-product .add-to-links a.action {background-color: '.$product_bottom_btns_bg.';}';
		}
		if($this->issetVar($product_bottom_btns_bg_h)) {
			$cssData .= PHP_EOL.
			'.products-grid .mobile-action-buttons .add-to-links a.action:hover,
			.large-product .lightbox-button:hover,
			.large-product .add-to-links a.action:hover {background-color: '.$product_bottom_btns_bg_h.';}';
		}
		if($this->issetVar($product_bottom_btns_bg_a)) {
			$cssData .= PHP_EOL.
			'.products-grid .mobile-action-buttons .add-to-links a.action:active,
			.large-product .lightbox-button:active,
			.large-product .add-to-links a.action:active {background-color: '.$product_bottom_btns_bg_a.';}';
		}
		if($this->issetVar($product_bottom_btns_bg_f)) {
			$cssData .= PHP_EOL.
			'.products-grid .mobile-action-buttons .add-to-links a.action:focus,
			.large-product .lightbox-button:focus,
			.large-product .add-to-links a.action:focus {background-color: '.$product_bottom_btns_bg_f.';}';
		}
		if($this->issetVar($product_bottom_btns_color)) {
			$cssData .= PHP_EOL.
			'.products-grid .mobile-action-buttons .add-to-links a.action,
			.large-product .lightbox-button,
			.large-product .add-to-links a.action {color: '.$product_bottom_btns_color.';}';
		}
		if($this->issetVar($product_bottom_btns_color_h)) {
			$cssData .= PHP_EOL.
			'.products-grid .mobile-action-buttons .add-to-links a.action:hover,
			.large-product .lightbox-button:hover,
			.large-product .add-to-links a.action:hover {color: '.$product_bottom_btns_color_h.';}';
		}
		if($this->issetVar($product_bottom_btns_color_a)) {
			$cssData .= PHP_EOL.
			'.products-grid .mobile-action-buttons .add-to-links a.action:active,
			.large-product .lightbox-button:active,
			.large-product .add-to-links a.action:active {color: '.$product_bottom_btns_color_a.';}';
		}
		if($this->issetVar($product_bottom_btns_color_f)) {
			$cssData .= PHP_EOL.
			'.products-grid .mobile-action-buttons .add-to-links a.action:focus,
			.large-product .lightbox-button:focus,
			.large-product .add-to-links a.action:focus {color: '.$product_bottom_btns_color_f.';}';
		}

		//Toolbar
		if($this->issetVar($toolbar_sorter_btn_color)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action {color: '.$toolbar_sorter_btn_color.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:hover {color: '.$toolbar_sorter_btn_color_h.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:active {color: '.$toolbar_sorter_btn_color_a.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:focus {color: '.$toolbar_sorter_btn_color_f.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_bg)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action {background-color: '.$toolbar_sorter_btn_bg.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_bg_h)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:hover {background-color: '.$toolbar_sorter_btn_bg_h.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_bg_a)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:active {background-color: '.$toolbar_sorter_btn_bg_a.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_bg_f)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:focus {background-color: '.$toolbar_sorter_btn_bg_f.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_border)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action {border-color: '.$toolbar_sorter_btn_border.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_border_h)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:hover {border-color: '.$toolbar_sorter_btn_border_h.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_border_a)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:active {border-color: '.$toolbar_sorter_btn_border_a.';}';
		}
		if($this->issetVar($toolbar_sorter_btn_border_f)) {
			$cssData .= PHP_EOL.
			'.toolbar .sorter .sorter-action:focus {border-color: '.$toolbar_sorter_btn_border_f.';}';
		}

		if($this->issetVar($toolbar_switcher_color)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item i {color: '.$toolbar_switcher_color.';}
			.toolbar .modes .item i.meigeeadditional-grid-1:before {background-color: '.$toolbar_switcher_color.';}';
		}
		if($this->issetVar($toolbar_switcher_color_h)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item:hover i {color: '.$toolbar_switcher_color_h.';}
			.toolbar .modes .item:hover i.meigeeadditional-grid-1:before {background-color: '.$toolbar_switcher_color_h.';}';
		}
		if($this->issetVar($toolbar_switcher_color_a)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item.current i {color: '.$toolbar_switcher_color_a.';}
			.toolbar .modes .item.current i.meigeeadditional-grid-1:before {background-color: '.$toolbar_switcher_color_a.';}';
		}
		if($this->issetVar($toolbar_switcher_color_f)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item:focus i {color: '.$toolbar_switcher_color_f.';}
			.toolbar .modes .item:focus i.meigeeadditional-grid-1:before {background-color: '.$toolbar_switcher_color_f.';}';
		}
		if($this->issetVar($toolbar_switcher_bg)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item i {background-color: '.$toolbar_switcher_bg.';}';
		}
		if($this->issetVar($toolbar_switcher_bg_h)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item:hover i {background-color: '.$toolbar_switcher_bg_h.';}';
		}
		if($this->issetVar($toolbar_switcher_bg_a)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item.current i {background-color: '.$toolbar_switcher_bg_a.';}';
		}
		if($this->issetVar($toolbar_switcher_bg_f)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item:focus i {background-color: '.$toolbar_switcher_bg_f.';}';
		}
		if($this->issetVar($toolbar_switcher_border)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item i {border-color: '.$toolbar_switcher_border.';}';
		}
		if($this->issetVar($toolbar_switcher_border_h)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item:hover i {border-color: '.$toolbar_switcher_border_h.';}';
		}
		if($this->issetVar($toolbar_switcher_border_a)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item.current i {border-color: '.$toolbar_switcher_border_a.';}';
		}
		if($this->issetVar($toolbar_switcher_border_f)) {
			$cssData .= PHP_EOL.
			'.toolbar .modes .item:focus i {border-color: '.$toolbar_switcher_border_f.';}';
		}

		if($this->issetVar($general_pagination_item_color)) {
			$cssData .= PHP_EOL.
			'.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item a {color: '.$general_pagination_item_color.';}';
		}
		if($this->issetVar($general_pagination_item_color_h)) {
			$cssData .= PHP_EOL.
			'.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item a:hover {color: '.$general_pagination_item_color_h.';}';
		}
		if($this->issetVar($general_pagination_item_color_a)) {
			$cssData .= PHP_EOL.
			'.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item a:active,
			.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item.active a {color: '.$general_pagination_item_color_a.';}';
		}
		if($this->issetVar($general_pagination_item_color_f)) {
			$cssData .= PHP_EOL.
			'.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item a:focus {color: '.$general_pagination_item_color_f.';}';
		}
		if($this->issetVar($general_pagination_item_bg)) {
			$cssData .= PHP_EOL.
			'.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item a {background-color: '.$general_pagination_item_bg.';}';
		}
		if($this->issetVar($general_pagination_item_bg_h)) {
			$cssData .= PHP_EOL.
			'.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item a:hover {background-color: '.$general_pagination_item_bg_h.';}';
		}
		if($this->issetVar($general_pagination_item_bg_a)) {
			$cssData .= PHP_EOL.
			'.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item a:active,
			.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item.active a {background-color: '.$general_pagination_item_bg_a.';}';
		}
		if($this->issetVar($general_pagination_item_bg_f)) {
			$cssData .= PHP_EOL.
			'.toolbar-bottom .toolbar.toolbar-products .pages .pagination li.item a:focus {background-color: '.$general_pagination_item_bg_f.';}';
		}

		if($this->issetVar($toolbar_filter_btn_color)) {
			$cssData .= PHP_EOL.
			'.toolbar .shop-by .shop-by-button {color: '.$toolbar_filter_btn_color.';}';
		}
		if($this->issetVar($toolbar_filter_current_text_color)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li span {color: '.$toolbar_filter_current_text_color.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li {
					color: '.$toolbar_filter_current_text_color.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_bg)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li {background-color: '.$toolbar_filter_current_bg.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li {
					background-color: '.$toolbar_filter_current_bg.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_border)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li {border-color: '.$toolbar_filter_current_border.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li {
					border-color: '.$toolbar_filter_current_border.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_remove_btn_color)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li a.remove {color: '.$toolbar_filter_current_remove_btn_color.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li a.remove {
					color: '.$toolbar_filter_current_remove_btn_color.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_remove_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li a.remove:hover {color: '.$toolbar_filter_current_remove_btn_color_h.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li a.remove:hover {
					color: '.$toolbar_filter_current_remove_btn_color_h.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_remove_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li a.remove:active {color: '.$toolbar_filter_current_remove_btn_color_a.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li a.remove:active {
					color: '.$toolbar_filter_current_remove_btn_color_a.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_remove_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li a.remove:focus {color: '.$toolbar_filter_current_remove_btn_color_f.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li a.remove:focus {
					color: '.$toolbar_filter_current_remove_btn_color_f.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_remove_btn_bg)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li a.remove {background-color: '.$toolbar_filter_current_remove_btn_bg.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li a.remove {
					background-color: '.$toolbar_filter_current_remove_btn_bg.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_remove_btn_bg_h)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li a.remove:hover {background-color: '.$toolbar_filter_current_remove_btn_bg_h.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li a.remove:hover {
					background-color: '.$toolbar_filter_current_remove_btn_bg_h.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_remove_btn_bg_a)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li a.remove:active {background-color: '.$toolbar_filter_current_remove_btn_bg_a.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li a.remove:active {
					background-color: '.$toolbar_filter_current_remove_btn_bg_a.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_remove_btn_bg_f)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-current .items li a.remove:focus {background-color: '.$toolbar_filter_current_remove_btn_bg_f.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-current .items li a.remove:focus {
					background-color: '.$toolbar_filter_current_remove_btn_bg_f.';
				}
			}';
		}

		if($this->issetVar($toolbar_filter_current_clear_btn_color)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-actions .btn.btn-default.filter-clear {color: '.$toolbar_filter_current_clear_btn_color.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-current .filter-actions .btn.btn-default.filter-clear {color: '.$toolbar_filter_current_clear_btn_color.';}
			}';
		}
		if($this->issetVar($toolbar_filter_current_clear_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-actions .btn.btn-default.filter-clear:hover {color: '.$toolbar_filter_current_clear_btn_color_h.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-current .filter-actions .btn.btn-default.filter-clear:hover {color: '.$toolbar_filter_current_clear_btn_color_h.';}
			}';
		}
		if($this->issetVar($toolbar_filter_current_clear_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-actions .btn.btn-default.filter-clear:active {color: '.$toolbar_filter_current_clear_btn_color_a.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-current .filter-actions .btn.btn-default.filter-clear:active {color: '.$toolbar_filter_current_clear_btn_color_a.';}
			}';
		}
		if($this->issetVar($toolbar_filter_current_clear_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.toolbar + #layered-filter-block-current .filter-actions .btn.btn-default.filter-clear:focus {color: '.$toolbar_filter_current_clear_btn_color_f.';}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-current .filter-actions .btn.btn-default.filter-clear:focus {color: '.$toolbar_filter_current_clear_btn_color_f.';}
			}';
		}
		if($this->issetVar($toolbar_filter_current_clear_btn_bg)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block-current .filter-actions .action.clear.filter-clear {
				border-color: '.$toolbar_filter_current_clear_btn_bg.';
				background-color: '.$toolbar_filter_current_clear_btn_bg.';
			}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-current .filter-actions .action.clear.filter-clear {
					background-color: '.$toolbar_filter_current_clear_btn_bg.';
					border-color: '.$toolbar_filter_current_clear_btn_bg.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_clear_btn_bg_h)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block-current .filter-actions .action.clear.filter-clear:hover {
				background-color: '.$toolbar_filter_current_clear_btn_bg_h.';
				border-color: '.$toolbar_filter_current_clear_btn_bg_h.';
			}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-current .filter-actions .action.clear.filter-clear:hover {
					background-color: '.$toolbar_filter_current_clear_btn_bg_h.';
					border-color: '.$toolbar_filter_current_clear_btn_bg_h.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_clear_btn_bg_a)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block-current .filter-actions .action.clear.filter-clear:active {
				background-color: '.$toolbar_filter_current_clear_btn_bg_a.';
				border-color: '.$toolbar_filter_current_clear_btn_bg_a.';
			}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-current .filter-actions .action.clear.filter-clear:active {
					background-color: '.$toolbar_filter_current_clear_btn_bg_a.';
					border-color: '.$toolbar_filter_current_clear_btn_bg_a.';
				}
			}';
		}
		if($this->issetVar($toolbar_filter_current_clear_btn_bg_f)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block-current .filter-actions .action.clear.filter-clear:focus {
				background-color: '.$toolbar_filter_current_clear_btn_bg_f.';
				border-color: '.$toolbar_filter_current_clear_btn_bg_f.';
			}
			@media only screen and (min-width: 768px) {
				.page-layout-1column #layered-filter-block .filter-content .filter-current .filter-actions .action.clear.filter-clear:focus {
					background-color: '.$toolbar_filter_current_clear_btn_bg_f.';
					border-color: '.$toolbar_filter_current_clear_btn_bg_f.';
				}
			}';
		}

		//Sidebar
		if($this->issetVar($cat_sidebar_border)) {
			$cssData .= PHP_EOL.
			'.sidebar .block {border-color: '.$cat_sidebar_border.';}';
		}
		if($this->issetVar($cat_sidebar_title_color)) {
			$cssData .= PHP_EOL.
			'.sidebar .block .block-title strong {color: '.$cat_sidebar_title_color.';}';
		}
		if($this->issetVar($cat_sidebar_color)) {
			$cssData .= PHP_EOL.
			'.sidebar .block,
			.sidebar .block .product-item-name > a,
			.sidebar .block .product-item-name > a:visited,
			.sidebar .block.block-wishlist .product-item .product-item-details .product-item-actions .action,
			body.account .block.block-collapsible-nav .block-collapsible-nav-content .nav.items .item a,
			body.account .block.block-collapsible-nav .block-collapsible-nav-content .nav.items .item strong {color: '.$cat_sidebar_color.';}
			.sidebar .block .product-items.product-items-names a.action:before {color: inherit; opacity: 0.7;}';
		}
		if($this->issetVar($cat_sidebar_links_color_hover)) {
			$cssData .= PHP_EOL.
			'.sidebar .block,
			.sidebar .block .product-item-name > a:hover,
			.sidebar .block .product-item-name > a:active,
			.sidebar .block.block-wishlist .product-item .product-item-details .product-item-actions .action:hover,
			.sidebar .block.block-wishlist .product-item .product-item-details .product-item-actions .action:active,
			body.account .block.block-collapsible-nav .block-collapsible-nav-content .nav.items .item.current > *,
			body.account .block.block-collapsible-nav .block-collapsible-nav-content .nav.items .item a:hover,
			body.account .block.block-collapsible-nav .block-collapsible-nav-content .nav.items .item strong:hover {color: '.$cat_sidebar_links_color_hover.';}';
		}

		if($this->issetVar($meigee_sidebar_blocks_shopby_title_color)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .block-subtitle {color: '.$meigee_sidebar_blocks_shopby_title_color.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_subtitle_color)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-title {color: '.$meigee_sidebar_blocks_shopby_subtitle_color.';}
			#layered-filter-block .filter-options-item .filter-options-title:after {color: inherit; opacity: 0.5;} ';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_links_color)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a {color: '.$meigee_sidebar_blocks_shopby_links_color.';}
			#layered-filter-block .filter-options-content .count {color: inherit; opacity: 0.8;}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_links_color_h)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a:hover {color: '.$meigee_sidebar_blocks_shopby_links_color_h.';}
			#layered-filter-block .filter-options-content .count {color: inherit; opacity: 0.8;}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_links_color_a)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a:active {color: '.$meigee_sidebar_blocks_shopby_links_color_a.';}
			#layered-filter-block .filter-options-content .count {color: inherit; opacity: 0.8;}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_links_color_f)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a:focus {color: '.$meigee_sidebar_blocks_shopby_links_color_f.';}
			#layered-filter-block .filter-options-content .count {color: inherit; opacity: 0.8;}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_color)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text {color: '.$meigee_sidebar_blocks_shopby_swatches_color.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_color_h)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:not(.disabled):hover,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):hover,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):hover,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:not(.disabled):hover {color: '.$meigee_sidebar_blocks_shopby_swatches_color_h.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_color_a)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text.selected,
			#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:active,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text.selected,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:active,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text.selected,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:active,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text.selected,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:active {color: '.$meigee_sidebar_blocks_shopby_swatches_color_a.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_color_f)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:focus,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):focus,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):focus,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:focus {color: '.$meigee_sidebar_blocks_shopby_swatches_color_f.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_bg)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text {background-color: '.$meigee_sidebar_blocks_shopby_swatches_bg.' !important;}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_bg_h)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:not(.disabled):hover,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):hover,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):hover,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:not(.disabled):hover {background-color: '.$meigee_sidebar_blocks_shopby_swatches_bg_h.' !important;}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_bg_a)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text.selected,
			#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:active,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text.selected,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:active,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text.selected,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:active,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text.selected,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:active {background-color: '.$meigee_sidebar_blocks_shopby_swatches_bg_a.' !important;}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_bg_f)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:focus,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):focus,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):focus,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:focus {background-color: '.$meigee_sidebar_blocks_shopby_swatches_bg_f.' !important;}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_border)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text {border-color: '.$meigee_sidebar_blocks_shopby_swatches_border.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_border_h)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:not(.disabled):hover,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):hover,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):hover,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:not(.disabled):hover {border-color: '.$meigee_sidebar_blocks_shopby_swatches_border_h.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_border_a)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text.selected,
			#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:active,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text.selected,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:active,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text.selected,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:active,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text.selected,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:active {border-color: '.$meigee_sidebar_blocks_shopby_swatches_border_a.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_border_f)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:focus,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):focus,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):focus,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:focus {border-color: '.$meigee_sidebar_blocks_shopby_swatches_border_f.';}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_shadow)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text {
			    -webkit-box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow.';
			    -moz-box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow.';
			    box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow.';
			}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_shadow_h)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:not(.disabled):hover,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):hover,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):hover,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:not(.disabled):hover {
				-webkit-box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_h.';
			    -moz-box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_h.';
			    box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_h.';
			}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_shadow_a)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text.selected,
			#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:active,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text.selected,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:active,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text.selected,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:active,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text.selected,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:active {
				-webkit-box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_a.';
			    -moz-box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_a.';
			    box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_a.';
			}';
		}
		if($this->issetVar($meigee_sidebar_blocks_shopby_swatches_shadow_f)) {
			$cssData .= PHP_EOL.
			'#layered-filter-block .filter-options-item .filter-options-content a .swatch-option.text:focus,
			.products-grid .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):focus,
			.products-list .product-item-info .swatches-wrapper .swatch-attribute .swatch-option.text:not(.disabled):focus,
			.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-options .swatch-option.text:focus {
				-webkit-box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_f.';
			    -moz-box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_f.';
			    box-shadow: 0px 3px 5px 0px '.$meigee_sidebar_blocks_shopby_swatches_shadow_f.';
			}';
		}

		//Product Page
		if($this->issetVar($productpage_info_name_color)) {
			$cssData .= PHP_EOL.
			' .product-info-main .page-title-wrapper h1 {color: '.$productpage_info_name_color.';}';
		}
		if($this->issetVar($product_general_price_regular)) {
			$cssData .= PHP_EOL.
			'.product-info-main .price-box .price-container .price,
			.product-add-form .price-box .price-container .price {color: '.$product_general_price_regular.';}';
		}
		if($this->issetVar($product_general_price_special)) {
			$cssData .= PHP_EOL.
			'.product-info-main .price-box .special-price .price-container .price,
			.product-add-form .price-box .special-price .price-container .price {color: '.$product_general_price_special.';}';
		}
		if($this->issetVar($product_general_price_old)) {
			$cssData .= PHP_EOL.
			'.product-info-main .price-box .old-price .price-container .price,
			.product-add-form .price-box .old-price .price-container .price {color: '.$product_general_price_old.';}';
		}
		if($this->issetVar($productpage_info_add_links_color)) {
			$cssData .= PHP_EOL.
			' .product-info-main .product-social-links a.mailto.friend,
			.product-add-form .product-social-links a.mailto.friend,
			.product-info-main .product-social-links a.action,
			.product-add-form .product-social-links a.action {color: '.$productpage_info_add_links_color.';}';
		}
		if($this->issetVar($productpage_info_add_links_color_h)) {
			$cssData .= PHP_EOL.
			' .product-info-main .product-social-links a.mailto.friend:hover,
			.product-add-form .product-social-links a.mailto.friend:hover,
			.product-info-main .product-social-links a.action:hover,
			.product-add-form .product-social-links a.action:hover {color: '.$productpage_info_add_links_color_h.';}';
		}
		if($this->issetVar($productpage_info_add_links_color_a)) {
			$cssData .= PHP_EOL.
			' .product-info-main .product-social-links a.mailto.friend:active,
			.product-add-form .product-social-links a.mailto.friend:active,
			.product-info-main .product-social-links a.action:active,
			.product-add-form .product-social-links a.action:active {color: '.$productpage_info_add_links_color_a.';}';
		}
		if($this->issetVar($productpage_info_add_links_color_f)) {
			$cssData .= PHP_EOL.
			' .product-info-main .product-social-links a.mailto.friend:focus,
			.product-add-form .product-social-links a.mailto.friend:focus,
			.product-info-main .product-social-links a.action:focus,
			.product-add-form .product-social-links a.action:focus {color: '.$productpage_info_add_links_color_f.';}';
		}
		if($this->issetVar($productpage_info_add_links_bg)) {
			$cssData .= PHP_EOL.
			' .product-info-main .product-social-links a.mailto.friend,
			.product-add-form .product-social-links a.mailto.friend,
			.product-info-main .product-social-links a.action,
			.product-add-form .product-social-links a.action {background-color: '.$productpage_info_add_links_bg.';}';
		}
		if($this->issetVar($productpage_info_add_links_bg_h)) {
			$cssData .= PHP_EOL.
			' .product-info-main .product-social-links a.mailto.friend:hover,
			.product-add-form .product-social-links a.mailto.friend:hover,
			.product-info-main .product-social-links a.action:hover,
			.product-add-form .product-social-links a.action:hover {background-color: '.$productpage_info_add_links_bg_h.';}';
		}
		if($this->issetVar($productpage_info_add_links_bg_a)) {
			$cssData .= PHP_EOL.
			' .product-info-main .product-social-links a.mailto.friend:active,
			.product-add-form .product-social-links a.mailto.friend:active,
			.product-info-main .product-social-links a.action:active,
			.product-add-form .product-social-links a.action:active {background-color: '.$productpage_info_add_links_bg_a.';}';
		}
		if($this->issetVar($productpage_info_add_links_bg_f)) {
			$cssData .= PHP_EOL.
			' .product-info-main .product-social-links a.mailto.friend:focus,
			.product-add-form .product-social-links a.mailto.friend:focus,
			.product-info-main .product-social-links a.action:focus,
			.product-add-form .product-social-links a.action:focus {background-color: '.$productpage_info_add_links_bg_f.';}';
		}

		if($this->issetVar($general_product_label_sale_color)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .image-wrapper .product-labels span,
			.products-list .product-item-info .image-wrapper .product-labels span {color: '.$general_product_label_sale_color.';}';
		}
		if($this->issetVar($general_product_label_sale_bg)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .image-wrapper .product-labels span,
			.products-list .product-item-info .image-wrapper .product-labels span {background-color: '.$general_product_label_sale_bg.';}';
		}
		if($this->issetVar($general_product_label_new_color)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .image-wrapper .product-labels span.label-new,
			.products-list .product-item-info .image-wrapper .product-labels span.label-new {color: '.$general_product_label_new_color.';}';
		}
		if($this->issetVar($general_product_label_new_bg)) {
			$cssData .= PHP_EOL.
			'.products-grid .product-item-info .image-wrapper .product-labels span.label-new,
			.products-list .product-item-info .image-wrapper .product-labels span.label-new {background-color: '.$general_product_label_new_bg.';}';
		}

		if($this->issetVar($productpage_info_cart_btn_color)) {
			$cssData .= PHP_EOL.
			'.box-tocart .actions .btn,
			.bundle-actions .btn,
			.box-tocart .actions button#bundle-slide,
			.bundle-actions button#bundle-slide {color: '.$productpage_info_cart_btn_color.';}';
		}
		if($this->issetVar($productpage_info_cart_btn_color_h)) {
			$cssData .= PHP_EOL.
			'.box-tocart .actions .btn:hover,
			.bundle-actions .btn:hover,
			.box-tocart .actions button#bundle-slide:hover,
			.bundle-actions button#bundle-slide:hover {color: '.$productpage_info_cart_btn_color_h.';}';
		}
		if($this->issetVar($productpage_info_cart_btn_color_a)) {
			$cssData .= PHP_EOL.
			'.box-tocart .actions .btn:active,
			.bundle-actions .btn:active,
			.box-tocart .actions button#bundle-slide:active,
			.bundle-actions button#bundle-slide:active {color: '.$productpage_info_cart_btn_color_a.';}';
		}
		if($this->issetVar($productpage_info_cart_btn_color_f)) {
			$cssData .= PHP_EOL.
			'.box-tocart .actions .btn:focus,
			.bundle-actions .btn:focus,
			.box-tocart .actions button#bundle-slide:focus,
			.bundle-actions button#bundle-slide:focus {color: '.$productpage_info_cart_btn_color_f.';}';
		}
		if($this->issetVar($productpage_info_cart_btn_bg)) {
			$cssData .= PHP_EOL.
			'.box-tocart .actions .btn,
			.bundle-actions .btn,
			.box-tocart .actions button#bundle-slide,
			.bundle-actions button#bundle-slide {
				background-color: '.$productpage_info_cart_btn_bg.';
				border-color: '.$productpage_info_cart_btn_bg.';
			}';
		}
		if($this->issetVar($productpage_info_cart_btn_bg_h)) {
			$cssData .= PHP_EOL.
			'.box-tocart .actions .btn:hover,
			.bundle-actions .btn:hover,
			.box-tocart .actions button#bundle-slide:hover,
			.bundle-actions button#bundle-slide:hover {
				background-color: '.$productpage_info_cart_btn_bg_h.';
				border-color: '.$productpage_info_cart_btn_bg_h.';
			}';
		}
		if($this->issetVar($productpage_info_cart_btn_bg_a)) {
			$cssData .= PHP_EOL.
			'.box-tocart .actions .btn:active,
			.bundle-actions .btn:active,
			.box-tocart .actions button#bundle-slide:active,
			.bundle-actions button#bundle-slide:active {
				background-color: '.$productpage_info_cart_btn_bg_a.';
				border-color: '.$productpage_info_cart_btn_bg_a.';
			}';
		}
		if($this->issetVar($productpage_info_cart_btn_bg_f)) {
			$cssData .= PHP_EOL.
			'.box-tocart .actions .btn:focus,
			.bundle-actions .btn:focus,
			.box-tocart .actions button#bundle-slide:focus,
			.bundle-actions button#bundle-slide:focus {
				background-color: '.$productpage_info_cart_btn_bg_f.';
				border-color: '.$productpage_info_cart_btn_bg_f.';
			}';
		}

		if($this->issetVar($productpage_options_label_color)) {
			$cssData .= PHP_EOL.
			'.product-info-main .product-add-form .product-options-wrapper .swatch-attribute .swatch-attribute-label {color: '.$productpage_options_label_color.';}';
		}
		if($this->issetVar($productpage_options_size_chart_color)) {
			$cssData .= PHP_EOL.
			'.block-link.size-chart {color: '.$productpage_options_size_chart_color.';}';
		}
		if($this->issetVar($productpage_options_size_chart_color_h)) {
			$cssData .= PHP_EOL.
			'.block-link.size-chart:hover {color: '.$productpage_options_size_chart_color_h.';}';
		}
		if($this->issetVar($productpage_options_size_chart_color_a)) {
			$cssData .= PHP_EOL.
			'.block-link.size-chart:active {color: '.$productpage_options_size_chart_color_a.';}';
		}
		if($this->issetVar($productpage_options_size_chart_color_f)) {
			$cssData .= PHP_EOL.
			'.block-link.size-chart:focus {color: '.$productpage_options_size_chart_color_f.';}';
		}

		if($this->issetVar($productpage_info_qty_bg)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control {background-color: '.$productpage_info_qty_bg.';}';
		}
		if($this->issetVar($productpage_info_qty_input_color)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control input.qty {color: '.$productpage_info_qty_input_color.';}';
		}
		if($this->issetVar($productpage_info_qty_btns_color)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control div.quantity-decrease,
			.product-info-main .quantity-wrapper .field.qty .control div.quantity-increase {color: '.$productpage_info_qty_btns_color.';}';
		}
		if($this->issetVar($productpage_info_qty_btns_color_h)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control div.quantity-decrease:hover,
			.product-info-main .quantity-wrapper .field.qty .control div.quantity-increase:hover {color: '.$productpage_info_qty_btns_color_h.';}';
		}
		if($this->issetVar($productpage_info_qty_btns_color_a)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control div.quantity-decrease:active,
			.product-info-main .quantity-wrapper .field.qty .control div.quantity-increase:active {color: '.$productpage_info_qty_btns_color_a.';}';
		}
		if($this->issetVar($productpage_info_qty_btns_color_f)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control div.quantity-decrease:focus,
			.product-info-main .quantity-wrapper .field.qty .control div.quantity-increase:focus {color: '.$productpage_info_qty_btns_color_f.';}';
		}
		if($this->issetVar($productpage_info_qty_input_btns_bg)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control div.quantity-decrease,
			.product-info-main .quantity-wrapper .field.qty .control div.quantity-increase {background-color: '.$productpage_info_qty_input_btns_bg.';}';
		}
		if($this->issetVar($productpage_info_qty_input_btns_bg_h)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control div.quantity-decrease:hover,
			.product-info-main .quantity-wrapper .field.qty .control div.quantity-increase:hover {background-color: '.$productpage_info_qty_input_btns_bg_h.';}';
		}
		if($this->issetVar($productpage_info_qty_input_btns_bg_a)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control div.quantity-decrease:active,
			.product-info-main .quantity-wrapper .field.qty .control div.quantity-increase:active {background-color: '.$productpage_info_qty_input_btns_bg_a.';}';
		}
		if($this->issetVar($productpage_info_qty_input_btns_bg_f)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .field.qty .control div.quantity-decrease:focus,
			.product-info-main .quantity-wrapper .field.qty .control div.quantity-increase:focus {background-color: '.$productpage_info_qty_input_btns_bg_f.';}';
		}
		if($this->issetVar($productpage_add_to_cart_free_shipping_color)) {
			$cssData .= PHP_EOL.
			'.product-info-main .quantity-wrapper .widget.block .free-shipping-info {color: '.$productpage_add_to_cart_free_shipping_color.';}';
		}
		if($this->issetVar($productpage_tabs_border)) {
			$cssData .= PHP_EOL.
			'.product.info.detailed .product.data.items {border-color: '.$productpage_tabs_border.';}';
		}
		if($this->issetVar($productpage_tabs_head_color)) {
			$cssData .= PHP_EOL.
			'.product.info.detailed .product.data.items > .item.title a.switch {color: '.$productpage_tabs_head_color.';}';
		}
		if($this->issetVar($productpage_tabs_head_color_h)) {
			$cssData .= PHP_EOL.
			'.product.info.detailed .product.data.items > .item.title a.switch:hover {color: '.$productpage_tabs_head_color_h.';}';
		}
		if($this->issetVar($productpage_tabs_head_color_a)) {
			$cssData .= PHP_EOL.
			'.product.info.detailed .product.data.items > .item.title.active a.switch {color: '.$productpage_tabs_head_color_a.';}';
		}
		if($this->issetVar($productpage_tabs_head_color_f)) {
			$cssData .= PHP_EOL.
			'.product.info.detailed .product.data.items > .item.title a.switch:focus {color: '.$productpage_tabs_head_color_f.';}';
		}

		if($this->issetVar($productpage_reviews_bg)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .top-block,
			#product-review-container .review-items .review-item .review-box {background-color: '.$productpage_reviews_bg.';}';
		}
		if($this->issetVar($productpage_reviews_border)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .top-block,
			#product-review-container .review-items .review-item .review-box {border-color: '.$productpage_reviews_border.';}';
		}
		if($this->issetVar($productpage_reviews_rating_general_color)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .rating-summary .rating-result {color: '.$productpage_reviews_rating_general_color.';}';
		}
		if($this->issetVar($productpage_reviews_rating_active_color)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .rating-summary .rating-result span {color: '.$productpage_reviews_rating_active_color.';}';
		}
		if($this->issetVar($productpage_reviews_title_color)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .top-block .review-title,
			#product-review-container .review-items .review-item .review-box .review-author {color: '.$productpage_reviews_title_color.';}';
		}
		if($this->issetVar($productpage_reviews_text_color)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .review-box {color: '.$productpage_reviews_text_color.';}';
		}
		if($this->issetVar($productpage_reviews_link_color)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .review-box .read-more-link {color: '.$productpage_reviews_link_color.';}';
		}
		if($this->issetVar($productpage_reviews_link_color_h)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .review-box .read-more-link:hover {color: '.$productpage_reviews_link_color_h.';}';
		}
		if($this->issetVar($productpage_reviews_link_color_a)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .review-box .read-more-link:active {color: '.$productpage_reviews_link_color_a.';}';
		}
		if($this->issetVar($productpage_reviews_link_color_f)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .review-box .read-more-link:focus {color: '.$productpage_reviews_link_color_f.';}';
		}
		if($this->issetVar($productpage_reviews_date_color)) {
			$cssData .= PHP_EOL.
			'#product-review-container .review-items .review-item .review-box .date,
			#product-review-container .review-items .review-item .top-block .customer-rating .review-ratings .rating-summary .label {color: '.$productpage_reviews_date_color.';}';
		}

		// Label "Sold Out"
		$product_sold_name_label_color = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'product_sold_name_label_color' );
		$product_sold_name_label_bg = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'product_sold_name_label_bg' );

		$cssData .= $this->cssData(
			array( $product_sold_name_label_color, $product_sold_name_label_bg ),
			'html body .item .product-item-info .image-wrapper .sold-out-label span',
			array( 'color', 'background-color' )
		);

		/* Product Hover Types begin */

			/* Type 1 begin */

				/* Type 1 variables begin */

					$hover_type1_cart_text = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_text_d' );
					$hover_type1_cart_text_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_text_h' );
					$hover_type1_cart_text_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_text_a' );
					$hover_type1_cart_text_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_text_f' );

					$hover_type1_cart_bg = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_bg_d' );
					$hover_type1_cart_bg_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_bg_h' );
					$hover_type1_cart_bg_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_bg_a' );
					$hover_type1_cart_bg_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_bg_f' );

					$hover_type1_cart_border = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_border_d' );
					$hover_type1_cart_border_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_border_h' );
					$hover_type1_cart_border_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_border_a' );
					$hover_type1_cart_border_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_cart_border_f' );

					$hover_type1_quickview_text = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_text_d' );
					$hover_type1_quickview_text_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_text_h' );
					$hover_type1_quickview_text_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_text_a' );
					$hover_type1_quickview_text_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_text_f' );

					$hover_type1_quickview_bg = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_bg_d' );
					$hover_type1_quickview_bg_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_bg_h' );
					$hover_type1_quickview_bg_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_bg_a' );
					$hover_type1_quickview_bg_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_bg_f' );

					$hover_type1_quickview_border = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_border_d' );
					$hover_type1_quickview_border_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_border_h' );
					$hover_type1_quickview_border_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_border_a' );
					$hover_type1_quickview_border_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_quickview_border_f' );

					$hover_type1_others_text = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_text_d' );
					$hover_type1_others_text_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_text_h' );
					$hover_type1_others_text_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_text_a' );
					$hover_type1_others_text_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_text_f' );

					$hover_type1_others_bg = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_bg_d' );
					$hover_type1_others_bg_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_bg_h' );
					$hover_type1_others_bg_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_bg_a' );
					$hover_type1_others_bg_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_bg_f' );

					$hover_type1_others_underlay = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type1_others_underlay' );

				/* Type 1 variables eof */

				/* Type 1 addCss begin */

					// Add to Cart
					$cssData .= $this->cssData(
						array( $hover_type1_cart_text, $hover_type1_cart_bg, $hover_type1_cart_border ),
						'html body .product-hover-1 .item .product-item-info .btn.btn-default',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_cart_text_h, $hover_type1_cart_bg_h, $hover_type1_cart_border_h ),
						'html body .product-hover-1 .item .product-item-info .btn.btn-default:hover',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_cart_text_a, $hover_type1_cart_bg_a, $hover_type1_cart_border_a ),
						'html body .product-hover-1 .item .product-item-info .btn.btn-default:active',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_cart_text_f, $hover_type1_cart_bg_f, $hover_type1_cart_border_f ),
						'html body .product-hover-1 .item .product-item-info .btn.btn-default:focus',
						array( 'color', 'background-color', 'border-color' )
					);

					// Quickview
					$cssData .= $this->cssData(
						array( $hover_type1_quickview_text, $hover_type1_quickview_bg, $hover_type1_quickview_border ),
						'html body .product-hover-1 .item .product-item-info .weltpixel-quickview',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_quickview_text_h, $hover_type1_quickview_bg_h, $hover_type1_quickview_border_h ),
						'html body .product-hover-1 .item .product-item-info .weltpixel-quickview:hover',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_quickview_text_a, $hover_type1_quickview_bg_a, $hover_type1_quickview_border_a ),
						'html body .product-hover-1 .item .product-item-info .weltpixel-quickview:active',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_quickview_text_f, $hover_type1_quickview_bg_f, $hover_type1_quickview_border_f ),
						'html body .product-hover-1 .item .product-item-info .weltpixel-quickview:focus',
						array( 'color', 'background-color', 'border-color' )
					);

					// Other buttons
					$cssData .= $this->cssData(
						array( $hover_type1_others_text, $hover_type1_others_bg ),
						'html body .product-hover-1 .item .product-item-info .hover-buttons .hover-btn-1',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_others_text_h, $hover_type1_others_bg_h ),
						'html body .product-hover-1 .item .product-item-info .hover-buttons .hover-btn-1:hover',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_others_text_a, $hover_type1_others_bg_a ),
						'html body .product-hover-1 .item .product-item-info .hover-buttons .hover-btn-1:active',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_others_text_f, $hover_type1_others_bg_f ),
						'html body .product-hover-1 .item .product-item-info .hover-buttons .hover-btn-1:focus',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type1_others_underlay ),
						'html body .product-hover-1 .item .product-item-info .hover-buttons',
						array( 'background-color' )
					);


				/* Type 1 addCss eof */

			/* Type 1 eof */

			/* Type 2 begin */

				/* Type 2 variables begin */

					$hover_type2_quickview_text = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_text_d' );
					$hover_type2_quickview_text_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_text_h' );
					$hover_type2_quickview_text_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_text_a' );
					$hover_type2_quickview_text_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_text_f' );

					$hover_type2_quickview_bg = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_bg_d' );
					$hover_type2_quickview_bg_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_bg_h' );
					$hover_type2_quickview_bg_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_bg_a' );
					$hover_type2_quickview_bg_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_bg_f' );

					$hover_type2_quickview_border = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_border_d' );
					$hover_type2_quickview_border_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_border_h' );
					$hover_type2_quickview_border_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_border_a' );
					$hover_type2_quickview_border_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_quickview_border_f' );

					$hover_type2_others_text = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_others_text_d' );
					$hover_type2_others_text_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_others_text_h' );
					$hover_type2_others_text_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_others_text_a' );
					$hover_type2_others_text_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_others_text_f' );

					$hover_type2_others_bg = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_others_bg_d' );
					$hover_type2_others_bg_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_others_bg_h' );
					$hover_type2_others_bg_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_others_bg_a' );
					$hover_type2_others_bg_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type2_others_bg_f' );

				/* Type 2 variables eof */

				/* Type 2 addCss begin */

					// Quickview
					$cssData .= $this->cssData(
						array( $hover_type2_quickview_text, $hover_type2_quickview_bg, $hover_type2_quickview_border ),
						'html body .product-hover-2 .item .product-item-info .weltpixel-quickview',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type2_quickview_text_h, $hover_type2_quickview_bg_h, $hover_type2_quickview_border_h ),
						'html body .product-hover-2 .item .product-item-info .weltpixel-quickview:hover',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type2_quickview_text_a, $hover_type2_quickview_bg_a, $hover_type2_quickview_border_a ),
						'html body .product-hover-2 .item .product-item-info .weltpixel-quickview:active',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type2_quickview_text_f, $hover_type2_quickview_bg_f, $hover_type2_quickview_border_f ),
						'html body .product-hover-2 .item .product-item-info .weltpixel-quickview:focus',
						array( 'color', 'background-color', 'border-color' )
					);

					// Other buttons
					$otherType2 = PHP_EOL . 'html body .product-hover-2 .item .product-item-info .hover-buttons';

					$cssData .= $this->cssData(
						array( $hover_type2_others_text, $hover_type2_others_bg ),
						$otherType2 . ' .btn.btn-default,' .
								$otherType2 . ' .add-to-links a.action.tocompare.hover-btn-2,' .
								$otherType2 . ' .lightbox-button.hover-btn-2',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type2_others_text_h, $hover_type2_others_bg_h ),
						$otherType2 . ' .btn.btn-default:hover,' .
								$otherType2 . ' .add-to-links a.action.tocompare.hover-btn-2:hover,' .
								$otherType2 . ' .lightbox-button:hover.hover-btn-2',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type2_others_text_a, $hover_type2_others_bg_a ),
						$otherType2 . ' .btn.btn-default:active,' .
								$otherType2 . ' .add-to-links a.action.tocompare.hover-btn-2:active,' .
								$otherType2 . ' .lightbox-button.hover-btn-2:active',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type2_others_text_f, $hover_type2_others_bg_f ),
						$otherType2 . ' .btn.btn-default:focus,' .
								$otherType2 . ' .add-to-links a.action.tocompare.hover-btn-2:focus,' .
								$otherType2 . ' .lightbox-button.hover-btn-2:focus',
						array( 'color', 'background-color' )
					);

				/* Type 2 addCss eof */

			/* Type 2 eof */

			/* Type 3 begin */

				/* Type 3 variables eof */

					$hover_type3_cart_text = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_text_d' );
					$hover_type3_cart_text_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_text_h' );
					$hover_type3_cart_text_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_text_a' );
					$hover_type3_cart_text_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_text_f' );

					$hover_type3_cart_bg = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_bg_d' );
					$hover_type3_cart_bg_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_bg_h' );
					$hover_type3_cart_bg_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_bg_a' );
					$hover_type3_cart_bg_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_bg_f' );

					$hover_type3_cart_border = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_border_d' );
					$hover_type3_cart_border_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_border_h' );
					$hover_type3_cart_border_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_border_a' );
					$hover_type3_cart_border_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_cart_border_f' );

					$hover_type3_others_text = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_others_text_d' );
					$hover_type3_others_text_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_others_text_h' );
					$hover_type3_others_text_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_others_text_a' );
					$hover_type3_others_text_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_others_text_f' );

					$hover_type3_others_bg = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_others_bg_d' );
					$hover_type3_others_bg_h = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_others_bg_h' );
					$hover_type3_others_bg_a = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_others_bg_a' );
					$hover_type3_others_bg_f = $this->_customDesign->getConfigData( $sectionId, 'category_tabs', 'hover_type3_others_bg_f' );

				/* Type 3 variables eof */

				/* Type 3 addCss begin */

					// Add to Cart
					$cssData .= $this->cssData(
						array( $hover_type3_cart_text, $hover_type3_cart_bg, $hover_type3_cart_border ),
						'html body .product-hover-3 .item .product-item-info .mobile-action-buttons .btn.btn-default',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type3_cart_text_h, $hover_type3_cart_bg_h, $hover_type3_cart_border_h ),
						'html body .product-hover-3 .item .product-item-info .mobile-action-buttons .btn.btn-default:hover',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type3_cart_text_a, $hover_type3_cart_bg_a, $hover_type3_cart_border_a ),
						'html body .product-hover-3 .item .product-item-info .mobile-action-buttons .btn.btn-default:active',
						array( 'color', 'background-color', 'border-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type3_cart_text_f, $hover_type3_cart_bg_f, $hover_type3_cart_border_f ),
						'html body .product-hover-3 .item .product-item-info .mobile-action-buttons .btn.btn-default:focus',
						array( 'color', 'background-color', 'border-color' )
					);

					// Other buttons
					$otherType3 = PHP_EOL . 'html body .product-hover-3 .item .product-item-info .hover-buttons';

					$cssData .= $this->cssData(
						array( $hover_type3_others_text, $hover_type3_others_bg ),
						$otherType3 . ' .add-to-links a.action.tocompare.hover-btn-1,' .
								$otherType3 . ' .weltpixel-quickview.hover-btn-1,' .
								$otherType3 . ' .lightbox-button.hover-btn-1',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type3_others_text_h, $hover_type3_others_bg_h ),
						$otherType3 . ' .add-to-links a.action.tocompare.hover-btn-1:hover,' .
								$otherType3 . ' .weltpixel-quickview.hover-btn-1:hover,' .
								$otherType3 . ' .lightbox-button:hover.hover-btn-1',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type3_others_text_a, $hover_type3_others_bg_a ),
						$otherType3 . ' .add-to-links a.action.tocompare.hover-btn-1:active,' .
								$otherType3 . ' .weltpixel-quickview.hover-btn-1:active,' .
								$otherType3 . ' .lightbox-button.hover-btn-1:active',
						array( 'color', 'background-color' )
					);

					$cssData .= $this->cssData(
						array( $hover_type3_others_text_f, $hover_type3_others_bg_f ),
						$otherType3 . ' .add-to-links a.action.tocompare.hover-btn-1:focus,' .
								$otherType3 . ' .weltpixel-quickview.hover-btn-1:focus,' .
								$otherType3 . ' .lightbox-button.hover-btn-1:focus',
						array( 'color', 'background-color' )
					);

				/* Type 3 addCss eof */

			/* Type 3 eof */

		/* Product Hover Types eof */

		   
		$cssData = trim(preg_replace('/\t+/', '', $cssData));
	
		return $cssData;
	}
} 