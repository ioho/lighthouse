<?php
namespace Meigee\Coccinelle\Observer;
 
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
// use \Magento\Store\Model\ScopeInterface;

class MeigeeObserver implements ObserverInterface
{
 
	 /**
     * @var RequestInterface
     */
    protected $appRequestInterface;
	private $_cssGenerate;
	private $_activation;

    public function __construct(
		\Magento\Backend\Block\Template\Context $context,
        RequestInterface $appRequestInterface,
		\Meigee\Coccinelle\Block\Frontend\CustomDesign $cssGenerate,
		\Meigee\Coccinelle\Block\Frontend\ThemeActivation $activation
    ) {
        $this->appRequestInterface = $appRequestInterface;
		$this->_cssGenerate = $cssGenerate;
		$this->_activation = $activation;
    }
 
	public function execute(Observer $observer)
	{
		$sectionId = $this->appRequestInterface->getParam('section');
		
		if($sectionId == 'coccinelle_activation') {
			// var_dump($this->appRequestInterface->getParams()); die('#');
			$this->_activation->createStaticContent($this->appRequestInterface->getParams());
		}
		
		$is_design = strrpos($sectionId, 'coccinelle_theme_design');
		
		if($is_design !== false) {
			 $this->_cssGenerate->saveOpt(true, $sectionId);
		}
	}
}

