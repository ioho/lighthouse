<?php
namespace Meigee\Coccinelle\Block\Frontend;    
use \Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\Store;
use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Theme\Model\Data\Design\Config as DesignConfig;
use \Magento\Framework\Controller\ResultFactory as ResultFactory;
use \Magento\Cms\Model\ResourceModel\Block\CollectionFactory as BlockCollection;

class ThemeActivation extends \Magento\Framework\View\Element\Template
{
	
	const DS = DIRECTORY_SEPARATOR;
	
	protected $_scopeConfig;
	protected $_filesystem;
	protected $configWriter;
	protected $_pageFactory;
	protected $_blockFactory;
	protected $_request;
	protected $_messageManager;
	protected $_dir;
	protected $_themeProvider;
	protected $_lessCompiler;
	protected $_themeCollectionFactory;
	protected $_themeConfig;
	protected $_reinitableConfig;
	protected $_indexerRegistry;
	protected $_resultRedirect;
	protected $_widgetFactory;
	protected $_pageRepositoryInterface;
	
	public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\Filesystem $_filesystem,
		\Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
		\Magento\Cms\Model\PageFactory $pageFactory,
		\Magento\Cms\Model\BlockFactory $blockFactory,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Framework\Message\ManagerInterface $messageManager,
		\Magento\Framework\Filesystem\DirectoryList $dir,
		\Magento\Framework\View\Design\Theme\ThemeProviderInterface $themeProvider,
		\Meigee\Coccinelle\Model\Less\LessCompiler $lessCompiler,
		\Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $themeCollectionFactory,
        \Magento\Theme\Model\Config $themeConfig,
		ReinitableConfigInterface $reinitableConfig,
		IndexerRegistry $indexerRegistry,
		ResultFactory $result,
		\Magento\Widget\Model\Widget\InstanceFactory $widgetFactory,
		\Magento\Cms\Api\PageRepositoryInterface $pageRepositoryInterface
    ) {
		$this->_scopeConfig = $scopeConfig;
		$this->_storeManager = $storeManager;
		$this->_filesystem = $_filesystem;
		$this->configWriter = $configWriter;
		$this->_pageFactory = $pageFactory;
		$this->_blockFactory = $blockFactory;
		$this->_request = $request;
		$this->_messageManager = $messageManager;
		$this->_dir = $dir;
		$this->_lessCompiler = $lessCompiler;
		$this->_themeProvider = $themeProvider;
		$this->_themeCollectionFactory = $themeCollectionFactory;
		$this->_themeConfig = $themeConfig;
		$this->_reinitableConfig = $reinitableConfig;
		$this->_indexerRegistry = $indexerRegistry;
		$this->_resultRedirect = $result;
		$this->_widgetFactory = $widgetFactory;
		$this->_pageRepositoryInterface = $pageRepositoryInterface;
    }
	
	public function getConfigData($section, $group, $field, $s_id = null) {
		($s_id == null ? $storeId = $this->_request->getParam('store') : $storeId = $s_id);
		($storeId == null ? $storeId = 0 : $storeId = $storeId);
				
		return $this->_scopeConfig->getValue($section.'/'.$group.'/'.$field, ScopeInterface::SCOPE_STORE, $storeId);
	}
	
	public function includeCss($storeId) 
	{
		$themeName = $this->getThemeData();
		if($themeName != false) {
			$file = $this->getConfigData('meigee_'.$themeName, 'general', 'css_file', $storeId);
			if($file == null && $storeId == 1) {
				$file = $this->getConfigData('meigee_'.$themeName, 'general', 'css_file', 0);
			}
			
			if($file != null) {
				$css = '<link rel="stylesheet" href="'.$this->getDesignDir(true).self::DS.$themeName.self::DS.$file.'" />';
			}
			 else {
				$css = false;
			}
			return $css;
		} 
		return false;
	}
	
	public function compileLess($skin, $color)
	{		
		$pub = $this->_dir->getPath('pub');
		$activationFolder = $pub.self::DS.'meigeeactivation'.self::DS.'skins';
		$activationFile = $activationFolder.self::DS.$skin.'.less';
		$this->_lessCompiler->movetoTmpFolder($activationFile, $activationFolder, $skin, $color);
	}
	
	public function createStaticContent($request) 
	{
		if(isset($request['groups'])) {
			$len = strlen('_confirm');
			if(isset($request['groups']['pages'])) {
				$cType = 'pages';
				$pages = $request['groups']['pages']['fields'];
				foreach($pages as $type => $page) {
					if (strpos($type, '_confirm') !== false) {
						if(isset($page['value'])) {
							if($page['value'] == '1') {
								$ntype = substr($type, 0, -$len);
								$this->writeContent($cType, $ntype, $pages[$ntype]['value']);
							} 
						} 
					} 
				}
			}
			if(isset($request['groups']['blocks'])) {
				$cType = 'blocks';
				$blocks = $request['groups']['blocks']['fields'];
				foreach($blocks as $type => $block) {
					if (strpos($type, '_confirm') !== false) {
						if(isset($block['value'])) {
							if($block['value'] == '1') {
								$ntype = substr($type, 0, -$len);
								$this->writeContent($cType, $ntype, $blocks[$ntype]['value']);
							}
						}
					} 
				}
			}
			if(isset($request['groups']['skins'])) {
				$skinsGroup = $request['groups']['skins']['fields'];
				if(isset($skinsGroup['confirm']['value'])) {
					if($skinsGroup['confirm']['value'] == 1) {
						$skin = $skinsGroup['skin']['value'];
						$color = 'color_'.$skin;
						if(isset($skinsGroup[$color]['value'])) {
							$color = $skinsGroup[$color]['value'];
							$this->compileLess($skin, $color);
						}
					}
				}
				if(isset($skinsGroup['skin']['value'])) {
					$skin = $skinsGroup['skin']['value'];
					$color = 'color_'.$skin;
					if(isset($skinsGroup[$color]['value'])) {
						$color = $skinsGroup[$color]['value'];
						$this->setConfigs($skin, $color);
					} else {
						$this->setConfigs($skin);
					}
				}
			}
		} else {
			return false;
		} 
	}
	
	public function setConfigs($skin, $color = null)
	{
		$storeId = $this->_request->getParam('store');
		if(isset($storeId) && $storeId != null) {
			$s_id = $storeId;
		} else {
			$s_id = 0;
		}
		$pub = $this->_dir->getPath('pub');
		$activationFolder = $pub.self::DS.'meigeeactivation'.self::DS.'config';
		if($color != null) $skin = $skin.'_'.$color;
		$file = $activationFolder.self::DS.$skin.'.xml';
		if(file_exists($file)) {
			$xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
			$configs = json_decode(json_encode($xml), true);
			foreach($configs['configs'] as $section => $groups) {
				foreach($groups as $group => $fields) {
					foreach($fields as $field => $value) {
						if($s_id == 0) {
							$this->configWriter->save($section.'/'.$group.'/'.$field, $value);							
						} else {
							$this->configWriter->save($section.'/'.$group.'/'.$field, $value, ScopeInterface::SCOPE_STORES, $s_id);							
						}
					}
				}
			}
			$theme = $configs['theme']['name'];
			$this->assignTheme($theme);
			$this->_reinitableConfig->reinit();
			$this->_indexerRegistry->get(DesignConfig::DESIGN_CONFIG_GRID_INDEXER_ID)->reindexAll();
			
			foreach($configs['pages'] as $type => $vals) {
				$this->writeContent('pages', $type, $vals);
			}
			if(isset($configs['widgets'])) {
				foreach($configs['widgets'] as $type => $vals) {
					$this->insertWidget($vals, $theme);
				}
			}
			foreach($configs['blocks'] as $type => $vals) {
				$this->writeContent('blocks', $type, $vals);
			}
			
		}
	}

	public function checkIssetStatic($block, $storeId)
	{
		$availableBlocks = [];

		$staticBlock = $this->_blockFactory->create();
		$staticBlock = $staticBlock->load($block['id'], 'identifier');
		if($staticBlock) {
			$availableBlocks[] = $block;
		}
		empty($availableBlocks) ? $status = false : $status = $availableBlocks;
		return $status;
	}
	
	public function insertWidget($data, $theme)
	{
		$pageGroupConfig = [
            'pages' => [
                'block' => '',
                'for' => 'all',
                'layout_handle' => 'default',
                'template' => 'widget/static_block/default.phtml',
                'page_id' => '',
            ],
            'all_pages' => [
                'block' => '',
                'for' => 'all',
                'layout_handle' => 'default',
                'template' => 'widget/static_block/default.phtml',
                'page_id' => '',
            ],
            'all_products' => [
                'block' => '',
                'for' => 'all',
                'layout_handle' => 'catalog_product_view',
                'template' => 'widget/static_block/default.phtml',
                'page_id' => '',
            ],
            'anchor_categories' => [
                'entities' => '',
                'for' => 'all',
                'is_anchor_only' => 0,
                'layout_handle' => 'catalog_category_view_type_layered',
                'template' => 'widget/static_block/default.phtml',
                'page_id' => '',
            ],
            'notanchor_categories' => [
                'entities' => '',
                'for' => 'all',
                'is_anchor_only' => 0,
                'layout_handle' => 'catalog_category_view_type_default',
                'template' => 'widget/static_block/default.phtml',
                'page_id' => '',
            ],
        ];
		
		$widgetInstance = $this->_widgetFactory->create();
		
		$widgetParameters = ['block_id' => $data['block_id']];
		if(isset($data['parameters'])) {
			$widgetParameters = array_merge($widgetParameters, $data['parameters']);
		}

		$code = $data['type'];
		$themeId = $this->_themeCollectionFactory->create()->getThemeByFullPath('frontend/'.$theme)->getId();
		
		$type = $widgetInstance->getWidgetReference('code', $code, 'type');
		$pageGroup = [];
		
		foreach($data['groups'] as $k => $item) {
			$key = str_replace('group', '', $k);
			$groupData = array_merge($pageGroupConfig[$item['page_group']], $item['group_data']);
			$pageGroup[$key] = [
				'page_group' => $item['page_group'],
				$item['page_group'] => $groupData
			];
		}

		$widgetInstance->setType($type)->setCode($code)->setThemeId($themeId);
		$widgetInstance->setTitle($data['title'])
			->setStoreIds([\Magento\Store\Model\Store::DEFAULT_STORE_ID])
			->setWidgetParameters($widgetParameters)
			->setPageGroups($pageGroup);
		$widgetInstance->save();
	}
	
	public function writeContent($cType, $type, $vals)
	{
		$storeId = $this->_request->getParam('store');
		if(isset($storeId) && $storeId != null) {
			$s_id = $storeId;
		} else {
			$s_id = 0;
		}
		try {
			if($cType == 'pages') {
				$content = $this->getContent($cType, $vals['id']);
				$page = $this->_pageFactory->create();
				$page->setTitle($vals['title'])
					->setIdentifier($vals['id'])
					->setIsActive(true)
					->setPageLayout('1column')
					->setStores([$s_id])
					->setContent($content)
					->save();
				if($type == 'homepage') {
					if($s_id == 0) {
						$this->configWriter->save('web/default/cms_home_page', $vals['id']);							
					} else {
						$this->configWriter->save('web/default/cms_home_page', $vals['id'], ScopeInterface::SCOPE_STORES, $s_id);							
					}
				}
			} elseif($cType == 'blocks') {
				$content = $this->getContent($cType, $vals['id']);
				$block = $this->_blockFactory->create();
				$block->setTitle($vals['title'])
					  ->setIdentifier($vals['id'])
					  ->setIsActive(true)
					  ->setContent($content)
					  ->setStores([$s_id])
					  ->save();
			}
        }
		catch(\Exception $e){
			$message = $e->getMessage();
			$this->_messageManager->addError($message);
        }
	}
	
	public function getContent($type, $id)
	{
		$pub = $this->_dir->getPath('pub');
		
		$activationFolder = $pub.self::DS.'meigeeactivation'.self::DS.$type;
		$activationFile = (string)$activationFolder.self::DS.$id.'.txt';
	
		$content = file_get_contents($activationFile);
		
		return $content;
	}
	
	public function moveFileToCssDir($file, $filename)
	{
		$storeId = $this->_request->getParam('store');
		($storeId == null ? $storeId = 0 : $storeId = $storeId);
		$designDir = $this->getDesignDir();
		$themeName = $this->getThemeData();
		$dirExist = is_dir($designDir);
		if(!$dirExist) {
			$designDir = mkdir($designDir, 0775);
		}
		$themeDir = $designDir.self::DS.$themeName;
		if(!is_dir($themeDir)) {
			 mkdir($themeDir, 0775);
		}
		copy($file, $themeDir.self::DS.$filename);
		$this->configWriter->save('meigee_'.$themeName.'/general/css_file',  $filename, ScopeInterface::SCOPE_STORES, $storeId);
	}
		
	public function getDesignDir($is_link = false) {
		if($is_link) {
			$mediapath = $this ->_storeManager-> getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
		} else {
			$mediapath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
		}
		$dir = 'meigee';
		return $mediapath.self::DS.$dir;
	}
	
	public function getThemeData() {
        $themeId = $this->_scopeConfig->getValue(
            \Magento\Framework\View\DesignInterface::XML_PATH_THEME_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()->getId()
        );

        $theme = $this->_themeProvider->getThemeById($themeId);
        $themeName = $theme->getData();
		if(isset($themeName['theme_title'])) {
			$themeName = $themeName['theme_title'];
			$themeName = strtolower($themeName);
			$themeName = str_replace(' ', '_', $themeName);
			
			return $themeName;
		}
		return false;
    }

    /**
     * Assign Theme
     *
     * @return void
     */
    protected function assignTheme($name)
    {
        $themes = $this->_themeCollectionFactory->create()->loadRegisteredThemes();
        /**
         * @var \Magento\Theme\Model\Theme $theme
         */
        foreach ($themes as $theme) {
            if ($theme->getCode() == $name) {
                $this->_themeConfig->assignToStore(
                    $theme,
                    [Store::DEFAULT_STORE_ID],
                    \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT
                );
            }
        }
    }
	
	// public function removeDataByPath($path) {	
		// $storeId = $this->_request->getParam('store');
		// $this->configWriter->delete($path, ScopeInterface::SCOPE_STORES, $storeId);
	// }
}