<?php
namespace Meigee\Coccinelle\Block\Adminhtml\System\Config\InputRadio;

class Horizontal extends \Meigee\Coccinelle\Block\Adminhtml\System\Config\InputRadio
{
    function getTypeClass()
    {
        return 'meigee-thumb-horizontal';
    }
}
