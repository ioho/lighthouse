<?php
namespace Meigee\Coccinelle\Block\Adminhtml\System\Config\InputRadio;

class Vertical extends \Meigee\Coccinelle\Block\Adminhtml\System\Config\InputRadio
{
    function getTypeClass()
    {
        return 'meigee-thumb-vertical';
    }
}
