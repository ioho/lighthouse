var config = {
    map: {
        '*': {
            MeigeeBootstrap: 'Meigee_Coccinelle/js/bootstrap.min',
			MeigeeCarousel: 'Meigee_Coccinelle/js/owl.carousel',
			jqueryBackstretch: 'Meigee_Coccinelle/js/jquery.backstretch.min',
			//googleMap: 'https://maps.googleapis.com/maps/api/js?sensor=false',
			//            lightBox: 'Meigee_Coccinelle/js/ekko-lightbox.min'
			lightBox: 'Meigee_Coccinelle/js/ekko-lightbox.customized',
			menu: 'Meigee_Coccinelle/js/menu',
			mobile_menu: 'Meigee_Coccinelle/js/mobile_menu',
			sticky_menu: 'Meigee_Coccinelle/js/sticky_menu',
			meigeeCookies: 'Meigee_Coccinelle/js/jquery.cookie',
			vEllipsis: 'Meigee_Coccinelle/js/jquery.vEllipsis.min'
        }
    }

};




