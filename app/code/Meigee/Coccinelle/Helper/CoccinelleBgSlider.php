<?php
namespace Meigee\Coccinelle\Helper;

class CoccinelleBgSlider extends Section
{
    protected function getThemeCnfBlock()
    {
        return 'coccinelle_bg_slider';
    }
}
