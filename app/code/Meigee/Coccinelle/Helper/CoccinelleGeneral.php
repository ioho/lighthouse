<?php
namespace Meigee\Coccinelle\Helper;

class CoccinelleGeneral extends Section
{
    protected function getThemeCnfBlock()
    {
        return 'coccinelle_general';
    }
}
