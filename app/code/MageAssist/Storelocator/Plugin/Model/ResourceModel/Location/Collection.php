<?php

namespace MageAssist\Storelocator\Plugin\Model\ResourceModel\Location;

use Amasty\Storelocator\Model\ResourceModel\Location\Collection as CollectionOld;

class Collection
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    public function __construct(\Magento\Framework\App\RequestInterface $request)
    {
        $this->request = $request;
    }

    public function afterApplyDefaultFilters(CollectionOld $subject,
                                             $result
    ) {

        $country = $this->request->getParam('country');

        if (false === empty($country)) {
            $subject->addFieldToFilter('country', $country);
        }

        return $result;
    }
}
