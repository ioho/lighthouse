<?php

namespace MageAssist\Storelocator\Helper;

class Country extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    private $context;
    /**
     * @var \Amasty\Storelocator\Model\ConfigProvider
     */
    private $configProvider;
    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\Collection
     */
    private $collection;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    private $locationJson;

    public function __construct(\Magento\Framework\App\Helper\Context $context,
                                \Amasty\Storelocator\Model\ConfigProvider $configProvider,
                                \Magento\Directory\Model\ResourceModel\Country\Collection $collection,
                                \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->context        = $context;
        $this->configProvider = $configProvider;
        $this->collection     = $collection;
        $this->storeManager   = $storeManager;
    }

    public function getOptionsByCountry()
    {
        $result = [
            [
                'label' => '',
                'value' => ''
            ]
        ];

        $connection = $this->collection->getSelect()->getConnection();
        $storeId    = $this->storeManager->getStore()->getId();
        $sql        = $connection->select()
            ->from($connection->getTableName('amasty_amlocator_location'), ['country', 'stores'])
            ->where('status=1')
            ->where('lat IS NOT NULL AND lat != ""')
            ->where('lng IS NOT NULL AND lng != ""')
            ->where('FIND_IN_SET("0", `stores`) OR FIND_IN_SET("' . $storeId . '", `stores`)');

        $tmpCountries   = $connection->fetchAll($sql);
        $countriesCodes = [];

        foreach ($tmpCountries as $country) {
            $countriesCodes[$country['country']] = $country['country'];
        }

        if (false === empty($countriesCodes)) {
            $this->collection->addFieldToFilter('country_id', ['in' => $countriesCodes]);

            foreach ($this->collection as $country) {
                /**
                 * @var \Magento\Directory\Model\Country $country
                 */
                $tmp          = [];
                $tmp['label'] = $country->getName();
                $tmp['value'] = $country->getCountryId();
                $result[]     = $tmp;
            }
        }

        return $result;
    }

    public function getJsonLocations(\Amasty\Storelocator\Model\ResourceModel\Location\Collection $collection)
    {

        if ($this->locationJson === null) {
            $storeId    = $this->storeManager->getStore()->getId();
            $newCollection = clone  $collection;
            $newCollection->clear();
            $newCollection->setPageSize(99999);
            $newCollection->setCurPage(1);

            foreach ($newCollection->getLocationData() as $location) {
                $locationArray['items'][] = $location;
            }
            $locationArray['totalRecords']   = count($locationArray['items']);
            $locationArray['currentStoreId'] = $storeId;

            $this->locationJson = json_encode($locationArray);
        }

        return $this->locationJson;
    }


    public function getBaloonTemplate()
    {
        $value= $this->scopeConfig->getValue('amlocator/locator/visual_settings/store_list_template');
        $array=['baloon'=>$value];

        return json_encode($array);
    }
}
