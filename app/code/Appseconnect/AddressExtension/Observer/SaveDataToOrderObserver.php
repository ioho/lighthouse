<?php
namespace Appseconnect\AddressExtension\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;


class SaveDataToOrderObserver implements ObserverInterface
{

    private $_quoteRepository;

    private $_resourceConnection;

    private $_checkoutSession;

    private $_scopeConfig;

    protected $logger;

    protected $debugMessage;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectmanager,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Magento\Quote\Model\QuoteRepository $quoteRepository,
                                \Magento\Framework\App\ResourceConnection $resourceConnection,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                LoggerInterface $logger
                                )
    {
        $this->_quoteRepository = $quoteRepository;
        $this->_checkoutSession = $checkoutSession;
        $this->_resourceConnection = $resourceConnection;
        $this->_scopeConfig = $scopeConfig;
        $this->logger = $logger;

        $this->debugMessage = true;
    }

    public function execute(EventObserver $observer)
    {
        if($this->debugMessage)
            $this->logger->info('SaveDataToOrderObserver - START');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $order = $observer->getOrder();
        $billingAddress = $order->getBillingAddress();

//        $resourceConnection = $objectManager->create('Magento\Framework\App\ResourceConnection');
//        $quoteRepository = $objectManager->create('\Magento\Quote\Api\CartRepositoryInterface');
        $addressRepository = $objectManager->create('\Magento\Customer\Api\AddressRepositoryInterface');
        $customerFactory = $objectManager->create('\Magento\Customer\Model\CustomerFactory');

        if ($order->getCustomerId()){

            if($this->debugMessage)
                $this->logger->info('SaveDataToOrderObserver - Customer registered (not guest)');

            try {

                //$this->updateCustomerAddress($order->getCustomerId(), null, null);
                $addresses = $customerFactory->create()->load($order->getCustomerId())->getAddresses();

                if($this->debugMessage)
                    $this->logger->info('SaveDataToOrderObserver - Num. of addresses: ' . count($addresses));

                foreach ($addresses as $address){

                    $addressEntity = $addressRepository->getById($address->getId());
                    if ($addressEntity->isDefaultBilling() == true){

                        if($this->debugMessage)
                            $this->logger->info('SaveDataToOrderObserver - Match default billing address with ID: ' . $addressEntity->getId());

                        //change
                        $billingAddress->setPrefix($addressEntity->getPrefix());
                        $billingAddress->setFirstname($addressEntity->getFirstname());
                        $billingAddress->setLastname($addressEntity->getLastname());
                        $billingAddress->setStreet($addressEntity->getStreet());
                        $billingAddress->setRegion($addressEntity->getRegion()->getRegion());
                        $billingAddress->setRegionId($addressEntity->getRegionId());
                        $billingAddress->setRegionCode($addressEntity->getRegion()->getRegionCode());
                        $billingAddress->setCity($addressEntity->getCity());
                        $billingAddress->setCountryId($addressEntity->getCountryId());
                        $billingAddress->setPostcode($addressEntity->getPostcode());
                        $billingAddress->setTelephone($addressEntity->getTelephone());
                        $billingAddress->setFax($addressEntity->getFax());
                        $billingAddress->setVatId($addressEntity->getVatId());
                        $billingAddress->setCompany($addressEntity->getCompany());

                        if($this->debugMessage)
                            $this->logger->info('SaveDataToOrderObserver - Billing address updated');

                        break;
                    }
                }

            } catch (\Exception $e) {

                if($this->debugMessage)
                    $this->logger->info('SaveDataToOrderObserver - EXCEPTION: ' . $e->getMessage());
            }

        }//if ($order->getCustomerId())

//        //setto gli address dell'ordine
//        $isBusinessAddress = null;
//        $shippingAddress->setTaxCode($this->_checkoutSession->getTaxCode());
//        $shippingAddress->setVatId($this->_checkoutSession->getVatId2());
//        $shippingAddress->setCompany($this->_checkoutSession->getCompany2());
//        $billingAddress->setTaxCode($this->_checkoutSession->getTaxCode());
//        $billingAddress->setVatId($this->_checkoutSession->getVatId2());
//        $billingAddress->setCompany($this->_checkoutSession->getCompany2());
//        if($this->_checkoutSession->getTaxCode() !== null || $this->_checkoutSession->getVatId2() !== null) {
//            $shippingAddress->setIsBusinessAddress(1);
//            $billingAddress->setIsBusinessAddress(1);
//            $isBusinessAddress = 1;
//        }
//
//        //verifico se di default devo mettere => $isBusinessAddress = 1
//        if ($isBusinessAddress == null){
//            $storeAbilitate = $this->_scopeConfig->getValue('core_customizer/storeViewAbilitateIsBusAddr/storeview_abilitate_is_bus_addr');
//            if($storeAbilitate != ''){
//                $storeAbilitateIds = explode(',', $storeAbilitate);
//                foreach ($storeAbilitateIds as $storeId){
//                    if ($storeId == $order->getStore()->getId()){
//                        $shippingAddress->setIsBusinessAddress(1);
//                        $billingAddress->setIsBusinessAddress(1);
//                        $isBusinessAddress = 1;
//                        break;
//                    }
//                }
//            }
//        }
//
//        //aggiorno address del quote
//        try {
//            $this->updateQuoteAddress($isBusinessAddress, $quote);
//        } catch (\Exception $e) {}
//
//        //annullo parametri nella checkout session
//        $this->_checkoutSession->setTaxCode(null);
//        $this->_checkoutSession->setCompany2(null);
//        $this->_checkoutSession->setVatId2(null);

        if($this->debugMessage)
            $this->logger->info('SaveDataToOrderObserver - END');

        return $this;
    }

    private function updateCustomerAddress($customerId, $vatId, $company){

        $connection = $this->_resourceConnection->getConnection();
        $query = "UPDATE `customer_address_entity` SET `vat_id`= :vatId, `company`= :company WHERE `parent_id`= :customerId";

        $binds = array(
            'company' => $company,
            'vatId'  => $vatId,
            'customerId' => $customerId
        );
        $connection->query($query, $binds);
    }

    private function updateQuoteAddress($isBusinessAddress, $quote){
        $connection = $this->_resourceConnection->getConnection();
        $query = "UPDATE `quote_address` SET `is_business_address`= :isBusinessAddress, `tax_code`= :taxCode, `company`= :company, `vat_id`= :vatId  WHERE `quote_id`= :quoteId";
        $binds = array(
            'isBusinessAddress' => $isBusinessAddress,
            'taxCode' => $this->_checkoutSession->getTaxCode(),
            'company' => $this->_checkoutSession->getCompany2(),
            'vatId'  => $this->_checkoutSession->getVatId2(),
            'quoteId' => $quote->getId()
        );
        $connection->query($query, $binds);
    }

}
