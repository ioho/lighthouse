<?php



namespace Appseconnect\AddressExtension\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;
    private $_attributeRepository;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    public function __construct(EavSetupFactory $eavSetupFactory,
                                CustomerSetupFactory $customerSetupFactory,
                                AttributeSetFactory $attributeSetFactory,
                                \Magento\Eav\Model\AttributeRepository $attributeRepository)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->_attributeRepository = $attributeRepository;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $customerSetupFac = $this->customerSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();
        $customerEntity = $customerSetupFac->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        //attributo customer address: address_type aggiunto alla tabella eav_attribute
        /*
         * B => billing, S => shipping
         */
        $customerSetupFac->addAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY, 'address_type',  [
            'label' => 'Address Type',
            'type' => 'varchar',
            'input' => 'text',
            'visible' => true,
            'required' => false,
            'source' => '',
            'system' => 0,
            'user_defined' => 1,
            'group'=>'General',
            'visible_on_front' => false,
            'is_used_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_filterable_in_grid' => false,
            'is_searchable_in_grid' => false,
            'backend' => ''
        ]);
        //setto visibilita
        $address_type = $customerSetupFac->getEavConfig()->getAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY, 'address_type');
        $address_type
            ->addData(['used_in_forms' => [
                'adminhtml_customer_address',
                'customer_address_edit',
                'customer_register_address'
            ]]);
        $address_type->save();

        $setup->endSetup();
    }
}
