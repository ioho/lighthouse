<?php
namespace Appseconnect\B2BMage\Setup;

use Magento\Eav\Model\Entity\Setup\Context;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class QuotationSetup extends \Magento\Eav\Setup\EavSetup
{
    /**
     * @var ScopeConfigInterface
     */
    public $config;

    /**
     * @var EncryptorInterface
     */
    public $encryptor;

    /**
     * @var string
     */
    private static $connectionName = 'sales';

    /**
     * @param ModuleDataSetupInterface $setup
     * @param Context $context
     * @param CacheInterface $cache
     * @param CollectionFactory $attrGroupCollectionFactory
     * @param ScopeConfigInterface $config
     */
    public function __construct(
        ModuleDataSetupInterface $setup,
        Context $context,
        CacheInterface $cache,
        CollectionFactory $attrGroupCollectionFactory,
        ScopeConfigInterface $config
    ) {
        $this->config = $config;
        $this->encryptor = $context->getEncryptor();
        parent::__construct($setup, $context, $cache, $attrGroupCollectionFactory);
    }

    /**
     * @return array
     */
    public function getDefaultEntities()
    {
        $entities = [
            'quotation' => [
                'entity_type_id' => 9,
                'entity_model' => 'Appseconnect\B2BMage\Model\Quotation\ResourceModel\Quote',
                'table' => 'insync_customer_quote',
                'increment_model' => 'Magento\Eav\Model\Entity\Increment\NumericValue',
                'increment_per_store' => true,
                'attributes' => [],
            ]
        ];
        return $entities;
    }

    /**
     * Get config model
     *
     * @return ScopeConfigInterface
     */
    public function getConfigModel()
    {
        return $this->config;
    }

    /**
     * @return EncryptorInterface
     */
    public function getEncryptor()
    {
        return $this->encryptor;
    }
}
