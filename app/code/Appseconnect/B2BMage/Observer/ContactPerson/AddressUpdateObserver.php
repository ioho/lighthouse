<?php
namespace Appseconnect\B2BMage\Observer\ContactPerson;

use Magento\Framework\Event\ObserverInterface;

class AddressUpdateObserver implements ObserverInterface
{

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public $date;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
    
        $this->customerFactory = $customerFactory;
        $this->date = $date;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return \Appseconnect\B2BMage\Observer\ContactPerson\AddressUpdateObserver|void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $address = $observer->getCustomerAddress();
        if (! $address->hasDataChanges()) {
            return $this;
        }
        $customerData = $this->customerFactory->create()->load($address->getCustomerId());
        $customerData->setUpdatedAt($this->date->gmtDate());
        $customerData->save();
    }
}
