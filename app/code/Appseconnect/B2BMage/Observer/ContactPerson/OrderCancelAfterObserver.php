<?php
namespace Appseconnect\B2BMage\Observer\ContactPerson;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class OrderCancelAfterObserver implements ObserverInterface
{

    /**
     * @var \Appseconnect\B2BMage\Helper\CreditLimit\Data
     */
    public $helperCreditLimit;

    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Helper\CreditLimit\Data $helperCreditLimit
     */
    public function __construct(
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Helper\CreditLimit\Data $helperCreditLimit
    ) {

        $this->helperContactPerson = $helperContactPerson;
        $this->helperCreditLimit = $helperCreditLimit;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderData = $observer->getEvent()->getData('order');
        $paymentMethod = $orderData
            ->getPayment()
            ->getMethodInstance()
            ->getCode();

        $userId = $orderData->getCustomerId();
        $check = $this->helperCreditLimit->isValidPayment($paymentMethod);
        if ($userId && $check) {
            $grandTotal = $orderData->getGrandTotal();
            $this->helperCreditLimit->creditLimitUpdate(
                $userId,
                $orderData,
                $grandTotal
            );
        }
    }
}
