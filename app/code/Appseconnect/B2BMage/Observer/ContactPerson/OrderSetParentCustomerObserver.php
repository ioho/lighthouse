<?php
namespace Appseconnect\B2BMage\Observer\ContactPerson;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;

class OrderSetParentCustomerObserver implements ObserverInterface
{
    /**
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     *
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Appseconnect\B2BMage\Helper\Quotation\Data
     */
    protected $quotationHelper;

    /**
     * OrderSetParentCustomerObserver constructor.
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Helper\Quotation\Data $quotationHelper
     * @param \Appseconnect\B2BMage\Model\QuoteFactory $quoteFactory
     */
    public function __construct(
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Helper\Quotation\Data $quotationHelper,
        \Appseconnect\B2BMage\Model\QuoteFactory $quoteFactory
    ) {

        $this->helperContactPerson = $helperContactPerson;
        $this->customerFactory = $customerFactory;
        $this->quotationHelper = $quotationHelper;
        $this->quoteFactory = $quoteFactory;
    }

    /**
     * Overriding the original customer details with its parent customer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerId = null;
        $parentCustomer = null;
        $quote = $observer->getQuote();
        $order = $observer->getOrder();

        $order->setQuotationInfo($quote->getQuotationInfo());
        if($quote->getQuotationInfo()){
            $quoteDetail=$this->quotationHelper->getQuotationInfo($quote->getQuotationInfo());
            $quote=$this->quoteFactory->create()->load($quoteDetail["id"]);
            $quote->setStatus("closed");
            $quote->save();
        }
        if ($quote->getCustomer()) {
            $customerId = $quote->getCustomer()->getId();
            $customer = $this->customerFactory->create()->load($customerId);
            if ($this->helperContactPerson->isContactPerson($customer)) {
                $parentCustomerMapData = $this->helperContactPerson->getCustomerId($customerId);
                $customerId = $parentCustomerMapData ? $parentCustomerMapData['customer_id'] : $customerId; //B2B
                $parentCustomer = $this->customerFactory->create()->load($customerId);
                $order->setCustomerId($customerId);
            }
        }
        if ($parentCustomer) {
            $order->getShippingAddress()->setEmail($parentCustomer->getEmail());
            $order->getBillingAddress()->setEmail($parentCustomer->getEmail());

            $order->setCustomerEmail($parentCustomer->getEmail());
            $order->setCustomerFirstname($parentCustomer->getFirstname());
            $order->setCustomerMiddlename($parentCustomer->getMiddlename());
            $order->setCustomerLastname($parentCustomer->getLastname());
        }


        $quoteItems = [];
        // Map Quote Item with Quote Item Id
        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            $quoteItems[$quoteItem->getId()] = $quoteItem;
        }

        foreach ($order->getAllVisibleItems() as $orderItem) {
            $quoteItemId = $orderItem->getQuoteItemId();
         //   $quoteItem = $quoteItems[$quoteItemId];
//            $additionalOptions = $quoteItem->getOptionByCode('additional_options');
//            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testrepo.log');
//            $logger = new \Zend\Log\Logger();
//            $logger->addWriter($writer);
//            $val=json_encode($orderItem->getData());
//            $logger->info("$val");

//            if (count($additionalOptions) > 0) {
                // Get Order Item's other options
//                $options = $orderItem->getProductOptions();
                // Set additional options to Order Item
//                $options['additional_options'] = unserialize($additionalOptions->getValue());
                //$orderItem->setProductOptions($options);
//            }
        }
    }
}
