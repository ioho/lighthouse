<?php
namespace Appseconnect\B2BMage\Observer\ContactPerson;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class InvoiceSaveAfterObserver implements ObserverInterface
{
    protected $invoice;
    /**
     * @var \Appseconnect\B2BMage\Helper\CreditLimit\Data
     */
    public $helperCreditLimit;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Helper\CreditLimit\Data $helperCreditLimit
     */
    public function __construct(
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Helper\CreditLimit\Data $helperCreditLimit
    ) {
            
            $this->helperContactPerson = $helperContactPerson;
            $this->helperCreditLimit = $helperCreditLimit;
    }
    
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $invoiceData = $observer->getEvent()->getData('invoice');
        $paymentMethod = $invoiceData->getOrder()
        ->getPayment()
        ->getMethodInstance()
        ->getCode();

        $customerDuscountPercent = $invoiceData->getOrder()->getCustomerDiscount();
        if($customerDuscountPercent) {
            $discountAmount = $invoiceData->getSubtotal() * ( $customerDuscountPercent / 100 );
            $invoiceData->setCustomerDiscountAmount($discountAmount);
            $invoiceData->save();
        }
        
        $userId = $invoiceData->getOrder()->getCustomerId();
        $check = $this->helperCreditLimit->isValidPayment($paymentMethod);
        if (!$this->invoice && $userId && $check) {
            $this->invoice=$invoiceData->getId();
            $grandTotal = $invoiceData->getGrandTotal();
            $this->helperCreditLimit->creditLimitUpdate(
                $userId,
                $invoiceData->getOrder(),
                $grandTotal
            );
        }
    }
}
