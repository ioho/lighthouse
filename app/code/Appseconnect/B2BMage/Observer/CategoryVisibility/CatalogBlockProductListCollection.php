<?php
namespace Appseconnect\B2BMage\Observer\CategoryVisibility;

use Magento\Framework\Event\Observer;
use Appseconnect\B2BMage\Model\ResourceModel\ContactFactory;
use Magento\Framework\App\Request\Http;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;

class CatalogBlockProductListCollection implements ObserverInterface
{

    /**
     *
     * @var Session
     */
    public $session;
    
    /**
     *
     * @var ContactFactory
     */
    public $contactResourceFactory;
    
    /**
     *
     * @var \Magento\Eav\Model\Entity\Attribute
     */
    public $eavAttribute;
    
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;
    
    /**
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     * @param Session $session
     * @param ContactFactory $contactResourceFactory
     * @param \Magento\Eav\Model\Entity\Attribute $eavAttribute
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     */
    public function __construct(
        Session $session,
        ContactFactory $contactResourceFactory,
        \Magento\Eav\Model\Entity\Attribute $eavAttribute,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
    ) {
        $this->contactResourceFactory = $contactResourceFactory;
        $this->eavAttribute = $eavAttribute;
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $session;
        $this->helperContactPerson = $helperContactPerson;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $collection = $observer->getEvent()->getCollection();
        $contactResourceModel = $this->contactResourceFactory->create();
        $customerId = $this->customerSession->getCustomer()->getId();
        $categoryVisibility = $this->scopeConfig
        ->getValue('insync_category_visibility/select_visibility/select_visibility_type', 'store');
        $productVisibility = $this->scopeConfig
        ->getValue('insync_category_visibility/select_product_visibility/active', 'store');
        
        if ($customerId && $categoryVisibility == 'group_wise_visibility') {
            $customerData = $this->helperContactPerson->getCustomerData($customerId);
            
            $customerType = $customerData["customer_type"];
            $groupId = $customerData["group_id"];
            if ($customerType == 3) {
                $attributeId = $this->eavAttribute->getIdByCode('catalog_category', 'customer_group');
                $collection = $contactResourceModel->getProductList($collection, $groupId, $attributeId);
            }
        } elseif (! $customerId && ! $productVisibility) {
            $collection->addAttributeToFilter('entity_id', 0);
        }
    }
}
