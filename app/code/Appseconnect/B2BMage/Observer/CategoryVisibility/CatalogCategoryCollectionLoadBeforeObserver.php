<?php
namespace Appseconnect\B2BMage\Observer\CategoryVisibility;

use Magento\Framework\Event\Observer;
use Magento\Framework\App\Request\Http;
use Appseconnect\B2BMage\Model\ResourceModel\ContactFactory;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;

class CatalogCategoryCollectionLoadBeforeObserver implements ObserverInterface
{

    /**
     *
     * @var Session
     */
    public $session;
    
    /**
     *
     * @var ContactFactory
     */
    public $contactResourceFactory;
    
    /**
     *
     * @var \Magento\Eav\Model\Entity\Attribute
     */
    public $eavAttribute;
    
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;
    
    /**
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     * @param Session $session
     * @param ContactFactory $contactResourceFactory
     * @param \Magento\Eav\Model\Entity\Attribute $eavAttribute
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     */
    public function __construct(
        Session $session,
        ContactFactory $contactResourceFactory,
        \Magento\Eav\Model\Entity\Attribute $eavAttribute,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
    ) {
        $this->contactResourceFactory = $contactResourceFactory;
        $this->eavAttribute = $eavAttribute;
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $session;
        $this->helperContactPerson = $helperContactPerson;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $categoryVisibility = $this->scopeConfig
        ->getValue('insync_category_visibility/select_visibility/select_visibility_type', 'store');
        $contactResourceModel = $this->contactResourceFactory->create();
        $groupId=0;
        $customerId = $this->customerSession->getCustomer()->getId();
        if ($categoryVisibility == 'group_wise_visibility') {
            if($customerId){
                $customerData = $this->helperContactPerson->getCustomerData($customerId);
                $groupId = $customerData["group_id"];
            }
                $attributeId = $this->eavAttribute->getIdByCode('catalog_category', 'customer_group');
                $collection = $observer->getEvent()->getCategoryCollection();
                $entity = $collection->getNewEmptyItem();
                $fileType = constant(get_class($entity) . '::ENTITY');
                if ($fileType == 'catalog_category') {
                    $contactResourceModel->getCateogoryList($collection, $groupId, $attributeId);
                }
        }
    }
}
