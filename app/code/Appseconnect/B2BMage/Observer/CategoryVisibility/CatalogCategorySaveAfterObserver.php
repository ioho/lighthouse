<?php
namespace Appseconnect\B2BMage\Observer\CategoryVisibility;

use Magento\Framework\Event\Observer;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;

class CatalogCategorySaveAfterObserver implements ObserverInterface
{

    /**
     *
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    public $categoryFactory;

    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->categoryFactory = $categoryFactory;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $categoryVisibility = $this->scopeConfig
        ->getValue('insync_category_visibility/select_visibility/select_visibility_type', 'store');
        if ($categoryVisibility == 'group_wise_visibility') {
            $category = $observer->getEvent()->getCategory();
            
            $customerGroup = $category->getData('customer_group');
            $childrenCount = $category->getData('children_count');
            if ($childrenCount > 0) {
                $categoryCollection = $this->categoryFactory->create()->load($category->getData('entity_id'));
                $childCategoryId = $categoryCollection->getResource()->getChildren($categoryCollection, true);
                $customerGroup = (! empty($customerGroup)) ? implode(',', $customerGroup) : "";
                if (! empty($childCategoryId)) {
                    foreach ($childCategoryId as $categoryId) {
                        $categoryModel = $this->loadCategory($categoryId);
                        $categoryModel->setData('customer_group', '' . $customerGroup . '');
                        $this->categoryFactory->create()
                            ->getResource()
                            ->saveAttribute($categoryModel, 'customer_group');
                    }
                }
            }
        }
    }
    
    /**
     * @param int $categoryId
     * @return \Magento\Catalog\Model\Category
     */
    private function loadCategory($categoryId)
    {
        $model = $this->categoryFactory->create()->load($categoryId);
        return $model;
    }
}
