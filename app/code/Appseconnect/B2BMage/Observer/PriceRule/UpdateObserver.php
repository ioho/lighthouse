<?php
namespace Appseconnect\B2BMage\Observer\PriceRule;

use Magento\Framework\Event\ObserverInterface;
use Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory;
use Magento\Customer\Model\Session;
use Magento\Catalog\Model\Product\Type;

class UpdateObserver implements ObserverInterface
{
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @var CollectionFactory
     */
    public $pricelistPriceCollectionFactory;
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var Magento\Catalog\Model\ProductFactory
     */
    public $productFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CategoryDiscount\Data
     */
    public $helperCategory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data
     */
    public $helperCustomerSpecialPrice;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\Pricelist\Data
     */
    public $helperPricelist;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data
     */
    public $helperTierprice;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\PriceRule\Data
     */
    public $helperPriceRule;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManagerInterface;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrencyInterface;

    /**
     * @var \Appseconnect\LightechDiscounts\Helper\Data
     */
    protected $lighTechDiscountHelper;

    /**
     * UpdateObserver constructor.
     * @param Session $session
     * @param CollectionFactory $pricelistPriceCollectionFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Helper\CategoryDiscount\Data $helperCategory
     * @param \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data $helperTierprice
     * @param \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist
     * @param \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data $helperCustomerSpecialPrice
     * @param \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrencyInterface
     * @param \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
     * @param \Appseconnect\LightechDiscounts\Helper\Data $lighTechDiscountHelper
     */
    public function __construct(
        Session $session,
        CollectionFactory $pricelistPriceCollectionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Helper\CategoryDiscount\Data $helperCategory,
        \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data $helperTierprice,
        \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist,
        \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data $helperCustomerSpecialPrice,
        \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrencyInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Appseconnect\LightechDiscounts\Helper\Data $lighTechDiscountHelper
    ) {
        $this->storeManagerInterface = $storeManagerInterface;
        $this->priceCurrencyInterface = $priceCurrencyInterface;
        $this->customerFactory = $customerFactory;
        $this->pricelistPriceCollectionFactory = $pricelistPriceCollectionFactory;
        $this->customerSession = $session;
        $this->helperContactPerson = $helperContactPerson;
        $this->productFactory = $productFactory;
        $this->helperCategory = $helperCategory;
        $this->helperCustomerSpecialPrice = $helperCustomerSpecialPrice;
        $this->helperTierprice = $helperTierprice;
        $this->helperPricelist = $helperPricelist;
        $this->helperPriceRule = $helperPriceRule;
        $this->lighTechDiscountHelper = $lighTechDiscountHelper;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void @codeCoverageIgnore
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testrepo.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info("Update");
        $customerId = $this->customerSession->getCustomer()->getId();
        $customerType = $this->customerSession->getCustomer()->getCustomerType();
        $websiteId = $this->customerSession->getCustomer()->getWebsiteId();
        $qtyItem = 1;
        
        $customerPricelistCode = $this->customerSession->getCustomer()->getData('pricelist_code');
        
        if ($customerType == 3) {
            $customerDetail = $this->helperContactPerson->getCustomerId($customerId);
            $customerCollection = $this->customerFactory->create()->load($customerDetail['customer_id']);
            $customerPricelistCode = $customerCollection->getData('pricelist_code');
            $customerId = $customerDetail['customer_id'];
        }
        if ($customerType == 2) {
            return $this;
        }
        $pricelistStatus = $this->getPricelistStatus(
            $customerPricelistCode,
            $websiteId
        );
        
        $item = $observer->getEvent()->getCart();
        if ($customerId) {
            foreach ($item->getQuote()->getAllItems() as $product) {
                $productId = $product->getProductId();
                $productTypeId = $this->getTypeId($productId);
                if ($productTypeId != Type::TYPE_BUNDLE) {
                    $qtyItem = $product->getQty();
                    $productDetail = $this->loadProduct($productId);
                    $finalPrice = $productDetail->getFinalPrice($qtyItem);
                    
                    if ($productTypeId == 'configurable') {
                        $productId = $this->getConfigProductIdBySku($product->getSku());
                        $configChildData = $this->loadConfigProduct($productId);
                        $finalPrice = $configChildData->getPrice();
                    }
                    $pricelistPrice = '';
                    if ($customerPricelistCode && $pricelistStatus) {
                        $pricelistPrice = $this->helperPricelist->getAmount(
                            $productId,
                            $finalPrice,
                            $customerPricelistCode,
                            true
                        );
                    }
                    $categoryIds = $productDetail->getCategoryIds();
                    $categoryDiscountedPrice = $this->helperCategory->getCategoryDiscountAmount(
                        $finalPrice,
                        $customerId,
                        $categoryIds
                    );
                                                                                                                                                     // for tier price
                    $tierPrice = '';
                    $productSku = $product->getSku();
                    $tierPrice = $this->helperTierprice->getTierprice(
                        $productId,
                        $productSku,
                        $customerId,
                        $websiteId,
                        $qtyItem,
                        $finalPrice
                    );
                    
                    $specialPrice = '';
                    
                    $specialPrice = $this->helperCustomerSpecialPrice->getSpecialPrice(
                        $productId,
                        $productSku,
                        $customerId,
                        $websiteId,
                        $finalPrice
                    );
                    $lightechDiscountPrice = '';
                    $lightechDiscountPrice = $this->lighTechDiscountHelper->getScontoData($productId,$productSku,1,$finalPrice);

                    if ($pricelistPrice) {
                        $finalPrice = $pricelistPrice;
                    }
                    $actualPrice = $this->helperPriceRule->getActualPrice(
                        $finalPrice,
                        $tierPrice,
                        $categoryDiscountedPrice,
                        $pricelistPrice,
                        $specialPrice,
                        $lightechDiscountPrice
                    );
                    
                    $customQuotationPrice=$product->getCustomQuotationPrice();
                    if ($customQuotationPrice) {
                        $actualPrice=$customQuotationPrice;
                    }
                    $this->setCustomPrices($product, $actualPrice);
                }
            }
            return $this;
        }
    }

    /**
     * @param float $amount
     * @param null|int $store
     * @param null|string $currency
     * @return float
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function convertPrice($amount = 0, $store = null, $currency = null)
    {
        if ($store == null) {
            $store = $this->storeManagerInterface->getStore()->getStoreId();
        }
        $rate = $this->priceCurrencyInterface->convert($amount, $store, $currency);
        return $rate;

    }

    /**
     * @param mixed $customerPricelistCode
     * @param int $websiteId
     * @return NULL|int
     */
    private function getPricelistStatus($customerPricelistCode, $websiteId)
    {
        $pricelistStatus = null;
        $pricelistCollection = $this->pricelistPriceCollectionFactory->create()
            ->addFieldToFilter('id', $customerPricelistCode)
            ->addFieldToFilter('website_id', $websiteId)
            ->getData();
        if (isset($pricelistCollection[0])) {
            $pricelistStatus = $pricelistCollection[0]['is_active'];
        }
        return $pricelistStatus;
    }
    
    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param float $actualPrice
     */
    private function setCustomPrices($product, $actualPrice)
    {
        $actualPrice=$this->convertPrice($actualPrice);
        $product->setCustomPrice($actualPrice);
        $product->setOriginalCustomPrice($actualPrice);
        $product->getProduct()->setIsSuperMode(true);
    }

    /**
     * @param int $id
     * @return string
     */
    private function getTypeId($id)
    {
        $typeId = null;
        $typeId = $this->productFactory->create()
            ->load($id)
            ->getTypeId();
        return $typeId;
    }

    /**
     * @param int $productId
     * @return \Magento\Catalog\Model\Product
     */
    private function loadProduct($productId)
    {
        return $this->productFactory->create()->load($productId);
    }

    /**
     * @param mixed $sku
     * @return int
     */
    private function getConfigProductIdBySku($sku)
    {
        $id = null;
        $id = $this->productFactory->create()->getIdBySku($sku);
        return $id;
    }

    /**
     * @param INT $productId
     * @return \Magento\Catalog\Model\Product
     */
    private function loadConfigProduct($productId)
    {
        return $this->productFactory->create()->load($productId);
    }
}
