<?php
namespace Appseconnect\B2BMage\Observer\PriceRule;

use Magento\Framework\Event\Observer;
use Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;

class AfterAddObserver implements ObserverInterface
{
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @var CollectionFactory
     */
    public $pricelistPriceCollectionFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\PriceRule\Data
     */
    public $helperPriceRule;
    
    /**
     * @param Session $session
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param CollectionFactory $pricelistPriceCollectionFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule
     */
    public function __construct(
        Session $session,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        CollectionFactory $pricelistPriceCollectionFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule
    ) {
    
        $this->customerSession = $session;
        $this->customerFactory = $customerFactory;
        $this->pricelistPriceCollectionFactory = $pricelistPriceCollectionFactory;
        $this->helperContactPerson = $helperContactPerson;
        $this->helperPriceRule = $helperPriceRule;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void @codeCoverageIgnore
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerId = $this->customerSession->getCustomer()->getId();
        $customerType = $this->customerSession->getCustomer()->getCustomerType();
        $websiteId = $this->customerSession->getCustomer()->getWebsiteId();
        $pricelistPrice = '';
        $customerPricelistCode = $this->customerSession->getCustomer()->getData('pricelist_code');
        if ($customerType == 3) {
            $customerDetail = $this->helperContactPerson->getCustomerId($customerId);
            $customerCollection = $this->customerFactory->create()->load($customerDetail['customer_id']);
            $customerPricelistCode = $customerCollection->getData('pricelist_code');
            $customerId = $customerDetail['customer_id'];
        }
        if ($customerType == 2) {
            return $this;
        }
        
        $pricelistStatus = null;
        $pricelistCollection = $this->pricelistPriceCollectionFactory->create()
        ->addFieldToFilter('id', $customerPricelistCode)
        ->addFieldToFilter('website_id', $websiteId)
        ->getData();
        if (isset($pricelistCollection[0])) {
            $pricelistStatus = $pricelistCollection[0]['is_active'];
        }
        if ($customerId) {
            $item = $observer->getEvent()->getData('quote_item');
            
            if ($item->getProduct()->getTypeId() == Type::TYPE_BUNDLE) {
                $this->helperPriceRule->processBundleProduct(
                    $item,
                    $customerPricelistCode,
                    $pricelistStatus,
                    $customerId,
                    $websiteId
                );
            } elseif ($item->getProduct()->getTypeId() == 'configurable') {
                $this->helperPriceRule->processConfigurableProduct(
                    $item,
                    $customerPricelistCode,
                    $pricelistStatus,
                    $customerId,
                    $websiteId
                );
            } else {
                $this->helperPriceRule->processSimpleProduct(
                    $item,
                    $customerPricelistCode,
                    $pricelistStatus,
                    $customerId,
                    $websiteId
                );
            }

            return $this;
        }
    }
}
