<?php
namespace Appseconnect\B2BMage\Observer\PriceRule;

use Magento\Framework\Event\ObserverInterface;
use Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory;
use Magento\Customer\Model\Session;

class GridViewObserver implements ObserverInterface
{
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @var CollectionFactory
     */
    public $pricelistPriceCollectionFactory;
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var Magento\Catalog\Model\ProductFactory
     */
    public $productFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CategoryDiscount\Data
     */
    public $helperCategory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data
     */
    public $helperCustomerSpecialPrice;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\Pricelist\Data
     */
    public $helperPricelist;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data
     */
    public $helperTierprice;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\PriceRule\Data
     */
    public $helperPriceRule;

    /**
     * @var \Appseconnect\LightechDiscounts\Helper\Data
     */
    protected $lighTechDiscountHelper;

    /**
     * GridViewObserver constructor.
     * @param Session $session
     * @param CollectionFactory $pricelistPriceCollectionFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Helper\CategoryDiscount\Data $helperCategory
     * @param \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data $helperTierprice
     * @param \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist
     * @param \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data $helperCustomerSpecialPrice
     * @param \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Appseconnect\LightechDiscounts\Helper\Data $lighTechDiscountHelper
     */
    public function __construct(
        Session $session,
        CollectionFactory $pricelistPriceCollectionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Helper\CategoryDiscount\Data $helperCategory,
        \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data $helperTierprice,
        \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist,
        \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data $helperCustomerSpecialPrice,
        \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Appseconnect\LightechDiscounts\Helper\Data $lighTechDiscountHelper
    ) {
    
        $this->customerFactory = $customerFactory;
        $this->pricelistPriceCollectionFactory = $pricelistPriceCollectionFactory;
        $this->customerSession = $session;
        $this->productFactory = $productFactory;
        $this->helperCategory = $helperCategory;
        $this->helperCustomerSpecialPrice = $helperCustomerSpecialPrice;
        $this->helperPriceRule = $helperPriceRule;
        $this->helperTierprice = $helperTierprice;
        $this->helperPricelist = $helperPricelist;
        $this->helperContactPerson = $helperContactPerson;
        $this->lighTechDiscountHelper = $lighTechDiscountHelper;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void @codeCoverageIgnore
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testrepo.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $val=json_encode('grid');
        $logger->info("$val");
        $customerId = $this->customerSession->getCustomer()->getId();
        $customerType = $this->customerSession->getCustomer()->getCustomerType();
        $websiteId = $this->customerSession->getCustomer()->getWebsiteId();
        $qtyItem = 1;
        
        $customerPricelistCode = $this->customerSession->getCustomer()->getData('pricelist_code');
        
        if ($customerType == 3) {
            $customerDetail = $this->helperContactPerson->getCustomerId($customerId);
            $customerCollection = $this->customerFactory->create()->load($customerDetail['customer_id']);
            $customerPricelistCode = $customerCollection->getData('pricelist_code');
            $customerId = $customerDetail['customer_id'];
        }
        if ($customerType == 2) {
            return $this;
        }
        
        $pricelistStatus = null;
        $pricelistCollection = $this->pricelistPriceCollectionFactory->create()
            ->addFieldToFilter('id', $customerPricelistCode)
            ->addFieldToFilter('website_id', $websiteId)
            ->getData();
        if (isset($pricelistCollection[0])) {
            $pricelistStatus = $pricelistCollection[0]['is_active'];
        }
        
        $item = $observer->getEvent()->getCollection();
        if ($customerId) {
            foreach ($item as $product) {
                $productId = $product->getEntityId();
                $finalPrice = $product->getPrice($qtyItem);
//                $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
//                $finalPrice = $objectManager->create('Magento\Catalog\Model\Product')->load($product->getId())->getPrice();
                if ($product->getTypeId() == 'simple') {
                    $pricelistPrice = '';
                    if ($customerPricelistCode && $pricelistStatus) {
                        $pricelistPrice = $this->helperPricelist->getAmount(
                            $productId,
                            $finalPrice,
                            $customerPricelistCode,
                            true
                        );
                    }
                    $categoryIds = $this->getCategoryIdsByProductId($productId);
                    $categoryDiscountedPrice = $this->helperCategory->getCategoryDiscountAmount(
                        $finalPrice,
                        $customerId,
                        $categoryIds
                    );
                    
                    $productSku = $product->getSku();
                    $tierPrice = $this->helperTierprice->getTierprice(
                        $productId,
                        $productSku,
                        $customerId,
                        $websiteId,
                        $qtyItem,
                        $finalPrice
                    );
                    
                    $specialPrice = '';
                    
                    $specialPrice = $this->helperCustomerSpecialPrice->getSpecialPrice(
                        $productId,
                        $productSku,
                        $customerId,
                        $websiteId,
                        $finalPrice
                    );

                    $lightechDiscountPrice = '';
                    $lightechDiscountPrice = $this->lighTechDiscountHelper->getScontoData($productId,$productSku,$qtyItem,$finalPrice);
                    
                    $actualPrice = '';
                    
                    if ($pricelistPrice) {
                        $finalPrice = $pricelistPrice;
                    }
                    
                    $actualPrice = $this->helperPriceRule->getActualPrice(
                        $finalPrice,
                        $tierPrice,
                        $categoryDiscountedPrice,
                        $pricelistPrice,
                        $specialPrice,
                        $lightechDiscountPrice
                    );

                    $product->setPrice($actualPrice);
                    $product->setBaseCost($finalPrice);
//                        $logger->info($actualPrice);
                }
            }
            
            return $this;
        }
    }

    /**
     * @param int $productId
     * @return mixed
     */
    private function getCategoryIdsByProductId($productId)
    {
        $ids = null;
        $ids = $this->productFactory->create()
            ->load($productId)
            ->getCategoryIds();
        
        return $ids;
    }
}
