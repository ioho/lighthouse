<?php
namespace Appseconnect\B2BMage\Observer\Pricelist;

use Magento\Framework\Event\Observer;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Event\ObserverInterface;

class ProductUpdateObserver implements ObserverInterface
{
    
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    public $resources;

    /**
     * @param \Magento\Framework\App\ResourceConnection $resources
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resources
    ) {
        $this->resources = $resources;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @codeCoverageIgnore
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $productData = $observer->getEvent()->getData('product');
        $productId = $productData->getId();
        $productPrice = $productData->getPrice();
        $pricelistTable = $this->resources->getTableName('insync_product_pricelist_map');
        $connection = $this->resources->getConnection();
        
        $productData = [];
        $productData['original_price'] = $productPrice;
        $where = [];
        $where['product_id=?'] = $productId;
        $connection->update($pricelistTable, $productData, $where);
    }
}
