<?php
namespace Appseconnect\B2BMage\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Customer\Model\Session;

class AfterOrderObserver implements ObserverInterface
{
    
    /**
     * @var \Magento\Catalog\Model\Session
     */
    public $catalogSession;
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Appseconnect\B2BMage\Model\CreditFactory
     */
    public $creditFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\OrderApproverFactory
     */
    public $orderApproverFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CreditLimit\Data
     */
    public $helperCreditLimit;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\Sales\Data
     */
    public $helperSales;
    
    /**
     * @param Session $session
     * @param \Appseconnect\B2BMage\Model\OrderApproverFactory $orderApproverFactory
     * @param \Appseconnect\B2BMage\Model\CreditFactory $creditFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Helper\CreditLimit\Data $helperCreditLimit
     * @param \Appseconnect\B2BMage\Helper\Sales\Data $helperSales
     */
    public function __construct(
        Session $session,
        \Appseconnect\B2BMage\Model\OrderApproverFactory $orderApproverFactory,
        \Appseconnect\B2BMage\Model\CreditFactory $creditFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Helper\CreditLimit\Data $helperCreditLimit,
        \Appseconnect\B2BMage\Helper\Sales\Data $helperSales
    ) {
            
            $this->customerSession = $session;
            $this->creditFactory = $creditFactory;
            $this->orderApproverFactory = $orderApproverFactory;
            $this->helperContactPerson = $helperContactPerson;
            $this->helperCreditLimit = $helperCreditLimit;
            $this->helperSales = $helperSales;
    }
    
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void @codeCoverageIgnore
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $item = $observer->getEvent()->getData('order');
        $item->setSalesrepId($this->getCatalogSession()
            ->getSalesrepId());
        if ($this->getCatalogSession()->getSalesrepId()) {
            $item->setIsPlacedbySalesrep(1);
        }
        
        $isContactPerson = $this->helperContactPerson->checkCustomerStatus(
            $this->customerSession->getData('customer_id'),
            true
        );
        
        if ($this->customerSession->isLoggedIn() && $isContactPerson['customer_type'] == 3) {
            $item->setContactPersonId($this->customerSession->getCustomer()->getId());
            $contactPersonId = $this->customerSession->getData('customer_id');
            $contactPersonData = $this->helperContactPerson->getCustomerId($contactPersonId);
            $customerId = $contactPersonData['customer_id'];
            $incrementId = $item->getIncrementId();
            $grandTotal = $item->getGrandTotal();
            $paymentMathod = $item->getPayment()
            ->getMethodInstance()
            ->getCode();
            
            $check = $this->helperCreditLimit->isValidPayment($paymentMathod);
            if ($check) {
                $customerCreditDetail = $this->helperCreditLimit->getCustomerCreditData($customerId);
                $creditLimit = $customerCreditDetail['credit_limit'];
                $availableBalance = $customerCreditDetail['available_balance'];
                $availableBalance = $availableBalance - $grandTotal;
                $creditLimitDataArray = [];
                $creditLimitDataArray['customer_id'] = $customerId;
                $creditLimitDataArray['credit_limit'] = $creditLimit;
                $creditLimitDataArray['increment_id'] = $incrementId;
                $creditLimitDataArray['available_balance'] = $availableBalance;
                $creditLimitDataArray['debit_amount'] = $grandTotal;
                $creditModel = $this->creditFactory->create();
                $creditModel->setData($creditLimitDataArray);
                $creditModel->save();
                
                if ($creditLimit) {
                    $this->helperCreditLimit->saveCreditLimit($customerId, $creditLimit);
                }
                if ($availableBalance) {
                    $this->helperCreditLimit->saveCreditBalance($customerId, $availableBalance);
                }
            }
            
            $approverId = $this->helperSales->getApproverId($customerId, $grandTotal);
            
            $salesrepId = $this->getCatalogSession()->getSalesrepId();
            
            if ($approverId && ($approverId['contact_person_id'] != $contactPersonId || $salesrepId)) {
                $status = "holded";
                $item->setStatus($status);
                $orderapproverModel = $this->orderApproverFactory->create();
                $orderapproverModel->setData('increment_id', $incrementId);
                $orderapproverModel->setData('customer_id', $customerId);
                $orderapproverModel->setData('contact_person_id', $approverId['contact_person_id']);
                $orderapproverModel->setData('grand_total', $grandTotal);
                $orderapproverModel->save();
            }
        }
        
        return $this;
    }
    
    /**
     * @return \Magento\Catalog\Model\Session
     */
    private function getCatalogSession()
    {
        if (! $this->catalogSession) {
            $this->catalogSession = ObjectManager::getInstance()->get(\Magento\Catalog\Model\Session::class);
        }
        return $this->catalogSession;
    }
}
