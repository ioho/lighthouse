<?php

namespace Appseconnect\B2BMage\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class QuoteSaveAfterCustom implements ObserverInterface
{
    protected $checkoutSession;

    protected $_product;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ProductFactory $productFactory
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->productFactory = $productFactory;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
        foreach ($quote->getAllVisibleItems() as $item){
//            if(!empty($item->getBaseCost())){
//                $product = $this->productFactory->create();
                $item->setBaseCost($productRepository->get($item->getSku())->getPrice());
                $item->save();
//            }
    }
        /* @var $quote \Magento\Quote\Model\Quote */

        if ($quote->getIsCheckoutCart()) {
            $this->checkoutSession->getQuoteId($quote->getId());
        }
    }
}
