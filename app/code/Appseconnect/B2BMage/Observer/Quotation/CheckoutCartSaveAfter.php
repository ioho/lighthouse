<?php

namespace Appseconnect\B2BMage\Observer\Quotation;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;

class CheckoutCartSaveAfter implements ObserverInterface
{

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var \Appseconnect\B2BMage\Helper\Quotation\Data
     */
    protected $quotationHelper;

    /**
     * CheckoutCartSaveAfter constructor.
     * @param Session $session
     * @param \Appseconnect\B2BMage\Helper\Quotation\Data $quotationHelper
     */
    public function __construct(Session $session,
                                \Appseconnect\B2BMage\Helper\Quotation\Data $quotationHelper
    )
    {
        $this->customerSession = $session;
        $this->quotationHelper = $quotationHelper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void @codeCoverageIgnore
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $cart = $observer->getEvent()->getData('cart');
        $quote = $cart->getData('quote');
        $quoteDetail = $this->customerSession->getQuotationData();
        $count = $cart->getQuote()->getItemsCount();
        if (!$quote->getQuotationInfo() && $quoteDetail) {
            $quoteDetail = $this->quotationHelper->getQuotationInfo($quoteDetail, "json");
            $quote->setQuotationInfo($quoteDetail);
            $quote->save();
        }
        if ($count < 1) {
            $quote->setQuotationInfo(NULL);
            $quote->save();
        }
        $this->customerSession->unsQuotationData();
        return $this;
    }
}
