<?php
namespace Appseconnect\B2BMage\Observer\Quotation;

use Magento\Framework\Event\ObserverInterface;
use Appseconnect\B2BMage\Model\Quote\Email\Sender\QuoteSender;

class ActionMailObserver implements ObserverInterface
{
    
    /**
     * @var QuoteSender
     */
    public $quoteSender;
    
    /**
     * @var \Psr\Log\LoggerInterface
     */
    public $logger;

    /**
     * @param QuoteSender $quoteSender
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(QuoteSender $quoteSender, \Psr\Log\LoggerInterface $logger)
    {
        $this->quoteSender = $quoteSender;
        $this->logger = $logger;
    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $action = $observer->getAction();
        
        try {
            $this->quoteSender->send($quote, $action);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
    }
}
