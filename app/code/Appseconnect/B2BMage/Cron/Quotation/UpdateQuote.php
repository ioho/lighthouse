<?php

namespace Appseconnect\B2BMage\Cron\Quotation;

class UpdateQuote
{
    /**
     * @var \Appseconnect\B2BMage\Helper\Quotation\Data
     */
    protected $quotationHelper;

    /**
     * UpdateQuote constructor.
     * @param \Appseconnect\B2BMage\Helper\Quotation\Data $quotationHelper
     */
    public function __construct(\Appseconnect\B2BMage\Helper\Quotation\Data $quotationHelper
    )
    {
        $this->quotationHelper = $quotationHelper;
    }
    public function execute()
    {
        $this->quotationHelper->updateQuote();
        return $this;

    }
}
