<?php
namespace Appseconnect\B2BMage\Controller\Sales\Approve;

use Magento\Sales\Controller\OrderInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Order extends \Magento\Customer\Controller\AbstractAccount
{

    /**
     *
     * @var PageFactory
     */
    public $resultPageFactory;
    
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    public $orderFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\OrderApproverFactory
     */
    public $orderApproverFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CreditLimit\Data
     */
    public $helperCreditLimit;
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\Sales\Email
     */
    public $helperSalesEmail;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Helper\Sales\Email $helperSalesEmail
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Appseconnect\B2BMage\Model\OrderApproverFactory $orderApproverFactory
     * @param Session $customerSession
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Helper\CreditLimit\Data $helperCreditLimit
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Helper\Sales\Email $helperSalesEmail,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Appseconnect\B2BMage\Model\OrderApproverFactory $orderApproverFactory,
        Session $customerSession,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Helper\CreditLimit\Data $helperCreditLimit,
        PageFactory $resultPageFactory
    ) {
    
        $this->orderFactory = $orderFactory;
        $this->customerFactory = $customerFactory;
        $this->scopeConfig = $scopeConfig;
        $this->helperSalesEmail = $helperSalesEmail;
        $this->orderApproverFactory = $orderApproverFactory;
        $this->helperContactPerson = $helperContactPerson;
        $this->helperCreditLimit = $helperCreditLimit;
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    
    /**
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute()
    {
        $status = $this->getRequest()->getParam('status');
        if (! ($customerSessionId = $this->customerSession->getCustomerId()) ||
            ! isset($status)) {
            $this->messageManager->addError(__('Access Denied.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }
        $orderId = $this->getRequest()->getParam('order_id');
        $approveId = $this->getRequest()->getParam('approve_id');
        
        if (isset($status)) {
            $orderApproverModel = $this->orderApproverFactory->create()->load($approveId);
            $order = $this->orderFactory->create()->load($orderId);
            if ($status == 'cancel') {
                $orderApproverModel->setStatus('Canceled');
                $this->messageManager->addSuccess(__('Order has been canceled.'));
                $this->sendApprovalMail('cancel', $order);
                $order->setStatus('canceled');
                $userId = $order->getData('contact_person_id');
                
                $paymentMethod = $order->getPayment()
                    ->getMethodInstance()
                    ->getCode();
                
                $contactPersonData = $this->helperContactPerson->getCustomerId($userId);
                
                $chack = $this->helperCreditLimit->isValidPayment($paymentMethod);
                
                if (! empty($contactPersonData) && $chack) {
                    $customerId = $contactPersonData['customer_id'];
                    $customerCreditDetail = $this->helperCreditLimit->creditLimitUpdate(
                        $customerId,
                        $order,
                        $order->getData('grand_total')
                    );
                }
            }
            if ($status == 'approve') {
                $orderApproverModel->setStatus('Approved');
                $this->messageManager->addSuccess(__('Order has been approved.'));
                $this->sendApprovalMail('approved', $order);
                $order->setStatus('pending');
            }
            $order->save();
            
            $orderApproverModel->save();
        }
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/approve_listing');
        return $resultRedirect;
    }
    
    /**
     * @param string $action
     * @param \Magento\Sales\Model\Order $order
     * @return void
     */
    private function sendApprovalMail($action, $order)
    {
        $senderName = $this->scopeConfig->getValue('trans_email/ident_sales/name', 'store');
        $senderEmail = $this->scopeConfig->getValue('trans_email/ident_sales/email', 'store');
        
        $senderInfo = [
            'name' => $senderName,
            'email' => $senderEmail
        ];
        
        $customer = $this->customerFactory->create()->load($order->getData('customer_id'));
        
        $receiverInfo = [
            'name' => $customer->getName(),
            'email' => $customer->getEmail()
        ];
        $emailTemplateVariables = [];
        $emailTempVariables['customer'] = $customer;
        $emailTempVariables['order'] = $order;
        $emailTempVariables['increment_id'] = $order->getData('increment_id');
        $emailTempVariables['created_at'] = $order->getData('created_at');
        
        $this->helperSalesEmail->yourCustomMailSendMethod(
            $emailTempVariables,
            $senderInfo,
            $receiverInfo,
            $action
        );
    }
}
