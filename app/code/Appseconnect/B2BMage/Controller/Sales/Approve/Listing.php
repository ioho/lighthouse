<?php
namespace Appseconnect\B2BMage\Controller\Sales\Approve;

use Magento\Sales\Controller\OrderInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Listing extends \Magento\Customer\Controller\AbstractAccount
{

    /**
     *
     * @var PageFactory
     */
    public $resultPageFactory;
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $contactPersonHelper;
    
    /**
     * @param Context $context
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $contactPersonHelper
     * @param Session $customerSession
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $contactPersonHelper,
        Session $customerSession,
        PageFactory $resultPageFactory
    ) {
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->contactPersonHelper = $contactPersonHelper;
        parent::__construct($context);
    }
    
    /**
     * @return PageFactory
     */
    public function execute()
    {
        if (! ($customerSessionId = $this->customerSession->getCustomerId())) {
            $this->messageManager->addError(__('Access Denied.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }
        $customerId = $this->customerSession->getCustomer()->getId();
        
        $customerData = $this->contactPersonHelper->checkCustomerStatus($customerId, true);
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()
            ->getTitle()
            ->set(__('Order List For Approval'));
        $block = $resultPage->getLayout()->getBlock('salesorder.approve.listing');
        if ($block) {
            $block->setRefererUrl($this->_redirect->getRefererUrl());
        }
        return $resultPage;
    }
}
