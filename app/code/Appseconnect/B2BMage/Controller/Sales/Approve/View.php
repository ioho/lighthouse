<?php
namespace Appseconnect\B2BMage\Controller\Sales\Approve;

use Magento\Framework\App\Action;
use Magento\Framework\View\Result\PageFactory;

class View extends Action\Action
{

    /**
     *
     * @var \Magento\Sales\Controller\AbstractController\OrderLoaderInterface
     */
    public $orderLoader;

    /**
     *
     * @var PageFactory
     */
    public $resultPageFactory;

    /**
     *
     * @param Action\Context $context
     * @param OrderLoaderInterface $orderLoader
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
        \Magento\Sales\Controller\AbstractController\OrderLoaderInterface $orderLoader,
        PageFactory $resultPageFactory
    ) {
        $this->orderLoader = $orderLoader;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Order view page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->orderLoader->load($this->_request);
        if ($result instanceof \Magento\Framework\Controller\ResultInterface) {
            return $result;
        }
        
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        
        /** @var \Magento\Framework\View\Element\Html\Links $navigationBlock */
        $navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation');
        $status = $this->getRequest()->getParam('approver');
        if (isset($status) && $status == 'yes' && $navigationBlock) {
            $navigationBlock->setActive('b2bmage/sales/approve_listing');
        } elseif ($navigationBlock) {
            $navigationBlock->setActive('sales/order/history');
        }
        
        return $resultPage;
    }
}
