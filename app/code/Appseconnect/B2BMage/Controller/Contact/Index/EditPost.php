<?php
namespace Appseconnect\B2BMage\Controller\Contact\Index;

use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\State\UserLockedException;

/**
 * Class EditPost
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EditPost extends \Magento\Customer\Controller\AbstractAccount
{

    /**
     * Form code for data extractor
     */
    const FORM_DATA_EXTRACTOR_CODE = 'customer_account_edit';

    /**
     *
     * @var AccountManagementInterface
     */
    public $customerAccountManagement;

    /**
     *
     * @var CustomerRepositoryInterface
     */
    public $customerRepository;

    /**
     *
     * @var Validator
     */
    public $formKeyValidator;

    /**
     *
     * @var CustomerExtractor
     */
    public $customerExtractor;    

    /** @var EmailNotificationInterface */
    private $emailNotification;

    /**
     *
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     *
     * @var AuthenticationInterface
     */
    private $authentication;

    /**
     *
     * @param Context $context
     * @param AccountManagementInterface $customerAccountManagement
     * @param CustomerRepositoryInterface $customerRepository
     * @param Validator $formKeyValidator
     * @param CustomerExtractor $customerExtractor
     */
    public function __construct(
        Context $context,
        AccountManagementInterface $customerAccountManagement,
        CustomerRepositoryInterface $customerRepository,
        Validator $formKeyValidator,
        CustomerExtractor $customerExtractor
    ) {
    
        parent::__construct($context);
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerRepository = $customerRepository;
        $this->formKeyValidator = $formKeyValidator;
        $this->customerExtractor = $customerExtractor;
    }

    /**
     * Get authentication
     *
     * @return AuthenticationInterface
     */
    private function getAuthentication()
    {
        if (! ($this->authentication instanceof AuthenticationInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(\Magento\Customer\Model\AuthenticationInterface::class);
        } else {
            return $this->authentication;
        }
    }

    /**
     * Get email notification
     *
     * @return EmailNotificationInterface
     * @deprecated
     *
     */
    private function getEmailNotification()
    {
        if (! ($this->emailNotification instanceof EmailNotificationInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(EmailNotificationInterface::class);
        } else {
            return $this->emailNotification;
        }
    }

    /**
     * Change customer email or password action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $originalRequestData = $this->getRequest()->getPostValue();
            $customer = $this->customerRepository->getById($originalRequestData['contactperson_id']);
            $customer->setFirstname($originalRequestData['firstname']);
            $customer->setLastname($originalRequestData['lastname']);
            if (isset($originalRequestData['email']) && $originalRequestData['email']) {
                $customer->setEmail($originalRequestData['email']);
            }
            $customer->setCustomAttribute('contactperson_role', $originalRequestData['role']);
            $customer->setCustomAttribute('customer_status', $originalRequestData['status']);
            $this->customerRepository->save($customer);
            $this->messageManager->addSuccess(__('You saved the account information.'));
        } catch (\Exception $e) {
            $this->messageManager->addException($e, $e->getMessage());
        }
        return $resultRedirect->setPath('*/*/index_listing');
    }

    /**
     * Get scope config
     *
     * @return ScopeConfigInterface
     */
    private function getScopeConfig()
    {
        if (! ($this->scopeConfig instanceof \Magento\Framework\App\Config\ScopeConfigInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()
                        ->get(\Magento\Framework\App\Config\ScopeConfigInterface::class);
        } else {
            return $this->scopeConfig;
        }
    }

    /**
     * Account editing action completed successfully event
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customerCandidateDataObject
     * @return void
     */
    private function dispatchSuccessEvent(\Magento\Customer\Api\Data\CustomerInterface $customerCandidateDataObject)
    {
        $this->_eventManager->dispatch('customer_account_edited', [
            'email' => $customerCandidateDataObject->getEmail()
        ]);
    }

    /**
     * Get customer data object
     *
     * @param int $customerId
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    private function getCustomerDataObject($customerId)
    {
        return $this->customerRepository->getById($customerId);
    }

    /**
     * Create Data Transfer Object of customer candidate
     *
     * @param \Magento\Framework\App\RequestInterface $inputData
     * @param \Magento\Customer\Api\Data\CustomerInterface $currentCustomerData
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    private function populateNewCustomerDataObject(
        \Magento\Framework\App\RequestInterface $inputData,
        \Magento\Customer\Api\Data\CustomerInterface $currentCustomerData
    ) {
    
        $customerDto = $this->customerExtractor->extract(self::FORM_DATA_EXTRACTOR_CODE, $inputData);
        $customerDto->setId($currentCustomerData->getId());
        if (! $customerDto->getAddresses()) {
            $customerDto->setAddresses($currentCustomerData->getAddresses());
        }
        if (! $inputData->getParam('change_email')) {
            $customerDto->setEmail($currentCustomerData->getEmail());
        }
        
        return $customerDto;
    }
}
