<?php
namespace Appseconnect\B2BMage\Controller\Contact\Index;

use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Helper\Address;
use Magento\Framework\UrlFactory;
use Magento\Customer\Model\Metadata\FormFactory;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Customer\Model\Registration;
use Magento\Framework\Escaper;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\InputException;

/**
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CreatePost extends \Magento\Customer\Controller\AbstractAccount
{

    /** @var AccountManagementInterface */
    public $accountManagement;

    /** @var Address */
    public $addressHelper;

    /** @var FormFactory */
    public $formFactory;

    /** @var SubscriberFactory */
    public $subscriberFactory;

    /** @var RegionInterfaceFactory */
    public $regionDataFactory;

    /** @var AddressInterfaceFactory */
    public $addressDataFactory;

    /** @var Registration */
    public $registration;

    /** @var CustomerInterfaceFactory */
    public $customerDataFactory;

    /** @var CustomerUrl */
    public $customerUrl;

    /** @var Escaper */
    public $escaper;

    /** @var CustomerExtractor */
    public $customerExtractor;

    /** @var \Magento\Framework\UrlInterface */
    public $urlModel;

    /** @var DataObjectHelper  */
    public $dataObjectHelper;

    /**
     *
     * @var Session
     */
    public $session;

    /**
     *
     * @var AccountRedirect
     */
    private $accountRedirect;

    /**
     *
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     *
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private $cookieMetadataManager;
    
    /**
     * @var \Appseconnect\B2BMage\Model\ContactFactory
     */
    public $contactFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var ScopeConfigInterface
     */
    public $scopeConfig;
    
    /**
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * @param Context $context
     * @param \Appseconnect\B2BMage\Model\ContactFactory $contactFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param Session $customerSession
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param AccountManagementInterface $accountManagement
     * @param Address $addressHelper
     * @param UrlFactory $urlFactory
     * @param FormFactory $formFactory
     * @param SubscriberFactory $subscriberFactory
     * @param RegionInterfaceFactory $regionDataFactory
     * @param AddressInterfaceFactory $addressDataFactory
     * @param CustomerInterfaceFactory $customerDataFactory
     * @param CustomerUrl $customerUrl
     * @param Registration $registration
     * @param Escaper $escaper
     * @param CustomerExtractor $customerExtractor
     * @param DataObjectHelper $dataObjectHelper
     * @param AccountRedirect $accountRedirect
     */
    public function __construct(
        Context $context,
        \Appseconnect\B2BMage\Model\ContactFactory $contactFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        Session $customerSession,
        StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManagement,
        Address $addressHelper,
        UrlFactory $urlFactory,
        FormFactory $formFactory,
        SubscriberFactory $subscriberFactory,
        RegionInterfaceFactory $regionDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        CustomerUrl $customerUrl,
        Escaper $escaper,
        CustomerExtractor $customerExtractor,
        DataObjectHelper $dataObjectHelper
    ) {
        $this->contactFactory = $contactFactory;
        $this->session = $customerSession;
        $this->helperContactPerson = $helperContactPerson;
        $this->storeManager = $storeManager;
        $this->accountManagement = $accountManagement;
        $this->addressHelper = $addressHelper;
        $this->formFactory = $formFactory;
        $this->subscriberFactory = $subscriberFactory;
        $this->regionDataFactory = $regionDataFactory;
        $this->addressDataFactory = $addressDataFactory;
        $this->customerUrl = $customerUrl;
        $this->escaper = $escaper;
        $this->customerExtractor = $customerExtractor;
        $this->urlModel = $urlFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context);
    }

    /**
     * Retrieve cookie manager
     *
     * @deprecated
     *
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private function getCookieManager()
    {
        if (! $this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()
                                            ->get(\Magento\Framework\Stdlib\Cookie\PhpCookieManager::class);
        }
        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated
     *
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (! $this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()
                                            ->get(\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class);
        }
        return $this->cookieMetadataFactory;
    }

    /**
     * Create contact account action
     *
     * @return void @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $originalRequestData = $this->getRequest()->getPostValue();
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        
        if (! $this->getRequest()->isPost()) {
            $url = $this->urlModel->create()->getUrl('*/*/index_add', [
                '_secure' => true
            ]);
            $resultRedirect->setUrl($this->_redirect->error($url));
            return $resultRedirect;
        }
        
        $this->session->regenerateId();
        
        try {
            
            $customer = $this->customerExtractor->extract('customer_account_create', $this->_request);
            
            $password = $this->getRequest()->getParam('password');
            $confirmation = $this->getRequest()->getParam('password_confirmation');
            $redirectUrl = $this->session->getBeforeAuthUrl();
            
            $this->checkPasswordConfirmation($password, $confirmation);
            $customer->setCustomAttribute('customer_type', 3);
            $customer->setCustomAttribute('contactperson_role', $originalRequestData['role']);
            $customer->setCustomAttribute('customer_status', $originalRequestData['status']);
            
            $customer = $this->accountManagement->createAccount($customer, $password, $redirectUrl);
            
            if ($this->getRequest()->getParam('is_subscribed', false)) {
                $this->subscriberFactory->create()->subscribeCustomerById($customer->getId());
            }
            
            $this->_eventManager->dispatch('customer_register_success', [
                'account_controller' => $this,
                'customer' => $customer
            ]);
            $contactPersonId = $customer->getId();
            $customerData = $this->helperContactPerson->getCustomerId($this->session->getCustomerId());
            $customerId = $customerData['customer_id'];
            // contact person work
            $contactPersonData = [];
            $contactPersonData['customer_id'] = $customerId;
            $contactPersonData['contactperson_id'] = $contactPersonId;
            $contactPersonData['is_active'] = $originalRequestData['status'];
            $contactModel = $this->contactFactory->create();
            $contactModel->setData($contactPersonData);
            $contactModel->save();
            
            $confirmationStatus = $this->accountManagement->getConfirmationStatus($customer->getId());
            if ($confirmationStatus === AccountManagementInterface::ACCOUNT_CONFIRMATION_REQUIRED) {
                $email = $this->customerUrl->getEmailConfirmationUrl($customer->getEmail());
                $this->messageManager->addSuccess(__('You must confirm your account.
                                                     Please check your email for the confirmation link or
                                                     <a href="%1">click here</a> for a new link.', $email));
                
                $url = $this->urlModel->create()->getUrl('*/*/index', [
                    '_secure' => true
                ]);
                $resultRedirect->setUrl($this->_redirect->success($url));
            } else {
                $this->messageManager->addSuccess(__("Your account has been successfully created"));
                $url = $this->urlModel->create()->getUrl('*/*/index_listing', [
                    '_secure' => true
                ]);
                $resultRedirect->setUrl($this->_redirect->success($url));
                return $resultRedirect;
            }
            if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                $metadata->setPath('/');
                $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
            }
            
            return $resultRedirect;
        } catch (StateException $e) {
            $forgotPwdUrl = $this->urlModel->create()->getUrl('customer/account/forgotpassword');
            $errorMessage = __('There is already an account with this email address.
                         If you are sure that it is your email address,
                         <a href="%1">click here</a> to get your password and access your account.', $forgotPwdUrl);
            
            $this->messageManager->addError($errorMessage);
        } catch (InputException $e) {
            $errorMessage = $e->getMessage();
            $this->messageManager->addError($this->escaper->escapeHtml($errorMessage));
            foreach ($e->getErrors() as $error) {
                $errorMessage = $error->getMessage();
                $this->messageManager->addError($this->escaper->escapeHtml($errorMessage));
            }
        } catch (LocalizedException $e) {
            $errorMessage = $e->getMessage();
            $this->messageManager->addError($this->escaper->escapeHtml($errorMessage));
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t save the contact person.'));
        }
        $postValue = $this->getRequest()->getPostValue();
        $this->session->setCustomerFormData($postValue);
        $defaultUrl = $this->urlModel->create()->getUrl('*/*/index_add', [
            '_secure' => true
        ]);
        $resultRedirect->setUrl($this->_redirect->error($defaultUrl));
        return $resultRedirect;
    }

    /**
     * Make sure that password and password confirmation matched
     *
     * @param string $password
     * @param string $confirmation
     * @return void
     * @throws InputException
     */
    private function checkPasswordConfirmation($password, $confirmation)
    {
        if ($password != $confirmation) {
            throw new InputException(__('Please make sure your passwords match.'));
        }
    }

    /**
     * Retrieve success message
     *
     * @return string
     */
    private function getSuccessMessage()
    {
        if ($this->addressHelper->isVatValidationEnabled()) {
            if ($this->addressHelper->getTaxCalculationAddressType() == Address::TYPE_SHIPPING) {
                $message = __(
                    'If you are a registered VAT customer,
                    please <a href="%1">click here</a> to enter your shipping address for proper VAT calculation.',
                    $this->urlModel->create()->getUrl('customer/address/edit')
                );
            } else {
                $message = __(
                    'If you are a registered VAT customer,
                    please <a href="%1">click here</a> to enter your billing address for proper VAT calculation.',
                    $this->urlModel->create()->getUrl('customer/address/edit')
                );
            }
        } else {
            $message = __(
                'Thank you for registering with %1.',
                $this->storeManager->getStore()->getFrontendName()
            );
        }
        return $message;
    }
}
