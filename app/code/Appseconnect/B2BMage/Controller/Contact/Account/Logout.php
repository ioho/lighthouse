<?php
namespace Appseconnect\B2BMage\Controller\Contact\Account;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;

class Logout extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     *
     * @var Session
     */
    public $session;

    /**
     *
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Catalog\Model\Session
     */
    public $catalogSession;

    /**
     *
     * @var PhpCookieManager
     */
    private $cookieMetadataManager;
    
    /**
     * @var \Magento\Framework\Indexer\IndexerInterface
     */
    public $indexer;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\Indexer\IndexerInterface $indexer
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Indexer\IndexerInterface $indexer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        Session $customerSession
    ) {
    
        $this->indexer = $indexer;
        $this->customerFactory = $customerFactory;
        $this->session = $customerSession;
        parent::__construct($context);
    }

    /**
     * Retrieve cookie manager
     *
     * @deprecated
     *
     * @return PhpCookieManager
     */
    private function getCookieManager()
    {
        if (! $this->cookieMetadataManager) {
            $this->cookieMetadataManager = ObjectManager::getInstance()->get(PhpCookieManager::class);
        }
        return $this->cookieMetadataManager;
    }

    private function getCatalogSession()
    {
        $this->catalogSession = ObjectManager::getInstance()->get(
            \Magento\Catalog\Model\Session::class
        );
        return $this->catalogSession;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated
     *
     * @return CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (! $this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = ObjectManager::getInstance()->get(CookieMetadataFactory::class);
        }
        return $this->cookieMetadataFactory;
    }

    /**
     * Customer logout action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $lastCustomerId = $this->session->getId();
        
        $salesrepId = $this->session->getSalesrepId();
        $this->getCatalogSession()->unsSalesrepId();
        $this->getCatalogSession()->unsSalesrepMessage();

        $this->session->logout()
            ->setBeforeAuthUrl($this->_redirect->getRefererUrl())
            ->setLastCustomerId($lastCustomerId);
        if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
            $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
            $metadata->setPath('/');
            $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
        }
        if ($salesrepId) {
            $customer = $this->customerFactory->create()->load($salesrepId);
            $this->session->setCustomerAsLoggedIn($customer);
            $this->session->unsSalesrepId();
            if ($this->session->isLoggedIn()) {
                $indexerId = "catalogrule_rule";
                $this->indexer->load($indexerId);
                $this->indexer->reindexAll();
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('b2bmage/salesrep/customer_listing');
                return $resultRedirect;
            }
        }
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/logoutSuccess');
        return $resultRedirect;
    }
}
