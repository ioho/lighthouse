<?php
namespace Appseconnect\B2BMage\Controller\QuickOrder\Cart;

use Magento\CatalogInventory\Api\StockConfigurationInterface as StockConfigurationInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductSearchList extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    public $productFactory;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    public $stockRegistry;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    public $categoryFactory;

    /**
     * @var ProductCollectionFactory
     */
    public $productCollectionFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultFactory;

    /**
     * @var Session
     */
    public $customerSession;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    public $resources;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    public $productRepository;

    /**
     * @var StockConfigurationInterface
     */
    public $stockConfiguration;

    /**
     * @var cart
     */
    public $cart;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param StockConfigurationInterface $stockConfiguration
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        ProductCollectionFactory $productCollectionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Checkout\Model\Cart  $cart,
        Session $customerSession
    ) {

        $this->productFactory = $productFactory;
        $this->stockRegistry = $stockRegistry;
        $this->storeManager = $storeManager;
        $this->categoryFactory = $categoryFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->customerFactory = $customerFactory;
        $this->resultFactory = $resultJsonFactory;
        $this->customerSession = $customerSession;
        $this->scopeConfig = $scopeConfig;
        $this->resources = $resourceConnection;
        $this->productRepository = $productRepository;
        $this->stockConfiguration = $stockConfiguration;
        $this->cart = $cart;
        parent::__construct($context);
    }

    /**
     * @param int $id
     * @return \Magento\Catalog\Model\Product|NULL
     */
    private function initProduct($id)
    {
        if ($id) {
            $model = $this->productFactory->create()->load($id);
            return $model;
        }
        return null;
    }

    /**
     * Searches product using SKU
     *
     * @return void
     */
    public function execute()
    {
        $customerSessionId = $this->customerSession->getCustomerId();
        $page = $this->getRequest()->getParam('page');
        if (! ($customerSessionId)) {
            $this->messageManager->addError(__('Access Denied.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }

        $customerId = $this->customerSession->getCustomerId();
        $allowedCategoryId = [];
        $customerGroupId = $this->customerFactory->create()
            ->load($customerId)
            ->getGroupId();
        $categoryVisibilityStatus = $this->scopeConfig->getValue(
            'insync_category_visibility/select_visibility/select_visibility_type',
            'store'
        );

        $collection = $this->productCollectionFactory->create();

        $collection = $this->getFilteredCollection($collection, $customerGroupId, $categoryVisibilityStatus);

        $productDetail = [];
        $output = [];
        $totalCount = $collection->count();
        $currentCount = $page * 30;
        $collection->setCurPage($page)->setPageSize(30);
        $netCount = $totalCount - $currentCount;
        if ($collection->getData() && $netCount >= -30) {
            foreach ($collection->getData() as $product) {
                $productVisibilityStatus = $this->productRepository->get($product['sku'])->getVisibility();
                if ($productVisibilityStatus == 1) {
                    continue;
                }
                $quantityStatus = $this->stockRegistry->getStockItem($product['entity_id']);
                $storeId = $this->initProduct($product['entity_id'])->getStoreId();
                $quantity = $quantityStatus['qty'];
                $useConfigMinSaleQty = $quantityStatus['use_config_min_sale_qty'];
                if ($useConfigMinSaleQty == 1 && $this->stockConfiguration->getMinSaleQty(
                    $storeId,
                    $customerGroupId
                )) {
                    $minSaleQty = $this->stockConfiguration->getMinSaleQty($storeId, $customerGroupId);
                } elseif ($useConfigMinSaleQty == 0 && $quantityStatus['min_sale_qty']) {
                    $minSaleQty = $quantityStatus['min_sale_qty'];
                } else {
                    $minSaleQty = 1;
                }
                $qtyIncrements = ($quantityStatus['qty_increments'] &&
                                    $quantityStatus['qty_increments'] > 0) ?
                                    $quantityStatus['qty_increments'] : 1;
                $productDetail['id'] = json_encode([
                    'sku' => $product['sku'],
                    'min_qty' => (int) $minSaleQty,
                    'product_id' => $product['entity_id'],
                    'qty_increments' => $qtyIncrements
                ]);
                $productDetail['text'] = $product['sku'];
                $productDetail['qty_increments'] = $qtyIncrements;
                $output[] = $productDetail;
            }
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        return $resultJson->setData($output);
    }

    /**
     * @param ProductCollectionFactory $collection
     * @param int $customerGroupId
     * @param string $categoryVisibilityStatus
     * @return ProductCollectionFactory
     */
    private function getFilteredCollection($collection, $customerGroupId, $categoryVisibilityStatus)
    {
        $productSku = $this->getRequest()->getParam('productSku');
        $page = $this->getRequest()->getParam('page');
        $availableSku = $this->getRequest()->getParam('object');
        if ($availableSku) {
            $skuArray = [];
            foreach ($availableSku['data'] as $sku) {
                $skuArray[] = "'" . $sku['sku'] . "'";
            }
        }
        $allowedCategory = $this->categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('customer_group', [
            'like' => '%' . $customerGroupId . '%'
            ])
            ->getData();
        $rootCategoryId = $this->storeManager->getStore()->getRootCategoryId();

        if (! empty($allowedCategory) && $categoryVisibilityStatus == 'group_wise_visibility') {
            foreach ($allowedCategory as $categoryData) {
                if ($categoryData['entity_id'] == $rootCategoryId) {
                    continue;
                }
                $allowedCategoryId[] = $categoryData['entity_id'];
            }
            $collection->addAttributeToSelect('*')
                ->joinField(
                    'category_id',
                    $this->resources->getTableName('catalog_category_product'),
                    'category_id',
                    'product_id = entity_id',
                    null,
                    'left'
                )
                ->addAttributeToFilter('category_id', $allowedCategoryId);
        }

        /*
         * To filter the cart item in quikorder
         */
        $items = $this->cart->getQuote()->getAllItems();
        $cartProduct = array();
        foreach($items as $item) {
            $cartProduct[] = $item->getProductId();
        }

        if ($cartProduct) {
            $collection->addAttributeToFilter('entity_id', [
                'nin' => $cartProduct
            ]);
        }

        if ($productSku) {
            $collection->addAttributeToFilter('sku', [
                'like' => '%' . $productSku . '%'
            ]);
        }
        $collection->addAttributeToSelect('*')->load();

        if ($availableSku) {
            $collection->addAttributeToFilter('sku', [
                'nin' => $skuArray
            ]);
        } else {
            $collection;
        }

        return $collection;
    }
}
