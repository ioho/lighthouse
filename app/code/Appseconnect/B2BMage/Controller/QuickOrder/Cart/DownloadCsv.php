<?php
namespace Appseconnect\B2BMage\Controller\QuickOrder\Cart;

use Magento\Sales\Controller\OrderInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class DownloadCsv extends \Magento\Framework\App\Action\Action
{
    protected $_productCollectionFactory;

    public function __construct(
        Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $notificationId = $this->getRequest()->getParam('notification_id');
        $heading = [
            __('Sku'),
            ('Qty')
        ];
        $outputFile = "Quickorder". date('Ymd_His').".csv";
        $handle = fopen($outputFile, 'w');
        fputcsv($handle, $heading);

        $row = [
            'INS4521',
            15,
        ];
        fputcsv($handle, $row);

        $this->downloadCsv($outputFile);
    }

    public function downloadCsv($file)
    {
        if (file_exists($file)) {
            //set appropriate headers
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();flush();
            readfile($file);
        }
    }
}
