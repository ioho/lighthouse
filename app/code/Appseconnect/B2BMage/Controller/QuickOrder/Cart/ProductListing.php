<?php
namespace Appseconnect\B2BMage\Controller\QuickOrder\Cart;

use Magento\Sales\Controller\OrderInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class ProductListing extends \Magento\Framework\App\Action\Action implements OrderInterface
{

    /**
     *
     * @var \Magento\Framework\View\Result\Page
     */
    public $resultPageFactory;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @var Session
     */
    public $customerSession;
   
    /**
     * @param Context $context
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param Session $customerSession
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        Session $customerSession,
        PageFactory $resultPageFactory
    ) {
    
        $this->customerFactory = $customerFactory;
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Redirect to the Quick Order UI page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $customerSessionId = $this->customerSession->getCustomerId();
        $customerType = $this->customerFactory->create()
            ->load($this->customerSession->getCustomerId())
            ->getCustomerType();
        if ($customerType == 1) {
            return $this->resultRedirectFactory->create()->setPath('customer/account');
        }
        if (! ($customerSessionId)) {
            $this->messageManager->addError(__('Access Denied.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()
            ->getTitle()
            ->set(__('Quick Order'));
        return $resultPage;
    }
}
