<?php
namespace Appseconnect\B2BMage\Controller\Quotation\AbstractController;

use Appseconnect\B2BMage\Model\ResourceModel\QuoteHistory;
use Magento\Framework\App\Action;
use Magento\Framework\View\Result\PageFactory;

abstract class Approve extends Action\Action
{

    /**
     *
     * @var \Appseconnect\B2BMage\Model\QuoteFactory
     */
    public $quoteFactory;

    /**
     *
     * @var PageFactory
     */
    public $resultPageFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\Service\QuotationService
     */
    public $quotationService;
    
    /**
     * @param Action\Context $context
     * @param \Appseconnect\B2BMage\Model\Service\QuotationService $quotationService
     * @param \Appseconnect\B2BMage\Model\QuoteFactory $quoteFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
        \Appseconnect\B2BMage\Model\Service\QuotationService $quotationService,
        \Appseconnect\B2BMage\Model\QuoteFactory $quoteFactory,
        PageFactory $resultPageFactory
    ) {
        $this->quotationService = $quotationService;
        $this->quoteFactory = $quoteFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Submit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $quoteId = $this->getRequest()->getParam('quote_id');
        try {
            $this->quotationService->approveQuoteById($quoteId);
            $this->messageManager->addSuccess(__('You have successfully approved the quote.'));
        } catch (\Exception $e) {
            $this->messageManager->addException(
                $e,
                __('Something went wrong while approving the quote.')
            );
        }
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('b2bmage/quotation/index_view', [
            'quote_id' => $quoteId,
            '_current' => true
        ]);
        return $resultRedirect;
    }
}
