<?php

namespace Appseconnect\B2BMage\Controller\Quotation\Index;

use Magento\Sales\Controller\OrderInterface;
use Magento\Customer\Model\Session;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface;
use Appseconnect\B2BMage\Api\Quotation\Data\QuoteProductInterface;
use Appseconnect\B2BMage\Model\ResourceModel\Quote\CollectionFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

class Checkout extends \Magento\Framework\App\Action\Action
{
    /**
     * @var QuoteInterface
     */
    public $quote;

    /**
     * @var \Zend_Filter_LocalizedToNormalizedFactory
     */
    public $filterFactory;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    public $resolverManager;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    public $cart;

    /**
     * @var \Appseconnect\B2BMage\Model\CustomCart
     */
    public $customCart;

    /**
     * @var \Appseconnect\B2BMage\Model\QuotationRepository
     */
    public $quotationRepository;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    public $productModel;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    public $customerModel;

    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     * @var QuoteProductInterface
     */
    public $quoteProduct;

    /**
     * @var Session
     */
    public $customerSession;

    /**
     * @var PageFactory
     */
    public $resultPageFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    public $productFactory;

    /**
     * Checkout constructor.
     * @param Context $context
     * @param \Zend_Filter_LocalizedToNormalizedFactory $filterFactory
     * @param \Magento\Framework\Locale\ResolverInterface $resolverManager
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Appseconnect\B2BMage\Model\CustomCart $customCart
     * @param \Appseconnect\B2BMage\Model\QuotationRepository $quotationRepository
     * @param \Magento\Catalog\Model\Product $productModel
     * @param \Magento\Customer\Model\Customer $customerModel
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param QuoteInterface $quote
     * @param QuoteProductInterface $quoteProduct
     * @param Session $customerSession
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Zend_Filter_LocalizedToNormalizedFactory $filterFactory,
        \Magento\Framework\Locale\ResolverInterface $resolverManager,
        \Magento\Checkout\Model\Cart $cart,
        \Appseconnect\B2BMage\Model\CustomCart $customCart,
        \Appseconnect\B2BMage\Model\QuotationRepository $quotationRepository,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Customer\Model\Customer $customerModel,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        QuoteInterface $quote,
        QuoteProductInterface $quoteProduct,
        Session $customerSession,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        PageFactory $resultPageFactory
    )
    {
        $this->quote = $quote;
        $this->filterFactory = $filterFactory;
        $this->resolverManager = $resolverManager;
        $this->cart = $cart;
        $this->customCart = $customCart;
        $this->quotationRepository = $quotationRepository;
        $this->productModel = $productModel;
        $this->customerModel = $customerModel;
        $this->helperContactPerson = $helperContactPerson;
        $this->quoteProduct = $quoteProduct;
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->productFactory = $productFactory;
        parent::__construct($context);
    }

    /**
     * @param int|NULL $productId
     * @return ProductRepositoryInterface|boolean
     */
    private function _initProduct($productId = null)
    {
        $productId = $productId ? $productId : (int)$this->getRequest()->getParam('product_id');
        if ($productId) {
            try {
                return $this->productFactory->create()->load($productId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }

    /**
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute()
    {
        $quoteId = $this->getRequest()->getParam('quote_id');
        try {
            $quote = $this->quotationRepository->get($quoteId);
            $items = $quote->getItemsCollection();
            $count = 0;
            $quoteDetail = array("id" => $quote->getId());
            $this->customerSession->setQuotationData($quoteDetail);
            $productData = array();
            $parentData = array();
            foreach ($items as $item) {
                $productId = $item->getProductId();
                $price = $item->getPrice();
                $productData[$productId] = $price;
                if ($item->getParentItemId()) {
                    $productData[$productId] = $parentData[$item->getParentItemId()];
                }
                if ($item->getProductType() == "configurable") {
                    $parentData[$item->getId()] = $price;
                }
            }
            foreach ($items as $item) {
                $itemId = $item->getId();
                if (isset($parentData[$itemId])) {
                    $productId = $item->getProductId();
                    $productData[$productId] = $parentData[$itemId];
                }
                $this->customerSession->setQuotationProduct($productData);
                if ($item->getParentItemId()) {
                    continue;
                }
                $params['qty'] = $item->getQty();
                if (isset($params['qty'])) {
                    $filter = $this->filterFactory->create([
                        'locale' => $this->resolverManager->getLocale()
                    ]);
                    $params['qty'] = $filter->filter($params['qty']);
                }
                if ($superAttribute = $item->getSuperAttribute()) {
                    $superAttributeData = json_decode($superAttribute, true);
                    $params['super_attribute'] = $superAttributeData;
                }
                $params["product"] = $item->getProductId();
                $product = $this->_initProduct($item->getProductId());
                $this->cart->addProduct($product, $params);
                $count++;
            }
            $this->customerSession->unsQuotationProduct();
            $this->cart->save();
            $message = __('You added %1 item(s) to your cart.', $count);
            $this->messageManager->addSuccessMessage($message);
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('checkout/cart/');
        return $resultRedirect;
    }
}
