<?php
namespace Appseconnect\B2BMage\Controller\Salesrep\Customer;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Login extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Catalog\Model\Session
     */
    public $catalogSession;

    /**
     *
     * @var PageFactory
     */
    public $resultPageFactory;

    /**
     *
     * @var PhpCookieManager
     */
    private $cookieMetadataManager;

    /**
     *
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Indexer\IndexerInterface
     */
    public $indexer;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     * @param Context $context
     * @param \Magento\Framework\Indexer\IndexerInterface $indexer
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param Magento\Customer\Model\Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param CustomerUrl $customerHelperData
     * @param Validator $formKeyValidator
     * @param AccountRedirect $accountRedirect
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Indexer\IndexerInterface $indexer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Customer\Model\Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        CustomerUrl $customerHelperData,
        Validator $formKeyValidator,
        AccountRedirect $accountRedirect
    ) {

        parent::__construct(
            $context
        );
        $this->indexer = $indexer;
        $this->customerFactory = $customerFactory;
        $this->helperContactPerson = $helperContactPerson;
        $this->session = $customerSession;

    }

    /**
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $return = false;
        $customerId = $this->getRequest()->getParam('customer_id');
        $customerId = $customerId;
        $customerData = $this->helperContactPerson->checkCustomerStatus($customerId, true);

        $lastCustomerId = $this->session->getId();
        $redirectSalesrepId=$this->getCatalogSession()->getRedirectSalesrepId();
        if($redirectSalesrepId){
            $this->getCatalogSession()->unsRedirectSalesrepId();
            $customer = $this->customerFactory->create()->load($redirectSalesrepId);
            $this->session->setCustomerAsLoggedIn($customer);
            if ($this->session->isLoggedIn()) {
                $this->reindex();
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('b2bmage/salesrep/customer_listing');
                return $resultRedirect;
            }
        }
        elseif ($customerData && ($customerData['customer_type'] == 3 || $customerData['customer_type'] == 1) && $customerData['customer_status'] && ! $return) {
            $this->session->logout()
                ->setBeforeAuthUrl($this->_redirect->getRefererUrl())
                ->setLastCustomerId($lastCustomerId);
            if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                $metadata->setPath('/');
                $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
            }

            $customer = $this->customerFactory->create()->load($customerId);
            $this->session->setCustomerAsLoggedIn($customer);
            $message = '( You have logged in as contact person )';
            $this->session->setSalesrepId($lastCustomerId);
            $this->getCatalogSession()->setSalesrepMessage($message);
            if ($this->session->isLoggedIn()) {
                $this->reindex();
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('customer/account');
                return $resultRedirect;
            }
        }
        $this->messageManager->addError(__('Access Denied.'));
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('');
        return $resultRedirect;
    }

    /**
     * @return void
     */
    private function reindex()
    {
        $indexerId = "catalogrule_rule";
        $this->indexer->load($indexerId);
        $this->indexer->reindexAll();
    }

    /**
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private function getCookieManager()
    {
        if (! $this->cookieMetadataManager) {
            $this->cookieMetadataManager = ObjectManager::getInstance()->get(PhpCookieManager::class);
        }
        return $this->cookieMetadataManager;
    }

    /**
     * @return \Magento\Catalog\Model\Session
     */
    private function getCatalogSession()
    {
        $this->catalogSession = ObjectManager::getInstance()->create(\Magento\Catalog\Model\Session::class);
        return $this->catalogSession;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated
     * @return CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (! $this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = ObjectManager::getInstance()->get(CookieMetadataFactory::class);
        }
        return $this->cookieMetadataFactory;
    }
}