<?php
namespace Appseconnect\B2BMage\Controller\Salesrep\Customer;

use Magento\Sales\Controller\OrderInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class View extends \Magento\Framework\App\Action\Action implements OrderInterface
{

    /**
     *
     * @var PageFactory
     */
    public $resultPageFactory;
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $contactPersonHelper;
    
    /**
     * @param Context $context
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $contactPersonHelper
     * @param Session $customerSession
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $contactPersonHelper,
        Session $customerSession,
        PageFactory $resultPageFactory
    ) {
    
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->contactPersonHelper = $contactPersonHelper;
        parent::__construct($context);
    }
   
    /**
     * @return PageFactory
     */
    public function execute()
    {
        if (! ($customerSessionId = $this->customerSession->getCustomerId())) {
            $this->messageManager->addError(__('Access Denied.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }
        
        $customerId = $this->getRequest()->getParam('customer_id');
        $customerId = $customerId;
        $customerData = $this->contactPersonHelper->checkCustomerStatus($customerId, true);
        $resultPage = $this->resultPageFactory->create();
        
        if ($customerData) {
            $resultPage->getConfig()
                ->getTitle()
                ->set(__($customerData['firstname'] . ' ' . $customerData['lastname']));
            if($customerData['customer_type'] == 1){
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('b2bmage/salesrep/customer_login/customer_id/'.$customerData['entity_id']);
                return $resultRedirect;
            }
        }
        
        $block = $resultPage->getLayout()->getBlock('salesrepresentative.customer.view');
        
        if ($block) {
            $block->setRefererUrl($this->_redirect->getRefererUrl());
        }
        
        $navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation');
        
        if ($navigationBlock) {
            $navigationBlock->setActive('b2bmage/salesrep/customer_listing');
        }
        return $resultPage;
    }
}
