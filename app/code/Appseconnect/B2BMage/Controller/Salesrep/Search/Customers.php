<?php
namespace Appseconnect\B2BMage\Controller\Salesrep\Search;

use Magento\Sales\Controller\OrderInterface;
use Appseconnect\B2BMage\Model\ResourceModel\SalesrepgridFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Controller\ResultFactory;

class Customers extends \Magento\Framework\App\Action\Action implements OrderInterface
{

    /**
     *
     * @var PageFactory
     */
    public $resultPageFactory;

    /**
     *
     * @var Session
     */
    public $customerSession;

    /**
     *
     * @var \Magento\Customer\Model\]CustomerFactory
     */
    public $customerFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory;
    
    /**
     * @var SalesrepgridFactory
     */
    public $salesRepResourceFactory;
    
    /**
     * @param Context $context
     * @param SalesrepgridFactory $salesRepResourceFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param Session $customerSession
     * @param PageFactory $resultPageFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        Context $context,
        SalesrepgridFactory $salesRepResourceFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Session $customerSession,
        PageFactory $resultPageFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
    
        $this->customerFactory = $customerFactory;
        $this->salesRepResourceFactory = $salesRepResourceFactory;
        $this->customerSession = $customerSession;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Customer order history
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        try {
            $searchText = $this->getRequest()->getPost('search_text');
            $customerId = $this->customerSession->getCustomerId();
            $salesRepResourceModel = $this->salesRepResourceFactory->create();
            $customerCollection = $this->customerFactory->create()->getCollection();
            
            $customerCollection->addNameToSelect();
            $customerCollection->addFieldToFilter('name', [
                'like' => '%' . $searchText . '%'
            ]);
            $customerCollection = $salesRepResourceModel->getSalesRepCustomers(
                $customerCollection,
                $customerId
            );
            $customerCollection->addAttributeToSelect('customer_status')
                               ->addFieldToFilter('customer_type', 4);
            
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            return $resultJson->setData($customerCollection->getData());
        } catch (\Exception $e) {
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            return $resultJson->setData([]);
        }
    }
}
