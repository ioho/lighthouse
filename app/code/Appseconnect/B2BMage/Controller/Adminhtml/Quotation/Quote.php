<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Quotation;

use Magento\Backend\App\Action;
use Appseconnect\B2BMage\Model\Quote\Email\Sender\QuoteCommentSender;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\InputException;
use Psr\Log\LoggerInterface;

/**
 * Adminhtml quote controller
 *
 * @author Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class Quote extends \Magento\Backend\App\Action
{

    /**
     * Array of actions which can be processed without secret key validation
     *
     * @var string[]
     */
    public $publicActions = [
        'view',
        'index'
    ];

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;

    /**
     *
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    public $fileFactory;

    /**
     *
     * @var \Magento\Framework\Translate\InlineInterface
     */
    public $translateInline;

    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;

    /**
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory;

    /**
     *
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    public $resultLayoutFactory;

    /**
     *
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    public $resultRawFactory;

    /**
     *
     * @var OrderManagementInterface
     */
    public $orderManagement;

    /**
     *
     * @var OrderRepositoryInterface
     */
    public $orderRepository;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var \Zend_Filter_LocalizedToNormalizedFactory
     */
    public $filterFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    public $url;

    /**
     * @var QuoteCommentSender
     */
    public $quoteCommentSender;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    public $resolver;

    /**
     * @var \Magento\Framework\Escaper
     */
    public $escaper;

    /**
     * @var \Appseconnect\B2BMage\Helper\Quotation\Data
     */
    public $helperQuotation;

    /**
     * @var \Appseconnect\B2BMage\Model\CustomCart
     */
    public $customCart;

    /**
     * @var \Appseconnect\B2BMage\Model\QuotationRepository
     */
    public $quotationRepository;

    /**
     * @var \Appseconnect\B2BMage\Model\Quote
     */
    public $customerQuote;

    /**
     * @param Action\Context $context
     * @param \Zend_Filter_LocalizedToNormalizedFactory $filterFactory
     * @param \Magento\Framework\Locale\ResolverInterface $resolver
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\UrlInterface $url
     * @param QuoteCommentSender $quoteCommentSender
     * @param \Appseconnect\B2BMage\Model\CustomCart $customCart
     * @param \Appseconnect\B2BMage\Helper\Quotation\Data $helperQuotation
     * @param \Appseconnect\B2BMage\Model\QuotationRepository $quotationRepository
     * @param \Appseconnect\B2BMage\Model\Quote $customerQuote
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param OrderManagementInterface $orderManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Action\Context $context,
        \Zend_Filter_LocalizedToNormalizedFactory $filterFactory,
        \Magento\Framework\Locale\ResolverInterface $resolver,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\UrlInterface $url,
        QuoteCommentSender $quoteCommentSender,
        \Appseconnect\B2BMage\Model\CustomCart $customCart,
        \Appseconnect\B2BMage\Helper\Quotation\Data $helperQuotation,
        \Appseconnect\B2BMage\Model\QuotationRepository $quotationRepository,
        \Appseconnect\B2BMage\Model\Quote $customerQuote,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger
    ) {
    
        $this->coreRegistry = $coreRegistry;
        $this->filterFactory = $filterFactory;
        $this->url = $url;
        $this->quoteCommentSender = $quoteCommentSender;
        $this->resolver = $resolver;
        $this->escaper = $escaper;
        $this->helperQuotation = $helperQuotation;
        $this->customCart = $customCart;
        $this->quotationRepository = $quotationRepository;
        $this->customerQuote = $customerQuote;
        $this->fileFactory = $fileFactory;
        $this->translateInline = $translateInline;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->orderManagement = $orderManagement;
        $this->orderRepository = $orderRepository;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Init layout, menu and breadcrumb
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Appseconnect_B2BMage::customer_quote');
        $resultPage->addBreadcrumb(__('Sales'), __('Sales'));
        $resultPage->addBreadcrumb(__('Quotes'), __('Quotes'));
        return $resultPage;
    }

    /**
     * @param string $backUrl
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function _goBack($backUrl = null)
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        
        if ($backUrl || $backUrl = $this->_redirect->getRefererUrl()) {
            $resultRedirect->setUrl($backUrl);
        }
        
        return $resultRedirect;
    }

    /**
     * Initialize quote model instance
     *
     * @return \Appseconnect\B2BMage\Model\Quote|false
     */
    public function _initQuote()
    {
        $id = $this->getRequest()->getParam('quote_id');
        try {
            $quote = $this->quotationRepository->get($id);
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addError(__('This quote no longer exists.'));
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
            return false;
        } catch (InputException $e) {
            $this->messageManager->addError(__('This quote no longer exists.'));
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
            return false;
        }
        $this->coreRegistry->register('insync_customer_quote', $quote);
        $this->coreRegistry->register('insync_current_customer_quote', $quote);
        return $quote;
    }

    /**
     *
     * @return bool
     */
    public function isValidPostRequest()
    {
        $formKeyIsValid = $this->_formKeyValidator->validate($this->getRequest());
        $isPost = $this->getRequest()->isPost();
        return ($formKeyIsValid && $isPost);
    }
}
