<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Quotation\Actions;

use Magento\Sales\Controller\OrderInterface;

class Unhold extends \Appseconnect\B2BMage\Controller\Quotation\AbstractController\Unhold
{

    /**
     * @return boolean
     */
    private function isAllowed()
    {
        return true;
    }
}
