<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Quotation\Actions;

use Magento\Sales\Controller\OrderInterface;

class Cancel extends \Appseconnect\B2BMage\Controller\Quotation\AbstractController\Cancel
{

    /**
     * @return boolean
     */
    private function isAllowed()
    {
        return true;
    }
}
