<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Quotation\Index;

use Magento\Backend\App\Action;
use Appseconnect\B2BMage\Model\Quote\Email\Sender\QuoteCommentSender;

class UpdateSubtotal extends \Appseconnect\B2BMage\Controller\Adminhtml\Quotation\Quote
{

    /**
     * Add order comment action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $quote = $this->_initQuote();
        return $this->resultPageFactory->create();
    }
}
