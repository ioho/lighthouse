<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Quotation\Index;

use Magento\Backend\App\Action;

class View extends \Appseconnect\B2BMage\Controller\Adminhtml\Quotation\Quote
{

    /**
     * View quote detail
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $quote = $this->_initQuote();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($quote) {
            try {
                $resultPage = $this->_initAction();
                $resultPage->getConfig()
                    ->getTitle()
                    ->prepend(__('Quotes'));
            } catch (\Exception $e) {
                $this->logger->critical($e);
                $this->messageManager->addError(__('Exception occurred during quote load'));
                $resultRedirect->setPath('/');
                return $resultRedirect;
            }
            $resultPage->getConfig()
                ->getTitle()
                ->prepend(sprintf("#%s", $quote->getId()));
            return $resultPage;
        }
        $resultRedirect->setPath('quotation');
        return $resultRedirect;
    }
}
