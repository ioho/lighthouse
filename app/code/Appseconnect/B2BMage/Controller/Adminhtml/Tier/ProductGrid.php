<?php

namespace Appseconnect\B2BMage\Controller\Adminhtml\Tier;

class ProductGrid extends \Magento\Customer\Controller\Adminhtml\Index
{
      
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
