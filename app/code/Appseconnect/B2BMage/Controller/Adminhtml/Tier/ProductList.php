<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Tier;

use Magento\Framework\Controller\ResultFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class ProductList extends \Magento\Framework\App\Action\Action
{
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data
     */
    public $helperTierprice;
    
    /**
     * @var CollectionFactory
     */
    public $productCollectionFactory;
    
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        CollectionFactory $productCollectionFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data $helperTierprice
    ) {
    
        $this->helperTierprice = $helperTierprice;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Default customer account page
     *
     * @return void
     */
    public function execute()
    {
        $productSku = $this->getRequest()->getParam('productSku');
        
        $collection = $this->productCollectionFactory->create();
        if ($productSku) {
            $collection->addAttributeToFilter('sku', [
                'like' => '%' . $productSku . '%'
            ]);
        }
        $collection->addAttributeToSelect('*')->load();
        $collection->setPageSize(20)->setCurPage(1);
        
        $productDetail = [];
        $output = [];
        foreach ($collection as $product) {
            $productDetail['id'] = $product->getSku();
            $productDetail['text'] = $product->getName();
            $output[] = $productDetail;
        }
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        if (empty($productDetail)) {
            return $resultJson->setData([]);
        } else {
            return $resultJson->setData($output);
        }
    }
}
