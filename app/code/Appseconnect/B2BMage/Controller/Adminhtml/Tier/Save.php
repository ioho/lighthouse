<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Tier;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Appseconnect\B2BMage\Model\ProductFactory;
use Magento\Store\Model\Store;
use Magento\Framework\Exception\AlreadyExistsException;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Indexer\Model\Processor
     */
    public $processor;

    /**
     * @var \Magento\Indexer\Model\Indexer
     */
    public $indexer;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var ProductFactory
     */
    public $tierPriceProductFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @param Action\Context $context
     * @param Session $session
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param ProductFactory $tierPriceProductFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Indexer\Model\Processor $processor
     * @param \Magento\Indexer\Model\IndexerFactory $indexer
     */
    public function __construct(
        Action\Context $context,
        Session $session,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        ProductFactory $tierPriceProductFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Indexer\Model\Processor $processor,
        \Magento\Indexer\Model\IndexerFactory $indexer
    ) {
        parent::__construct($context);
        $this->session = $session;
        $this->tierPriceProductFactory = $tierPriceProductFactory;
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->indexer = $indexer;
        $this->processor = $processor;
    }
    
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $customer = $this->customerFactory->create()->load($data['customer_id']);
            $data['customer_name'] = $customer->getName();
            $tierPriceModel = $this->tierPriceProductFactory->create();
            try {
                $tireId=(isset($data['id']))?$data['id']:null;
                if ($tierPriceModel->isCustomerAlreadyAssigned($data['customer_id'],$tireId)) {
                    throw new AlreadyExistsException(__('Selected customer is already assigned.'));
                }
                $tierPriceModel->setData($data);
                $tierPriceModel->save();
                $this->messageManager->addSuccess(__('The tier price has been saved.'));
                $this->session->setFormData(false);
                $this->reindex();
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', [
                        'id' => $tierPriceModel->getId(),
                        '_current' => true
                    ]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (AlreadyExistsException $e) {
                $this->messageManager->addError($e->getMessage());
            }
            catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the tier price.'));
            }
            
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', [
                'id' => $this->getRequest()
                    ->getParam('id')
            ]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return void
     */
    private function reindex()
    {
        $indexer = $this->indexer->create();
        $indexer->load('catalogrule_rule');
        $indexer->reindexAll();
    }
}
