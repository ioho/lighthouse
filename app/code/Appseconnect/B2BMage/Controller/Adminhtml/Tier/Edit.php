<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Tier;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Appseconnect\B2BMage\Model\ProductFactory;

class Edit extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var ProductFactory
     */
    public $tierPriceProductFactory;

    /**
     * @param Action\Context $context
     * @param Session $session
     * @param ProductFactory $tierPriceProductFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        Session $session,
        ProductFactory $tierPriceProductFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->session = $session;
        $this->tierPriceProductFactory = $tierPriceProductFactory;
        $this->coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    private function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Appseconnect_Pricelist::tierprice_manage')
            ->addBreadcrumb(__('Tier Price'), __('Tier Price'))
            ->addBreadcrumb(__('Manage Tier Price'), __('Manage Tier Price'));
        return $resultPage;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->tierPriceProductFactory->create();
        
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                $this->messageManager->addError(__('This tier price no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                
                return $resultRedirect->setPath('*/*/');
            }
        }
        
        $data = $this->session->getFormData(true);
        
        if ($data) {
            $model->setData($data);
        }
        
        $this->coreRegistry->register('insync_pricelist', $model);
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb($id ?
            __('Edit Tier Price') :
            __('New Tier Price'), $id ?
            __('Edit Tier Price') :
            __('New Tier Price'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend(__('Tier Price'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend($model->getId() ?
                __('Edit Tier Price') :
                __('New Tier Price'));
        
        return $resultPage;
    }
}
