<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Tier;

class TierpriceAdd extends \Magento\Backend\App\Action
{

    /**
     * @var \Appseconnect\B2BMage\Model\TierpriceFactory
     */
    public $tierPriceFactory;
    
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Appseconnect\B2BMage\Model\TierpriceFactory $tierPriceFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Appseconnect\B2BMage\Model\TierpriceFactory $tierPriceFactory
    ) {
    
        $this->tierPriceFactory = $tierPriceFactory;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    public function execute()
    {
        $data = [];
        $data = $this->_request->getParams();
        $variable = $data['action'];
        switch ($variable) {
            case 'delete':
                $modelData = $this->tierPriceFactory->create()->load($data['tierPriceId']);
                $modelData->delete();
                break;
            
            case 'process':
                if (isset($data['data']['update_data'])) {
                    $this->saveUpdateData($data);
                }
                if (isset($data['data']['insert_data'])) {
                    $this->savePostData($data);
                }
                break;
        }
    }

    /**
     * @param array $data
     * @return void
     */
    private function savePostData($data)
    {
        $insertData = $data['data']['insert_data'];
        foreach ($insertData as $insertValues) {
            if ($insertValues['tier_price'] != '' &&
                $insertValues['quantity'] != '' &&
                $insertValues['quantity'] != 0 &&
                $insertValues['quantity'] <= 100000000) {
                $insertValues['parent_id'] = $data['tierPriceId'];
                $this->linkProducts($insertValues);
            }
        }
    }
    
    /**
     * @param array $data
     * @return void
     */
    private function saveUpdateData($data)
    {
        $updateData = $data['data']['update_data'];
        foreach ($updateData as $updateValues) {
            if ($updateValues['quantity'] != '' &&
                $updateValues['tier_price'] != '' &&
                $updateValues['quantity'] != 0 &&
                $updateValues['quantity'] <= 100000000) {
                $updateValues['parent_id'] = $data['tierPriceId'];
                if ($updateValues['id']) {
                    $this->linkProducts($updateValues);
                }
            }
        }
    }
    
    /**
     * @param array $data
     * @return void
     */
    private function linkProducts($data)
    {
        $model = $this->tierPriceFactory->create();
        $model->setData($data)->save();
    }
}
