<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Tier;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Appseconnect\B2BMage\Model\ResourceModel\Product\CollectionFactory;

/**
 * Class MassEnable
 */
class MassEnable extends \Magento\Backend\App\Action
{
    
    /**
     * @var Filter
     */
    public $filter;
    
    /**
     * @var CollectionFactory
     */
    public $collectionFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\ProductFactory
     */
    public $customerTierPriceFactory;
    
    /**
     * @param Context $context
     * @param \Appseconnect\B2BMage\Model\ProductFactory $customerTierPriceFactory
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        \Appseconnect\B2BMage\Model\ProductFactory $customerTierPriceFactory,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
    
        $this->filter = $filter;
        $this->customerTierPriceFactory = $customerTierPriceFactory;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $selectedIds = $this->getRequest()->getParam('selected');
        $excludedStatus = $this->getRequest()->getParam('excluded');
        if ($this->getRequest()->getParam('excluded') && $excludedStatus == 'false') {
            $customerTierPriceCollection = $this->collectionFactory->create();
            $deletedCount = count($customerTierPriceCollection);
            
            foreach ($customerTierPriceCollection as $customerTierPrice) {
                $this->bulkEnable($customerTierPrice->getId());
            }
        } elseif ($this->getRequest()->getParam('selected') && $selectedIds) {
            $deletedCount = count($selectedIds);
            foreach ($selectedIds as $customerTierPriceId) {
                $this->bulkEnable($customerTierPriceId);
            }
        }
        
        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been enabled.', $deletedCount)
        );
        
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
    
    /**
     * @param int $id
     * @return void
     */
    private function bulkEnable($id)
    {
        $model = $this->customerTierPriceFactory->create()->load($id);
        $model->setIsActive(1);
        $model->save();
    }
}
