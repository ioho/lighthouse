<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\CategoryDiscount\Index;

class Discount extends \Magento\Customer\Controller\Adminhtml\Index
{

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
