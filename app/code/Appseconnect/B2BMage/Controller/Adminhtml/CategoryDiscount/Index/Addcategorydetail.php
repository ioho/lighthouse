<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\CategoryDiscount\Index;

use Magento\Backend\App\Action;
use Appseconnect\B2BMage\Model\CategorydiscountFactory;

class Addcategorydetail extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Indexer\Model\Processor
     */
    public $processor;

    /**
     * @var \Magento\Indexer\Model\Indexer
     */
    public $indexer;

    /**
     * @var CategorydiscountFactory
     */
    public $categoryDiscountFactory;

    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    public $resources;

    /**
     * @param Action\Context $context
     * @param CategorydiscountFactory $categoryDiscountFactory
     * @param \Magento\Framework\App\ResourceConnection $resources
     * @param \Magento\Indexer\Model\Processor $processor
     * @param \Magento\Indexer\Model\IndexerFactory $indexer
     */
    public function __construct(
        Action\Context $context,
        CategorydiscountFactory $categoryDiscountFactory,
        \Magento\Framework\App\ResourceConnection $resources,
        \Magento\Indexer\Model\Processor $processor,
        \Magento\Indexer\Model\IndexerFactory $indexer
    ) {
    
        parent::__construct($context);
        $this->categoryDiscountFactory = $categoryDiscountFactory;
        $this->resources = $resources;
        $this->indexer = $indexer;
        $this->processor = $processor;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $data = [];
        $data = $this->_request->getParams();
        $connection = $this->resources->getConnection();
        $variable = $data['action'];
        switch ($variable) {
            case 'delete':
                $tablename = $this->resources->getTableName('insync_categorydiscount');
                $where['categorydiscount_id =?'] = $data['cod'];
                $where['customer_id =?'] = $data['cus'];
                $resultData = $connection->delete($tablename, $where);
                break;
            
            case 'process':
                $tablename = $this->resources->getTableName('insync_categorydiscount');
                if (isset($data['data']['CatagoryDetailUpdate'])) {
                    $updateData = $data['data']['CatagoryDetailUpdate'];
                    foreach ($updateData as $val) {
                        if ($val['discountfactor_up'] != '' && $val['category_id_up'] != '') {
                            $insertVal = [];
                            $where = [];
                            $insertVal['discount_factor'] = $val['discountfactor_up'];
                            $insertVal['category_id'] = $val['category_id_up'];
                            $insertVal['is_active'] = $val['status_up'];
                            $insertVal['categorydiscount_id'] = $val['categorydiscount_id'];
                            $this->putData($insertVal);
                        }
                    }
                    $this->reindex();
                }
                if (isset($data['data']['CatagoryDetail'])) {
                    $dataSend = $data['data']['CatagoryDetail'];
                    foreach ($dataSend as $val) {
                        if ($val['discount_factor'] != '' && $val['category_id'] != '') {
                            $val['customer_id'] = $data['cus'];
                            $this->postData($val);
                        }
                    }
                    $this->reindex();
                }
                break;
        }
    }

    /**
     * @return void
     */
    private function reindex()
    {
        $indexer = $this->indexer->create();
        $indexer->load('catalogrule_rule');
        $indexer->reindexAll();
    }

    /**
     * @param array $data
     * @return void
     */
    private function postData($data)
    {
        $model = $this->categoryDiscountFactory->create();
        $model->setData($data);
        $model->save();
    }
    
    /**
     * @param array $data
     * @return void
     */
    private function putData($data)
    {
        if ($data['categorydiscount_id']) {
            $model = $this->categoryDiscountFactory->create();
            $model->setData($data);
            $model->save();
        }
    }
}
