<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Special;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;

class Edit extends \Magento\Backend\App\Action
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;

    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\CustomerFactory
     */
    public $specialPriceCustomerFactory;
    
    /**
     * @var Session
     */
    public $session;
    
    /**
     * @param Action\Context $context
     * @param Session $session
     * @param \Appseconnect\B2BMage\Model\CustomerFactory $specialPriceCustomerFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        Session $session,
        \Appseconnect\B2BMage\Model\CustomerFactory $specialPriceCustomerFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->session = $session;
        $this->specialPriceCustomerFactory = $specialPriceCustomerFactory;
        $this->coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    private function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Appseconnect_Pricelist::specialprice_manage')
            ->addBreadcrumb(__('Special Price'), __('Special Price'))
            ->addBreadcrumb(__('Manage Special Price'), __('Manage Special Price'));
        return $resultPage;
    }

    /**
     * Edit Blog Post
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->specialPriceCustomerFactory->create();
        
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                $this->messageManager->addError(__('This special price no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                
                return $resultRedirect->setPath('*/*/');
            }
        }
        $data = $this->session->getFormData(true);
        
        if ($data) {
            $model->setData($data);
        }
        
        $this->coreRegistry->register('insync_pricelist', $model);
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb($id ?
            __('Edit Special Price') :
            __('New Special Price'), $id ?
            __('Edit Special Price') :
            __('New Special Price'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend(__('Special Price'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend($model->getId() ?
                'Edit Special Price' :
                __('New Special Price'));
        
        return $resultPage;
    }
}
