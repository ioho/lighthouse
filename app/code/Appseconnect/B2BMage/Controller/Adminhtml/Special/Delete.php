<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Special;

use Magento\Backend\App\Action;
use Appseconnect\B2BMage\Model\CustomerFactory;

class Delete extends \Magento\Backend\App\Action
{
    
    /**
     * @var CustomerFactory
     */
    public $specialPriceCustomerFactory;
    
    /**
     * @param Action\Context $context
     * @param CustomerFactory $specialPriceCustomerFactory
     */
    public function __construct(
        Action\Context $context,
        CustomerFactory $specialPriceCustomerFactory
    ) {
        parent::__construct($context);
        $this->specialPriceCustomerFactory = $specialPriceCustomerFactory;
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $specialPriceModel = $this->specialPriceCustomerFactory->create();
                $specialPriceModel->load($id);
                $specialPriceModel->delete();
                $this->messageManager->addSuccess(__('The Customer Special Price has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', [
                    'id' => $id
                ]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a Customer Special Price to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
