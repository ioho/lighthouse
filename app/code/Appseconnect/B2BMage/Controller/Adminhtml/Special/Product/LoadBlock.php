<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Special\Product;

class LoadBlock extends \Magento\Backend\App\Action
{
    
    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    public $resultLayoutFactory;
    
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    public $resultRawFactory;
    
    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    public $layoutFactory;
    
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
    ) {
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory = $layoutFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $layout = $this->layoutFactory->create()
                ->createBlock(
                    'Appseconnect\B2BMage\Block\Adminhtml\CustomerSpecialPrice\Edit\Tab\View\ProductGrid',
                    'specialprice.product.grid'
                )
                ->toHtml();
        
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw->setContents($layout);
    }
}
