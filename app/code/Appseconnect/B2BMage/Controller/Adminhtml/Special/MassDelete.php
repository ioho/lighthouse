<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Special;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Appseconnect\B2BMage\Model\ResourceModel\Customer\CollectionFactory;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{

    /**
     * @var Filter
     */
    public $filter;

    /**
     * @var CollectionFactory
     */
    public $collectionFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\CustomerFactory
     */
    public $customerSpecialPriceFactory;

    /**
     * @param Context $context
     * @param \Appseconnect\B2BMage\Model\CustomerFactory $customerSpecialPriceFactory
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        \Appseconnect\B2BMage\Model\CustomerFactory $customerSpecialPriceFactory,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
    
        $this->filter = $filter;
        $this->customerSpecialPriceFactory = $customerSpecialPriceFactory;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $selectedIds = $this->getRequest()->getParam('selected');
        $excludedStatus = $this->getRequest()->getParam('excluded');
        if ($this->getRequest()->getParam('excluded') && $excludedStatus == 'false') {
            $customerSpecialPriceCollection = $this->collectionFactory->create();
            
            $deletedCount = count($customerSpecialPriceCollection);
            
            foreach ($customerSpecialPriceCollection as $customerSpecialPrice) {
                $this->unmap($customerSpecialPrice->getId());
            }
        } elseif ($this->getRequest()->getParam('selected') && $selectedIds) {
            $deletedCount = count($selectedIds);
            foreach ($selectedIds as $customerSpecialPriceId) {
                $this->unmap($customerSpecialPriceId);
            }
        }
        
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $deletedCount));
        
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param int $customerSpecialPriceId
     * @return void
     */
    private function unmap($customerSpecialPriceId)
    {
        $this->customerSpecialPriceFactory->create()
            ->load($customerSpecialPriceId)
            ->delete();
    }
}
