<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Special;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\AlreadyExistsException;
use Appseconnect\B2BMage\Api\CustomerSpecialPrice\SpecialPriceRepositoryInterface;
use Appseconnect\B2BMage\Api\CustomerSpecialPrice\Data\SpecialPriceProductInterfaceFactory;
use Magento\Backend\Model\Session;
use Magento\Catalog\Model\ProductRepository;
use Magento\Store\Model\Store;

class Save extends \Magento\Backend\App\Action
{

    /**
     *
     * @var \Magento\Indexer\Model\Processor
     */
    public $processor;

    /**
     *
     * @var \Magento\Indexer\Model\Indexer
     */
    public $indexer;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;
    
    /**
     * @var \Appseconnect\B2BMage\Model\CustomerFactory
     */
    public $specialPriceCustomerFactory;
    
    /**
     * @var Session
     */
    public $session;
    
    /**
     * @var SpecialPriceRepositoryInterface
     */
    public $specialPriceRepository;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @var ProductRepository
     */
    public $productRepository;
    
    /**
     * @var SpecialPriceProductInterfaceFactory
     */
    public $specialPriceProductInterfaceFactory;
    
    /**
     * @param ProductRepository $productRepository
     * @param Session $session
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Model\CustomerFactory $specialPriceCustomerFactory
     * @param SpecialPriceRepositoryInterface $specialPriceRepository
     * @param SpecialPriceProductInterfaceFactory $specialPriceProductInterfaceFactory
     * @param Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Indexer\Model\Processor $processor
     * @param \Magento\Indexer\Model\IndexerFactory $indexer
     */
    public function __construct(
        ProductRepository $productRepository,
        Session $session,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Model\CustomerFactory $specialPriceCustomerFactory,
        SpecialPriceRepositoryInterface $specialPriceRepository,
        SpecialPriceProductInterfaceFactory $specialPriceProductInterfaceFactory,
        Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Indexer\Model\Processor $processor,
        \Magento\Indexer\Model\IndexerFactory $indexer
    ) {
    
        parent::__construct($context);
        $this->session = $session;
        $this->specialPriceRepository = $specialPriceRepository;
        $this->specialPriceCustomerFactory = $specialPriceCustomerFactory;
        $this->customerFactory = $customerFactory;
        $this->productRepository = $productRepository;
        $this->specialPriceProductInterfaceFactory = $specialPriceProductInterfaceFactory;
        $this->storeManager = $storeManager;
        $this->indexer = $indexer;
        $this->processor = $processor;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function getCurrentWebsiteId()
    {
        return $this->storeManager->getStore()->getWebsiteId();
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $customer = $this->customerFactory->create()->load($data['customer_id']);
            $data['customer_name'] = $customer->getName();
            $specialPriceModel = $this->specialPriceCustomerFactory->create();

            try {
                if ($specialPriceModel->isCustomerAlreadyAssigned($data)) {
                    throw new AlreadyExistsException(__('Selected customer and dates already exists.'));
                }
                $specialPriceModel->setData($data);
                if (isset($data['special_price_products'])) {
                    $specialPriceProducts = $data['special_price_products'];
                    $specialPriceProducts = json_decode($specialPriceProducts, true);

                    $productData = $this->populateProductData($specialPriceProducts);
                }
                if ($data['start_date'] > $data['end_date']) {
                    $this->messageManager->addError('Please check your start or end date.');
                    
                    $id = $this->getRequest()->getParam('id');
                    
                    if ($id) {
                        return $resultRedirect->setPath('*/*/edit', [
                            'id' => $this->getRequest()
                                ->getParam('id'),
                            '_current' => true
                        ]);
                    } else {
                        return $resultRedirect->setPath('*/*/new');
                    }
                }
                $specialPriceModel->save();
                $lastInsertId = $specialPriceModel->getId();
                
                if ($lastInsertId && isset($data['special_price_products'])) {
                    $this->specialPriceRepository->assignProducts($productData, $lastInsertId, true);
                }
                
                $this->messageManager->addSuccess(__('The special price has been saved.'));
                $this->session->setFormData(false);
                $this->reindex();
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', [
                        'id' => $specialPriceModel->getId(),
                        '_current' => true
                    ]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            }
            catch (AlreadyExistsException $e) {
                $this->messageManager->addError($e->getMessage());
            }
            catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __('Something went wrong while saving the special price.')
                );
            }
            
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', [
                'id' => $this->getRequest()
                    ->getParam('id')
            ]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return void
     */
    private function reindex()
    {
        $indexer = $this->indexer->create();
        $indexer->load('catalogrule_rule');
        $indexer->reindexAll();
    }

    /**
     * @param array $specialPriceProducts
     * @return array
     */
    private function populateProductData($specialPriceProducts)
    {
        $postData = [];
        foreach ($specialPriceProducts as $key => $value) {
            $productSku = $this->productRepository->getById($key)->getSku();
            $postData['product_details'][] = [
                'product_id' => $key,
                'product_sku' => $productSku,
                'special_price' => $value
            ];
        }
        
        return $postData;
    }
}
