<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Credit;

class Listing extends \Magento\Customer\Controller\Adminhtml\Index
{
      
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
