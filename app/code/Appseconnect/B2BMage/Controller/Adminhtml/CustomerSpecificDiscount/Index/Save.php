<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\CustomerSpecificDiscount\Index;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save extends \Magento\Customer\Controller\Adminhtml\Index
{

    public $soldTo;

    /**
     *
     * @var EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * Save customer action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $this->soldTo = - 1;
        $returnToEdit = false;
        $originalRequestData = $this->getRequest()->getPostValue();
        $customerId = isset($originalRequestData['customer']['entity_id']) ?
        $originalRequestData['customer']['entity_id'] : null;
        if (isset($originalRequestData['customer']['customer_specific_discount'])) {
            $customerSpecificDiscount = $originalRequestData['customer']['customer_specific_discount'];
        } else {
            $customerSpecificDiscount = 0;
        }
        
        $error = $this->_validateAdditionalDiscount($customerSpecificDiscount);
        
        if ($customerId && $error) {
            return $this->resultRedirectFactory->create()->setPath('customer/*/edit', [
                'id' => $customerId,
                '_current' => true
            ]);
        } elseif (! $customerId && $error) {
            return $this->resultRedirectFactory->create()->setPath('customer/*/new', [
                '_current' => true
            ]);
        }
        
        if ($originalRequestData) {
            try {
                $currentCustomer = null;
                $customerData = $this->getCustomerData();
                unset($customerData['created_at']);
                $addressesData = $this->_extractCustomerAddressData($customerData);
                
                $request = $this->getRequest();
                $isExistingCustomer = (bool) $customerId;
                $customer = $this->customerDataFactory->create();
                if ($isExistingCustomer) {
                    $currentCustomer = $this->_customerRepository->getById($customerId);
                    $customerData = array_merge(
                        $this->customerMapper->toFlatArray($currentCustomer),
                        $customerData
                    );
                    $customerData['id'] = $customerId;
                }
                
                $this->dataObjectHelper->populateWithArray(
                    $customer,
                    $customerData,
                    '\Magento\Customer\Api\Data\CustomerInterface'
                );
                 $processedAddresses = $this->processAddress($addressesData);
                
                 $this->_eventManager->dispatch('adminhtml_customer_prepare_save', [
                    'customer' => $customer,
                    'request' => $request
                 ]);
                 $customer->setAddresses($processedAddresses);
                 $customer->setStoreId($customerData['sendemail_store_id']);
                 $customerId = $this->processCustomer($isExistingCustomer, $customer, $customerId, $currentCustomer);
                
                 $this->saveSoldToAddress($customerId, $customer);
                
                 $this->processSubscription($customerId);
                
                 $this->_eventManager->dispatch('adminhtml_customer_save_after', [
                    'customer' => $customer,
                    'customer_id' => $customerId,
                    'request' => $request
                 ]);
                 $this->_getSession()->unsCustomerFormData();
                
                 $this->_coreRegistry->register(RegistryConstants::CURRENT_CUSTOMER_ID, $customerId);
                 $successMessage = 'You saved the customer.';
                 $this->messageManager->addSuccess(__($successMessage));
                 $returnToEdit = (bool) $this->getRequest()->getParam('back', false);
            } catch (\Magento\Framework\Validator\Exception $e) {
                $messages = $e->getMessages();
                if (empty($messages)) {
                    $messages = $e->getMessage();
                }
                $this->_addSessionErrorMessages($messages);
                $this->_getSession()->setCustomerFormData($originalRequestData);
                $returnToEdit = true;
            } catch (LocalizedException $e) {
                $this->_addSessionErrorMessages($e->getMessage());
                $this->_getSession()->setCustomerFormData($originalRequestData);
                $returnToEdit = true;
            } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __('Something went wrong while saving the customer.')
                );
                $this->_getSession()->setCustomerFormData($originalRequestData);
                $returnToEdit = true;
            }
        }
        $output = $this->checkReturn($customerId, $returnToEdit);
        return $output;
    }
    
    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @param string $formCode
     * @param \Magento\Customer\Api\AddressMetadataInterface $entityType
     * @param array $additionalAttributes
     * @param string $scope
     * @param \Magento\Customer\Model\Metadata\Form $metadataForm
     * @return boolean|array
     */
    private function _extractData(
        \Magento\Framework\App\RequestInterface $request,
        $formCode,
        $entityType,
        $additionalAttributes = [],
        $scope = null,
        \Magento\Customer\Model\Metadata\Form $metadataForm = null
    ) {
            
        if ($metadataForm === null) {
            $metadataForm = $this->_formFactory->create(
                $entityType,
                $formCode,
                [],
                false,
                \Magento\Customer\Model\Metadata\Form::DONT_IGNORE_INVISIBLE
            );
        }
            $filteredData = $metadataForm->extractData($request, $scope);
            
            $object = $this->_objectFactory->create([
                'data' => $request->getPostValue()
            ]);
            $requestData = $object->getData($scope);
            foreach ($additionalAttributes as $attributeCode) {
                $filteredData[$attributeCode] = isset($requestData[$attributeCode]) ?
                $requestData[$attributeCode] : false;
            }
            
            $formAttributes = $metadataForm->getAttributes();
            /** @var \Magento\Customer\Api\Data\AttributeMetadataInterface $attribute */
            foreach ($formAttributes as $attribute) {
                $attributeCode = $attribute->getAttributeCode();
                $frontendInput = $attribute->getFrontendInput();
            }
            
            return $filteredData;
    }
    
    /**
     * Saves default_billing and default_shipping flags for customer address
     *
     * @param array $addressIdList
     * @param array $extractedCustomerData
     * @return array
     */
    private function saveDefaultFlags(array $addressIdList, array & $extractedCustomerData)
    {
        $result = [];
        $extractedCustomerData[CustomerInterface::DEFAULT_BILLING] = null;
        $extractedCustomerData[CustomerInterface::DEFAULT_SHIPPING] = null;
        $extractedCustomerData['default_sold'] = null;
        foreach ($addressIdList as $addressId) {
            $scope = sprintf('address/%s', $addressId);
            $addressData = $this->_extractData(
                $this->getRequest(),
                'adminhtml_customer_address',
                \Magento\Customer\Api\AddressMetadataInterface::ENTITY_TYPE_ADDRESS,
                [
                    'default_billing',
                    'default_shipping',
                    'default_sold'
                ],
                $scope
            );
            
            if (is_numeric($addressId)) {
                $addressData['id'] = $addressId;
            }
            // Set default billing and shipping flags to customer
            if (! empty($addressData['default_billing']) &&
                $addressData['default_billing'] === 'true') {
                    $extractedCustomerData[CustomerInterface::DEFAULT_BILLING] = $addressId;
                    $addressData['default_billing'] = true;
            } else {
                $addressData['default_billing'] = false;
            }
            if (! empty($addressData['default_shipping']) &&
                    $addressData['default_shipping'] === 'true') {
                $extractedCustomerData[CustomerInterface::DEFAULT_SHIPPING] = $addressId;
                $addressData['default_shipping'] = true;
            } else {
                $addressData['default_shipping'] = false;
            }
            if (! empty($addressData['default_sold']) &&
                        $addressData['default_sold'] === 'true') {
                $extractedCustomerData['default_sold'] = $addressId;
                $addressData['default_sold'] = true;
            } else {
                $addressData['default_sold'] = false;
            }
                        $result[] = $addressData;
        }
        return $result;
    }
    
    /**
     * Reformat customer addresses data to be compatible with customer service interface
     *
     * @param array $extractedCustomerData
     * @return array
     */
    private function _extractCustomerAddressData(array & $extractedCustomerData)
    {
        $addresses = $this->getRequest()->getPost('address');
        $result = [];
        if (is_array($addresses)) {
            if (isset($addresses['_template_'])) {
                unset($addresses['_template_']);
            }
            $addressIdList = array_keys($addresses);
            $result = $this->saveDefaultFlags($addressIdList, $extractedCustomerData);
        }
        
        return $result;
    }
    
    /**
     * @return array
     */
    private function getCustomerData()
    {
        $customerData = [];
        if ($this->getRequest()->getPost('customer')) {
            $serviceAttributes = [
                CustomerInterface::DEFAULT_BILLING,
                CustomerInterface::DEFAULT_SHIPPING,
                'default_sold',
                'confirmation',
                'sendemail_store_id'
            ];
            
            $customerData = $this->_extractData(
                $this->getRequest(),
                'adminhtml_customer',
                \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
                $serviceAttributes,
                'customer'
            );
        }
        
        if (isset($customerData['disable_auto_group_change'])) {
            $customerData['disable_auto_group_change'] = (int) filter_var(
                $customerData['disable_auto_group_change'],
                FILTER_VALIDATE_BOOLEAN
            );
        }
        
        return $customerData;
    }
    
    /**
     * @param int $customerId
     * @param boolean $returnToEdit
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    private function checkReturn($customerId, $returnToEdit)
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($returnToEdit) {
            if ($customerId) {
                $resultRedirect->setPath('customer/*/edit', [
                    'id' => $customerId,
                    '_current' => true
                ]);
            } else {
                $resultRedirect->setPath('customer/*/new', [
                    '_current' => true
                ]);
            }
        } else {
            $resultRedirect->setPath('customer/index');
        }
        return $resultRedirect;
    }

    /**
     * Get email notification
     *
     * @return EmailNotificationInterface
     * @deprecated
     */
    private function getEmailNotification()
    {
        if (! ($this->emailNotification instanceof EmailNotificationInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()
            ->get(EmailNotificationInterface::class);
        } else {
            return $this->emailNotification;
        }
    }
    
    /**
     * @param int $customerId
     * @return void
     */
    private function processSubscription($customerId)
    {
        $isSubscribed = null;
        if ($this->_authorization->isAllowed(null)) {
            $isSubscribed = $this->getRequest()->getPost('subscription');
        }
        if ($isSubscribed !== null) {
            if ($isSubscribed !== 'false') {
                $this->_subscriberFactory->create()->subscribeCustomerById($customerId);
            } else {
                $this->_subscriberFactory->create()->unsubscribeCustomerById($customerId);
            }
        }
    }
    
    /**
     * @param boolean $isExistingCustomer
     * @param \Magento\Customer\Model\CustomerFactory $customer
     * @param int $customerId
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $currentCustomer
     * @return int
     */
    private function processCustomer($isExistingCustomer, $customer, $customerId, $currentCustomer = null)
    {
        if ($isExistingCustomer) {
            $customerId = $customer->getId();
            $this->_customerRepository->save($customer);
            
            $this->getEmailNotification()->credentialsChanged(
                $customer,
                $currentCustomer->getEmail()
            );
        } else {
            $customer = $this->customerAccountManagement->createAccount($customer);
            $customerId = $customer->getId();
        }
        
        return $customerId;
    }

    /**
     * @param int $customerId
     * @param \Magento\Customer\Model\CustomerFactory $customer
     * @return void
     */
    private function saveSoldToAddress($customerId, $customer)
    {
        $customerDetail = $this->_customerFactory->create()->load($customerId);
        $addressId = null;
        if ($this->soldTo >= 0) {
            foreach ($customer->getAddresses() as $key => $val) {
                if ($key == $this->soldTo) {
                    $addressId = $val->getId();
                }
            }
        }
        
        $customerDetail->setDefaultSold($addressId);
        $customerDetail->save();
    }

    /**
     * @param int $customerSpecificDiscount
     * @return string
     */
    private function _validateAdditionalDiscount($customerSpecificDiscount)
    {
        $message = '';
        if ($customerSpecificDiscount < 0 || $customerSpecificDiscount > 100) {
            $message = 'Additional Discount must be in between 1 and 100';
        } elseif ($customerSpecificDiscount && ! is_numeric($customerSpecificDiscount)) {
            $message = 'Invalid data for Additional Discount';
        }
        
        return $message;
    }

    /**
     * @param array $addressesData
     * @return array
     */
    private function processAddress($addressesData)
    {
        $this->soldTo = - 1;
        $addresses = [];
        foreach ($addressesData as $addressKey => $addressData) {
            if (isset($addressData['default_sold']) && $addressData['default_sold'] != '') {
                $this->soldTo = $addressKey;
            }
            
            $region = isset($addressData['region']) ? $addressData['region'] : null;
            $regionId = isset($addressData['region_id']) ? $addressData['region_id'] : null;
            $addressData['region'] = [
                'region' => $region,
                'region_id' => $regionId
            ];
            $addressDataObject = $this->addressDataFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $addressDataObject,
                $addressData,
                '\Magento\Customer\Api\Data\AddressInterface'
            );
            $addresses[] = $addressDataObject;
        }
        return $addresses;
    }
}
