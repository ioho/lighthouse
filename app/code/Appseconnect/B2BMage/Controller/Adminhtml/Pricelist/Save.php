<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Pricelist;

use Magento\Backend\App\Action;
use Appseconnect\B2BMage\Api\Pricelist\Data\ProductAssignInterfaceFactory;
use Magento\Backend\Model\Session;
use Magento\Catalog\Model\ProductRepository;
use Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory;

class Save extends \Magento\Backend\App\Action
{
    
    /**
     * @var \Magento\Indexer\Model\Processor
     */
    public $processor;
    
    /**
     * @var \Appseconnect\B2BMage\Model\PriceFactory
     */
    public $pricelistPriceFactory;
    
    /**
     * @var CollectionFactory
     */
    public $collectionFactory;
    
    /**
     * @var ProductRepository
     */
    public $productRepository;
    
    /**
     * @var \Appseconnect\B2BMage\Model\PricelistRepository
     */
    public $pricelistRepository;
    
    /**
     * @var ProductAssignInterfaceFactory
     */
    public $productAssignInterfaceFactory;
    
    /**
     * @var Session
     */
    public $session;
    
    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    public $indexer;

    public function __construct(
        ProductRepository $productRepository,
        \Appseconnect\B2BMage\Model\PriceFactory $pricelistPriceFactory,
        Session $session,
        \Appseconnect\B2BMage\Model\PricelistRepository $pricelistRepository,
        ProductAssignInterfaceFactory $productAssignInterfaceFactory,
        Action\Context $context,
        CollectionFactory $collectionFactory,
        \Magento\Indexer\Model\Processor $processor,
        \Magento\Indexer\Model\IndexerFactory $indexer
    ) {
    
        $this->pricelistPriceFactory = $pricelistPriceFactory;
        $this->collectionFactory = $collectionFactory;
        $this->session = $session;
        $this->productRepository = $productRepository;
        $this->pricelistRepository = $pricelistRepository;
        $this->productAssignInterfaceFactory = $productAssignInterfaceFactory;
        parent::__construct($context);
        $this->indexer = $indexer;
        $this->processor = $processor;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        
        $productElements = $this->populateProductData($data);
        
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $pricelistModel = $this->pricelistPriceFactory->create();
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $pricelistModel->load($id);
            }
            $pricelistModel->setData($data);
            $pricelistModel->save();
            try {
                $lastInsertId = $pricelistModel->getId();
                
                if ($lastInsertId && isset($data['pricelist_products'])) {
                    $this->linkProducts($lastInsertId, $productElements);
                }
                $this->messageManager->addSuccess(__('The pricelist has been saved.'));
                $this->session->setFormData(false);
                $this->reindex();
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', [
                        'id' => $pricelistModel->getId(),
                        '_current' => true
                    ]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __('Something went wrong while saving the pricelist.')
                );
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', [
                'id' => $this->getRequest()
                    ->getParam('id')
            ]);
        }
        return $resultRedirect->setPath('*/*/');
    }
    
    /**
     * @return void
     */
    private function reindex()
    {
        $indexer = $this->indexer->create();
        $indexer->load('catalogrule_rule');
        $indexer->reindexAll();
    }
    
    /**
     * @return void
     */
    private function linkProducts($lastInsertId, $productData)
    {
        $productAssignInterfaceModel = $this->productAssignInterfaceFactory->create();
        $productAssignInterfaceModel->setPricelistId($lastInsertId);
        $productAssignInterfaceModel->setProductData($productData);
        $this->pricelistRepository->assignProducts($productAssignInterfaceModel, true);
    }

    /**
     * @param array $data
     * @return array
     */
    private function populateProductData($data)
    {
        $productData = [];
        $pricelistProducts = [];
        if (isset($data['pricelist_products'])) {
            $pricelistProducts = $data['pricelist_products'];
            $pricelistProducts = json_decode($pricelistProducts, true);
            
            foreach ($pricelistProducts as $key => $value) {
                $productSku = $this->productRepository->getById($key)->getSku();
                $productData[] = [
                    'sku' => $productSku,
                    'price' => $value
                ];
            }
        }
        return $productData;
    }
}
