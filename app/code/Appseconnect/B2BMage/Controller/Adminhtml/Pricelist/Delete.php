<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Pricelist;

use Appseconnect\B2BMage\Model\PriceFactory;
use Magento\Backend\App\Action;

class Delete extends Action
{
    
    /**
     * @var PriceFactory
     */
    public $pricelistPriceFactory;
    
    /**
     * @param Action\Context $context
     * @param PriceFactory $pricelistPriceFactory
     */
    public function __construct(
        Action\Context $context,
        PriceFactory $pricelistPriceFactory
    ) {
    
        $this->pricelistPriceFactory = $pricelistPriceFactory;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                $model = $this->pricelistPriceFactory->create();
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                $this->messageManager->addSuccess(__('The pricelist has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', [
                    'id' => $id
                ]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a pricelist to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
