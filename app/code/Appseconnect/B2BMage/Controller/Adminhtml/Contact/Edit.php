<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Contact;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Catalog\Model\Session;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Customer\Model\Address\Mapper;
use Magento\Framework\Message\Error;
use Magento\Framework\DataObjectFactory as ObjectFactory;
use Magento\Framework\Api\DataObjectHelper;

class Edit extends \Magento\Customer\Controller\Adminhtml\Index
{
    
    /**
     * @var \Magento\Framework\Registry
     */
    public $registry;
    
    /**
     * @var Session
     */
    public $catalogSession;
    
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    public $httpFileFactory;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerModelFactory;
    
    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    public $addressModelFactory;
    
    /**
     * @var \Magento\Customer\Model\Metadata\FormFactory
     */
    public $formMetaDataFactory;
    
    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    public $subscriberModelFactory;
    
    /**
     * @var \Magento\Customer\Helper\View
     */
    public $viewCustomerHelper;
    
    /**
     * @var \Magento\Framework\Math\Random
     */
    public $randomFramework;
    
    /**
     * @var CustomerRepositoryInterface
     */
    public $customerRepository;
    
    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    public $extensibleDataObjectConverter;
    
    /**
     * @var Mapper
     */
    public $addressMapper;
    
    /**
     * @var AccountManagementInterface
     */
    public $customerAccountManagement;
    
    /**
     * @var AddressRepositoryInterface
     */
    public $addressRepository;
    
    /**
     * @var CustomerInterfaceFactory
     */
    public $customerDataFactory;
    
    /**
     * @var AddressInterfaceFactory
     */
    public $addressDataFactory;
    
    /**
     * @var \Magento\Customer\Model\Customer\Mapper
     */
    public $customerMapper;
    
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    public $dataObjectProcessor;
    
    /**
     * @var ObjectFactory
     */
    public $objectFactory;
    
    /**
     * @var DataObjectHelper
     */
    public $dataObjectHelper;
    
    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    public $layoutFactory;
    
    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    public $resultLayoutFactory;
    
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;
    
    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    public $resultForwardFactory;
    
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\ContactFactory
     */
    public $contactFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @param \Magento\Backend\App\Action\Context $actionContext
     * @param Session $catalogSession
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Response\Http\FileFactory $httpFileFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerModelFactory
     * @param \Magento\Customer\Model\AddressFactory $addressModelFactory
     * @param \Magento\Customer\Model\Metadata\FormFactory $formMetaDataFactory
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberModelFactory
     * @param \Magento\Customer\Helper\View $viewCustomerHelper
     * @param \Magento\Framework\Math\Random $randomFramework
     * @param CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param Mapper $addressMapper
     * @param AccountManagementInterface $customerAccountManagement
     * @param AddressRepositoryInterface $addressRepository
     * @param CustomerInterfaceFactory $customerDataFactory
     * @param AddressInterfaceFactory $addressDataFactory
     * @param \Magento\Customer\Model\Customer\Mapper $customerMapper
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param ObjectFactory $objectFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Appseconnect\B2BMage\Model\ContactFactory $contactFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $actionContext,
        Session $catalogSession,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Response\Http\FileFactory $httpFileFactory,
        \Magento\Customer\Model\CustomerFactory $customerModelFactory,
        \Magento\Customer\Model\AddressFactory $addressModelFactory,
        \Magento\Customer\Model\Metadata\FormFactory $formMetaDataFactory,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberModelFactory,
        \Magento\Customer\Helper\View $viewCustomerHelper,
        \Magento\Framework\Math\Random $randomFramework,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        Mapper $addressMapper,
        AccountManagementInterface $customerAccountManagement,
        AddressRepositoryInterface $addressRepository,
        CustomerInterfaceFactory $customerDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        \Magento\Customer\Model\Customer\Mapper $customerMapper,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        ObjectFactory $objectFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Appseconnect\B2BMage\Model\ContactFactory $contactFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
    ) {
        $this->catalogSession = $catalogSession;
        $this->formMetaDataFactory = $formMetaDataFactory;
        $this->viewCustomerHelper = $viewCustomerHelper;
        $this->customerRepository = $customerRepository;
        $this->addressMapper = $addressMapper;
        $this->addressRepository = $addressRepository;
        $this->customerDataFactory = $customerDataFactory;
        $this->addressDataFactory = $addressDataFactory;
        $this->customerMapper = $customerMapper;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->resultPageFactory = $resultPageFactory;
        $this->contactFactory = $contactFactory;
        $this->helperContactPerson = $helperContactPerson;
        parent::__construct(
            $actionContext,
            $registry,
            $httpFileFactory,
            $customerModelFactory,
            $addressModelFactory,
            $formMetaDataFactory,
            $subscriberModelFactory,
            $viewCustomerHelper,
            $randomFramework,
            $customerRepository,
            $extensibleDataObjectConverter,
            $addressMapper,
            $customerAccountManagement,
            $addressRepository,
            $customerDataFactory,
            $addressDataFactory,
            $customerMapper,
            $dataObjectProcessor,
            $dataObjectHelper,
            $objectFactory,
            $layoutFactory,
            $resultLayoutFactory,
            $resultPageFactory,
            $resultForwardFactory,
            $resultJsonFactory
        );
    }

    /**
     *
     * @return void
     */
    public function execute()
    {
        $customerId = $this->initCurrentCustomer();
        $this->catalogSession->unsParentCustomerId();
        $this->catalogSession->setParentCustomerId($this->getRequest()
            ->getParam('customer_id'));
        $this->_getSession()->setCustomerId($this->getRequest()
            ->getParam('customer_id'));
        $this->_getSession()->setContactpersonId($this->getRequest()
            ->getParam('contactperson_id'));
        $customerData = [];
        $customerData['account'] = [];
        $customerData['address'] = [];
        $customer = null;
        $isExistingCustomer = (bool) $customerId;
        if ($isExistingCustomer) {
            try {
                $customer = $this->customerRepository->getById($customerId);
                $customerData['account'] = $this->customerMapper->toFlatArray($customer);
                $customerData['account'][CustomerInterface::ID] = $customerId;
                try {
                    $addresses = $customer->getAddresses();
                    foreach ($addresses as $address) {
                        $customerData['address'][$address->getId()] = $this->addressMapper->toFlatArray($address);
                        $customerData['address'][$address->getId()]['id'] = $address->getId();
                    }
                } catch (NoSuchEntityException $e) {
                    $this->messageManager->addException(
                        $e,
                        __('Something went wrong while editing the contact person.')
                    );
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setPath('customer/*/index');
                    return $resultRedirect;
                }
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addException($e, __('Something went wrong while editing the contact person.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('customer/*/index');
                return $resultRedirect;
            }
        }
        $customerData['customer_id'] = $customerId;
        
        $data = $this->_getSession()->getCustomerData(true);
        
        if ($data && (! isset($data['customer_id']) ||
            isset($data['customer_id']) &&
            $data['customer_id'] == $customerId)) {
            $this->processAddress($data, $customer, $customerData);
        }
        
        $this->_getSession()->setCustomerData($customerData);
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magento_Customer::customer_manage');
        $this->prepareDefaultCustomerTitle($resultPage);
        $resultPage->setActiveMenu('Magento_Customer::customer');
        if ($isExistingCustomer) {
            $resultPage->getConfig()
                ->getTitle()
                ->prepend($this->viewCustomerHelper->getCustomerName($customer));
        } else {
            $resultPage->getConfig()
                ->getTitle()
                ->prepend(__('New Contact Person'));
        }
        return $resultPage;
    }

    /**
     * @param array $data
     * @param unknown $customer
     * @param unknown $customerData
     * @return void
     */
    private function processAddress($data, $customer, $customerData)
    {
        $customerId = $customer->getId();
        $request = clone $this->getRequest();
        $request->setParams($data);
        
        if (isset($data['account']) && is_array($data['account'])) {
            $customerForm = $this->formMetaDataFactory->create(
                'customer',
                'adminhtml_customer',
                $customerData['account'],
                true
            );
            $formData = $customerForm->extractData($request, 'account');
            $customerData['account'] = $customerForm->restoreData($formData);
            $customer = $this->customerDataFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $customer,
                $customerData['account'],
                '\Magento\Customer\Api\Data\CustomerInterface'
            );
        }
        
        if (isset($data['address']) && is_array($data['address'])) {
            foreach (array_keys($data['address']) as $addressId) {
                if ($addressId == '_template_') {
                    continue;
                }
                
                try {
                    $address = $this->addressRepository->getById($addressId);
                    if (empty($customerId) || $address->getCustomerId() != $customerId) {
                        $address = $this->addressDataFactory->create();
                    }
                } catch (NoSuchEntityException $e) {
                    $address = $this->addressDataFactory->create();
                    $address->setId($addressId);
                }
                if (! empty($customerId)) {
                    $address->setCustomerId($customerId);
                }
                $address->setIsDefaultBilling(
                    ! empty($data['account'][CustomerInterface::DEFAULT_BILLING])
                    && $data['account'][CustomerInterface::DEFAULT_BILLING] == $addressId
                );
                $address->setIsDefaultShipping(
                    ! empty($data['account'][CustomerInterface::DEFAULT_SHIPPING])
                    && $data['account'][CustomerInterface::DEFAULT_SHIPPING] == $addressId
                );
                $requestScope = sprintf('address/%s', $addressId);
                $addressForm = $this->formMetaDataFactory->create(
                    'customer_address',
                    'adminhtml_customer_address',
                    $this->addressMapper->toFlatArray($address)
                );
                $formData = $addressForm->extractData($request, $requestScope);
                $customerData['address'][$addressId] = $addressForm->restoreData($formData);
                $customerData['address'][$addressId][\Magento\Customer\Api\Data\AddressInterface::ID] = $addressId;
            }
        }
    }
}
