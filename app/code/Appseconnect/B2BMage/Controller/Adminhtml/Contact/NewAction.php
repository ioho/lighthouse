<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Contact;

class NewAction extends \Magento\Backend\App\Action
{

    /**
     * @return void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
