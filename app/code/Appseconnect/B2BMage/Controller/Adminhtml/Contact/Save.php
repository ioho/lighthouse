<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Contact;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Customer\Model\Address\Mapper;
use Magento\Framework\Message\Error;
use Magento\Framework\DataObjectFactory as ObjectFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Newsletter\Model\SubscriptionManagerInterface;
use Magento\Customer\Model\AddressRegistry;
use Magento\Framework\App\ObjectManager;

/**
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Save extends \Magento\Customer\Controller\Adminhtml\Index\Save
{
    
    public $soldTo;
    
    /**
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry;
    
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    public $fileFactory;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    public $addressFactory;
    
    /**
     * @var \Magento\Customer\Model\Metadata\FormFactory
     */
    public $formFactory;
    
    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    public $subscriberFactory;
    
    /**
     * @var \Magento\Customer\Helper\View
     */
    public $viewCustomerHelper;
    
    /**
     * @var \Magento\Framework\Math\Random
     */
    public $random;
    
    /**
     * @var CustomerRepositoryInterface
     */
    public $customerRepository;
    
    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    public $extensibleDataObjectConverter;
    
    /**
     * @var Mapper
     */
    public $addressMapper;
    
    /**
     * @var AccountManagementInterface
     */
    public $customerAccountManagement;
    
    /**
     * @var AddressRepositoryInterface
     */
    public $addressRepository;
    
    /**
     * @var CustomerInterfaceFactory
     */
    public $customerDataFactory;
    
    /**
     * @var AddressInterfaceFactory
     */
    public $addressDataFactory;
    
    /**
     * @var \Magento\Customer\Model\Customer\Mapper
     */
    public $customerMapper;
    
    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    public $dataObjectProcessor;
    
    /**
     * @var ObjectFactory
     */
    public $objectFactory;
    
    /**
     * @var DataObjectHelper
     */
    public $dataObjectHelper;
    
    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    public $layoutFactory;
    
    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    public $resultLayoutFactory;
    
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;
    
    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    public $resultForwardFactory;
    
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\ContactFactory
     */
    public $contactFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    public $subscriptionManager;
    public $addressRegistry;
    
    /**
     * @param \Magento\Backend\App\Action\Context $actionContext
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Customer\Model\Metadata\FormFactory $formFactory
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param \Magento\Customer\Helper\View $viewCustomerHelper
     * @param \Magento\Framework\Math\Random $random
     * @param CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param Mapper $addressMapper
     * @param AccountManagementInterface $customerAccountManagement
     * @param AddressRepositoryInterface $addressRepository
     * @param CustomerInterfaceFactory $customerDataFactory
     * @param AddressInterfaceFactory $addressDataFactory
     * @param \Magento\Customer\Model\Customer\Mapper $customerMapper
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param ObjectFactory $objectFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Appseconnect\B2BMage\Model\ContactFactory $contactFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $actionContext,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Customer\Model\Metadata\FormFactory $formFactory,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Helper\View $viewCustomerHelper,
        \Magento\Framework\Math\Random $random,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        Mapper $addressMapper,
        AccountManagementInterface $customerAccountManagement,
        AddressRepositoryInterface $addressRepository,
        CustomerInterfaceFactory $customerDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        \Magento\Customer\Model\Customer\Mapper $customerMapper,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        ObjectFactory $objectFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Appseconnect\B2BMage\Model\ContactFactory $contactFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        SubscriptionManagerInterface $subscriptionManager,
        AddressRegistry $addressRegistry = null
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->customerFactory = $customerFactory;
        $this->subscriberFactory = $subscriberFactory;
        $this->viewCustomerHelper = $viewCustomerHelper;
        $this->customerRepository = $customerRepository;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerDataFactory = $customerDataFactory;
        $this->addressDataFactory = $addressDataFactory;
        $this->customerMapper = $customerMapper;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->contactFactory = $contactFactory;
        $this->helperContactPerson = $helperContactPerson;
        $this->subscriptionManager = $subscriptionManager;
        $this->addressRegistry = $addressRegistry ?: ObjectManager::getInstance()->get(AddressRegistry::class);
        parent::__construct(
            $actionContext,
            $coreRegistry,
            $fileFactory,
            $customerFactory,
            $addressFactory,
            $formFactory,
            $subscriberFactory,
            $viewCustomerHelper,
            $random,
            $customerRepository,
            $extensibleDataObjectConverter,
            $addressMapper,
            $customerAccountManagement,
            $addressRepository,
            $customerDataFactory,
            $addressDataFactory,
            $customerMapper,
            $dataObjectProcessor,
            $dataObjectHelper,
            $objectFactory,
            $layoutFactory,
            $resultLayoutFactory,
            $resultPageFactory,
            $resultForwardFactory,
            $resultJsonFactory,
            $subscriptionManager,
            $addressRegistry
        );
    }

    /**
     *
     * @var EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * Save customer action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $returnToEdit = false;
        $customerId = $this->_getSession()->getCustomerId();
        $originalRequestData = $this->getRequest()->getPostValue();
        $entityId = $this->_getSession()->getContactpersonId();
        
        $customerData = $this->helperContactPerson->checkCustomerStatus($customerId, true);
        $customerWebsiteId = $customerData['website_id'];
        
        $contactPersonId = isset($originalRequestData['customer']['entity_id']) ?
                                                     $originalRequestData['customer']['entity_id'] :
                                                     null;
        
        if ($originalRequestData) {
            try {
                $customerData = $this->_extractCustomerData();
                $addressesData = $this->_extractCustomerAddressData($customerData);
                $request = $this->getRequest();
                $isExistingCustomer = (bool) $contactPersonId;
                $customer = $this->customerDataFactory->create();
                if ($isExistingCustomer) {
                    $savedCustomerData = $this->customerRepository->getById($contactPersonId);
                    $customerData = array_merge($this->customerMapper->toFlatArray($savedCustomerData), $customerData);
                    $customerData['id'] = $contactPersonId;
                }
                
                $this->dataObjectHelper->populateWithArray(
                    $customer,
                    $customerData,
                    '\Magento\Customer\Api\Data\CustomerInterface'
                );
                $processedAddresses = $this->processAddress($addressesData);
                
                $this->_eventManager->dispatch('adminhtml_customer_prepare_save', [
                    'customer' => $customer,
                    'request' => $request
                ]);
                
                $this->setParentCustomerAttributes(
                    $customer,
                    $customerId,
                    $processedAddresses,
                    $customerData,
                    $customerWebsiteId
                );
                
                $contactPersonId = $this->processContactPerson($isExistingCustomer, $customer, $contactPersonId);
                
                $this->saveSoldToAddress($customer, $contactPersonId);
                
                $this->processSubscription($customerId);
                
                $this->_eventManager->dispatch('adminhtml_customer_save_after', [
                    'customer' => $customer,
                    'request' => $request
                ]);
                $parent_customer_id = $this->_getSession()->getCustomerId();
                $this->_getSession()->unsCustomerData();
                
                $this->saveContactPersonMapping($originalRequestData, $customerId, $contactPersonId, $entityId);
                
                $this->_getSession()->unsCustomerId();
                $this->_getSession()->unsContactpersonId();
                
                $this->coreRegistry->register(RegistryConstants::CURRENT_CUSTOMER_ID, $contactPersonId);
                $this->messageManager->addSuccess(__('You saved the contact person.'));
                $returnToEdit = (bool) $this->getRequest()->getParam('back', false);
            } catch (\Magento\Framework\Validator\Exception $exception) {
                $messages = $exception->getMessages();
                if (empty($messages)) {
                    $messages = $exception->getMessage();
                }
                $this->_addSessionErrorMessages($messages);
                $this->_getSession()->setCustomerData($originalRequestData);
                $returnToEdit = true;
            } catch (LocalizedException $exception) {
                $this->_addSessionErrorMessages($exception->getMessage());
                $this->_getSession()->setCustomerData($originalRequestData);
                $returnToEdit = true;
            } catch (\Exception $exception) {
                $this->messageManager->addException(
                    $exception,
                    __('Something went wrong while saving the contact person.')
                );
                $this->_getSession()->setCustomerData($originalRequestData);
                $returnToEdit = true;
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($returnToEdit) {
            if ($contactPersonId) {
                $resultRedirect->setPath('b2bmage/contact/edit', [
                    'id' => $contactPersonId,
                    'contactperson_id' => $entityId,
                    'customer_id' => $customerId,
                    'is_contactperson' => 1,
                    '_current' => false
                ]);
            } else {
                $resultRedirect->setPath('b2bmage/contact/edit', [
                    'customer_id' => $customerId,
                    'is_contactperson' => 1,
                    '_current' => true
                ]);
            }
        } else {
            $resultRedirect->setPath('customer/index/edit', [
                'id' => $customerId,
                '_current' => true
            ]);
        }
        return $resultRedirect;
    }
    
    /**
     * @param int $customerId
     * @return void
     */
    private function processSubscription($customerId)
    {
        $isSubscribed = null;
        if ($this->_authorization->isAllowed(null)) {
            $isSubscribed = $this->getRequest()->getPost('subscription');
        }
        if ($isSubscribed !== null) {
            if ($isSubscribed !== 'false') {
                $this->subscriberFactory->create()->subscribeCustomerById($customerId);
            } else {
                $this->subscriberFactory->create()->unsubscribeCustomerById($customerId);
            }
        }
    }
    
    /**
     * @param boolean $isExistingCustomer
     * @param CustomerInterfaceFactory $customer
     * @param int $contactPersonId
     * @return int
     */
    private function processContactPerson($isExistingCustomer, $customer, $contactPersonId)
    {
        if ($isExistingCustomer) {
            $this->customerRepository->save($customer);
            $contactPersonId = $customer->getId();
        } else {
            $customer = $this->customerAccountManagement->createAccount($customer);
            $contactPersonId = $customer->getId();
        }
        
        return $contactPersonId;
    }

    /**
     * @param CustomerInterfaceFactory $customer
     * @param int $contactPersonId
     * @return void
     */
    private function saveSoldToAddress($customer, $contactPersonId)
    {
        $customerDetail = $this->customerFactory->create()->load($contactPersonId);
        $addressId = null;
        if ($this->soldTo >= 0) {
            foreach ($customer->getAddresses() as $key => $val) {
                if ($key == $this->soldTo) {
                    $addressId = $val->getId();
                }
            }
        }
        
        $customerDetail->setDefaultSold($addressId);
        $customerDetail->save();
    }

    /**
     * @param array $originalRequestData
     * @param int $customerId
     * @param int $contactPersonId
     * @param int $entityId
     * @return void
     */
    private function saveContactPersonMapping($originalRequestData, $customerId, $contactPersonId, $entityId)
    {
        $contactPersonData = [];
        $contactPersonData['customer_id'] = $customerId;
        $contactPersonData['contactperson_id'] = $contactPersonId;
        $contactPersonData['is_active'] = $originalRequestData['customer']['customer_status'];
        $contactModel = $this->contactFactory->create();
        $contactModel->setData($contactPersonData);
        if (isset($originalRequestData['customer']['entity_id']) &&
            $originalRequestData['customer']['entity_id'] != '') {
            $contactPersonData['id'] = $entityId;
            $contactModel->addData($contactPersonData);
        } else {
            $contactModel->setData($contactPersonData);
        }
        $contactModel->save();
    }

    /**
     * @param CustomerInterfaceFactory $customer
     * @param int $customerId
     * @param mixed $addresses
     * @param array $customerData
     * @param int $customerWebsiteId
     * @return void
     */
    private function setParentCustomerAttributes(
        $customer,
        $customerId,
        $addresses,
        $customerData,
        $customerWebsiteId
    ) {
        $parentCustomerGroupId = $this->customerFactory->create()
            ->load($customerId)
            ->getGroupId();
        $customer->setAddresses($addresses);
        $customer->setCustomAttribute('customer_type', 3);
        $customer->setStoreId($customerData['sendemail_store_id']);
        $customer->setGroupId($parentCustomerGroupId);
        $customer->setWebsiteId($customerWebsiteId);
    }
    
    /**
     * @param array $addressesData
     * @return array
     */
    private function processAddress($addressesData)
    {
        $this->soldTo = - 1;
        $addresses = [];
        foreach ($addressesData as $addressKey => $addressData) {
            if (isset($addressData['default_sold']) && $addressData['default_sold'] != '') {
                $this->soldTo = $addressKey;
            }
            
            $region = isset($addressData['region']) ? $addressData['region'] : null;
            $regionId = isset($addressData['region_id']) ? $addressData['region_id'] : null;
            $addressData['region'] = [
                'region' => $region,
                'region_id' => $regionId
            ];
            $addressDataObject = $this->addressDataFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $addressDataObject,
                $addressData,
                '\Magento\Customer\Api\Data\AddressInterface'
            );
            $addresses[] = $addressDataObject;
        }
        return $addresses;
    }
}
