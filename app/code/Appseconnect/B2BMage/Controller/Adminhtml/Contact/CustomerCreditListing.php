<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Contact;

class CustomerCreditListing extends \Magento\Customer\Controller\Adminhtml\Index
{
     
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
