<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Contact;

use Appseconnect\B2BMage\Controller\Adminhtml\Contact;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Customerlist extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    public $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
      
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Appseconnect_ContactPerson::customers');
        $resultPage->addBreadcrumb(__('Customers'), __('Customers'));
        $resultPage->addBreadcrumb(__('Manage Customers'), __('Manage Customers'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend(__('Customers'));
        return $resultPage;
    }
}
