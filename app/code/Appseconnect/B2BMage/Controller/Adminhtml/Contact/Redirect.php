<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Contact;

use Appseconnect\B2BMage\Controller\Adminhtml\Contact;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Redirect extends \Magento\Backend\App\Action
{
    
    /**
     * @var PageFactory
     */
    public $resultPageFactory;
    
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('b2bmage/contact/customerlist');
        return $resultRedirect;
    }
}
