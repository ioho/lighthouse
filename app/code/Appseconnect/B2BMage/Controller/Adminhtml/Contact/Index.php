<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Contact;

class Index extends \Magento\Customer\Controller\Adminhtml\Index\Index
{

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $customerId = $this->_getSession()->getCustomerId();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($customerId) {
            return $resultRedirect->setPath('customer/index/edit/id/' . $customerId);
        } else {
            return $resultRedirect->setPath('*/*/');
        }
    }
}
