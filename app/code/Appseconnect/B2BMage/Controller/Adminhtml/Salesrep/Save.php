<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Salesrep;

use Magento\Customer\Api\Data\CustomerInterface;
use Appseconnect\B2BMage\Model\SalesrepgridFactory;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\App\Action;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\Address\Mapper;
use Magento\Framework\Message\Error;
use Magento\Framework\DataObjectFactory as ObjectFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Newsletter\Model\SubscriptionManagerInterface;
use Magento\Customer\Model\AddressRegistry;
use Magento\Framework\App\ObjectManager;

class Save extends \Magento\Customer\Controller\Adminhtml\Index\Save
{

    /**
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    public $fileFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    public $addressFactory;

    /**
     * @var \Magento\Customer\Model\Metadata\FormFactory
     */
    public $formFactory;

    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    public $subscriberFactory;


    /**
     * @var \Magento\Customer\Helper\View
     */
    public $viewCustomerHelper;


    /**
     * @var \Magento\Framework\Math\Random
     */
    public $mathRandomFramework;


    /**
     * @var CustomerRepositoryInterface
     */
    public $customerRepository;


    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    public $extensibleDataObjectConverter;


    /**
     * @var $addressMapper
     */
    public $addressMapper;


    /**
     * @var AccountManagementInterface
     */
    public $customerAccountManagement;


    /**
     * @var AddressRepositoryInterface
     */
    public $addressRepository;


    /**
     * @var CustomerInterfaceFactory
     */
    public $customerDataFactory;


    /**
     * @var AddressInterfaceFactory
     */
    public $addressDataFactory;


    /**
     * @var \Magento\Customer\Model\Customer\Mapper
     */
    public $customerMapper;


    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    public $dataObjectProcessor;


    /**
     * @var ObjectFactory
     */
    public $objectFactory;


    /**
     * @var DataObjectHelper
     */
    public $dataObjectHelper;


    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    public $layoutFactory;


    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    public $resultLayoutFactory;


    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;


    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    public $resultForwardFactory;


    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    public $resultJsonFactory;


    /**
     * @var SalesrepgridFactory
     */
    public $salesRepGridFactory;
    
    public $subscriptionManager;
    public $addressRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $actionContext
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Customer\Model\Metadata\FormFactory $formFactory
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param \Magento\Customer\Helper\View $viewCustomerHelper
     * @param \Magento\Framework\Math\Random $mathRandomFramework
     * @param CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param Mapper $addressMapper
     * @param AccountManagementInterface $customerAccountManagement
     * @param AddressRepositoryInterface $addressRepository
     * @param CustomerInterfaceFactory $customerDataFactory
     * @param AddressInterfaceFactory $addressDataFactory
     * @param \Magento\Customer\Model\Customer\Mapper $customerMapper
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param DataObjectHelper $dataObjectHelper
     * @param ObjectFactory $objectFactory
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param SalesrepgridFactory $salesRepGridFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $actionContext,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Customer\Model\Metadata\FormFactory $formFactory,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Helper\View $viewCustomerHelper,
        \Magento\Framework\Math\Random $mathRandomFramework,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        Mapper $addressMapper,
        AccountManagementInterface $customerAccountManagement,
        AddressRepositoryInterface $addressRepository,
        CustomerInterfaceFactory $customerDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        \Magento\Customer\Model\Customer\Mapper $customerMapper,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        DataObjectHelper $dataObjectHelper,
        ObjectFactory $objectFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        SalesrepgridFactory $salesRepGridFactory,
        SubscriptionManagerInterface $subscriptionManager,
        AddressRegistry $addressRegistry = null
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->subscriberFactory = $subscriberFactory;
        $this->customerRepository = $customerRepository;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerDataFactory = $customerDataFactory;
        $this->addressDataFactory = $addressDataFactory;
        $this->customerMapper = $customerMapper;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->salesRepGridFactory = $salesRepGridFactory;
        $this->subscriptionManager = $subscriptionManager;
        $this->addressRegistry = $addressRegistry ?: ObjectManager::getInstance()->get(AddressRegistry::class);
        parent::__construct(
            $actionContext,
            $coreRegistry,
            $fileFactory,
            $customerFactory,
            $addressFactory,
            $formFactory,
            $subscriberFactory,
            $viewCustomerHelper,
            $mathRandomFramework,
            $customerRepository,
            $extensibleDataObjectConverter,
            $addressMapper,
            $customerAccountManagement,
            $addressRepository,
            $customerDataFactory,
            $addressDataFactory,
            $customerMapper,
            $dataObjectProcessor,
            $dataObjectHelper,
            $objectFactory,
            $layoutFactory,
            $resultLayoutFactory,
            $resultPageFactory,
            $resultForwardFactory,
            $resultJsonFactory,
            $subscriptionManager,
            $addressRegistry
        );
    }

    /**
     * Save customer action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $returnToEdit = false;
        $originalRequestData = $this->getRequest()->getPostValue();
        $customerId = isset($originalRequestData['customer']['entity_id']) ?
        $originalRequestData['customer']['entity_id'] :
        null;
        if ($originalRequestData) {
            try {
                $customerData = $this->_extractCustomerData();
                $addressesData = $this->_extractCustomerAddressData($customerData);
                $request = $this->getRequest();
                $isExistingCustomer = (bool) $customerId;
                $customer = $this->customerDataFactory->create();
                if ($isExistingCustomer) {
                    $savedCustomerData = $this->customerRepository->getById($customerId);
                    $customerData = array_merge($this->customerMapper->toFlatArray($savedCustomerData), $customerData);
                    $customerData['id'] = $customerId;
                }
                
                $this->dataObjectHelper->populateWithArray(
                    $customer,
                    $customerData,
                    '\Magento\Customer\Api\Data\CustomerInterface'
                );
                $processedAddresses = $this->processAddress($addressesData);
                
                $this->_eventManager->dispatch('adminhtml_customer_prepare_save', [
                    'customer' => $customer,
                    'request' => $request
                ]);
                $customer->setAddresses($processedAddresses);
                $customer->setCustomAttribute('customer_type', 2);
                $customer->setStoreId($customerData['sendemail_store_id']);
                
                $customerId = $this->processSalesRep($isExistingCustomer, $customer, $customerId);
                
                $this->processSubscription($customerId);
                
                $this->_eventManager->dispatch('adminhtml_customer_save_after', [
                    'customer' => $customer,
                    'request' => $request
                ]);
                
                $this->saveSalesRepMapping($originalRequestData, $customerId);
                $this->_getSession()->unsCustomerData();
                $this->coreRegistry->register(RegistryConstants::CURRENT_CUSTOMER_ID, $customerId);
                $this->messageManager->addSuccess(__('You saved the sales representative.'));
                $returnToEdit = (bool) $this->getRequest()->getParam('back', false);
            } catch (\Magento\Framework\Validator\Exception $exception) {
                $messages = $exception->getMessages();
                if (empty($messages)) {
                    $messages = $exception->getMessage();
                }
                $this->_addSessionErrorMessages($messages);
                $this->_getSession()->setCustomerData($originalRequestData);
                $returnToEdit = true;
            } catch (LocalizedException $exception) {
                $this->_addSessionErrorMessages($exception->getMessage());
                $this->_getSession()->setCustomerData($originalRequestData);
                $returnToEdit = true;
            } catch (\Exception $exception) {
                $this->messageManager->addException(
                    $exception,
                    __('Something went wrong while saving sales representative.')
                );
                $this->_getSession()->setCustomerData($originalRequestData);
                $returnToEdit = true;
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($returnToEdit) {
            if ($customerId) {
                $resultRedirect->setPath('b2bmage/salesrep/edit', [
                    'id' => $customerId,
                    '_current' => false
                ]);
            } else {
                $resultRedirect->setPath('b2bmage/salesrep/new', [
                    '_current' => false
                ]);
            }
        } else {
            $resultRedirect->setPath('b2bmage/salesrep/index');
        }
        return $resultRedirect;
    }
    
    /**
     * @param int $customerId
     * @return void
     */
    private function processSubscription($customerId)
    {
        $isSubscribed = null;
        if ($this->_authorization->isAllowed(null)) {
            $isSubscribed = $this->getRequest()->getPost('subscription');
        }
        if ($isSubscribed !== null) {
            if ($isSubscribed !== 'false') {
                $this->subscriberFactory->create()->subscribeCustomerById($customerId);
            } else {
                $this->subscriberFactory->create()->unsubscribeCustomerById($customerId);
            }
        }
    }
    
    /**
     * @param boolean $isExistingCustomer
     * @param CustomerInterfaceFactory $customer
     * @param int $customerId
     * @return int
     */
    private function processSalesRep($isExistingCustomer, $customer, $customerId)
    {
        if ($isExistingCustomer) {
            $this->customerRepository->save($customer);
        } else {
            $customer = $this->customerAccountManagement->createAccount($customer);
            $customerId = $customer->getId();
        }
        
        return $customerId;
    }

    /**
     * @param array $originalRequestData
     * @param int $customerId
     * @return void
     */
    private function saveSalesRepMapping($originalRequestData, $customerId)
    {
        $salesrepId = $this->_getSession()->getSalesrepId();
        $salesrepModel = $this->salesRepGridFactory->create();
        $originalRequestData['customer']['name'] = $originalRequestData['customer']['firstname'] . " " .
                                                    $originalRequestData['customer']['middlename'] . " " .
                                                    $originalRequestData['customer']['lastname'];
        $originalRequestData['customer']['salesrep_customer_id'] = $customerId;
        $salesrepModel->setData($originalRequestData['customer']);
        if (isset($originalRequestData['customer']['entity_id']) &&
            $originalRequestData['customer']['entity_id'] != '') {
            $salesrepModel->setData('id', $salesrepId);
        }
        $salesrepModel->save();
    }
    
    /**
     * @param array $addressesData
     * @return array
     */
    private function processAddress($addressesData)
    {
        $addresses = [];
        foreach ($addressesData as $addressData) {
            $region = isset($addressData['region']) ? $addressData['region'] : null;
            $regionId = isset($addressData['region_id']) ? $addressData['region_id'] : null;
            $addressData['region'] = [
                'region' => $region,
                'region_id' => $regionId
            ];
            $addressDataObject = $this->addressDataFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $addressDataObject,
                $addressData,
                '\Magento\Customer\Api\Data\AddressInterface'
            );
            $addresses[] = $addressDataObject;
        }
        return $addresses;
    }
}
