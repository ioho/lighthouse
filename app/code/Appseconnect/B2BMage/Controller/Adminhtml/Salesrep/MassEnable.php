<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Salesrep;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Appseconnect\B2BMage\Model\SalesrepgridFactory;
use Appseconnect\B2BMage\Model\ResourceModel\Salesrepgrid\CollectionFactory;

/**
 * Class MassEnable
 */
class MassEnable extends \Magento\Backend\App\Action
{
    
    /**
     *
     * @var Filter
     */
    public $filter;
    
    /**
     *
     * @var CollectionFactory
     */
    public $collectionFactory;
    
    /**
     * @var SalesrepgridFactory
     */
    public $salesRepFactory;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     *
     * @param Context $context
     * @param SalesrepgridFactory $salesRepFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        SalesrepgridFactory $salesRepFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
    
        $this->salesRepFactory = $salesRepFactory;
        $this->customerFactory = $customerFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $selectedIds = $this->getRequest()->getParam('selected');
        $excludedStatus = $this->getRequest()->getParam('excluded');
        if ($this->getRequest()->getParam('excluded') && $excludedStatus == 'false') {
            $salesRepCollection = $this->collectionFactory->create();
            $deletedCount = count($salesRepCollection);
            
            foreach ($salesRepCollection as $salesRep) {
                $this->bulkEnable($salesRep->getId());
            }
        } elseif ($this->getRequest()->getParam('selected') && $selectedIds) {
            $deletedCount = count($selectedIds);
            foreach ($selectedIds as $salesRepId) {
                $this->bulkEnable($salesRepId);
            }
        }
        
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been enabled.', $deletedCount));
        
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
    
    /**
     * @param int $id
     * @return void
     */
    private function bulkEnable($id)
    {
        $model = $this->salesRepFactory->create()->load($id);
        $salesRepCusomerId = $model->getSalesrepCustomerId();
        $model->setIsActive(1);
        $model->save(1);
        $customer = $this->customerFactory->create()->load($salesRepCusomerId);
        $customer->setCustomerStatus(1);
        $customer->save();
    }
}
