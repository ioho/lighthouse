<?php
/**
 * Copyright © 2017 Appseconnect. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Appseconnect\B2BMage\Controller\Adminhtml\Salesrep;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     *
     * @var PageFactory
     */
    public $resultPageFactory;
    
    /**
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    
    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /**
         * @var \Magento\Backend\Model\View\Result\Page $resultPage
         */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Appseconnect_Salesrepresentative::salesrepresentative');
        $resultPage->addBreadcrumb(__('Manage Sales Representative'), __('Manage Sales Representative'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Sales Representative'));
        
        return $resultPage;
    }
}
