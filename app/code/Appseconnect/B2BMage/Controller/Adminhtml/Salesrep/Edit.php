<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Salesrep;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Appseconnect\B2BMage\Model\ResourceModel\Salesrepgrid\CollectionFactory;

class Edit extends \Magento\Backend\App\Action
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\SalesrepgridFactory
     */
    public $salesRepGridFactory;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var CollectionFactory
     */
    public $salesRepGridCollectionFactory;

    /**
     * @param Action\Context $context
     * @param Session $session
     * @param \Appseconnect\B2BMage\Model\SalesrepgridFactory $salesRepGridFactory
     * @param CollectionFactory $salesRepGridCollectionFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        Session $session,
        \Appseconnect\B2BMage\Model\SalesrepgridFactory $salesRepGridFactory,
        CollectionFactory $salesRepGridCollectionFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
    
        $this->salesRepGridFactory = $salesRepGridFactory;
        $this->session = $session;
        $this->salesRepGridCollectionFactory = $salesRepGridCollectionFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    private function _initAction()
    {
        /**
         *
         * @var \Magento\Backend\Model\View\Result\Page $resultPage
         */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Appseconnect_Salesrepresentative::salesrepresentative')
            ->addBreadcrumb(__('Salesrepresentative'), __('Salesrepresentative'))
            ->addBreadcrumb(__('Manage Sales Representative'), __('Manage Sales Representative'));
        return $resultPage;
    }
    
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $salesrepId = '';
        
        $resultRedirect = $this->resultRedirectFactory->create();
        
        if (isset($id)) {
            $salesRepGridCollection = $this->salesRepGridCollectionFactory->create();
            $salesRepGridCollection = $salesRepGridCollection->addFieldToFilter('salesrep_customer_id', $id)->getData();
            $salesrepId = $salesRepGridCollection[0]['id'];
        }
        $this->_getSession()->unsSalesrepId();
        $this->_getSession()->setSalesrepId($salesrepId);
        
        $model = $this->salesRepGridFactory->create();
        
        if ($id) {
            $model->load($salesrepId);
            if (! $model->getId()) {
                $this->messageManager->addError(__('This salesrep no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        
        $data = $this->session->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }
        
        $this->coreRegistry->register('insync_salesrepresentative', $model);
        
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb($id ?
            __('Edit Sales Representative') :
            __('New Sales Representative'), $id ?
            __('Edit Sales Representative') :
            __('New Sales Representative'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend(__('Sales Representative'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend($model->getId() ?
                $model->getName() :
                __('New Sales Representative'));
        
        return $resultPage;
    }
}
