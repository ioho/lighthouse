<?php
/**
 * Copyright © 2017 Appseconnect. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Appseconnect\B2BMage\Controller\Adminhtml\Salesrep;

class CustomerLayout extends \Magento\Customer\Controller\Adminhtml\Index
{
    /**
     * Customer Layout grid
     *
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
