<?php
namespace Appseconnect\B2BMage\Controller\Adminhtml\Approver;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;

class OrderSave extends Action
{
    
    /**
     * @var \Appseconnect\B2BMage\Model\ApproverFactory
     */
    public $approverFactory;
    
    /**
     * @param Action\Context $context
     * @param \Appseconnect\B2BMage\Model\ApproverFactory $approverFactory
     */
    public function __construct(
        Action\Context $context,
        \Appseconnect\B2BMage\Model\ApproverFactory $approverFactory
    ) {
        $this->approverFactory = $approverFactory;
        parent::__construct($context);
    }
    
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = [];
        $data = $this->_request->getParams();
        
        $variable = $data['data']['action'];
        
        switch ($variable) {
            case 'delete':
                $output = [];
                $approverModel = $this->approverFactory->create();
                $approverModel->load($data['data']['id']);
                $approverModel->delete();
                break;
            
            case 'process':
                $output = [];
                $newData = $data['data']['new'];
                if ($newData) {
                    foreach ($newData as $val) {
                        $this->saveTransactionalData($val);
                    }
                }
                break;
        }
    }

    /**
     * @param array $data
     * @return void
     */
    private function saveTransactionalData($data)
    {
        $approverModel = $this->approverFactory->create();
        if (isset($data['insync_approver_id'])) {
            $approverModel->addData($data);
        } else {
            $approverModel->setData($data);
        }
        $approverModel->save();
    }
}
