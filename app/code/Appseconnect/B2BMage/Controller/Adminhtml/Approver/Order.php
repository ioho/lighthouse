<?php

namespace Appseconnect\B2BMage\Controller\Adminhtml\Approver;

class Order extends \Magento\Customer\Controller\Adminhtml\Index
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
