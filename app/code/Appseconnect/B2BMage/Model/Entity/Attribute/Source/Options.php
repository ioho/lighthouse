<?php
namespace Appseconnect\B2BMage\Model\Entity\Attribute\Source;

class Options implements \Magento\Framework\Option\ArrayInterface
{
    
    /**
     * @var \Appseconnect\B2BMage\Helper\CategoryVisibility\Data
     */
    public $categoryVisibilityHelper;
    
    /**
     * @param \Appseconnect\B2BMage\Helper\CategoryVisibility\Data $categoryVisibilityHelper
     */
    public function __construct(
        \Appseconnect\B2BMage\Helper\CategoryVisibility\Data $categoryVisibilityHelper
    ) {
            $this->categoryVisibilityHelper = $categoryVisibilityHelper;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $groupOptions = $this->categoryVisibilityHelper->getCustomerGroups();
        return $groupOptions;
    }
}
