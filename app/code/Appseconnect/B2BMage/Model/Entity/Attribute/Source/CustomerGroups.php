<?php
namespace Appseconnect\B2BMage\Model\Entity\Attribute\Source;

use Magento\Framework\DB\Ddl\Table;

class CustomerGroups extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
   /**
     * Option values
     */
    const VALUE_YES = 1;

    const VALUE_NO = 0;

    /**
     *
     * @var \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory
     */
    public $eavAttrEntity;
    
    /**
     *
     * @var \Appseconnect\B2BMage\Helper\CategoryVisibility\Data
     */
    public $categoryVisibilityHelper;
    
    /**
     *
     * @var \Appseconnect\B2BMage\Model\ResourceModel\Customer\CollectionFactory
     */
    public $customerCollection;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory $eavAttrEntity
     * @param \Appseconnect\B2BMage\Helper\CategoryVisibility\Data $categoryVisibilityHelper
     * @param \Appseconnect\B2BMage\Model\ResourceModel\Customer\CollectionFactory $customerCollection
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory $eavAttrEntity,
        \Appseconnect\B2BMage\Helper\CategoryVisibility\Data $categoryVisibilityHelper,
        \Appseconnect\B2BMage\Model\ResourceModel\Customer\CollectionFactory $customerCollection
    ) {
    
        $this->eavAttrEntity = $eavAttrEntity;
        $this->categoryVisibilityHelper = $categoryVisibilityHelper;
        $this->customerCollection = $customerCollection;
    }

    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        $groupOptions = $this->categoryVisibilityHelper->getCustomerGroups();
        return $groupOptions;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_optionsData = [];
        foreach ($this->getAllOptions() as $_optionData) {
            $_optionsData[] = $_optionData['label'];
        }
        return $_optionsData;
    }

    /**
     * Get a text for option value
     *
     * @param string|int $requestValue
     * @return string|false
     */
    public function getOptionText($requestValue)
    {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option['value'] == $requestValue) {
                return $option['label'];
            }
        }
        return false;
    }

    /**
     * Retrieve flat column definition
     *
     * @return array
     */
    public function getFlatColumns()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        return [
            $attributeCode => [
                'unsigned' => false,
                'default' => null,
                'extra' => null,
                'type' => Table::TYPE_SMALLINT,
                'length' => 1,
                'nullable' => true,
                'comment' => $attributeCode . ' column'
            ]
        ];
    }

    /**
     * Retrieve Indexes(s) for Flat
     *
     * @return array
     */
    public function getFlatIndexes()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $indexes = [];
        $index = 'IDX_' . strtoupper($attributeCode);
        $indexes[$index] = [
            'type' => 'index',
            'fields' => [
                $attributeCode
            ]
        ];
        return $indexes;
    }

    /**
     * Retrieve Select For Flat Attribute update
     *
     * @param int $store
     * @return \Magento\Framework\DB\Select|null
     */
    public function getFlatUpdateSelect($store)
    {
        $attribute = $this->getAttribute();
        return $this->eavAttrEntity->create()->getFlatUpdateSelect($attribute, $store);
    }

    /**
     * Get a text for index option value
     *
     * @param string|int $requestValue
     * @return string|bool
     */
    public function getIndexOptionText($requestValue)
    {
        switch ($requestValue) {
            case self::VALUE_YES:
                return 'Yes';
            case self::VALUE_NO:
                return 'No';
        }
        
        return parent::getIndexOptionText($requestValue);
    }

    /**
     * Add Value Sort To Collection Select
     *
     * @param \Magento\Eav\Model\Entity\Collection\AbstractCollection $collectionData
     * @param string $dir
     *
     * @return \Magento\Eav\Model\Entity\Attribute\Source\Boolean
     */
    public function addValueSortToCollection($collectionData, $dir = \Magento\Framework\DB\Select::SQL_ASC)
    {
        return $this->customerCollection->create()->getSortToCollection($collectionData, $dir);
    }
}
