<?php
namespace Appseconnect\B2BMage\Model;

use Magento\Customer\Model\Data\Customer;
use \Appseconnect\B2BMage\Api\ContactPerson\Data\ContactPersonExtendInterface;

/**
 * Class ContactPersonExtend
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class ContactPersonExtend extends Customer implements ContactPersonExtendInterface
{

    /**
     * Get customer id
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Set customer id
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get Contact Person id
     *
     * @return int|null
     */
    public function getContactPersonId()
    {
        return $this->_get(self::CONTACTPERSON_ID);
    }

    /**
     * Set Contact Person id
     *
     * @param int $contactPersonId
     * @return $this
     */
    public function setContactPersonId($contactPersonId)
    {
        return $this->setData(self::CONTACTPERSON_ID, $contactPersonId);
    }
}
