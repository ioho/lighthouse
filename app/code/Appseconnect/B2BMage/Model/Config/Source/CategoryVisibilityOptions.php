<?php
namespace Appseconnect\B2BMage\Model\Config\Source;

class CategoryVisibilityOptions implements \Magento\Framework\Option\ArrayInterface
{

    /**
     *
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'all_groups',
                'label' => __('All Customer Groups')
            ],
            [
                'value' => 'group_wise_visibility',
                'label' => __(' Customer Group wise category visible')
            ]
        ];
    }
}
