<?php
namespace Appseconnect\B2BMage\Model\Config\Source;

class Pricerule implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     *
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return [
            ['value' =>'price_list', 'label' => __('Price List')],
            ['value' => 'tier_price', 'label' => __('Tier Price')],
            ['value' =>'special_price', 'label' => __('Special Price')],
            ['value' =>'category_price', 'label' => __('Category Price')],
            ['value' =>'item_group_price', 'label' => __('Item Group Price')],
            ['value' =>'lightech_discount_price', 'label' => __('Lightech Discount Price')]
        ];
    }
}
