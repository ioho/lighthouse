<?php
namespace Appseconnect\B2BMage\Model\Data;

class CategoryDiscountData extends \Magento\Framework\Api\AbstractExtensibleObject implements
    \Appseconnect\B2BMage\Api\CategoryDiscount\Data\CategoryDiscountDataInterface
{

    /**
     * Get category discount id
     *
     * @return int|null
     */
    public function getCategorydiscountId()
    {
        return $this->_get(self::CATEGORYDISCOUNT_ID);
    }

    /**
     * Get category id
     *
     * @return int|null
     */
    public function getCategoryId()
    {
        return $this->_get(self::CATEGORY_ID);
    }

    /**
     * Get discount factor
     *
     * @return string|null
     */
    public function getDiscountFactor()
    {
        return $this->_get(self::DISCOUNT_FACTOR);
    }

    /**
     * Get is active
     *
     * @return int|null
     */
    public function getIsActive()
    {
        return $this->_get(self::IS_ACTIVE);
    }

    /**
     * Set category discount id
     *
     * @param int $categoryDiscountId
     * @return $this
     */
    public function setCategorydiscountId($categoryDiscountId)
    {
        return $this->setData(self::CATEGORYDISCOUNT_ID, $categoryDiscountId);
    }

    /**
     * Set category id
     *
     * @param int $categoryId
     * @return $this
     */
    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }

    /**
     * Set discount factor
     *
     * @param string $discountFactor
     * @return $this
     */
    public function setDiscountFactor($discountFactor)
    {
        return $this->setData(self::DISCOUNT_FACTOR, $discountFactor);
    }

    /**
     * Set is active
     *
     * @param int $isActive
     * @return $this
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }
}
