<?php
namespace Appseconnect\B2BMage\Model\Data;

class ContactPerson extends \Magento\Framework\Api\AbstractExtensibleObject implements
    \Appseconnect\B2BMage\Api\ContactPerson\Data\ContactPersonInterface
{

    /**
     * Get customer id
     *
     * @return \Appseconnect\B2BMage\Api\ContactPerson\Data\CustomerExtendInterface[]|null
     */
    public function getContactperson()
    {
        return $this->_get(self::CONTACTPERSON);
    }

    /**
     * Set customer id
     *
     * @param \Appseconnect\B2BMage\Api\ContactPerson\Data\CustomerExtendInterface[] $contactperson
     * @return $this
     */
    public function setContactperson(array $contactperson = null)
    {
        return $this->setData(self::CONTACTPERSON, $contactperson);
    }
}
