<?php
namespace Appseconnect\B2BMage\Model\Data;

use Magento\Framework\Api\AttributeValueFactory;

class OrderResult extends \Magento\Framework\Api\AbstractExtensibleObject implements
    \Appseconnect\B2BMage\Api\Sales\Data\OrderResultInterface
{

    /**
     * Get Order Id
     *
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->_get(self::ORDER_ID);
    }
    /**
     * Set Order Id
     *
     * @param string $orderId
     * @return $this
     */
    public function setOrderId($orderId = null)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }
    
    /**
     * Get Increment Id.
     *
     * @return string|null
     */
    public function getIncrementId()
    {
        return $this->_get(self::INCREMENT_ID);
    }
    
    /**
     * Set Increment Id.
     *
     * @param string $incrementId
     * @return $this
     */
    public function setIncrementId($incrementId = null)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }
}
