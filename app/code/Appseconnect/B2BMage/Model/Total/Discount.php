<?php
namespace Appseconnect\B2BMage\Model\Total;

use Magento\Customer\Model\Session;
use Magento\Quote\Model\Quote\Address;

class Discount extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customer;

    /**
     * @param Session $session
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Magento\Customer\Model\CustomerFactory $customer
     */
    public function __construct(
        Session $session,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Customer\Model\CustomerFactory $customer
    ) {
        $this->customerSession = $session;
        $this->helperContactPerson = $helperContactPerson;
        $this->customer = $customer;
    }

    /**
     * @param Address\Total $total
     */
    public function clearValues(Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }
    
    /**
     * Assign subtotal amount and label to address object
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param Address\Total $total
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(
        \Magento\Quote\Model\Quote $quote,
        Address\Total $total
    ) {
    
        $customerId=$this->customerSession->getCustomer()->getId();
        if ($customerId) {
            $customerSpecificDiscount = 0;
            $customerType = $this->customerSession->getCustomer()->getCustomerType();
            if ($customerType==1) {
                $customerSpecificDiscount = $this->customerSession
                ->getCustomer()->getCustomerSpecificDiscount();
            }
            if ($customerType==3) {
                $customerDetail=$this->helperContactPerson->getCustomerId($customerId);
                $customerCollection = $this->customer->create()->load($customerDetail['customer_id']);
                $customerSpecificDiscount=$customerCollection->getCustomerSpecificDiscount();
            }
            if ($customerType==2) {
                $customerSpecificDiscount=0;
            }
            return [
                  'code' => 'customer_discount',
                  'title' => $this->getLabel($customerSpecificDiscount),
                  'value' => $quote->getSubtotal() * ( $customerSpecificDiscount / 100 )
              ];
        } else {
            return [
                    'code' => 'customer_discount',
                    'title' => $this->getLabel(),
                    'value' => 'Not yet calculated'
            ];
        }
    }

    /**
     * Get Subtotal label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel($customerSpecificDiscount = 0)
    {
        if($customerSpecificDiscount > 0) {
            return __('Customer Discount ( ' .$customerSpecificDiscount. '% )');
        } else {
            return __('Customer Discount');
        }
    }
}
