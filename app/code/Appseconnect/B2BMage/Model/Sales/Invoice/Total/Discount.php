<?php
namespace Appseconnect\B2BMage\Model\Sales\Invoice\Total;

class Discount extends \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
{

    /**
     *
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $discountPercent = 0.00;
        $discountAmount = 0.00;
        $subtotal = $invoice->getSubtotal();
        if ($discountPercent = $invoice->getOrder()->getCustomerDiscount()) {
            $discountAmount = $subtotal * ( $discountPercent / 100);
        }
        $invoice->setGrandTotal($invoice->getGrandTotal() - $discountAmount);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $discountAmount);
        return $this;
    }
}
