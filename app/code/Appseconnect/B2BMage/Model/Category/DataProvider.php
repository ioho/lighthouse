<?php
/**
 * Created by PhpStorm.
 * User: arijit
 * Date: 03-09-2018
 * Time: 17:00
 */

namespace Appseconnect\B2BMage\Model\Category;

class DataProvider extends \Magento\Catalog\Model\Category\DataProvider
{

    /**
     * Elements with use config setting
     *
     * @var array
     * @since 101.0.0
     */
    protected $elementsWithUseConfigSetting = [
        'available_sort_by',
        'customer_group',
        'default_sort_by',
        'filter_price_range',
    ];


    /**
     * Category's fields default values
     *
     * @param array $result
     * @return array
     * @since 101.0.0
     */
    public function getDefaultMetaData($result)
    {
        $result['parent']['default'] = (int)$this->request->getParam('parent');
        $result['use_config.available_sort_by']['default'] = true;
        $result['use_config.customer_group']['default'] = true;
        $result['use_config.default_sort_by']['default'] = true;
        $result['use_config.filter_price_range']['default'] = true;

        return $result;
    }
}