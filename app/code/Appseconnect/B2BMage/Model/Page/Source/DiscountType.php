<?php
namespace Appseconnect\B2BMage\Model\Page\Source;

use Magento\Framework\Data\OptionSourceInterface;

class DiscountType implements OptionSourceInterface
{
    /**
     *
     * @var \Magento\Cms\Model\Page
     */
    public $cmsPage;
    
    /**
     * Constructor
     *
     * @param \Magento\Cms\Model\Page $cmsPage
     */
    public function __construct(\Magento\Cms\Model\Page $cmsPage)
    {
        $this->cmsPage = $cmsPage;
    }
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        
        $result =  [
                0 =>  [
                    'label' => 'Fixed',
                    'value' => 0
                ], 1 =>  [
                    'label' => 'Percentage',
                    'value' => 1
                ]];
        return $result;
    }
}
