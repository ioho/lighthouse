<?php
namespace Appseconnect\B2BMage\Model\Page\Source;

use Magento\Framework\Data\OptionSourceInterface;

class ParentPricelist implements OptionSourceInterface
{
    /**
     *
     * @var \Magento\Cms\Model\Page
     */
    public $cmsPage;
    
    /**
     *
     * @var \Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory
     */
    public $pricelistPriceCollection;
    
    /**
     * Constructor
     *
     * @param \Magento\Cms\Model\Page $cmsPage
     */
    public function __construct(
        \Magento\Cms\Model\Page $cmsPage,
        \Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory $pricelistPriceCollection
    ) {
    
        $this->cmsPage = $cmsPage;
        $this->pricelistPriceCollection = $pricelistPriceCollection;
    }
    
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $parentPricelistCollection = $this->pricelistPriceCollection->create();
        $output = $parentPricelistCollection->getData();
        $result =  [];
        $result[0] = [
                    'label' => "Base Price",
                    'value' => 0
                
        ];
        foreach ($output as $val) {
            $result [] =  [
                    'label' => $val ['pricelist_name'],
                    'value' => $val ["parent_id"]
            ];
        }
        return $result;
    }
}
