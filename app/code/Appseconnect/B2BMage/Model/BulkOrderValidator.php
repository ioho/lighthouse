<?php
namespace Appseconnect\B2BMage\Model;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\CustomerMetadataInterface;

/**
 * Bulk Order Validator
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class BulkOrderValidator
{

    /**
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    ) {
    
        $this->productFactory = $productFactory;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @param mixed $requestData
     * @return boolean
     */
    public function validate($requestData)
    {
        $error = false;
        $productRepository = $this->productFactory->create();
        foreach ($requestData as $value) {
            $sku = $value[0];
            $qty = $value[1];
            $productId = $this->getProductIdBySku($productRepository, $sku);
            if (! $productId) {
                $error = true;
            } elseif (! $qty) {
                $error = true;
            } elseif ($qty && $productId) {
                $productStockObj = $this->stockRegistry->getStockItem($productId);
                $productModel = $this->loadProduct($productRepository, $productId);
                if ($qty > $productStockObj->getQty() && $productStockObj->getBackorders() == 0) {
                    $error = true;
                }
                if ($productModel->getStatus() == 2) {
                    $error = true;
                }
            }
        }
        return $error;
    }
    
    /**
     * @param \Magento\Catalog\Model\ProductFactory $productRepository
     * @param int $productId
     * @return mixed
     */
    public function loadProduct($productRepository, $productId)
    {
        $productModel = $productRepository->load($productId);
        return $productModel;
    }
    /**
     * @param \Magento\Catalog\Model\ProductFactory $productRepository
     * @param string $sku
     * @return int
     */
    public function getProductIdBySku($productRepository, $sku)
    {
        $productId = $productRepository->getIdBySku($sku);
        return $productId;
    }
}
