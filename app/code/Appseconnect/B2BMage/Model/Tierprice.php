<?php
namespace Appseconnect\B2BMage\Model;

class Tierprice extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Appseconnect\B2BMage\Model\ResourceModel\Tierprice');
    }
}
