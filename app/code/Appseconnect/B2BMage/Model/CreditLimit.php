<?php
namespace Appseconnect\B2BMage\Model;

use Magento\Customer\Model\Session;
use Magento\Framework\App\ObjectManager;
use Appseconnect\B2BMage\Helper\ContactPerson\Data as ContactPersonHelper;
use Appseconnect\B2BMage\Helper\CreditLimit\Data as CreditLimitHelper;

class CreditLimit extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    public $checkoutSession;
    /**
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $contactPersonHelper;
    /**
     * @var \Appseconnect\B2BMage\Helper\CreditLimit\Data
     */
    public $creditLimitHelper;

   /**
    * @var string
    */
    const PAYMENT_METHOD_CREDIT_LIMIT_CODE = 'creditlimit';

    /**
     * Payment method code
     *
     * @var string
     */
    public $_code = self::PAYMENT_METHOD_CREDIT_LIMIT_CODE;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param Session $customerSession
     * @param ContactPersonHelper $contactPersonHelper
     * @param CreditLimitHelper $creditLimitHelper
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        Session $customerSession,
        ContactPersonHelper $contactPersonHelper,
        CreditLimitHelper $creditLimitHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
    
        $this->customerSession = $customerSession;
        $this->contactPersonHelper = $contactPersonHelper;
        $this->creditLimitHelper = $creditLimitHelper;
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @return \Magento\Checkout\Model\Session
     */
    private function getCheckoutSession()
    {
        $this->checkoutSession = ObjectManager::getInstance()->create(\Magento\Checkout\Model\Session::class);
        return $this->checkoutSession;
    }

    /**
     * Check whether payment method can be used
     *
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (! $this->isActive($quote ? $quote->getStoreId() : null)) {
            return false;
        }
        
        $customerId = $this->customerSession->getCustomerId();
        $customerType = null;
        if ($customerId) {
            $customerDetail = $this->contactPersonHelper->getCustomerData($customerId);
            $customerType = $customerDetail['customer_type'];
        }
        $quote = $this->getCheckoutSession()->getQuote();
        
        if ($quote && $customerId && $customerType == 3) {
            $b2bcustomerDetail = $this->contactPersonHelper->getCustomerId($customerId);
            $customerCreditBalance = $this->creditLimitHelper->getCustomerCreditData($b2bcustomerDetail['customer_id']);
            $quoteData = $quote->getData();
            $grandTotal = $quoteData['grand_total'];
            if ($customerCreditBalance['available_balance'] >= $grandTotal) {
                return true;
            }
        }
        
        return false;
    }
}
