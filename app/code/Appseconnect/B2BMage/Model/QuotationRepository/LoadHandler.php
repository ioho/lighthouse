<?php
namespace Appseconnect\B2BMage\Model\QuotationRepository;

use Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface;
use Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentProcessor;
use Magento\Quote\Api\Data\CartExtensionFactory;

class LoadHandler
{

    /**
     *
     * @var ShippingAssignmentProcessor
     */
    private $shippingAssignmentProcessor;

    /**
     *
     * @var CartExtensionFactory
     */
    private $cartExtensionFactory;

    /**
     *
     * @param ShippingAssignmentProcessor $shippingAssignmentProcessor
     * @param CartExtensionFactory $cartExtensionFactory
     */
    public function __construct(
        ShippingAssignmentProcessor $shippingAssignmentProcessor,
        CartExtensionFactory $cartExtensionFactory
    ) {
    
        $this->shippingAssignmentProcessor = $shippingAssignmentProcessor;
        $this->cartExtensionFactory = $cartExtensionFactory;
    }

    /**
     *
     * @param QuoteInterface $quote
     * @return QuoteInterface
     */
    public function load(QuoteInterface $quote)
    {
        /** @var \Appseconnect\B2BMage\Model\Quote $quote */
        $quote->setItems($quote->getAllVisibleItems());
        
        return $quote;
    }
}
