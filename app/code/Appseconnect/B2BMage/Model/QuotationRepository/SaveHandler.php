<?php
namespace Appseconnect\B2BMage\Model\QuotationRepository;

use Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface;
use Appseconnect\B2BMage\Model\Quote\Product\QuoteProductPersister;
use Magento\Quote\Model\Quote\Address\BillingAddressPersister;
use Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentPersister;
use Magento\Framework\Exception\InputException;

class SaveHandler
{

    /**
     *
     * @var \Appseconnect\B2BMage\Model\Quote\Product\QuoteProductPersister
     */
    private $quoteItemPersister;

    /**
     *
     * @var \Magento\Quote\Model\Quote\Address\BillingAddressPersister
     */
    private $billingAddressPersister;

    /**
     *
     * @var \Appseconnect\B2BMage\Model\ResourceModel\Quote
     */
    private $quoteResourceModel;

    /**
     *
     * @var \Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentPersister
     */
    private $shippingAssignmentPersister;

    /**
     *
     * @param \Appseconnect\B2BMage\Model\ResourceModel\Quote $quoteResource
     * @param \Appseconnect\B2BMage\Model\Quote\Product\QuoteProductPersister $quoteItemPersister
     * @param \Magento\Quote\Model\Quote\Address\BillingAddressPersister $billingAddressPersister
     * @param ShippingAssignmentPersister $shippingAssignmentPersister
     */
    public function __construct(
        \Appseconnect\B2BMage\Model\ResourceModel\Quote $quoteResource,
        QuoteProductPersister $quoteItemPersister,
        BillingAddressPersister $billingAddressPersister,
        ShippingAssignmentPersister $shippingAssignmentPersister
    ) {
    
        $this->quoteResourceModel = $quoteResource;
        $this->quoteItemPersister = $quoteItemPersister;
        $this->billingAddressPersister = $billingAddressPersister;
        $this->shippingAssignmentPersister = $shippingAssignmentPersister;
    }

    /**
     *
     * @param QuoteInterface $quote
     * @return QuoteInterface
     *
     * @throws InputException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function save(QuoteInterface $quote, $flag)
    {
        /** @var \Appseconnect\B2BMage\Model\Quote $quote */
        $items = $quote->getItems();
        if ($items) {
            foreach ($items as $item) {
                /** @var \Appseconnect\B2BMage\Model\QuoteProduct $item */
                if (! $item->isDeleted()) {
                    $quote->setLastAddedItem($this->saveQuoteItem($quote, $item, $flag));
                }
            }
        }
        
        $this->quoteResourceModel->save($quote->collectTotals());
        return $quote;
    }
    
    /**
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @param \Appseconnect\B2BMage\Model\QuoteProduct $item
     * @param boolean $flag
     * @return mixed
     */
    private function saveQuoteItem($quote, $item, $flag)
    {
        return $this->quoteItemPersister->save($quote, $item, $flag);
    }

    /**
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return void
     * @throws InputException
     */
    private function processShippingAssignment($quote)
    {
        $extensionAttributes = $quote->getExtensionAttributes();
        if (! $quote->isVirtual() &&
            $extensionAttributes &&
            $extensionAttributes->getShippingAssignments()) {
            $shippingAssignments = $extensionAttributes->getShippingAssignments();
            if (count($shippingAssignments) > 1) {
                throw new InputException(__("Only 1 shipping assignment can be set"));
            }
            $this->shippingAssignmentPersister->save($quote, $shippingAssignments[0]);
        }
    }
}
