<?php
namespace Appseconnect\B2BMage\Model;

use Appseconnect\B2BMage\Api\Quotation\Data\QuoteStatusInterface;

class QuoteStatus extends \Magento\Framework\Model\AbstractModel implements QuoteStatusInterface
{

    /**
     * Status table
     *
     * @var string
     */
    public $stateTable;

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Appseconnect\B2BMage\Model\ResourceModel\QuoteStatus');
        $this->statusTable = $this->getTable('insync_quotation_status');
    }

    /**
     * Get Status
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->getData(QuoteStatusInterface::STATUS);
    }

    /**
     * Set Status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(QuoteStatusInterface::STATUS, $status);
    }

    /**
     * Get Label
     *
     * @return string|null
     */
    public function getLabel()
    {
        return $this->getData(QuoteStatusInterface::LABEL);
    }

    /**
     * Set Label
     *
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        return $this->setData(QuoteStatusInterface::LABEL, $label);
    }

    public function getStatusLabel()
    {
        return $this->getLabel();
    }
}
