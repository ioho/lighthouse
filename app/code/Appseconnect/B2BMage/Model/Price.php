<?php
namespace Appseconnect\B2BMage\Model;

class Price extends \Magento\Framework\Model\AbstractModel
{

   /**
    * Constructor
    *
    * @return void
    */
    public function _construct()
    {
        parent::_construct();
        $this->_init('Appseconnect\B2BMage\Model\ResourceModel\Price');
    }
}
