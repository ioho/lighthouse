<?php
namespace Appseconnect\B2BMage\Model;

use Appseconnect\B2BMage\Model\ResourceModel\Product\CollectionFactory;

class Product extends \Magento\Framework\Model\AbstractModel
{

    /**
     * @var CollectionFactory
     */
    private $tierPriceCollectionFactory;

    /**
     * Product constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param CollectionFactory $tierPriceCollectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        CollectionFactory $tierPriceCollectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->tierPriceCollectionFactory = $tierPriceCollectionFactory;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Appseconnect\B2BMage\Model\ResourceModel\Product');
    }

    /**
     * @param $customerId
     * @return bool
     */
    public function isCustomerAlreadyAssigned($customerId, $tierPriceId)
    {
        $collection = $this->tierPriceCollectionFactory->create();
        $collection->addFieldToFilter('customer_id', $customerId);
        if($tierPriceId) $collection->addFieldToFilter('id', array('neq' => $tierPriceId));
        if ($collection->getData()) {
            return true;
        }
        return false;
    }
}
