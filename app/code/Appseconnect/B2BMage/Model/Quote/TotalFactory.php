<?php
namespace Appseconnect\B2BMage\Model\Quote;

use Magento\Framework\App\ObjectManager;

class TotalFactory
{

    /**
     * Create class instance with specified parameters
     *
     * @param string $instanceName
     * @param array $data
     * @return \Appseconnect\B2BMage\Model\Quote\Total
     */
    public function create($instanceName, array $data = [])
    {
        return ObjectManager::getInstance()->create($instanceName, $data);
    }
}
