<?php
namespace Appseconnect\B2BMage\Model\Quote;

use Magento\Quote\Model\Quote\Address\Total\Collector;
use Appseconnect\B2BMage\Model\QuoteProduct;
use Magento\Quote\Model\Quote\Address\Total\CollectorFactory;
use Magento\Quote\Model\Quote\Address\Total\CollectorInterface;

class TotalsCollector
{

    /**
     * Total models collector
     *
     * @var \Magento\Quote\Model\Quote\Address\Total\Collector
     */
    public $totalCollector;

    /**
     *
     * @var \Magento\Quote\Model\Quote\Address\Total\CollectorFactory
     */
    public $totalCollectorFactory;

    /**
     * Application Event Dispatcher
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    public $eventManager;

    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     *
     * @var \Appseconnect\B2BMage\Model\Quote\TotalFactory
     */
    public $totalFactory;

    /**
     *
     * @var \Magento\Quote\Model\Quote\TotalsCollectorList
     */
    public $collectorList;

    /**
     * Quote validator
     *
     * @var \Magento\Quote\Model\QuoteValidator
     */
    public $quoteValidator;

    /**
     *
     * @var \Magento\Quote\Model\ShippingFactory
     */
    public $shippingFactory;

    /**
     *
     * @var \Magento\Quote\Model\ShippingAssignmentFactory
     */
    public $shippingAssignmentFactory;

    /**
     *
     * @param Collector $totalCollector
     * @param CollectorFactory $totalCollectorFactory
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Address\TotalFactory $totalFactory
     * @param TotalsCollectorList $collectorList
     * @param \Magento\Quote\Model\ShippingFactory $shippingFactory
     * @param \Magento\Quote\Model\ShippingAssignmentFactory $shippingAssignmentFactory
     * @param \Magento\Quote\Model\QuoteValidator $quoteValidator
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Collector $totalCollector,
        CollectorFactory $totalCollectorFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Appseconnect\B2BMage\Model\Quote\TotalFactory $totalFactory,
        \Magento\Quote\Model\Quote\TotalsCollectorList $collectorList,
        \Magento\Quote\Model\ShippingFactory $shippingFactory,
        \Magento\Quote\Model\ShippingAssignmentFactory $shippingAssignmentFactory,
        \Magento\Quote\Model\QuoteValidator $quoteValidator
    ) {
    
        $this->totalCollector = $totalCollector;
        $this->totalCollectorFactory = $totalCollectorFactory;
        $this->eventManager = $eventManager;
        $this->helperPriceRule = $helperPriceRule;
        $this->storeManager = $storeManager;
        $this->totalFactory = $totalFactory;
        $this->collectorList = $collectorList;
        $this->shippingFactory = $shippingFactory;
        $this->shippingAssignmentFactory = $shippingAssignmentFactory;
        $this->quoteValidator = $quoteValidator;
    }

    /**
     *
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @return \Appseconnect\B2BMage\Model\Quote\Total
     */
    public function collect(\Appseconnect\B2BMage\Model\Quote $quote)
    {
        
        /** @var \Appseconnect\B2BMage\Model\Quote\Total $total */
        $total = $this->totalFactory->create('\Appseconnect\B2BMage\Model\Quote\Total');
        
        $this->_collectItemsQtys($quote);
        
        $total->setSubtotal(0);
        $total->setBaseSubtotal(0);
        
        $total->setGrandTotal(0);
        $total->setBaseGrandTotal(0);
        
        /** @var \Appseconnect\B2BMage\Model\QuoteProduct $item */
        foreach ($quote->getAllItems() as $item) {
            if ($this->_initItem($item) && $item->getQty() > 0 && ! $item->isDeleted()) {
                if (! $item->getParentItem()) {
                    $total->setSubtotal((float) $total->getSubtotal() + $item->getRowTotal());
                    $total->setBaseSubtotal((float) $total->getBaseSubtotal() + $item->getBaseRowTotal());
                    
                    $total->setGrandTotal((float) $total->getGrandTotal() + $total->getSubtotal());
                    $total->setBaseGrandTotal((float) $total->getBaseGrandTotal() + $total->getBaseSubtotal());
                }
            }
        }
        $total->setGrandTotal((float) $total->getSubtotal());
        $total->setBaseGrandTotal((float) $total->getBaseSubtotal());
        
        return $total;
    }

    public function _initItem($item)
    {
        if ($item instanceof QuoteProduct && $item->getId()) {
            $quoteItem = $item->getQuote()->getItemById($item->getId());
        } else {
            $quoteItem = $item;
        }
        
        $product = $quoteItem->getProduct();
        $product->setCustomerGroupId($quoteItem->getQuote()
            ->getCustomerGroupId());
        $quoteWebsiteId = $quoteItem->getQuote()
            ->getStore()
            ->getWebsiteId();
        
        $originalPrice = $product->getPrice();
        $finalPrice = $product->getFinalPrice($quoteItem->getQty());
        
        $discountedPrice = $this->helperPriceRule->getDiscountedPrice(
            $product->getId(),
            $quoteItem->getQty(),
            $quoteItem->getQuote()
            ->getContactId(),
            $quoteWebsiteId
        );
        
        if ($quoteItem->getParentItem()) {
            $this->_calculateRowTotal($quoteItem, $discountedPrice, $originalPrice);
        } elseif (! $quoteItem->getParentItem()) {
            $childProductId = null;
            foreach ($quoteItem->getChildren() as $child) {
                $childProductId = $child->getProductId();
            }
            if ($childProductId) {
                $discountedPrice = $this->helperPriceRule->getDiscountedPrice(
                    $childProductId,
                    $quoteItem->getQty(),
                    $quoteItem->getQuote()
                    ->getContactId(),
                    $quoteWebsiteId
                );
            }
            $this->_calculateRowTotal($quoteItem, $discountedPrice, $originalPrice);
        }
        return true;
    }

    public function _calculateRowTotal($item, $finalPrice, $originalPrice)
    {
        if (! $originalPrice) {
            $originalPrice = $finalPrice;
        }
        
        $item->setOriginalPrice($originalPrice)->setBaseOriginalPrice($originalPrice);
        if (! $item->getPrice() && ! $item->getBasePrice()) {
            $item->setPrice($finalPrice)->setBasePrice($finalPrice);
        }
        $item->calcRowTotal();
        return $this;
    }

    /**
     * Collect items qty
     *
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @return $this
     */
    public function _collectItemsQtys(\Appseconnect\B2BMage\Model\Quote $quote)
    {
        $quote->setItemsCount(0);
        $quote->setItemsQty(0);
        
        foreach ($quote->getAllItems() as $item) {
            $quote->setItemsCount($quote->getItemsCount() + 1);
            $quote->setItemsQty((float) $quote->getItemsQty() + $item->getQty());
        }
        return $this;
    }
}