<?php
namespace Appseconnect\B2BMage\Model\Quote\Product;

use Magento\Catalog\Model\Product;
use Appseconnect\B2BMage\Model\QuoteProductFactory;
use Appseconnect\B2BMage\Model\QuoteProduct;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\State;
use Magento\Framework\DataObject;
use Appseconnect\B2BMage\Api\Quotation\Data\QuoteProductInterface;
use Magento\Quote\Api\Data\CartItemInterface;

class Processor
{

    /**
     *
     * @var \Appseconnect\B2BMage\Model\QuoteProductFactory
     */
    public $quoteProductFactory;

    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     *
     * @var \Magento\Framework\App\State
     */
    public $appState;

    /**
     *
     * @param QuoteProductFactory $quoteProductFactory
     * @param StoreManagerInterface $storeManager
     * @param State $appState
     */
    public function __construct(
        QuoteProductFactory $quoteProductFactory,
        StoreManagerInterface $storeManager,
        State $appState
    ) {
    
        $this->quoteProductFactory = $quoteProductFactory;
        $this->storeManager = $storeManager;
        $this->appState = $appState;
    }

    /**
     * Initialize quote item object
     *
     * @param \Magento\Framework\DataObject $request
     * @param Product $product
     *
     * @return \Appseconnect\B2BMage\Model\QuoteProduct
     */
    public function init(Product $product, $request)
    {
        $item = $this->quoteProductFactory->create();
        if ($request->getSuperAttribute()) {
            $jsonValue = json_encode($request->getSuperAttribute());
            $item->setSuperAttribute($jsonValue);
        }
        
        /**
         * We can't modify existing child items
         */
        if ($item->getId() && $product->getParentProductId()) {
            return $item;
        }
        
        $item->setOptions($product->getCustomOptions());
        $item->setProduct($product);
        
        if ($request->getResetCount() &&
            ! $product->getStickWithinParent() &&
            $item->getId() === $request->getId()) {
            $item->setData(QuoteProductInterface::QTY, 0);
        }
        
        return $item;
    }

    /**
     * Set qty and custom price for quote item
     *
     * @param QuoteProduct $item
     * @param \Magento\Framework\DataObject $request
     * @param Product $candidate
     * @return void
     */
    public function prepare(QuoteProduct $item, DataObject $request, Product $candidate)
    {
        /**
         * We specify qty after we know about parent (for stock)
         */
        if ($request->getResetCount() &&
            ! $candidate->getStickWithinParent() &&
            $item->getId() == $request->getId()) {
            $item->setData(QuoteProductInterface::QTY, 0);
        }
        $item->addQty($candidate->getCartQty());
    }

    /**
     * Set store_id value to quote item
     *
     * @param Product $item
     * @return void
     */
    public function setProductStoreId(Product $item)
    {
        if ($this->appState->getAreaCode() === \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE) {
            $storeId = $this->storeManager->getStore($this->storeManager->getStore()
                ->getId())
                ->getId();
            $item->setStoreId($storeId);
        } else {
            $item->setStoreId($this->storeManager->getStore()
                ->getId());
        }
    }
}
