<?php
namespace Appseconnect\B2BMage\Model\Quote\Product;

use Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface;
use Magento\Quote\Model\Quote\Item\CartItemOptionsProcessor;
use Appseconnect\B2BMage\Api\Quotation\Data\QuoteProductInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Catalog\Api\ProductRepositoryInterface;

class QuoteProductPersister
{

    /**
     *
     * @var \Magento\Framework\DataObject\Factory
     */
    private $objectFactory;

    /**
     *
     * @var CartItemOptionsProcessor
     */
    private $cartItemOptionProcessor;
    
    /**
     *
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\DataObject\Factory $objectFactory
     * @param CartItemOptionsProcessor $cartItemOptionProcessor
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\DataObject\Factory $objectFactory,
        CartItemOptionsProcessor $cartItemOptionProcessor
    ) {
    
        $this->objectFactory = $objectFactory;
        $this->productRepository = $productRepository;
        $this->cartItemOptionProcessor = $cartItemOptionProcessor;
    }

    /**
     * @param QuoteInterface $quote
     * @param QuoteProductInterface $item
     * @param boolean $flag
     * @throws NoSuchEntityException
     * @throws LocalizedException
     * @throws CouldNotSaveException
     * @return \Appseconnect\B2BMage\Model\QuoteProduct
     */
    public function save(QuoteInterface $quote, QuoteProductInterface $item, $flag = false)
    {
        $params = $this->objectFactory->create();
        /** @var \Appseconnect\B2BMage\Model\Quote $quote */
        $qty = $item->getQty();
        
        if (! is_numeric($qty) || $qty <= 0) {
            throw InputException::invalidFieldValue('qty', $qty);
        }
        $quoteId = $item->getQuoteId();
        $itemId = $item->getId();
        $params->setData('qty', $qty);
        try {
            
            /**
             * Update existing item
             */
            if (isset($itemId)) {
                $currentItem = $quote->getItemById($itemId);
                if (! $currentItem) {
                    throw new NoSuchEntityException(__('Quote %1 does not contain item %2', $quoteId, $itemId));
                }
                $this->updateParams($currentItem, $item, $flag);
            } else {
                /**
                 * add new item to shopping cart
                 */
                $product = $this->productRepository->get($item->getProductSku());
                $item = $this->processItemToCart($product, $params, $quote);
                if (is_string($item)) {
                    throw new LocalizedException(__($item));
                }
            }
        } catch (NoSuchEntityException $e) {
            throw $e;
        } catch (LocalizedException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Could not save quote'));
        }
        $itemId = $item->getId();
        foreach ($quote->getAllItems() as $quoteItem) {
            /** @var \Appseconnect\B2BMage\Model\QuoteProduct $quoteItem */
            if ($itemId == $quoteItem->getId()) {
                return $quoteItem;
            }
        }
    }
    
    /**
     * @param \Appseconnect\B2BMage\Model\QuoteProduct $currentItem
     * @param \Appseconnect\B2BMage\Model\QuoteProduct $item
     * @param boolean $flag
     * @return void
     */
    private function updateParams($currentItem, $item, $flag)
    {
        if ($flag) {
            $currentItem->setQty($item->getQty());
        }
    }
    
    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param mixed $params
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @return mixed
     */
    private function processItemToCart($product, $params, $quote)
    {
        $productType = $product->getTypeId();
        $result = $quote->addProduct($product, $params);
        return $result;
    }
}
