<?php
namespace Appseconnect\B2BMage\Model\Quote\Product;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class Repository implements \Appseconnect\B2BMage\Api\Quotation\QuotationItemRepositoryInterface
{

    /**
     * Quotation repository.
     *
     * @var \Appseconnect\B2BMage\Api\Quotation\QuotationRepositoryInterface
     */
    public $quotationRepository;

    /**
     * Product repository.
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    public $productRepository;

    /**
     *
     * @var \Appseconnect\B2BMage\Api\Quotation\Data\QuoteProductInterfaceFactory
     */
    public $itemDataFactory;

    /**
     *
     * @param \Appseconnect\B2BMage\Api\Quotation\QuotationRepositoryInterface $quotationRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Appseconnect\B2BMage\Api\Quotation\Data\QuotationItemInterfaceFactory $itemDataFactory
     */
    public function __construct(
        \Appseconnect\B2BMage\Api\Quotation\QuotationRepositoryInterface $quotationRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Appseconnect\B2BMage\Api\Quotation\Data\QuoteProductInterfaceFactory $itemDataFactory
    ) {
    
        $this->quotationRepository = $quotationRepository;
        $this->productRepository = $productRepository;
        $this->itemDataFactory = $itemDataFactory;
    }

    /**
     *
     * {@inheritdoc}
     */
    public function getList($quoteId)
    {
        $output = [];
        /** @var  \Appseconnect\B2BMage\Model\Quote $quote */
        $quote = $this->quotationRepository->getActive($quoteId);
        
        /** @var  \Magento\Quote\Model\Quote\Item  $item */
        foreach ($quote->getAllVisibleItems() as $item) {
            $output[] = $item;
        }
        return $output;
    }

    /**
     *
     * {@inheritdoc}
     */
    public function save(\Appseconnect\B2BMage\Api\Quotation\Data\QuoteProductInterface $quoteItem)
    {
        /** @var \Appseconnect\B2BMage\Model\Quote $quote */
        $quoteId = $quoteItem->getQuoteId();
        $quote = $this->quotationRepository->getActive($quoteId);
        
        $quoteItems = $quote->getItems();
        $quoteItems[] = $quoteItem;
        
        $quote->setItems($quoteItems);
        $this->quotationRepository->save($quote);
        return $quote->getLastAddedItem();
    }

    /**
     *
     * {@inheritdoc}
     */
    public function deleteById($quoteId, $itemId)
    {
        /** @var \Appseconnect\B2BMage\Model\Quote $quote */
        $quote = $this->quotationRepository->getActive($quoteId);
        $quoteItem = $quote->getItemById($itemId);
        if (! $quoteItem) {
            throw new NoSuchEntityException(__('Quote %1 doesn\'t contain item  %2', $quoteId, $itemId));
        }
        try {
            $quote->removeItem($itemId);
            $this->quotationRepository->save($quote);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Could not remove item from quote'));
        }
        
        return true;
    }
}
