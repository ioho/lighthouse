<?php
namespace Appseconnect\B2BMage\Model\Quote\Product;

use Appseconnect\B2BMage\Model\QuoteProduct;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\AttributeValueFactory;

abstract class AbstractItem extends \Magento\Framework\Model\AbstractModel
{

    /**
     *
     * @var QuoteProduct|null
     */
    public $parentItem = null;

    /**
     *
     * @var \Appseconnect\B2BMage\Model\Quote\Product\AbstractItem[]
     */
    public $children = [];
    
    /**
     * @var ProductRepositoryInterface
     */
    public $productRepository;
    
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    public $priceCurrency;
    
    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    public $localeFormat;
    
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
    
        $this->productRepository = $productRepository;
        $this->priceCurrency = $priceCurrency;
        $this->localeFormat = $localeFormat;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Set parent item
     *
     * @param QuoteProduct $parentItem
     * @return $this
     */
    public function setParentItem($parentItem)
    {
        if ($parentItem) {
            $this->parentItem = $parentItem;
            $parentItem->addChild($this);
        }
        return $this;
    }

    /**
     * Specify parent item id before saving data
     *
     * @return $this
     */
    public function beforeSave()
    {
        parent::beforeSave();
        if ($this->getParentItem()) {
            $this->setParentItemId($this->getParentItem()
                ->getId());
        }
        return $this;
    }

    /**
     * Get parent item
     *
     * @return QuoteProduct
     */
    public function getParentItem()
    {
        return $this->parentItem;
    }

    /**
     * Get child items
     *
     * @return \Appseconnect\B2BMage\Model\Quote\Product\AbstractItem[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add child item
     *
     * @param \Appseconnect\B2BMage\Model\Quote\Product\AbstractItem $child
     * @return $this
     */
    public function addChild($child)
    {
        $this->setHasChildren(true);
        $this->children[] = $child;
        return $this;
    }
}
