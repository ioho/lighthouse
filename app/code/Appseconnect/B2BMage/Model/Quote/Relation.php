<?php
namespace Appseconnect\B2BMage\Model\Quote;

use Magento\Framework\Model\ResourceModel\Db\VersionControl\RelationInterface;

class Relation implements RelationInterface
{

    /**
     * Process object relations
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return void
     */
    public function processRelation(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($object->itemsCollectionWasSet()) {
            $object->getItemsCollection()->save();
        }
    }
}
