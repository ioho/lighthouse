<?php
namespace Appseconnect\B2BMage\Model\Quote\Email;

use Magento\Framework\Mail\Template\TransportBuilder;
use Appseconnect\B2BMage\Model\Quote\Email\Container\IdentityInterface;
use Appseconnect\B2BMage\Model\Quote\Email\Container\Template;

class SenderBuilder
{

    /**
     *
     * @var Template
     */
    public $templateContainer;

    /**
     *
     * @var IdentityInterface
     */
    public $identityContainer;

    /**
     *
     * @var TransportBuilder
     */
    public $transportBuilder;

    /**
     *
     * @param Template $templateContainer
     * @param IdentityInterface $identityContainer
     * @param TransportBuilder $transportBuilder
     */
    public function __construct(
        Template $templateContainer,
        IdentityInterface $identityContainer,
        TransportBuilder $transportBuilder
    ) {
    
        $this->templateContainer = $templateContainer;
        $this->identityContainer = $identityContainer;
        $this->transportBuilder = $transportBuilder;
    }

    /**
     * Prepare and send email message
     *
     * @return void
     */
    public function send()
    {
        $this->configureEmailTemplate();
        
        $this->transportBuilder->addTo(
            $this->identityContainer->getCustomerEmail(),
            $this->identityContainer->getCustomerName()
        );
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
    }

    /**
     * Configure email template
     *
     * @return void
     */
    public function configureEmailTemplate()
    {
        $this->transportBuilder->setTemplateIdentifier($this->templateContainer->getTemplateId());
        $this->transportBuilder->setTemplateOptions($this->templateContainer->getTemplateOptions());
        $this->transportBuilder->setTemplateVars($this->templateContainer->getTemplateVars());
        $this->transportBuilder->setFrom($this->identityContainer->getEmailIdentity());
    }
}
