<?php
namespace Appseconnect\B2BMage\Model\Quote\Email;

use Appseconnect\B2BMage\Model\Quote;

abstract class NotifySender extends Sender
{

    /**
     * Send email to customer
     *
     * @param Quote $quote
     * @param string $action
     * @param bool $notify
     * @return bool
     */
    public function checkAndSend(Quote $quote, $action = null, $notify = true)
    {
        $this->identityContainer->setStore($quote->getStore());
        if (! $this->identityContainer->isEnabled()) {
            return false;
        }
        $this->prepareTemplate($quote, $action);
        
        /** @var SenderBuilder $sender */
        $sender = $this->getSender();
        
        if ($notify) {
            $sender->send();
        } else {
            $sender->sendCopyTo();
        }
        
        return true;
    }
}
