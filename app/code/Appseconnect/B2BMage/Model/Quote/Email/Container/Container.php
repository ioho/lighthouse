<?php
namespace Appseconnect\B2BMage\Model\Quote\Email\Container;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Store;
use Magento\Framework\App\Config\ScopeConfigInterface;

abstract class Container implements IdentityInterface
{

    /**
     *
     * @var StoreManagerInterface
     */
    public $storeManager;

    /**
     * Core store config
     *
     * @var ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     *
     * @var Store
     */
    public $store;

    /**
     *
     * @var string
     */
    public $customerName;

    /**
     *
     * @var string
     */
    public $customerEmail;

    /**
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(ScopeConfigInterface $scopeConfig, StoreManagerInterface $storeManager)
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Return store configuration value
     *
     * @param string $path
     * @param int $storeId
     * @return mixed
     */
    public function getConfigValue($path, $storeId)
    {
		$configValue = $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        return $configValue;
    }

    /**
     * Set current store
     *
     * @param Store $currentStore
     * @return void
     */
    public function setStore(Store $currentStore)
    {
        $this->store = $currentStore;
    }

    /**
     * Return store
     *
     * @return Store
     */
    public function getStore()
    {
        // current store
        if ($this->store instanceof Store) {
            return $this->store;
        }
        return $this->storeManager->getStore();
    }

    /**
     * Set customer name
     *
     * @param string $customerName
     * @return void
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * Set customer email
     *
     * @param string $customerEmail
     * @return void
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }

    /**
     * Return customer name
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Return customer email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }
}
