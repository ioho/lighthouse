<?php
namespace Appseconnect\B2BMage\Model\Quote\Email\Container;

use Magento\Store\Model\Store;

interface IdentityInterface
{

    /**
     *
     * @return bool
     */
    public function isEnabled();

    /**
     *
     * @return mixed
     */
    public function getGuestTemplateId();

    /**
     *
     * @return mixed
     */
    public function getNewTemplateId();

    /**
     *
     * @return mixed
     */
    public function getApproveTemplateId();

    /**
     *
     * @return mixed
     */
    public function getHoldTemplateId();

    /**
     *
     * @return mixed
     */
    public function getUnholdTemplateId();

    /**
     *
     * @return mixed
     */
    public function getCancelTemplateId();

    /**
     *
     * @return mixed
     */
    public function getEmailIdentity();

    /**
     *
     * @return string
     */
    public function getCustomerEmail();

    /**
     *
     * @return string
     */
    public function getCustomerName();

    /**
     *
     * @return Store
     */
    public function getStore();

    /**
     *
     * @param Store $store
     * @return mixed
     */
    public function setStore(Store $store);

    /**
     *
     * @param string $email
     * @return mixed
     */
    public function setCustomerEmail($email);

    /**
     *
     * @param string $name
     * @return mixed
     */
    public function setCustomerName($name);
}
