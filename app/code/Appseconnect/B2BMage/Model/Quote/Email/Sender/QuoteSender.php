<?php
namespace Appseconnect\B2BMage\Model\Quote\Email\Sender;

use Magento\Payment\Helper\Data as PaymentHelper;
use Appseconnect\B2BMage\Model\Quote;
use Appseconnect\B2BMage\Model\Quote\Email\Container\QuoteIdentity;
use Appseconnect\B2BMage\Model\Quote\Email\Container\Template;
use Appseconnect\B2BMage\Model\Quote\Email\Sender;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Framework\Event\ManagerInterface;

class QuoteSender extends Sender
{

    /**
     *
     * @var PaymentHelper
     */
    public $paymentHelper;

    /**
     *
     * @var OrderResource
     */
    public $orderResource;

    /**
     * Global configuration storage.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $globalConfig;

    /**
     *
     * @var Renderer
     */
    public $addressRenderer;

    /**
     * Application Event Dispatcher
     *
     * @var ManagerInterface
     */
    public $eventManager;
    
    /**
     * @var \Magento\Framework\DataObject\Factory
     */
    public $objectFactory;

    /**
     * @param Template $templateContainer
     * @param \Magento\Framework\DataObject\Factory $objectFactory
     * @param QuoteIdentity $identityContainer
     * @param \Appseconnect\B2BMage\Model\Quote\Email\SenderBuilderFactory $senderBuilderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param Renderer $addressRenderer
     * @param PaymentHelper $paymentHelper
     * @param OrderResource $orderResource
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        Template $templateContainer,
        \Magento\Framework\DataObject\Factory $objectFactory,
        QuoteIdentity $identityContainer,
        \Appseconnect\B2BMage\Model\Quote\Email\SenderBuilderFactory $senderBuilderFactory,
        \Psr\Log\LoggerInterface $logger,
        Renderer $addressRenderer,
        PaymentHelper $paymentHelper,
        OrderResource $orderResource,
        \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
        ManagerInterface $eventManager
    ) {
    
        parent::__construct(
            $templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer
        );
        $this->paymentHelper = $paymentHelper;
        $this->objectFactory = $objectFactory;
        $this->orderResource = $orderResource;
        $this->globalConfig = $globalConfig;
        $this->addressRenderer = $addressRenderer;
        $this->eventManager = $eventManager;
    }

    /**
     * Sends order email to the customer.
     *
     * Email will be sent immediately in two cases:
     *
     * - if asynchronous email sending is disabled in global settings
     * - if $forceSyncMode parameter is set to TRUE
     *
     * Otherwise, email will be sent later during running of
     * corresponding cron job.
     *
     * @param Quote $quote
     * @param string $action
     * @param bool $forceSyncMode
     * @return bool
     */
    public function send(Quote $quote, $action = null)
    {
        $quote->setSendEmail(true);
        if ($this->checkAndSend($quote, $action)) {
            $quote->setEmailSent(true);
            return true;
        }
        
        return false;
    }

    /**
     * Prepare email template with variables
     *
     * @param string $action
     * @param Quote $quote
     * @return void
     */
    public function prepareTemplate(Quote $quote, $action = null)
    {
        $transport = [
            'quote' => $quote,
            'store' => $quote->getStore()
        ];
        $transport = $this->objectFactory->create($transport);
        
        $this->templateContainer->setTemplateVars($transport->getData());
        
        parent::prepareTemplate($quote, $action);
    }

    /**
     *
     * @param Order $order
     * @return string
     */
    public function getPaymentHtml(Order $order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->identityContainer->getStore()
            ->getStoreId()
        );
    }
}
