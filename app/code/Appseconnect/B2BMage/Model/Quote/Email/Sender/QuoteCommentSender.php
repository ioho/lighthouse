<?php
namespace Appseconnect\B2BMage\Model\Quote\Email\Sender;

use Appseconnect\B2BMage\Model\Quote;
use Appseconnect\B2BMage\Model\Quote\Email\Container\QuoteCommentIdentity;
use Appseconnect\B2BMage\Model\Quote\Email\Container\Template;
use Appseconnect\B2BMage\Model\Quote\Email\NotifySender;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Framework\Event\ManagerInterface;

class QuoteCommentSender extends NotifySender
{

    /**
     *
     * @var Renderer
     */
    public $addressRenderer;

    /**
     * Application Event Dispatcher
     *
     * @var ManagerInterface
     */
    public $eventManager;

    /**
     *
     * @param Template $templateContainer
     * @param QuoteCommentIdentity $identityContainer
     * @param Quote\Email\SenderBuilderFactory $senderBuilderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param Renderer $addressRenderer
     * @param ManagerInterface $eventManager
     */
    public function __construct(
        Template $templateContainer,
        QuoteCommentIdentity $identityContainer,
        \Appseconnect\B2BMage\Model\Quote\Email\SenderBuilderFactory $senderBuilderFactory,
        \Psr\Log\LoggerInterface $logger,
        Renderer $addressRenderer,
        ManagerInterface $eventManager
    ) {
    
        parent::__construct(
            $templateContainer,
            $identityContainer,
            $senderBuilderFactory,
            $logger,
            $addressRenderer
        );
        $this->addressRenderer = $addressRenderer;
        $this->eventManager = $eventManager;
    }

    /**
     * Send email to customer
     *
     * @param Quote $quote
     * @param string $action
     * @param string $commentProvider
     * @param bool $notify
     * @param string $comment
     * @return bool
     */
    public function send(
        Quote $quote,
        $action = null,
        $commentProvider = null,
        $notify = true,
        $comment = ''
    ) {
    
        $transport = [
            'quote' => $quote,
            'comment' => $comment,
            'comment_provider' => $commentProvider,
            'store' => $quote->getStore()
        ];
        
        $this->templateContainer->setTemplateVars($transport);
        
        return $this->checkAndSend($quote, $action, $notify);
    }
}
