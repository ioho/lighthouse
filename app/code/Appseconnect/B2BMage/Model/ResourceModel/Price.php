<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Price extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_pricelist', 'id');
    }
    
    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @param int $pricelistId
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function filter($collection, $pricelistId)
    {
        $collection->getSelect()->join([
            'pricelist_map' =>
            $this->_resources->getTableName('insync_product_pricelist_map')
        ], 'pricelist_map.product_id = e.entity_id', [
            'pricelist_id'
        ]);
        
        $collection->getSelect()
        ->where("pricelist_map.pricelist_id = " . $pricelistId);
        
        return $collection;
    }
}
