<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class QuoteStatus extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_quotation_status', 'status');
    }
}
