<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Specialprice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_specialprice_map', 'id');
    }

    /**
     * @param int $id
     * @return boolean
     */
    public function removeMapping($id)
    {
        $connection = $this->getConnection();
        
        $connection->delete($this->_resources->getTableName('insync_specialprice_map'), [
            'parent_id = ?' => $id
        ]);
        
        return true;
    }
}
