<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Credit extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_credit_transaction', 'id');
    }
}
