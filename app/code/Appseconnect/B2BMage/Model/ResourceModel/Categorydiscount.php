<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Categorydiscount extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_categorydiscount', 'categorydiscount_id');
    }
}
