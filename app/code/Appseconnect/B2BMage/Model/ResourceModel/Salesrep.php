<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Salesrep extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_salesrepresentative', 'id');
    }
    
    /**
     * @param \Magento\Customer\Model\ResourceModel\Customer\Collection $collection
     * @param int $result
     * @return \Magento\Customer\Model\ResourceModel\Customer\Collection
     */
    public function getJoinSalesRepData($collection, $result)
    {
        $collection->getSelect()
        ->join([
            'customer' => $this->_resources->getTableName('insync_salesrepresentative')
        ], 'e.entity_id = customer.customer_id', [
            'customer.salesrep_id'
        ])
        ->where("salesrep_grid.salesrep_customer_id	 = ?", $result)
        ->join([
            'salesrep_grid' => $this->_resources->getTableName('insync_salesrep_grid')
        ], 'customer.salesrep_id = salesrep_grid.id', [
            'salesrep_grid.salesrep_customer_id'
        ]);
        return $collection;
    }
}
