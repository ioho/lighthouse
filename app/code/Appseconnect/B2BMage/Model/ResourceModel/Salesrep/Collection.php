<?php
namespace Appseconnect\B2BMage\Model\ResourceModel\Salesrep;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    
    /**
     *
     * @var string
     */
    public $idFieldName = 'id';
    
    /**
     * Define resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Appseconnect\B2BMage\Model\Salesrep',
            'Appseconnect\B2BMage\Model\ResourceModel\Salesrep'
        );
        $this->_map ['fields'] ['id'] = 'main_table.id';
    }
}
