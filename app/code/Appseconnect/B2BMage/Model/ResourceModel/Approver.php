<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

use Magento\Customer\Model\ResourceModel\Customer\Collection as CustomerCollection;

class Approver extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_approver', 'insync_approver_id');
    }

    /**
     * @param int $customerId
     * @param CustomerCollection $customerCollection
     * @return \Magento\Customer\Model\ResourceModel\Customer\Collection
     */
    public function getContacts($customerId, $customerCollection)
    {
        $customerCollection->getSelect()
            ->where("customer.customer_id = ?", $customerId)
            ->join([
            'customer' => $this->_resources->getTableName('insync_contactperson')
            ], 'customer.contactperson_id = e.entity_id', [
            'contactperson_id'
            ]);
        return $customerCollection;
    }
}
