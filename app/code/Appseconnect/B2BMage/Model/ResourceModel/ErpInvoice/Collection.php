<?php
namespace Appseconnect\B2BMage\Model\ResourceModel\ErpInvoice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    public $idFieldName = 'insync_erp_invoice_id';

    /**
     * Define resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Appseconnect\B2BMage\Model\ErpInvoice',
            'Appseconnect\B2BMage\Model\ResourceModel\ErpInvoice'
        );
        $this->_map['fields']['insync_erp_invoice_id'] = 'main_table.insync_erp_invoice_id';
    }

    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
}
