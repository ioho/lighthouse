<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Salesrepgrid extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_salesrep_grid', 'id');
    }

    /**
     * @param \Magento\Customer\Model\ResourceModel\Customer\Collection $collection
     * @param int $customerId
     * @return \Magento\Customer\Model\ResourceModel\Customer\Collection
     */
    public function getSalesRepCustomers($collection, $customerId)
    {
        $collection->getSelect()
            ->where("customergrid.salesrep_customer_id = ?", $customerId)
            ->join([
            'customer' =>
                $this->_resources->getTableName('insync_salesrepresentative')
            ], 'e.entity_id = customer.customer_id', [
            'salesrep_id'
            ])
            ->join([
            'customergrid' =>
                $this->_resources->getTableName('insync_salesrep_grid')
            ], 'customer.salesrep_id = customergrid.id', [
            'salesrep_customer_id'
            ]);
        
        return $collection;
    }
}
