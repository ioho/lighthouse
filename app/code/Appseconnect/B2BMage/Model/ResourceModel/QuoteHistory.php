<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class QuoteHistory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_quotation_status_history', 'entity_id');
    }
}
