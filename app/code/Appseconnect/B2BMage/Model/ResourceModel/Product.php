<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Product extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_customer_tierprice', 'id');
    }

    /**
     * @param \Appseconnect\B2BMage\Model\ResourceModel\Product\Collection $collection
     * @param mixed $productSku
     * @param int $qtyItem
     * @return \Appseconnect\B2BMage\Model\ResourceModel\Product\Collection
     */
    public function getAssignedProducts($collection, $productSku, $qtyItem)
    {
        $collection->getSelect()
            ->where("map.product_sku = ?", $productSku)
            ->where("map.quantity <= ?", $qtyItem)
            ->order('map.quantity  DESC')
            ->join([
            'map' => $this->_resources->getTableName('insync_tierprice_map')
            ], 'main_table.id = map.parent_id', [
            'parent_id' => 'parent_id',
            'quantity' => 'quantity',
            'tier_price' => 'tier_price'
            ])
            ->limit(1);
        return $collection;
    }
}
