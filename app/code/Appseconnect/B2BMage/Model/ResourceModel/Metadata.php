<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

use Magento\Framework\App\ObjectManager;

class Metadata
{

    /**
     *
     * @var string
     */
    public $resourceClassName;

    /**
     *
     * @var string
     */
    public $modelClassName;

    /**
     *
     * @param string $resourceClassName
     * @param string $modelClassName
     */
    public function __construct($resourceClassName, $modelClassName)
    {
        $this->resourceClassName = $resourceClassName;
        $this->modelClassName = $modelClassName;
    }

    /**
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\AbstractDb
     */
    public function getMapper()
    {
        return ObjectManager::getInstance()->get($this->resourceClassName);
    }

    /**
     *
     * @return \Magento\Framework\Api\ExtensibleDataInterface
     */
    public function getNewInstance()
    {
        return ObjectManager::getInstance()->create($this->modelClassName);
    }
}
