<?php
namespace Appseconnect\B2BMage\Model\ResourceModel\Categorydiscount;

use Magento\Catalog\Model\ProductFactory;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     *
     * @var string
     */
    public $idFieldName = 'categorydiscount_id';
    
    /**
     *
     * @var ProductFactory
     */
    private $productFactory;
    
    /**
     *
     * @var \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory
     */
    public $attributeFactory;
    
    /**
     * @param ProductFactory $productFactory
     * @param \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource
     */
    public function __construct(
        ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
            $this->productFactory = $productFactory;
            $this->attributeFactory = $attributeFactory;
            parent::__construct(
                $entityFactory,
                $logger,
                $fetchStrategy,
                $eventManager,
                $connection,
                $resource
            );
    }

    /**
     * Define resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Appseconnect\B2BMage\Model\Categorydiscount',
            'Appseconnect\B2BMage\Model\ResourceModel\Categorydiscount'
        );
        $this->_map['fields']['categorydiscount_id'] = 'main_table.categorydiscount_id';
    }

    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [
            self::STATUS_ENABLED => __('Enabled'),
            self::STATUS_DISABLED => __('Disabled')
        ];
    }
    /**
     * @param mixed $filterGroups
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getAttributeFilter($filterGroups)
    {
        $collection = $this->productFactory->create()->getCollection();
        $collection->joinTable(
            $this->_resources->getTableName('cataloginventory_stock_item'),
            'product_id=entity_id',
            ['*'],
            null,
            'left'
        );
        foreach ($filterGroups as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $conditionType = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = ['attribute' => $filter->getField(), $conditionType => $filter->getValue()];
            }
        }
        foreach ($fields as $fieldValue) {
            $attr = $this->attributeFactory->create()
            ->loadByCode($entity, $fieldValue['attribute']);
            
            if ($attr->getId()) {
                $collection->addAttributeToFilter($fieldValue['attribute'], $fieldValue['eq']);
            } else {
                $collection->getSelect()->where("".$fieldValue['attribute']."=".$fieldValue['eq']);
            }
        }
        return $collection;
    }
}
