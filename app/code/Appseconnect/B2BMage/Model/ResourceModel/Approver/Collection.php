<?php
namespace Appseconnect\B2BMage\Model\ResourceModel\Approver;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    public $idFieldName = 'insync_approver_id';

    /**
     * Define resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Appseconnect\B2BMage\Model\Approver',
            'Appseconnect\B2BMage\Model\ResourceModel\Approver'
        );
        $this->_map['fields']['insync_approver_id'] = 'main_table.insync_approver_id';
    }

    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
}
