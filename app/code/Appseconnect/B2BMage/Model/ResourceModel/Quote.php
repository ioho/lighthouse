<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\VersionControl\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\VersionControl\RelationComposite;
use Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot;
use Magento\SalesSequence\Model\Manager;

class Quote extends AbstractDb
{

    /**
     *
     * @var \Magento\SalesSequence\Model\Manager
     */
    public $sequenceManager;

    /**
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param Snapshot $entitySnapshot,
     * @param RelationComposite $entityRelationComposite,
     * @param \Magento\SalesSequence\Model\Manager $sequenceManager
     * @param string $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        Snapshot $entitySnapshot,
        RelationComposite $entityRelationComposite,
        Manager $sequenceManager,
        $connectionName = null
    ) {
    
        parent::__construct($context, $entitySnapshot, $entityRelationComposite, $connectionName);
        $this->sequenceManager = $sequenceManager;
    }

    /**
     * Initialize table nad PK name
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_customer_quote', 'id');
    }

    /**
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    public function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if (! $object->getId()) {
            /** @var \Magento\Store\Model\Store $store */
            $store = $object->getStore();
            $name = [
                $store->getWebsite()->getName(),
                $store->getGroup()->getName(),
                $store->getName()
            ];
            $object->setStoreName(implode(PHP_EOL, $name));
        }
        
        $isNewCustomer = ! $object->getCustomerId() || $object->getCustomerId() === true;
        if ($isNewCustomer && $object->getCustomer()) {
            $object->setCustomerId($object->getCustomer()
                ->getId());
        }
        return parent::_beforeSave($object);
    }

    /**
     * Load quote data by contact identifier
     *
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @param int $contactId
     * @return $this
     */
    public function loadByContactId($quote, $contactId)
    {
        $connection = $this->getConnection();
        $select = $this->_getLoadSelect('contact_id', $contactId, $quote)
            ->where('status = ?', 'open')
            ->limit(1);
        
        $data = $connection->fetchRow($select);
        
        if ($data) {
            $quote->setData($data);
        }
        
        return $this;
    }
}
