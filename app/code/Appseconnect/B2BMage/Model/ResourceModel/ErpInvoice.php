<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class ErpInvoice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_erp_invoice', 'insync_erp_invoice_id');
    }
}
