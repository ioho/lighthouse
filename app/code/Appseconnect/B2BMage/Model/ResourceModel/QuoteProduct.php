<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class QuoteProduct extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_customer_quote_product', 'id');
    }
}
