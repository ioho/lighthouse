<?php
namespace Appseconnect\B2BMage\Model\ResourceModel\Salesrepgrid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    
    /**
     *
     * @var string
     */
    public $idFieldName = 'id';
    
    /**
     * Define resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Appseconnect\B2BMage\Model\Salesrepgrid',
            'Appseconnect\B2BMage\Model\ResourceModel\Salesrepgrid'
        );
        $this->_map ['fields'] ['id'] = 'main_table.id';
    }
}
