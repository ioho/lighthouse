<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Customer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_customer_specialprice', 'id');
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @param int $specialPriceId
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function filter($collection, $specialPriceId)
    {
        $collection->getSelect()->join([
            'specialprice_map' =>
            $this->_resources->getTableName('insync_specialprice_map')
        ], 'specialprice_map.product_sku = e.sku', [
            'parent_id'
        ]);
        
        $collection->getSelect()->where("specialprice_map.parent_id = " . $specialPriceId);
        
        return $collection;
    }

    /**
     * @param int $productSku
     * @param \Appseconnect\B2BMage\Model\ResourceModel\Customer\Collection $collection
     * @return \Appseconnect\B2BMage\Model\ResourceModel\Customer\Collection
     */
    public function addProductMapCollection($productSku, $collection)
    {
        $collection->getSelect()
            ->where("specialprice.product_sku = ?", $productSku)
            ->join([
            'specialprice' =>
                $this->_resources->getTableName('insync_specialprice_map')
            ], 'main_table.id = specialprice.parent_id', [
            'special_price',
            'product_sku'
            ]);
            
        return $collection;
    }
}
