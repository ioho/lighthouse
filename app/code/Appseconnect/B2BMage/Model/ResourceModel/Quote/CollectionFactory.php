<?php
namespace Appseconnect\B2BMage\Model\ResourceModel\Quote;

use Magento\Framework\App\ObjectManager;

/**
 * Class CollectionFactory
 */
class CollectionFactory implements CollectionFactoryInterface
{

    /**
     * Instance name to create
     *
     * @var string
     */
    private $instanceName = null;

    /**
     * Factory constructor
     *
     * @param string $instanceName
     */
    public function __construct(
        $instanceName = '\\Appseconnect\\B2BMage\\Model\\ResourceModel\\Quote\\Collection'
    ) {
        $this->instanceName = $instanceName;
    }

    /**
     *
     * {@inheritdoc}
     */
    public function create($customerId = null, $contactPersonId = null)
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $collection */
        $collection = ObjectManager::getInstance()->create($this->instanceName);
        
        if ($customerId && $contactPersonId) {
            $collection->addFieldToFilter('customer_id', $customerId)
                       ->addFieldToFilter('contact_id', $contactPersonId);
        } elseif ($customerId) {
            $collection->addFieldToFilter('customer_id', $customerId);
        } elseif ($contactPersonId) {
            $collection->addFieldToFilter('contact_id', $contactPersonId);
        }
        
        return $collection;
    }
}
