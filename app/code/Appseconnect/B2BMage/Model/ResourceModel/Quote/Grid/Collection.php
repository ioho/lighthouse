<?php
namespace Appseconnect\B2BMage\Model\ResourceModel\Quote\Grid;

use Magento\Framework\Api\Search\SearchResultInterface;
use Appseconnect\B2BMage\Model\ResourceModel\Quote\Collection as QuoteCollection;
use Magento\Framework\Search\AggregationInterface;

class Collection extends QuoteCollection implements SearchResultInterface
{

    /**
     *
     * @var AggregationInterface
     */
    public $aggregations;
    
    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    public $eventPrefix = '';
    
    /**
     * Name of event parameter
     *
     * @var string
     */
    public $eventObject = '';

    /**
     *
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param mixed|null $mainTable
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $eventPrefix
     * @param mixed $eventObject
     * @param mixed $resourceModel
     * @param string $model
     * @param null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        $mainTable,
        $eventPrefix,
        $eventObject,
        $resourceModel,
        $model = 'Magento\Framework\View\Element\UiComponent\DataProvider\Document',
        $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
    
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $connection,
            $resource
        );
        $this->eventPrefix = $eventPrefix;
        $this->eventObject = $eventObject;
        $this->_init($model, $resourceModel);
        $this->setMainTable($mainTable);
    }

    /**
     *
     * @return AggregationInterface
     */
    public function getAggregations()
    {
        $aggregations = $this->aggregations;
        return $aggregations;
    }

    /**
     *
     * @param AggregationInterface $aggregationsValue
     * @return $this
     */
    public function setAggregations($aggregationsValue)
    {
        $this->aggregations = $aggregationsValue;
    }

    /**
     * Retrieve all ids for collection
     * Backward compatibility with EAV collection
     *
     * @param int $limitValue
     * @param int $offsetValue
     * @return array
     */
    public function getAllIds($limitValue = null, $offsetValue = null)
    {
        return $this->getConnection()->fetchCol(
            $this->_getAllIdsSelect($limitValue, $offsetValue),
            $this->_bindParams
        );
    }

    /**
     * Get search criteria.
     *
     * @return \Magento\Framework\Api\SearchCriteriaInterface|null
     */
    public function getSearchCriteria()
    {
        $value = null;
        return $value;
    }

    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return $this @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setSearchCriteria(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null
    ) {
        $currentObject = $this;
        return $currentObject;
    }

    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        $totalSize = $this->getSize();
        return $totalSize;
    }

    /**
     * Set total count.
     *
     * @param int $totalCount
     * @return $this @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setTotalCount($totalCount)
    {
        $currentObject = $this;
        return $currentObject;
    }

    /**
     * Set items list.
     *
     * @param \Magento\Framework\Api\ExtensibleDataInterface[] $items
     * @return $this @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setItems(array $items = null)
    {
        $currentObject = $this;
        return $currentObject;
    }
}
