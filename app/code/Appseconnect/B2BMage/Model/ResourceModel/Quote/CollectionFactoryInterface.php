<?php
namespace Appseconnect\B2BMage\Model\ResourceModel\Quote;

interface CollectionFactoryInterface
{

    /**
     * Create class instance with specified parameters
     *
     * @param int $customerId
     * @return \Appseconnect\B2BMage\Model\ResourceModel\Quote\Collection
     */
    public function create($customerId = null);
}
