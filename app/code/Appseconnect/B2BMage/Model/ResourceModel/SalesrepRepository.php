<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Customer\Model\Customer\NotificationStorage;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ImageProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Appseconnect\B2BMage\Api\Salesrep\SalesrepRepositoryInterface;
use Appseconnect\B2BMage\Model\ResourceModel\SalesrepFactory as SalesrepResourceFactory;

class SalesrepRepository implements SalesrepRepositoryInterface
{
    
    /**
     * @var \Magento\Customer\Model\AccountManagement
     */
    public $accountManager;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\Salesrep\Data
     */
    public $helperSalesrep;
    
    /**
     * @var \Appseconnect\B2BMage\Model\SalesrepgridFactory
     */
    public $salesrepGridFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\SalesrepFactory
     */
    public $salesrepModelFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\ResourceModel\Salesrep\CollectionFactory
     */
    public $salesrepCollectionFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var \Appseconnect\B2BMage\Model\SalesrepProcessor
     */
    public $salesrepProcessor;
    
    /**
     * @var SalesrepResourceFactory
     */
    public $salesrepResourceModelFactory;
    
    /**
     * @var \Magento\Customer\Api\Data\CustomerSearchResultsInterfaceFactory
     */
    public $searchResultsFactory;
    
    /**
     * @var ImageProcessorInterface
     */
    public $imageProcessor;
    
    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    public $extensibleDataObjectConverter;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;
    
    /**
     * @var CustomerRepository
     */
    public $customerRepository;
    
    /**
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    public $customerRegistry;
    
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    public $eventManager;
    
    /**
     * @param CustomerRepository $customerRepository
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Model\SalesrepProcessor $salesrepProcessor
     * @param \Magento\Customer\Model\AccountManagement $accountManager
     * @param \Appseconnect\B2BMage\Model\SalesrepgridFactory $salesrepGridFactory
     * @param \Appseconnect\B2BMage\Helper\Salesrep\Data $helperSalesrep
     * @param \Appseconnect\B2BMage\Model\SalesrepFactory $salesrepModelFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Model\ResourceModel\Salesrep\CollectionFactory $salesrepCollectionFactory
     * @param \Magento\Customer\Model\CustomerRegistry $customerRegistry
     * @param \Magento\Customer\Api\Data\CustomerSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param ImageProcessorInterface $imageProcessor
     * @param SalesrepResourceFactory $salesrepResourceModelFactory
     */
    public function __construct(
        CustomerRepository $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Model\SalesrepProcessor $salesrepProcessor,
        \Magento\Customer\Model\AccountManagement $accountManager,
        \Appseconnect\B2BMage\Model\SalesrepgridFactory $salesrepGridFactory,
        \Appseconnect\B2BMage\Helper\Salesrep\Data $helperSalesrep,
        \Appseconnect\B2BMage\Model\SalesrepFactory $salesrepModelFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Model\ResourceModel\Salesrep\CollectionFactory $salesrepCollectionFactory,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        \Magento\Customer\Api\Data\CustomerSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        ImageProcessorInterface $imageProcessor,
        SalesrepResourceFactory $salesrepResourceModelFactory
    ) {
    
        $this->accountManager = $accountManager;
        $this->helperSalesrep = $helperSalesrep;
        $this->salesrepGridFactory = $salesrepGridFactory;
        $this->salesrepModelFactory = $salesrepModelFactory;
        $this->salesrepCollectionFactory = $salesrepCollectionFactory;
        $this->helperContactPerson = $helperContactPerson;
        $this->salesrepProcessor = $salesrepProcessor;
        $this->salesrepResourceModelFactory = $salesrepResourceModelFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->imageProcessor = $imageProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->customerFactory = $customerFactory;
        $this->storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->customerRegistry = $customerRegistry;
        $this->eventManager = $eventManager;
    }

    /**
     * {@inheritdoc}
     */
    public function createAccount(CustomerInterface $salesrepData, $password = null, $redirectUrl = '')
    {
        if ($password !== null) {
            $this->accountManager->checkPasswordStrength($password);
            $hash = $this->accountManager->createPasswordHash($password);
        } else {
            $hash = null;
        }
        $salesrepData->setCustomAttribute('customer_type', 2);
        if ($salesrepData->getCustomAttribute('customer_status')) {
            $salesrepData->setCustomAttribute(
                'customer_status',
                $salesrepData->getCustomAttribute('customer_status')
                ->getValue()
            );
        }
        $customer = $this->accountManager->createAccountWithPasswordHash($salesrepData, $hash, $redirectUrl);
        if ($customer->getId()) {
            $salesrepId = $customer->getId();
            $salesrepModel = $this->salesrepGridFactory->create();
            $originalRequestData['name'] =
            $customer->getFirstname() . " " . $customer->getMiddlename() . " " . $customer->getLastname();
            $originalRequestData['email'] = $customer->getEmail();
            $originalRequestData['salesrep_customer_id'] = $customer->getId();
            $originalRequestData['website_id'] = $customer->getWebsiteId();
            $originalRequestData['gender'] = $customer->getGender();
            $originalRequestData['is_active'] = $customer->getCustomAttribute('customer_status')->getValue();
            $salesrepModel->setData($originalRequestData);
            $salesrepModel->save();
        }
        return $customer;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magento\Customer\Api\Data\CustomerInterface $salesrepData, $passwordHash = null)
    {
        $salesrepCustomerId = $salesrepData->getId();
        $salesrepIdExist = $this->salesrepGridFactory->create()
            ->getCollection()
            ->addFieldToFilter('salesrep_customer_id', $salesrepCustomerId)
            ->getData() ? true : false;
        if (! $salesrepIdExist) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __("Salesrep ID doesn't exist", $salesrepCustomerId)
            );
        }
        if ($salesrepData->getCustomAttribute('customer_status')) {
            $salesrepData->setCustomAttribute('customer_status', $salesrepData->getCustomAttribute('customer_status')
                ->getValue());
        }
        if (($salesrepData->getCustomAttribute('customer_type') != null)) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __("[customer_type] cannot be changed")
            );
        }
        $salesrepData->setId($salesrepCustomerId);
        $prevCustomerData = null;
        if ($salesrepData->getId()) {
            $prevCustomerData = $this->customerRepository->getById($salesrepData->getId());
        }
        $salesrepData = $this->imageProcessor
        ->save($salesrepData, CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER, $prevCustomerData);
        $origAddresses = $salesrepData->getAddresses();
        $salesrepData->setAddresses([]);
        $customerData = $this->extensibleDataObjectConverter->
        toNestedArray($salesrepData, [], '\Magento\Customer\Api\Data\CustomerInterface');
        $salesrepData->setAddresses($origAddresses);
        $customerModel = $this->customerFactory->create([
            'data' => $customerData
        ]);
        $storeId = $customerModel->getStoreId();
        if ($storeId === null) {
            $customerModel->setStoreId($this->storeManager->getStore()
                ->getId());
        }
        $customerModel->setId($salesrepData->getId());
        
        // Need to use attribute set or future updates can cause data loss
        if (! $customerModel->getAttributeSetId()) {
            $customerModel->setAttributeSetId(CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER);
        }
        // Populate model with secure data
        if ($salesrepData->getId()) {
            $customerSecure = $this->customerRegistry->retrieveSecureData($salesrepData->getId());
            $customerModel->setRpToken($customerSecure->getRpToken());
            $customerModel->setRpTokenCreatedAt($customerSecure->getRpTokenCreatedAt());
            $customerModel->setPasswordHash($customerSecure->getPasswordHash());
            $customerModel->setFailuresNum($customerSecure->getFailuresNum());
            $customerModel->setFirstFailure($customerSecure->getFirstFailure());
            $customerModel->setLockExpires($customerSecure->getLockExpires());
        } else {
            if ($passwordHash) {
                $customerModel->setPasswordHash($passwordHash);
            }
        }
        
        // If customer email was changed, reset RpToken info
        if ($prevCustomerData && $prevCustomerData->getEmail() !== $customerModel->getEmail()) {
            $customerModel->setRpToken(null);
            $customerModel->setRpTokenCreatedAt(null);
        }
        $customerModel->save();
        $this->customerRegistry->push($customerModel);
        $customerId = $customerModel->getId();
        
        $savedCustomer = $this->customerRepository->get($salesrepData->getEmail(), $salesrepData->getWebsiteId());
        $this->eventManager->dispatch('customer_save_after_data_object', [
            'customer_data_object' => $savedCustomer,
            'orig_customer_data_object' => $salesrepData
        ]);
        $this->addSalesrepResentative($savedCustomer, $salesrepCustomerId);
        return $savedCustomer;
    }

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $savedCustomer
     * @param int $salesrepCustomerId
     */
    public function addSalesRepresentative($savedCustomer, $salesrepCustomerId)
    {
        if ($savedCustomer->getId()) {
            $salesrepModel = $this->salesrepGridFactory->create();
            $selsrepData = $this->helperSalesrep->isSalesrep($salesrepCustomerId, true);
            $salesrepId = $selsrepData[0]['id'];
            $originalRequestData['id'] = $salesrepId;
            $originalRequestData['website_id'] = $savedCustomer->getWebsiteId();
            $savedCustFname = $savedCustomer->getFirstname();
            $savedCustMname = $savedCustomer->getMiddlename();
            $savedCustLname = $savedCustomer->getLastname();
            $savedCustFullName = $savedCustFname . " " . $savedCustMname . " " . $savedCustLname;
            $originalRequestData['name'] =$savedCustFullName;
            $originalRequestData['salesrep_customer_id'] = $savedCustomer->getId();
            $originalRequestData['is_active'] = $savedCustomer->getCustomAttribute('customer_status')->getValue();
            $originalRequestData['email'] = $savedCustomer->getEmail();
            $originalRequestData['gender'] = $savedCustomer->getGender();
            $salesrepModel->setData($originalRequestData);
            $salesrepModel->save();
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function assignCustomer(\Appseconnect\B2BMage\Api\Salesrep\Data\SalesrepCustomerAssignInterface $requestData)
    {
        $salesrepCustomerMapModel = $this->salesrepModelFactory->create();
        $requestDataArray = $this->extensibleDataObjectConverter
        ->toNestedArray($requestData, [], 'Appseconnect\B2BMage\Api\Salesrep\Data\SalesrepCustomerAssignInterface');
        if (! isset($requestDataArray['salesrep_id'])) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__("[salesrep_id] is required field"));
        }
        $selsrepData = $this->helperSalesrep->isSalesrep($requestDataArray['salesrep_id'], true);
        $selsrepId = $requestDataArray['salesrep_id'];
        $requestDataArray['salesrep_id'] = $selsrepData[0]['id'];
        if (! ($this->salesrepGridFactory->create()
            ->load($requestDataArray['salesrep_id'])
            ->getId())) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __("Salesrep id doesn't exist", $requestDataArray['salesrep_id'])
            );
        } elseif (empty($requestDataArray['customer_ids'])) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                __("[customer_ids] is required field")
            );
        }
        $data = [];
        $return = [];
        $output = $this->salesrepProcessor->process($requestDataArray);
        return $output;
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getCustomerData(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        /** @var \Magento\Customer\Model\ResourceModel\Customer\Collection $collection */
        $collection = $this->customerFactory->create()->getCollection();
        $collection->addAttributeToSelect('id');
        // Add filters from root filter group to the collection
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $result = $this->addFilterCustomerData($group, $collection);
        }
        // join Salesrep filter
        if ($result) {
            $collection = $this->salesrepResourceModelFactory->create()->getJoinSalesRepData(
                $collection,
                $result
            );
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection
                ->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $customers = [];
        /** @var \Magento\Customer\Model\Customer $customerModel */
        foreach ($collection as $customerModel) {
            $customers[] = $customerModel->getDataModel();
        }
        $searchResults->setItems($customers);
        return $searchResults;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Magento\Customer\Model\ResourceModel\Customer\Collection $collection
     * @return mixed
     * @throws \Magento\Framework\Exception\InputException
     */
    public function addFilterCustomerData(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Customer\Model\ResourceModel\Customer\Collection $collection
    ) {
    
        $salesrepId = '';
        $fields = [];
        foreach ($filterGroup->getFilters() as $filter) {
            if (trim($filter->getField()) == 'salesrep_id') {
                $salesrepId = $filter->getValue();
                $salesrepCollection = $this->helperSalesrep->isSalesrep($salesrepId);
                if (! $salesrepCollection) {
                    throw new \Magento\Framework\Exception\NoSuchEntityException(
                        __("Request salesrep doesn't exist", $salesrepId)
                    );
                }
                continue;
            }
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = [
                'attribute' => $filter->getField(),
                $condition => $filter->getValue()
            ];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields);
        }
        return $salesrepId;
    }
}
