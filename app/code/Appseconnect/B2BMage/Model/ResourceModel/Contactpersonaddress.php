<?php
namespace Appseconnect\B2BMage\Model\ResourceModel;

class Contactpersonaddress extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('insync_contact_address', 'insync_contact_address_id');
    }
}
