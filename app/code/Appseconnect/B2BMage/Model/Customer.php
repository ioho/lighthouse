<?php
namespace Appseconnect\B2BMage\Model;

use Appseconnect\B2BMage\Model\ResourceModel\Customer\CollectionFactory;

class Customer extends \Magento\Framework\Model\AbstractModel
{

    /**
     * @var CollectionFactory
     */
    private $specialPriceCollectionFactory;

    /**
     * Customer constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param CollectionFactory $specialPriceCollectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        CollectionFactory $specialPriceCollectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->specialPriceCollectionFactory = $specialPriceCollectionFactory;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Appseconnect\B2BMage\Model\ResourceModel\Customer');
    }

    /**
     * @param $data
     * @return bool
     */
    public function isCustomerAlreadyAssigned($data)
    {
        $collection = $this->specialPriceCollectionFactory->create();
        $collection->addFieldToFilter('customer_id', $data['customer_id']);
        if (isset($data['id']) && !empty($data['id'])) {
            $collection->addFieldToFilter('id', ['nin' => $data['id']]);
        }
        $collection->getSelect()
        ->where('main_table.start_date = "'.$data['start_date']. '" or
                main_table.end_date = "'.$data['end_date'].'"');
        if ($collection->getData()) {
            return true;
        }
        return false;
    }
}
