<?php
namespace Appseconnect\B2BMage\Model\Service;

class QuotationService implements \Appseconnect\B2BMage\Api\Quotation\QuotationServiceInterface
{
    
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    public $eventManager;
    
    /**
     * @var \Appseconnect\B2BMage\Api\Quotation\QuotationRepositoryInterface
     */
    public $quotationRepository;

    /**
     * QuotationService constructor.
     *
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Appseconnect\B2BMage\Model\QuotationRepositoryInterface $quotationRepository
     */
    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Appseconnect\B2BMage\Api\Quotation\QuotationRepositoryInterface $quotationRepository
    ) {
    
        $this->eventManager = $eventManager;
        $this->quotationRepository = $quotationRepository;
    }

    public function submitQuoteById($id)
    {
        $quote = $this->quotationRepository->get($id);
        if ($quote->submit()) {
            $quote->setUpdatedAt(date('Y-m-d h:i:s'));
            $this->quotationRepository->save($quote);
            
            $this->eventManager->dispatch('sales_quotation_process_after', [
                'quote' => $quote,
                'action' => 'submit'
            ]);
            
            return true;
        }
        return false;
    }

    public function approveQuoteById($id)
    {
        $quote = $this->quotationRepository->get($id);
        if ($quote->approve()) {
            $quote->setUpdatedAt(date('Y-m-d h:i:s'));
            $this->quotationRepository->save($quote);
            
            $this->eventManager->dispatch('sales_quotation_process_after', [
                'quote' => $quote,
                'action' => 'approve'
            ]);
            
            return true;
        }
        return false;
    }

    public function holdQuoteById($id)
    {
        $quote = $this->quotationRepository->get($id);
        if ($quote->hold()) {
            $quote->setUpdatedAt(date('Y-m-d h:i:s'));
            $this->quotationRepository->save($quote);
            
            $this->eventManager->dispatch('sales_quotation_process_after', [
                'quote' => $quote,
                'action' => 'hold'
            ]);
            
            return true;
        }
        return false;
    }

    public function unholdQuoteById($id)
    {
        $quote = $this->quotationRepository->get($id);
        if ($quote->unhold()) {
            $quote->setUpdatedAt(date('Y-m-d h:i:s'));
            $this->quotationRepository->save($quote);
            
            $this->eventManager->dispatch('sales_quotation_process_after', [
                'quote' => $quote,
                'action' => 'unhold'
            ]);
            
            return true;
        }
        return false;
    }

    public function cancelQuoteById($id)
    {
        $quote = $this->quotationRepository->get($id);
        if ($quote->cancel()) {
            $quote->setUpdatedAt(date('Y-m-d h:i:s'));
            $this->quotationRepository->save($quote);
            
            $this->eventManager->dispatch('sales_quotation_process_after', [
                'quote' => $quote,
                'action' => 'cancel'
            ]);
            
            return true;
        }
        return false;
    }
}
