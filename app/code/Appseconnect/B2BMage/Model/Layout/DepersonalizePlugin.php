<?php
namespace Appseconnect\B2BMage\Model\Layout;

use Magento\PageCache\Model\DepersonalizeChecker;
use Magento\Customer\Model\Session;
use Magento\Framework\Session\SessionManagerInterface;

class DepersonalizePlugin
{
    /**
     * @var DepersonalizeChecker
     */
    public $depersonalizeChecker;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    public $session;

    /**
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var \Magento\Customer\Model\Visitor
     */
    public $visitor;

    /**
     * @var int
     */
    public $customerGroupId;

    /**
     * @var string
     */
    public $formKey;

    /**
     * @param DepersonalizeChecker $depersonalizeChecker
     * @param \Magento\Framework\Session\SessionManagerInterface $session
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\Visitor $visitor
     */
    public function __construct(
        DepersonalizeChecker $depersonalizeChecker,
        SessionManagerInterface $session,
        Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Visitor $visitor
    ) {
        $this->session = $session;
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        $this->visitor = $visitor;
        $this->depersonalizeChecker = $depersonalizeChecker;
    }

    /**
     * Before generate Xml
     *
     * @param \Magento\Framework\View\LayoutInterface $subject
     * @return array
     */
    public function beforeGenerateXml(\Magento\Framework\View\LayoutInterface $subject)
    {	
		$check = false;
		$check = $this->depersonalizeChecker->checkIfDepersonalize($subject);
        if ($this->depersonalizeChecker->checkIfDepersonalize($subject)) {
            $this->customerGroupId = $this->customerSession->getCustomerGroupId();
            $this->formKey = $this->session->getData(\Magento\Framework\Data\Form\FormKey::FORM_KEY);
        }
        return [];
    }

    /**
     * After generate Xml
     *
     * @param \Magento\Framework\View\LayoutInterface $subject
     * @param \Magento\Framework\View\LayoutInterface $result
     * @return \Magento\Framework\View\LayoutInterface
     */
    public function afterGenerateElements(\Magento\Framework\View\LayoutInterface $subject, $result)
    {
		$check = false;
		$check = $this->depersonalizeChecker->checkIfDepersonalize($subject);
        if ($this->depersonalizeChecker->checkIfDepersonalize($subject)) {
            $this->visitor->setSkipRequestLogging(true);
            $this->visitor->unsetData();
        }
        return $result;
    }
}
