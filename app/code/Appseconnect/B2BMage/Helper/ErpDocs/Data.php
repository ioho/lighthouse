<?php
namespace Appseconnect\B2BMage\Helper\ErpDocs;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    /**
     * @var \Appseconnect\B2BMage\Model\ErpInvoiceFactory
     */
    public $erpInvoiceFactory;
    
    /**
     * @param \Appseconnect\B2BMage\Model\ErpInvoiceFactory $erpInvoiceFactory
     */
    public function __construct(
        \Appseconnect\B2BMage\Model\ErpInvoiceFactory $erpInvoiceFactory
    ) {
    
        $this->erpInvoiceFactory = $erpInvoiceFactory;
    }

    /**
     * @param string $invoiceId
     * @return NULL|array
     */
    public function getPdfData($invoiceId)
    {
        $pdfCollection = $this->erpInvoiceFactory->create()
            ->getCollection()
            ->addFieldToFilter('invoice_increment_id', $invoiceId);
        $data = $pdfCollection->getData();
        $data = isset($data[0]) ? $data[0] : null;
        
        return $data;
    }
}
