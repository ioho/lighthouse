<?php
namespace Appseconnect\B2BMage\Helper\Pricelist;

use Appseconnect\B2BMage\Model\ResourceModel\PricelistProduct\CollectionFactory as PricelistProductCollectionFactory;
use Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory as PricelistPriceCollectionFactory;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Appseconnect\B2BMage\Model\Price
     */
    public $pricelistModel;

    /**
     * @var PricelistProductCollectionFactory
     */
    public $pricelistProductCollectionFactory;

    /**
     * @var PricelistPriceCollectionFactory
     */
    public $pricelistPriceCollectionFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\Price
     */
    public $pricelist;

    /**
     * @param PricelistProductCollectionFactory $pricelistProductCollectionFactory
     * @param PricelistPriceCollectionFactory $pricelistPriceCollectionFactory
     * @param \Appseconnect\B2BMage\Model\Price $pricelistModel
     */
    public function __construct(
        PricelistProductCollectionFactory $pricelistProductCollectionFactory,
        PricelistPriceCollectionFactory $pricelistPriceCollectionFactory,
        \Appseconnect\B2BMage\Model\Price $pricelistModel
    ) {
        $this->pricelistProductCollectionFactory = $pricelistProductCollectionFactory;
        $this->pricelistModel = $pricelistModel;
        $this->pricelistPriceCollectionFactory = $pricelistPriceCollectionFactory;
    }

    /**
     * @param int $priceListId
     * @return array
     */
    public function getPricelistId($priceListId)
    {
        $pricelistModel = $this->pricelistPriceCollectionFactory->create();
        $pricelistId = $priceListId;
        $resultData = [];
        $resultData[0] = "Base Price";
        if ($pricelistId) {
            $pricelistModel->addFieldToFilter('id', [
                'nin' => $pricelistId
            ]);
        }
        foreach ($pricelistModel->getData() as $val) {
            $resultData[$val['id']] = $val['pricelist_name'];
        }
        
        return $resultData;
    }

    /**
     * @param int $productId
     * @param float $finalPrice
     * @param string $customerPricelistCode
     * @param boolean $priceRuleObserver
     * @return float
     */
    public function getAmount(
        $productId = null,
        $finalPrice = null,
        $customerPricelistCode = null,
        $priceRuleObserver = false
    ) {
    
        $pricelistModel = $this->pricelistPriceCollectionFactory->create()
                                ->getPricelistProduct($customerPricelistCode, $productId);
        $output = $pricelistModel->getData();
        
        if (is_array($output) && ! empty($output)) {
            foreach ($output as $data) {
                if ($data['is_active'] == '1') {
                    $price = $data['discount_factor'] * $data['final_price'];
                } elseif ($priceRuleObserver) {
                    $price = '';
                } else {
                    $price = $finalPrice;
                }
            }
        } elseif ($priceRuleObserver) {
            $price = '';
        } else {
            $price = $finalPrice;
        }
        return $price;
    }

    /**
     * @param array $result
     * @param float $finalPrice
     * @return NULL|float
     */
    private function getCalculatedPrice($result, $finalPrice)
    {
        $price = null;
        if ($result) {
            foreach ($result as $value) {
                $price = $finalPrice * (1 - ($value['discount_factor'] / 100));
            }
            $price = $price * (1 - ($data['discount_factor'] / 100));
        } else {
            $price = $finalPrice * (1 - ($data['discount_factor'] / 100));
        }
        return $price;
    }

    /**
     * @param int $id
     * @return PricelistProductCollectionFactory
     */
    public function getPricelistProducts($id)
    {
        $productCollection = $this->pricelistProductCollectionFactory->create();
        $productCollection->addFieldToFilter('pricelist_id', $id);
        return $productCollection;
    }

    /**
     * @return array
     */
    public function getPricelist()
    {
        $priceList = $this->pricelistPriceCollectionFactory->create();
        $output = [];
        $outputResult = [];
        $output['label'] = 'Choose';
        $output['value'] = '';
        $outputResult[] = $output;
        foreach ($priceList->getData() as $val) {
            $output['label'] = $val['pricelist_name'];
            $output['value'] = $val['id'];
            $outputResult[] = $output;
        }
        return $outputResult;
    }
}