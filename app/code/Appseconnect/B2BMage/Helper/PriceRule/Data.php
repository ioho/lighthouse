<?php

namespace Appseconnect\B2BMage\Helper\PriceRule;

use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\Session;
use Appseconnect\B2BMage\Model\ResourceModel\Price\Collection as PricelistPriceCollection;
use Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data as SpecialPriceHelper;
use Insync\B2BMage\Block\ContactPerson\Address\Book;
use Magento\Catalog\Model\Product\Type;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Session
     */
    public $customerSession;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @var PricelistPriceCollection
     */
    public $pricelistCollection;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    public $productFactory;

    /**
     * @var \Appseconnect\B2BMage\Helper\CategoryDiscount\Data
     */
    public $helperCategory;

    /**
     * @var \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data
     */
    public $helperTierprice;

    /**
     * @var SpecialPriceHelper
     */
    public $helperCustomerSpecialPrice;

    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     * @var \Appseconnect\B2BMage\Helper\Pricelist\Data
     */
    public $helperPricelist;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManagerInterface;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrencyInterface;

    /**
     * @var \Appseconnect\LightechDiscounts\Helper\Data
     */
    protected $lighTechDiscountHelper;

    /**
     * Data constructor.
     * @param Context $context
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param PricelistPriceCollection $pricelistCollection
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param Session $session
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Appseconnect\B2BMage\Helper\CategoryDiscount\Data $helperCategory
     * @param \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist
     * @param \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data $helperTierprice
     * @param SpecialPriceHelper $helperCustomerSpecialPrice
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrencyInterface
     * @param \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
     * @param \Appseconnect\LightechDiscounts\Helper\Data $lighTechDiscountHelper
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        PricelistPriceCollection $pricelistCollection,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Session $session,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Appseconnect\B2BMage\Helper\CategoryDiscount\Data $helperCategory,
        \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist,
        \Appseconnect\B2BMage\Helper\CustomerTierPrice\Data $helperTierprice,
        SpecialPriceHelper $helperCustomerSpecialPrice,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrencyInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Appseconnect\LightechDiscounts\Helper\Data $lighTechDiscountHelper
    )
    {
        $this->storeManagerInterface = $storeManagerInterface;
        $this->priceCurrencyInterface = $priceCurrencyInterface;
        $this->customerSession = $session;
        $this->customerFactory = $customerFactory;
        $this->pricelistCollection = $pricelistCollection;
        $this->scopeConfig = $scopeConfig;
        $this->productFactory = $productFactory;
        $this->helperCategory = $helperCategory;
        $this->helperTierprice = $helperTierprice;
        $this->helperCustomerSpecialPrice = $helperCustomerSpecialPrice;
        $this->helperContactPerson = $helperContactPerson;
        $this->helperPricelist = $helperPricelist;
        $this->lighTechDiscountHelper = $lighTechDiscountHelper;
        parent::__construct($context);
    }

    /**
     * @param int $id
     * @return \Magento\Catalog\Model\ProductFactory
     */
    public function loadProduct($id)
    {
        $productModel = $this->productFactory->create();
        $productModel->load($id);
        return $productModel;
    }

    /**
     * @param $finalPrice
     * @param $tierPrice
     * @param $categoryDiscountedPrice
     * @param $pricelistPrice
     * @param $specialPrice
     * @param $lightech_discount_price
     * @return int|mixed|string
     */
    public function getActualPrice(
        $finalPrice,
        $tierPrice,
        $categoryDiscountedPrice,
        $pricelistPrice,
        $specialPrice,
        $lightech_discount_price
    )
    {
        $priorityStatus = $this->scopeConfig->getValue('insync_pricerule/setpriority/enable', 'store');
        $priority1 = $this->scopeConfig->getValue('insync_pricerule/setpriority/priority1', 'store');
        $priority2 = $this->scopeConfig->getValue('insync_pricerule/setpriority/priority2', 'store');
        $priority3 = $this->scopeConfig->getValue('insync_pricerule/setpriority/priority3', 'store');
        $priority4 = $this->scopeConfig->getValue('insync_pricerule/setpriority/priority4', 'store');
        $priority5 = $this->scopeConfig->getValue('insync_pricerule/setpriority/priority5', 'store');

        if ($priorityStatus == 1) {
            $priorityPrice = '';

            $productPrice = [
                'final_price' => $finalPrice,
                'tier_price' => $tierPrice,
                'special_price' => $specialPrice,
                'category_price' => $categoryDiscountedPrice,
                'price_list' => $pricelistPrice,
                'lightech_discount_price' => $lightech_discount_price
            ];

            if ($priority1 && $productPrice[$priority1]) {
                $priorityPrice = $productPrice[$priority1];
            } elseif ($priority2 && $productPrice[$priority2]) {
                $priorityPrice = $productPrice[$priority2];
            } elseif ($priority3 && $productPrice[$priority3]) {
                $priorityPrice = $productPrice[$priority3];
            } elseif ($priority4 && $productPrice[$priority4]) {
                $priorityPrice = $productPrice[$priority4];
            } elseif ($priority5 && $productPrice[$priority5]) {
                $priorityPrice = $productPrice[$priority5];
            } else {
                $priorityPrice = $finalPrice;
            }

            return $priorityPrice;
        } else {
            $price = [
                $finalPrice,
                $tierPrice,
                $categoryDiscountedPrice,
                $pricelistPrice,
                $specialPrice,
                $lightech_discount_price
            ];
            $minVal = max(
                $finalPrice,
                $tierPrice,
                $categoryDiscountedPrice,
                $pricelistPrice,
                $specialPrice,
                $lightech_discount_price
            );
            foreach ($price as $value) {
                if (is_numeric($value) && $value < $minVal) {
                    $minVal = $value;
                }
            }
            $newPrice = $minVal;

            return $newPrice;
        }
    }

    /**
     * @param \Magento\Catalog\Model\Product $item
     * @param string $customerPricelistCode
     * @param boolean $pricelistStatus
     * @param int $customerId
     * @param int $websiteId
     * @return void
     */
    public function processBundleProduct(
        $item,
        $customerPricelistCode,
        $pricelistStatus,
        $customerId,
        $websiteId
    )
    {

        $product = $this->productFactory->create()->load($item->getProductId());
        $qtyItem = $item->getQty();
        $qtyItem = ($qtyItem) ? $qtyItem : 1;
        $productId = $product->getId();
        $categoryIds = $product->getCategoryIds();

        if ($item->getProduct()->getPriceType() == 1) {
            $productFinalPrice = $item->getProduct()->getFinalPrice();
            $sku = $this->productFactory->create()
                ->load($item->getProduct()
                    ->getId())
                ->getSku();
            $pricelistAmount = '';
            if ($customerPricelistCode && $pricelistStatus) {
                $pricelistAmount = $this->helperPricelist->getAmount($item->getProduct()
                    ->getId(), $item->getProduct()
                    ->getFinalPrice(), $customerPricelistCode, true);
            }
            $categoryDiscountedAmount = '';
            $categoryDiscountedAmount = $this->helperCategory->getCategoryDiscountAmount($item->getProduct()
                ->getFinalPrice(), $customerId, $categoryIds);
            $tierPriceAmount = '';
            $tierPriceAmount = $this->helperTierprice->getTierprice($item->getProduct()
                ->getId(), $sku, $customerId, $websiteId, $qtyItem, $item->getProduct()
                ->getFinalPrice());
            $specialPriceAmount = '';
            $specialPriceAmount = $this->helperCustomerSpecialPrice->getSpecialPrice($item->getProduct()
                ->getId(), $sku, $customerId, $websiteId, $item->getProduct()
                ->getFinalPrice());
            $lightechDiscountPrice = '';
            $lightechDiscountPrice = $this->lighTechDiscountHelper->getScontoData($item->getProduct(),$sku,$qtyItem,$item->getProduct()
                ->getFinalPrice());

            if ($pricelistAmount) {
                $productFinalPrice = $pricelistAmount;
            }
        $bundleCalculatedPriceAmount = $this->getActualPrice(
                $productFinalPrice,
                $tierPriceAmount,
                $categoryDiscountedAmount,
                $pricelistAmount,
                $specialPriceAmount,
                $lightechDiscountPrice
            );
            $bundleCalculatedPriceAmount = $this->convertPrice($bundleCalculatedPriceAmount);
            $item->setCustomPrice($bundleCalculatedPriceAmount);
            $item->setOriginalCustomPrice($bundleCalculatedPriceAmount);
        } else {
            foreach ($item->getQuote()->getAllItems() as $bundleItems) {
                if ($bundleItems->getProduct()->getTypeId() == Type::TYPE_BUNDLE) {
                    continue;
                }
                $productPrice = $this->loadProduct(
                    $bundleItems->getProduct()
                        ->getEntityId()
                )
                    ->getFinalPrice();
                $pricelistPrice = '';
                if ($customerPricelistCode && $pricelistStatus) {
                    $pricelistPrice = $this->helperPricelist->getAmount($bundleItems->getProduct()
                        ->getEntityId(), $productPrice, $customerPricelistCode, true);
                }
                $categoryDiscountedPrice = '';
                $categoryDiscountedPrice = $this->helperCategory->getCategoryDiscountAmount(
                    $productPrice,
                    $customerId,
                    $categoryIds
                );
                $productSku = $bundleItems->getProduct()->getSku();
                $tierPrice = '';
                $tierPrice = $this->helperTierprice->getTierprice(
                    $productId,
                    $productSku,
                    $customerId,
                    $websiteId,
                    $qtyItem,
                    $productPrice
                );

                $specialPrice = '';
                $specialPrice = $this->helperCustomerSpecialPrice->getSpecialPrice($bundleItems->getProduct()
                    ->getEntityId(), $productSku, $customerId, $websiteId, $productPrice);

                $lightechDiscountPrice = '';
                $lightechDiscountPrice = $this->lighTechDiscountHelper->getScontoData($bundleItems->getProduct()
                    ->getEntityId(),$productSku,$qtyItem,$productPrice);

                if ($pricelistPrice) {
                    $productPrice = $pricelistPrice;
                }
                $bundleCalculatedPrice = $this->getActualPrice(
                    $productPrice,
                    $tierPrice,
                    $categoryDiscountedPrice,
                    $pricelistPrice,
                    $specialPrice,
                    $lightechDiscountPrice
                );

                $bundleCalculatedPrice = $this->convertPrice($bundleCalculatedPrice);
                $bundleItems->setCustomPrice($bundleCalculatedPrice);
                $bundleItems->setOriginalCustomPrice($bundleCalculatedPrice);
                $bundleItems->getProduct()->setIsSuperMode(true);
            }
        }

        $item->getProduct()->setIsSuperMode(true);
    }

    /**
     * @param \Magento\Catalog\Model\Product $item
     * @param string $customerPricelistCode
     * @param boolean $pricelistStatus
     * @param int $customerId
     * @param int $websiteId
     * @return void
     */
    public function processConfigurableProduct(
        $item,
        $customerPricelistCode,
        $pricelistStatus,
        $customerId,
        $websiteId
    )
    {
        $product = $this->productFactory->create()->load($item->getProductId());
        $quotationProductPrice = $this->customerSession->getQuotationProduct();
        $qtyItem = $item->getQty();
        $qtyItem = ($qtyItem) ? $qtyItem : 1;
        $categoryIds = $product->getCategoryIds();
        $quotationPrice = null;
        foreach ($item->getQuote()->getAllItems() as $configItems) {
            $productId = $configItems->getProduct()->getId();
            $customQuotationPrice = $configItems->getCustomQuotationPrice();
            if ($configItems->getId() && !$customQuotationPrice) {
                $quotationPrice = null;
            } else if ($quotationProductPrice || $customQuotationPrice) {
                if (isset($quotationProductPrice[$productId]) || $customQuotationPrice) {
                    $quotationPrice = ($customQuotationPrice) ? $customQuotationPrice : $quotationProductPrice[$productId];
                    $configItems->setCustomQuotationPrice($quotationPrice);
                    $configCalculatedPrice = $quotationPrice;
                }
            }
            if ($configItems->getProduct()->getTypeId() == 'configurable' && !$quotationPrice) {
                continue;
            }
            if ($quotationPrice == null) {
                $price = $configItems->getProduct()->getFinalPrice();

                $pricelistPrice = '';
                if ($customerPricelistCode && $pricelistStatus) {
                    $pricelistPrice = $this->helperPricelist->getAmount($configItems->getProduct()
                        ->getEntityId(), $price, $customerPricelistCode, true);
                }

                $categoryDiscountedPrice = $this->helperCategory->getCategoryDiscountAmount(
                    $price,
                    $customerId,
                    $categoryIds
                );

                $productSku = $configItems->getProduct()->getSku();
                $tierPrice = $this->helperTierprice->getTierprice($configItems->getProduct()
                    ->getEntityId(), $productSku, $customerId, $websiteId, $qtyItem, $configItems->getProduct()
                    ->getFinalPrice());

                $specialPrice = $this->helperCustomerSpecialPrice->getSpecialPrice($configItems->getProduct()
                    ->getEntityId(), $productSku, $customerId, $websiteId, $price);

                $lightechDiscountPrice = '';
                $lightechDiscountPrice = $this->lighTechDiscountHelper->getScontoData($configItems->getProduct()
                    ->getEntityId(),$productSku,$qtyItem,$price);
                if ($pricelistPrice) {
                    $price = $pricelistPrice;
                }

                $configCalculatedPrice = $this->getActualPrice(
                    $price,
                    $tierPrice,
                    $categoryDiscountedPrice,
                    $pricelistPrice,
                    $specialPrice,
                    $lightechDiscountPrice
                );
            }
            $configCalculatedPrice = $this->convertPrice($configCalculatedPrice);
            $configItems->setCustomPrice($configCalculatedPrice);
            $configItems->setOriginalCustomPrice($configCalculatedPrice);
            $configItems->getProduct()->setIsSuperMode(true);
        }
    }

    /**
     * @param float $amount
     * @param null|int $store
     * @param null|string $currency
     * @return float
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function convertPrice($amount = 0, $store = null, $currency = null)
    {
        if ($store == null) {
            $store = $this->storeManagerInterface->getStore()->getStoreId();
        }
        $rate = $this->priceCurrencyInterface->convert($amount, $store, $currency);
        return $rate;

    }

    /**
     * @param \Magento\Catalog\Model\Product $item
     * @param string $customerPricelistCode
     * @param boolean $pricelistStatus
     * @param int $customerId
     * @param int $websiteId
     * @return void
     */
    public function processSimpleProduct(
        $item,
        $customerPricelistCode,
        $pricelistStatus,
        $customerId,
        $websiteId
    )
    {
        $product = $this->productFactory->create()->load($item->getProductId());
        $quotationProductPrice = $this->customerSession->getQuotationProduct();
        $qtyItem = $item->getQty();
        $qtyItem = ($qtyItem) ? $qtyItem : 1;
        $productId = $product->getId();
        $categoryIds = $product->getCategoryIds();
        $finalPrice = $product->getFinalPrice($item->getQty());
        $quotationPrice = null;
        $customQuotationPrice = $item->getCustomQuotationPrice();
        if ($quotationProductPrice || $customQuotationPrice) {
            if (isset($quotationProductPrice[$productId]) || $customQuotationPrice) {
                $quotationPrice = ($customQuotationPrice) ? $customQuotationPrice : $quotationProductPrice[$productId];
                $item->setCustomQuotationPrice($quotationPrice);
                $simpleCalculatedPrice = $quotationPrice;
            }
        }
        if ($quotationPrice == null) {
            $pricelistPrice = '';
            if ($customerPricelistCode && $pricelistStatus) {
                $pricelistPrice = $this->helperPricelist->getAmount(
                    $productId,
                    $finalPrice,
                    $customerPricelistCode,
                    true
                );
            }

            $categoryDiscountedPrice = $this->helperCategory->getCategoryDiscountAmount(
                $finalPrice,
                $customerId,
                $categoryIds
            );

            $productSku = $item->getSku();
            $tierPrice = '';
            $tierPrice = $this->helperTierprice->getTierprice(
                $productId,
                $productSku,
                $customerId,
                $websiteId,
                $qtyItem,
                $finalPrice
            );

            $specialPrice = '';
            $specialPrice = $this->helperCustomerSpecialPrice->getSpecialPrice(
                $productId,
                $productSku,
                $customerId,
                $websiteId,
                $finalPrice
            );

            $lightechDiscountPrice = '';
            $lightechDiscountPrice = $this->lighTechDiscountHelper->getScontoData($productId,$productSku,$qtyItem,$finalPrice);


            if ($pricelistPrice) {
                $finalPrice = $pricelistPrice;
            }

            $simpleCalculatedPrice = $this->getActualPrice(
                $finalPrice,
                $tierPrice,
                $categoryDiscountedPrice,
                $pricelistPrice,
                $specialPrice,
                $lightechDiscountPrice
            );

            if ($quotationProductPrice) {
                $simpleCalculatedPrice = $quotationProductPrice;
                $item->setCustomQuotationPrice($simpleCalculatedPrice);
            }
        }
        $simpleCalculatedPrice = $this->convertPrice($simpleCalculatedPrice);
        $item->setCustomPrice($simpleCalculatedPrice);
        $item->setOriginalCustomPrice($simpleCalculatedPrice);
        $item->setBaseCost($finalPrice);
        $item->getProduct()->setIsSuperMode(true);
    }

    /**
     * @param int $productId
     * @param int $qty
     * @param int $customerId
     * @param int $websiteId
     * @return float
     */
    public function getDiscountedPrice($productId, $qty, $customerId, $websiteId = null)
    {
        $customer = $this->customerFactory->create()->load($customerId);
        $product = $this->productFactory->create()->load($productId);
        $websiteId = $websiteId ? $websiteId : $this->customerSession->getCustomer()->getWebsiteId();
        $customerType = $customer->getCustomerType();
        $customerPricelistCode = $customer->getData('pricelist_code');
        $finalPrice = $product->getPrice($qty);

        if ($customerType == 3) {
            $customerDetail = $this->helperContactPerson->getCustomerId($customerId);
            $customerCollection = $this->customerFactory->create()->load($customerDetail['customer_id']);
            $customerPricelistCode = $customerCollection->getData('pricelist_code');
            $customerId = $customerDetail['customer_id'];
        }

        $pricelistStatus = null;
        $pricelistCollection = $this->pricelistCollection
            ->addFieldToFilter('id', $customerPricelistCode)
            ->addFieldToFilter('website_id', $websiteId)
            ->getData();

        if (isset($pricelistCollection[0])) {
            $pricelistStatus = $pricelistCollection[0]['is_active'];
        }
        $pricelistPrice = '';
        if ($customerPricelistCode && $pricelistStatus) {
            $pricelistPrice = $this->helperPricelist->getAmount(
                $productId,
                $finalPrice,
                $customerPricelistCode,
                true
            );
        }
        $categoryIds = $product->getCategoryIds();
        $categoryDiscountedPrice = $this->helperCategory->getCategoryDiscountAmount(
            $finalPrice,
            $customerId,
            $categoryIds
        );
        // for tierprice
        $productSku = $product->getSku();
        $tierPrice = $this->helperTierprice->getTierprice(
            $productId,
            $productSku,
            $customerId,
            $websiteId,
            $qty,
            $finalPrice
        );

        $specialPrice = '';

        $specialPrice = $this->helperCustomerSpecialPrice->getSpecialPrice(
            $productId,
            $productSku,
            $customerId,
            $websiteId,
            $finalPrice
        );

        $lightechDiscountPrice = '';
        $lightechDiscountPrice = $this->lighTechDiscountHelper->getScontoData($productId,$productSku,$qty,$finalPrice);

        $discountedPrice = '';

        $discountedPrice = $this->getActualPrice(
            $finalPrice,
            $tierPrice,
            $categoryDiscountedPrice,
            $pricelistPrice,
            $specialPrice,
            $lightechDiscountPrice
        );
        return $discountedPrice;
    }
}
