<?php
namespace Appseconnect\B2BMage\Helper\CategoryVisibility;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    /**
     * @var Magento\Customer\Model\ResourceModel\Group\CollectionFactory
     */
    public $groupCollectionFactory;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;
    
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
    ) {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
        $this->groupCollectionFactory = $groupCollectionFactory;
    }

    /**
     * @return string
     */
    public function getCategoryVisbility()
    {
        $categoryVisibilityType = $this->scopeConfig
                    ->getValue(
                        'insync_category_visibility/select_visibility/select_visibility_type',
                        'default'
                    );
        return $categoryVisibilityType;
    }

    /**
     * @return \Magento\Customer\Model\ResourceModel\Group\CollectionFactory
     */
    public function getCustomerGroups()
    {
        $groupOptions = $this->groupCollectionFactory->create()->toOptionArray();
        return $groupOptions;
    }
}
