<?php
namespace Appseconnect\B2BMage\Helper\CustomerSpecialPrice;

use Appseconnect\B2BMage\Model\ResourceModel\CustomerFactory;
use Appseconnect\B2BMage\Model\ResourceModel\Specialprice\CollectionFactory;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $storeManager;

    /**
     * @var CustomerFactory
     */
    public $specialPriceResourceFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\CustomerFactory
     */
    public $specialPriceCollectionFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\ResourceModel\Specialprice\CollectionFactory
     */
    public $specialPriceProductCollectionFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory
     */
    public $pricelistPriceCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    public $productCollectionFactory;

    /**
     * @var \Appseconnect\B2BMage\Helper\Pricelist\Data
     */
    public $helperPricelist;

    /**
     * @param CollectionFactory $specialPriceProductCollectionFactory
     * @param CustomerFactory $specialPriceResourceFactory
     * @param \Appseconnect\B2BMage\Model\CustomerFactory $specialPriceCollectionFactory
     * @param \Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory $pricelistPriceCollectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        CollectionFactory $specialPriceProductCollectionFactory,
        CustomerFactory $specialPriceResourceFactory,
        \Appseconnect\B2BMage\Model\CustomerFactory $specialPriceCollectionFactory,
        \Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory $pricelistPriceCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
    
        $this->specialPriceResourceFactory = $specialPriceResourceFactory;
        $this->specialPriceCollectionFactory = $specialPriceCollectionFactory;
        $this->specialPriceProductCollectionFactory = $specialPriceProductCollectionFactory;
        $this->pricelistPriceCollectionFactory = $pricelistPriceCollectionFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->helperPricelist = $helperPricelist;
        $this->storeManager = $storeManager;
    }

    /**
     * @param int $specialPriceId
     * @param int $customerId
     * @return array
     */
    public function getAssignedCustomerId($specialPriceId = null, $customerId = null)
    {
        $customerCollection = $this->specialPriceCollectionFactory->create()->getCollection();
        if ($customerId) {
            $customerCollection->addFieldToFilter('customer_id', $customerId);
        }
        
        $output = $customerCollection->getData();
        if ($customerId) {
            return $output;
        }
        
        $result = [];
        
        foreach ($output as $val) {
            if ($specialPriceId != $val['id']) {
                $result[] = $val['customer_id'];
            }
        }
        
        return $result;
    }

    /**
     * @param int $id
     * @return \Appseconnect\B2BMage\Model\ResourceModel\Specialprice\CollectionFactory
     */
    public function getSpecialPriceProducts($id)
    {
        $productCollection = $this->specialPriceProductCollectionFactory->create();
        $productCollection->addFieldToFilter('parent_id', $id);
        return $productCollection;
    }

    /**
     * @param int $productId
     * @param string $productSku
     * @param int $customerId
     * @param int $websiteId
     * @param float $finalPrice
     * @return null|boolean
     */
    public function getSpecialPrice($productId, $productSku, $customerId, $websiteId, $finalPrice)
    {
        $specialPrice = '';
        $specialPriceResourceModel = $this->specialPriceResourceFactory->create();
        
        $customerSpecialPriceCollection = $this->specialPriceCollectionFactory->create()->getCollection();
        
        $customerSpecialPriceCollection = $specialPriceResourceModel->addProductMapCollection(
            $productSku,
            $customerSpecialPriceCollection
        );
        
        $customerSpecialPriceCollection->addFieldToFilter('customer_id', $customerId);
        $customerSpecialPriceCollection->addFieldToFilter('is_active', 1);
        $customerSpecialPriceCollection->addFieldToFilter('website_id', $websiteId);
        $customerSpecialPriceCollection->addFieldToFilter('start_date', [
            'lteq' => date('Y-m-d')
        ]);
        $customerSpecialPriceCollection->addFieldToFilter('end_date', [
            'gteq' => date('Y-m-d')
        ]);
        
        $specialPriceData = $customerSpecialPriceCollection->getData();
        $specialPriceData = isset($specialPriceData[0])?$specialPriceData[0]:null;
        
        if ($specialPriceData) {
            $discountType = $specialPriceData['discount_type'];
            $specialPrice = $specialPriceData['special_price'];
            
            $pricelistCollection = $this->pricelistPriceCollectionFactory->create();
            $pricelistCollection->addFieldToFilter('id', $specialPriceData['pricelist_id'])
                ->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('website_id', $websiteId);
            $pricelistData = $pricelistCollection->getData();
            $pricelistData = isset($pricelistData[0])?$pricelistData[0]:null;
            if (! empty($pricelistData)) {
                $finalPrice = $this->helperPricelist->getAmount(
                    $productId,
                    $finalPrice,
                    $specialPriceData['pricelist_id']
                );
            }
            
            if ($discountType) {
                $specialPrice = $finalPrice * (100 - $specialPrice) / 100;
            }
        }
        return $specialPrice;
    }
}