<?php
namespace Appseconnect\B2BMage\Helper\Quotation;

use Magento\Framework\App\Helper\Context;
use Appseconnect\B2BMage\Model\ResourceModel\Quote\CollectionFactoryInterface as QuoteCollectionFactoryInterface;

class Data extends \Magento\Framework\Url\Helper\Data
{

    /**
     * Path to controller to delete item from cart
     */
    const DELETE_URL = 'b2bmage/quotation/index_delete';

    const CHECKOUT_URL = 'b2bmage/quotation/index_checkout';

    const XML_PATH_ENABLE_QUOTATION = 'insync_quotes/general/enable_quote';

    const XML_PATH_QUOTATION_LIFETIME = 'insync_quotes/general/lifetime';

    /**
     * @var \Magento\Store\Model\Store
     */
    public $storeManager;
    
    /**
     * @var asdsad
     */
    public $helperPriceRule;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\PriceRule\Data
     */
    public $helperPricelist;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;
    
    /**
     * @var \Magento\CatalogInventory\Api\StockStateInterface
     */
    public $stockState;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helper;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var QuoteCollectionFactoryInterface
     */
    protected $quoteCollectionFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * Data constructor.
     * @param Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\Store $storeManager
     * @param \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule
     * @param \Magento\CatalogInventory\Api\StockStateInterface $stockState
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param QuoteCollectionFactoryInterface $quoteCollectionFactory
     * @param \Appseconnect\B2BMage\Model\QuoteFactory $quoteFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\Store $storeManager,
        \Appseconnect\B2BMage\Helper\PriceRule\Data $helperPriceRule,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        QuoteCollectionFactoryInterface $quoteCollectionFactory,
        \Appseconnect\B2BMage\Model\QuoteFactory $quoteFactory

    ) {
        $this->storeManager = $storeManager;
        $this->helperPriceRule = $helperPriceRule;
        $this->scopeConfig = $scopeConfig;
        $this->stockState = $stockState;
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->quoteCollectionFactory = $quoteCollectionFactory;
        $this->quoteFactory = $quoteFactory;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    public function updateQuote(){
        $enable=$this->isQuotationEnabled();
        if($enable){
            try {
                $days=$this->isQuotationLifeTime();
                $currentDate=date('Y-m-d');
                $daysAgo = date('Y-m-d 23:59:59', strtotime('-'.$days.' days', strtotime($currentDate)));
                $quoteCollection= $this->quoteCollectionFactory
                    ->create()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('status', array('eq' => 'approved'))
                    ->addFieldToFilter('updated_at', array('lteq' => $daysAgo))
                    ->setOrder('updated_at', 'asc');
                foreach ($quoteCollection as $quote){
                    $quoteObject=$this->quoteFactory->create()->load($quote->getId());
                    $quoteObject->setStatus("closed");
                    $quoteObject->save();
                }

            }catch (\Exception $e) {
            }
        }

    }

    /**
     * @param array $data
     * @param null|string $type
     * @return string|array
     */
    public function getQuotationInfo($data,$type=null){
        if($type=="json"){
            $result = $this->jsonHelper->jsonEncode($data);
        }else{
            $result = $this->jsonHelper->jsonDecode($data);
        }
        return $result;
    }

    /**
     * Get post parameters for delete from cart
     *
     * @param \Appseconnect\B2BMage\Model\QuoteProduct $item
     * @return string
     */
    public function getDeletePostJson($item)
    {
        $url = $this->_getUrl(self::DELETE_URL);
        
        $data = [
            'item_id' => $item->getId()
        ];
        if (! $this->_request->isAjax()) {
            $data[\Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED] = $this->getCurrentBase64Url();
        }
        return json_encode([
            'action' => $url,
            'data' => $data
        ]);
    }

    /**
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @return string
     */
    public function getCheckoutPostJson($quote)
    {
        $url = $this->_getUrl(self::CHECKOUT_URL);
        
        $data = [
            'quote_id' => $quote->getId()
        ];
        if (! $this->_request->isAjax()) {
            $data[\Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED] = $this->getCurrentBase64Url();
        }
        return json_encode([
            'action' => $url,
            'data' => $data
        ]);
    }

    /**
     * @return mixed
     */
    public function isQuotationLifeTime()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_QUOTATION_LIFETIME, 'store');
    }

    /**
     * @return mixed
     */
    public function isQuotationEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_ENABLE_QUOTATION, 'store');
    }

    /**
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @param array $requestData
     * @return \Appseconnect\B2BMage\Model\Quote
     */
    public function prepareItems($quote, $requestData)
    {
        $requestData = $this->suggestItemsQty($quote, $requestData);
        $quoteModel = $this->updateItems($quote, $requestData);
        return $quoteModel;
    }

    /**
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @param array $data
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Appseconnect\B2BMage\Model\Quote
     */
    public function updateItems($quote, $data)
    {
        $quoteWebsiteId = $this->storeManager->load($quote->getStoreId())
            ->getWebsiteId();
        
        $qtyRecalculatedFlag = false;
        foreach ($data as $itemId => $itemInfo) {
            $item = $quote->getItemById($itemId);
            if (! $item) {
                continue;
            }
            
            $qty = isset($itemInfo['qty']) ? (double) $itemInfo['qty'] : false;
            $price = isset($itemInfo['price']) ? (double) $itemInfo['price'] : false;
            if ($qty > 0) {
                $item->setQty($qty);
                
                $customPrice = $this->helperPriceRule->getDiscountedPrice($item->getProduct()
                    ->getId(), $item->getQty(), $quote->getContactId(), $quoteWebsiteId);
                $item->setPrice($customPrice);
                $item->setBasePrice($customPrice);
                
                if ($item->getHasError()) {
                    throw new \Magento\Framework\Exception\LocalizedException(__($item->getMessage()));
                }
                
                if (isset($itemInfo['before_suggest_qty']) && $itemInfo['before_suggest_qty'] != $qty) {
                    $qtyRecalculatedFlag = true;
                    $this->messageManager->addNotice(
                        __(
                            'Quantity was recalculated from %1 to %2',
                            $itemInfo['before_suggest_qty'],
                            $qty
                        ),
                        'quote_item' . $item->getId()
                    );
                }
            }
            
            if ($price && $price > 0) {
                $item->setPrice($price);
                $item->setBasePrice($price);
                
                if ($item->getHasError()) {
                    throw new \Magento\Framework\Exception\LocalizedException(__($item->getMessage()));
                }
            }
        }
        
        if ($qtyRecalculatedFlag) {
            $this->messageManager->addNotice(
                __('We adjusted product quantities to fit the required increments.')
            );
        }
        
        return $quote;
    }

    /**
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @param array $data
     * @return array
     */
    public function suggestItemsQty($quote, $data)
    {
        foreach ($data as $itemId => $quote) {
            if (! isset($itemInfo['qty'])) {
                continue;
            }
            $qty = (float) $itemInfo['qty'];
            if ($qty <= 0) {
                continue;
            }
            
            $quoteItem = $quote->getItemById($itemId);
            if (! $quoteItem) {
                continue;
            }
            
            $product = $quoteItem->getProduct();
            if (! $product) {
                continue;
            }
            
            $data[$itemId]['before_suggest_qty'] = $qty;
            $data[$itemId]['qty'] = $this->stockState
                                    ->suggestQty($product->getId(), $qty, $product->getStore()
                ->getWebsiteId());
        }
        return $data;
    }

    /**
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @param int $itemId
     * @return \Appseconnect\B2BMage\Model\Quote
     */
    public function removeItem($quote, $itemId)
    {
        $item = $quote->getItemById($itemId);
        if ($item) {
            $item->isDeleted(true);
            
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $child->isDeleted(true);
                }
            }
            
            $parent = $item->getParentItem();
            if ($parent) {
                $parent->isDeleted(true);
            }
            
            if (! $quote->getAllVisibleItems()) {
                $quote->isDeleted(true);
            }
        }
        
        return $quote;
    }
}
