<?php
namespace Appseconnect\B2BMage\Helper\Salesrep;

use Magento\Store\Model\ResourceModel\Website\CollectionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Customer\Model\Session;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    /**
     * @var CollectionFactory
     */
    public $websiteCollectionFactory;
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Appseconnect\B2BMage\Model\SalesrepgridFactory
     */
    public $salesrepGridFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Model\SalesrepFactory
     */
    public $salesrepFactory;
    
    /**
     * @var \Magento\Catalog\Model\Session
     */
    public $catalogSession;
    
    /**
     * @param CollectionFactory $websiteCollectionFactory
     * @param Session $customerSession
     * @param \Appseconnect\B2BMage\Model\SalesrepFactory $salesrepFactory
     * @param \Appseconnect\B2BMage\Model\SalesrepgridFactory $salesrepGridFactory
     */
    public function __construct(
        CollectionFactory $websiteCollectionFactory,
        Session $customerSession,
        \Appseconnect\B2BMage\Model\SalesrepFactory $salesrepFactory,
        \Appseconnect\B2BMage\Model\SalesrepgridFactory $salesrepGridFactory
    ) {
        $this->websiteCollectionFactory = $websiteCollectionFactory;
        $this->customerSession = $customerSession;
        $this->salesrepGridFactory = $salesrepGridFactory;
        $this->salesrepFactory = $salesrepFactory;
    }

    /**
     * @return array
     */
    public function getWebsite()
    {
        $customer = $this->websiteCollectionFactory->create();
        $output = $customer->getData();
        $result = [];
        
        foreach ($output as $val) {
            $result[$val['website_id']] = $val['name'];
        }
        
        return $result;
    }

    /**
     * @param int $salesrepId
     * @param boolean $reset
     * @return array
     */
    public function getCustomerId($salesrepId, $reset)
    {
        $salesrepCollection = $this->salesrepFactory->create()->getCollection();
        
        $output = $salesrepCollection->getData();
        $result = [];
        
        foreach ($output as $val) {
            if ($reset && $salesrepId != $val['salesrep_id']) {
                $result[] = $val['customer_id'];
            } elseif (! $reset && $salesrepId == $val['salesrep_id']) {
                $result[] = $val['customer_id'];
            }
        }
        
        return $result;
    }

    /**
     * @param int $customerId
     * @return array
     */
    public function getSalesrepId($customerId)
    {
        $salesrepCollection = $$this->salesrepFactory->create()
            ->getCollection()
            ->addFieldToFilter('customer_id', $customerId);
        $salesrepCollection->addFieldToSelect([
            'salesrep_id',
            'id'
        ]);
        return $salesrepCollection->getData();
    }

    /**
     * @param int $customerId
     * @param boolean $getData
     * @return boolean|array
     */
    public function isSalesrep($customerId, $getData = false)
    {
        $return = false;
        $salesrepCollection = $this->salesrepGridFactory->create()
            ->getCollection()
            ->addFieldToFilter('salesrep_customer_id', $customerId);
        $salesrepCollection->addFieldToSelect([
            'id',
            'website_id'
        ]);
        $salesrepData = $salesrepCollection->getData();
        if ($salesrepData && $getData) {
            $return = $salesrepData;
        } elseif ($salesrepData) {
            $return = true;
        }
        return $return;
    }
    
    /**
     * @return boolean
     */
    public function isAllow()
    {
        $customerType = $this->customerSession->getCustomer()->getCustomerType();
        return $customerType != 2 ? true : false;
    }
    
    /**
     * @return int
     */
    public function getType()
    {
        $customerType = $this->customerSession->getCustomer()->getCustomerType();
        return $customerType;
    }
    
    /**
     * @return \Magento\Catalog\Model\Session
     */
    private function getCatalogSession()
    {
        $this->catalogSession = ObjectManager::getInstance()->create(\Magento\Catalog\Model\Session::class);
        return $this->catalogSession;
    }
    
    /**
     * @return int
     */
    public function getSalesrepIdFromSession()
    {
        return $this->getCatalogSession()->getSalesrepId();
    }
}
