<?php
namespace Appseconnect\B2BMage\Helper\Salesrep;

class Url
{

    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    public $urlBuilder;

    /**
     *
     * @param \Magento\Framework\UrlInterface $urlBuilder
     */
    public function __construct(\Magento\Framework\UrlInterface $urlBuilder)
    {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->urlBuilder->getUrl('/*/Customergrid', [
            'id' => $this->getRequest()
                ->getParam('id')
        ]);
    }
}
