<?php
namespace Appseconnect\B2BMage\Helper\CustomerTierPrice;

use Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory;
use Appseconnect\B2BMage\Model\ResourceModel\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Appseconnect\B2BMage\Model\ProductFactory as CustomerTierProductFactory;
use Appseconnect\B2BMage\Model\ResourceModel\Product\CollectionFactory as TierProductCollectionFactory;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var CustomerTierProductFactory
     */
    public $customerTierProductFactory;
    
    /**
     * @var ProductFactory
     */
    public $tierPriceResourceFactory;
    
    /**
     * @var CollectionFactory
     */
    public $pricelistPriceCollectionFactory;
    
    /**
     * @var ProductCollectionFactory
     */
    public $productCollectionFactory;
    
    /**
     * @var TierProductCollectionFactory
     */
    public $tierProductCollectionFactory;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\Pricelist\Data
     */
    public $helperPricelist;

    /**
     * @param \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist
     * @param ProductFactory $tierPriceResourceFactory
     * @param CollectionFactory $pricelistPriceCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param TierProductCollectionFactory $tierProductCollectionFactory
     * @param CustomerTierProductFactory $customerTierProductFactory
     */
    public function __construct(
        \Appseconnect\B2BMage\Helper\Pricelist\Data $helperPricelist,
        ProductFactory $tierPriceResourceFactory,
        CollectionFactory $pricelistPriceCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        TierProductCollectionFactory $tierProductCollectionFactory,
        CustomerTierProductFactory $customerTierProductFactory
    ) {
        $this->pricelistPriceCollectionFactory = $pricelistPriceCollectionFactory;
        $this->tierPriceResourceFactory = $tierPriceResourceFactory;
        $this->customerTierProductFactory = $customerTierProductFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->tierProductCollectionFactory = $tierProductCollectionFactory;
        $this->helperPricelist = $helperPricelist;
    }

    /**
     * @param int $productId
     * @param string $productSku
     * @param int $customerId
     * @param int $websiteId
     * @param int $qtyItem
     * @param float $finalPrice
     * @return null|number
     */
    public function getTierprice(
        $productId,
        $productSku,
        $customerId,
        $websiteId,
        $qtyItem,
        $finalPrice
    ) {
    
        $tierprice = '';
        $tierpriceCollection = $this->tierProductCollectionFactory->create();
        $tierPriceResourceModel = $this->tierPriceResourceFactory->create();
        $tierpriceCollection = $tierPriceResourceModel->getAssignedProducts(
            $tierpriceCollection,
            $productSku,
            $qtyItem
        );
        
        $tierpriceCollection
            ->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('website_id', $websiteId);
        $tierpriceData = $tierpriceCollection->getData();
        $tierpriceData = isset($tierpriceData[0]) ? $tierpriceData[0] : null;
        
        if (! empty($tierpriceData)) {
            $pricelistCollection = $this->pricelistPriceCollectionFactory->create();
            $pricelistCollection
                ->addFieldToFilter('id', $tierpriceData['pricelist_id'])
                ->addFieldToFilter('is_active', 1)
                ->addFieldToFilter('website_id', $websiteId);
            $pricelistData = $pricelistCollection->getData();
            $pricelistData = isset($pricelistData[0]) ? $pricelistData[0] : null;
            if (! empty($pricelistData)) {
                $finalPrice = $this->helperPricelist->getAmount(
                    $productId,
                    $finalPrice,
                    $tierpriceData['pricelist_id']
                );
            }
            
            $tierprice = $tierpriceData['tier_price'];
            $discountType = $tierpriceData['discount_type'];
            
            if ($discountType) {
                $tierprice = $finalPrice * (100 - $tierprice) / 100;
            }
        }
        return $tierprice;
    }

    /**
     * @param float $finalPrice
     * @param float $tierprice
     * @param float $categoryDiscountedPrice
     * @param float $pricelistPrice
     * @return mixed
     */
    public function getActualPrice(
        $finalPrice,
        $tierprice,
        $categoryDiscountedPrice,
        $pricelistPrice
    ) {
    
        $price = [
            $finalPrice,
            $tierprice,
            $categoryDiscountedPrice,
            $pricelistPrice
        ];
        return min(array_filter($price));
    }

    /**
     * @param int $tierPriceId
     * @return array
     */
    public function getAssignedCustomerId($tierPriceId = null)
    {
        $customerCollection = $this->customerTierProductFactory->create()
                                ->getCollection();
        
        $output = $customerCollection->getData();
        $result = [];
        
        foreach ($output as $val) {
            if ($tierPriceId != $val['id']) {
                $result[] = $val['customer_id'];
            }
        }
        
        return $result;
    }

    /**
     * @param string $productSku
     * @return array
     */
    public function getAllProduct($productSku = null)
    {
        $collection = $this->productCollectionFactory->create();
        if ($productSku) {
            $collection->addAttributeToFilter('sku', [
                'like' => '%' . $productSku . '%'
            ]);
        }
        $collection->addAttributeToSelect('*')->load();
        $collection->setCurPage(20);
        
        $productSku = [];
        foreach ($collection as $product) {
            $productSku['sku'][] = $product->getSku();
            $productSku['name'][] = $product->getName();
        }
        return $productSku;
    }
}