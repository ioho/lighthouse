<?php
/**
 * Copyright © 2016 InSync. All rights reserved.
 */
namespace Appseconnect\B2BMage\Api\CreditLimit\Data;

/**
 * Credit Limit interface.
 * @api
 */
interface CreditLimitInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants defined for keys of the data array.
     * Identical to the name of the getter in snake case
     */
    const CUSTOMER_ID = 'customer_id';

    const CREDIT_LIMIT = 'credit_limit';

    const AVAILABLE_BALANCE = 'available_balance';

    const INCREMENT_ID = 'increment_id';

    const DEBIT_AMOUNT = 'debit_amount';

    const CREDIT_AMOUNT = 'credit_amount';

    /**
     * to set credit amount
     *
     * @return float|null
     */
    public function getCreditAmount();

    /**
     * to set credit amount
     *
     * @param float $debitAmount
     * @return $this
     */
    public function setCreditAmount($creditAmount);

    /**
     * to set debit amount
     *
     * @return float|null
     */
    public function getDebitAmount();

    /**
     * to set debit amount
     *
     * @param float $debitAmount
     * @return $this
     */
    public function setDebitAmount($debitAmount);

    /**
     * Order Increment Id
     *
     * @return string|null
     */
    public function getIncrementId();

    /**
     * Order Increment Id
     *
     * @param string $incrementId
     * @return $this
     */
    public function setIncrementId($incrementId);

    /**
     * Get Customer id
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set Customer id
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * Get Credit Limit
     *
     * @return float|null
     */
    public function getCreditLimit();

    /**
     * Set Credit Limit
     *
     * @param float $creditLimit
     * @return $this
     */
    public function setCreditLimit($creditLimit);

    /**
     * Get Available Balance
     *
     * @return float|null
     */
    public function getAvailableBalance();

    /**
     * Set Available Balance
     *
     * @param float $creditBalance
     * @return $this
     */
    public function setAvailableBalance($availableBalance);
}
