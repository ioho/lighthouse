<?php
/**
 *
 * Copyright © 2016 InSync. All rights reserved.
 */
namespace Appseconnect\B2BMage\Api\CreditLimit;

/**
 * Interface for managing credit limit.
 * @api
 */
interface CreditLimitRepositoryInterface
{

    /**
     * Get credit limit of a B2B customer
     *
     * @param int $customerId
     * @return \Appseconnect\B2BMage\Api\CreditLimit\Data\CreditLimitInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($customerId);

    /**
     * Add credit limit for a B2B customer
     *
     * @param \Appseconnect\B2BMage\Api\CreditLimit\Data\CreditLimitInterface $creditLimitData
     * @return \Appseconnect\B2BMage\Api\CreditLimit\Data\CreditLimitInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Appseconnect\B2BMage\Api\CreditLimit\Data\CreditLimitInterface $creditLimitData);

    /**
     * Update credit limit for a B2B customer
     *
     * @param \Appseconnect\B2BMage\Api\CreditLimit\Data\CreditLimitInterface $creditLimitData
     * @return \Appseconnect\B2BMage\Api\CreditLimit\Data\CreditLimitInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function update(\Appseconnect\B2BMage\Api\CreditLimit\Data\CreditLimitInterface $creditLimitData);
}
