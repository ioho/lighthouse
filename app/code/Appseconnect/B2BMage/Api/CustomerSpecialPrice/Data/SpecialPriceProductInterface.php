<?php
namespace Appseconnect\B2BMage\Api\CustomerSpecialPrice\Data;

/**
 * Special Price Product Interface.
 * @api
 */
interface SpecialPriceProductInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case
     */
    
    /**
     * Special price product ID
     */
    const ID = 'id';

    /**
     * Parent id.
     */
    const PARENT_ID = 'parent_id';

    /**
     * product sku.
     */
    const PRODUCT_SKU = 'product_sku';

    /**
     * price.
     */
    const SPECIAL_PRICE = 'special_price';

    /**
     * error
     */
    const ERROR = 'error';

    /**
     * Get the id for special price product.
     *
     * @return int|null Pricelist Id.
     */
    public function getId();

    /**
     * Get special price id.
     *
     * @return int|null Website Id.
     */
    public function getParentId();

    /**
     * Get product sku.
     *
     * @return string|null .
     */
    public function getProductSku();

    /**
     * Get special price product amount.
     *
     * @return double|null .
     */
    public function getSpecialPrice();

    /**
     * Set the id for special price product.
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Set special price id.
     *
     * @param int $id
     * @return $this
     */
    public function setParentId($id);

    /**
     * Set product sku.
     *
     * @param string $sku
     * @return $this
     */
    public function setProductSku($sku);

    /**
     * Set amount.
     *
     * @param double $price
     * @return $this
     */
    public function setSpecialPrice($price);

    /**
     * Get error.
     *
     * @return string|null Website Id.
     */
    public function getError();

    /**
     * Set error.
     *
     * @param string $error
     * @return $this
     */
    public function setError($error);
}
