<?php
namespace Appseconnect\B2BMage\Api\CustomerSpecialPrice;

use Appseconnect\B2BMage\Api\CustomerSpecialPrice\Data\SpecialPriceInterface;

/**
 * Special Price Repository Interface.
 *
 * @api
 */
interface SpecialPriceRepositoryInterface
{

    /**
     * Loads special price.
     *
     * @param int $specialpriceId
     * @return \Appseconnect\B2BMage\Api\CustomerSpecialPrice\Data\SpecialPriceInterface
     */
    public function get($specialpriceId);

    /**
     * Update special price.
     *
     * @param SpecialPriceInterface $specialprice
     * @return \Appseconnect\B2BMage\Api\CustomerSpecialPrice\Data\SpecialPriceInterface
     */
    public function update(SpecialPriceInterface $specialprice);

    /**
     * Add special price.
     *
     * @param SpecialPriceInterface $specialprice
     * @return \Appseconnect\B2BMage\Api\CustomerSpecialPrice\Data\SpecialPriceInterface
     */
    public function create(SpecialPriceInterface $specialprice);
}
