<?php

namespace Appseconnect\B2BMage\Api\Sales\Data;

/**
 * Order Result interface.
 * @api
 */
interface OrderResultInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**#@+
	 * Constants defined for keys of the data array. Identical to the name of the getter in snake case
	 */
    const ORDER_ID = 'order_id';

    const INCREMENT_ID = 'increment_id';
    /**#@-*/

    /**
     * Get Order Id.
     *
     * @return string|null
     */
    public function getOrderId();
    
    /**
     * Set Order Id.
     *
     * @param string $orderId
     * @return $this
     */
    public function setOrderId($orderId = null);
    
    /**
     * Get Increment Id.
     *
     * @return string|null
     */
    public function getIncrementId();
    
    /**
     * Set Increment Id.
     *
     * @param string $incrementId
     * @return $this
     */
    public function setIncrementId($incrementId = null);
}
