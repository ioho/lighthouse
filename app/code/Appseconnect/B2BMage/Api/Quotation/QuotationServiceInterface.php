<?php
namespace Appseconnect\B2BMage\Api\Quotation;

/**
 * Quotation service interface.
 *
 * @api
 */
interface QuotationServiceInterface
{
    /**
     * Submit a quote.
     *
     * @param int $id Quote ID.
     * @return bool
     */
    public function submitQuoteById($id);
    
    /**
     * Approve a quote.
     *
     * @param int $id Quote ID.
     * @return bool
     */
    public function approveQuoteById($id);
    
    /**
     * Hold a quote.
     *
     * @param int $id Quote ID.
     * @return bool
     */
    public function holdQuoteById($id);
    
    /**
     * Unhold a quote.
     *
     * @param int $id Quote ID.
     * @return bool
     */
    public function unholdQuoteById($id);
    
    /**
     * Cancel a quote.
     *
     * @param int $id Quote ID.
     * @return bool
     */
    public function cancelQuoteById($id);
}
