<?php

namespace Appseconnect\B2BMage\Api\Quotation\Data;

/**
 * Interface for quotes search results.
 * @api
 */
interface QuoteSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get quotes list.
     *
     * @return \Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface[]
     */
    public function getItems();

    /**
     * Set quotes list.
     *
     * @param \Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
