<?php
namespace Appseconnect\B2BMage\Api\Quotation\Data;

/**
 * Quotation Status interface.
 */
interface QuoteStatusInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    /**
     * Status
     */
    const STATUS = "status";

    /**
     * Label
     */
    const LABEL = "label";

    /**
     * Gets the Status .
     *
     * @return string|null Status .
     */
    public function getStatus();

    /**
     * Sets Status .
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Gets the Label .
     *
     * @return string|null Label .
     */
    public function getLabel();

    /**
     * Sets Label .
     *
     * @param string $label
     * @return $this
     */
    public function setLabel($label);
}
