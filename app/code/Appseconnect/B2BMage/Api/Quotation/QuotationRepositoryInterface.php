<?php
namespace Appseconnect\B2BMage\Api\Quotation;

use Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface;
use Appseconnect\B2BMage\Api\Quotation\Data\QuoteSearchResultsInterface;

/**
 * Quotation repository interface.
 *
 * @api
 */
interface QuotationRepositoryInterface
{

    /**
     * Loads a specified quotation.
     *
     * @param int $id
     * @return \Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface
     */
    public function get($id);

    /**
     * Get quote by contact Id
     *
     * @param int $contactId
     * @param int[] $sharedStoreIds
     * @return \Appseconnect\B2BMage\Api\Quotation\Data\QuoteInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getForContact($contactId, array $sharedStoreIds = []);

    /**
     * Save quotation
     *
     * @param QuoteInterface $quote
     * @return void
     */
    public function save(QuoteInterface $quote);

    /**
     * Retrieve quotes which match a specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Appseconnect\B2BMage\Api\Quotation\Data\QuoteSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Creates an empty quote for a specified contact person.
     *
     * @param int $contactPersonId
     * @return int Quote ID.
     * @throws \Magento\Framework\Exception\CouldNotSaveException The empty quote could not be created.
     */
    public function createEmptyQuoteForContact($contactPersonId);
}
