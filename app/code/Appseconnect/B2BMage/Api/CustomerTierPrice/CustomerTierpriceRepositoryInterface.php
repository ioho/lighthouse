<?php

namespace Appseconnect\B2BMage\Api\CustomerTierPrice;

/**
 * Interface for managing customer specific tier price.
 * @api
 */
interface CustomerTierpriceRepositoryInterface
{

    /**
     * Create customer specific tier price.
     *
     * @param \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\CustomerTierpriceInterface $tierPrice
     * @return \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\CustomerTierpriceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\CustomerTierpriceInterface $tierPrice
    );
    
    /**
     * Update customer specific tier price.
     *
     * @param \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\CustomerTierpriceInterface $tierPrice
     * @return \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\CustomerTierpriceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function update(
        \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\CustomerTierpriceInterface $tierPrice
    );

    /**
     * Get customer specific tier price.
     *
     * @param int $customerId
     * @return \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\CustomerTierpriceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByCustomerId($customerId);
}
