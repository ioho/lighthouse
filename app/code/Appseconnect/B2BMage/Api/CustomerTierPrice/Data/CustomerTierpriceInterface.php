<?php

namespace Appseconnect\B2BMage\Api\CustomerTierPrice\Data;

/**
 * Customer Tierprice.
 * @api
 */
interface CustomerTierpriceInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**#@+
	 * Constants defined for keys of the data array. Identical to the name of the getter in snake case
	 */
    const ID = 'id';
    const WEBSITE_ID = 'website_id';
    const CUSTOMER_ID = 'customer_id';
    const PRICELIST_ID = 'pricelist_id';
    const DISCOUNT_TYPE = 'discount_type';
    const IS_ACTIVE = 'is_active';
    const PRODUCT_DATA='product_data';
    /**#@-*/

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId();
    
    /**
     * Set id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);
    
    /**
     * Get website id
     *
     * @return int|null
     */
    public function getWebsiteId();

    /**
     * Set website id
     *
     * @param int $websiteId
     * @return $this
     */
    public function setWebsiteId($websiteId);
    /**
     * Get Customer id
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set Customer id
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId);
    /**
     * Get Pricelist Id
     *
     * @return int|null
     */
    public function getPricelistId();

    /**
     * Set Pricelist Id
     *
     * @param int $pricelistId
     * @return $this
     */
    public function setPricelistId($pricelistId);
    
    /**
     * Get discount type
     *
     * @return int|null
     */
    public function getDiscountType();

    /**
     * Set discount type
     *
     * @param int $discountType
     * @return $this
     */
    public function setDiscountType($discountType);
    
    /**
     * Get is active
     *
     * @return int|null
     */
    public function getIsActive();
    
    /**
     * Set is active
     *
     * @param int $isActive
     * @return $this
     */
    public function setIsActive($isActive);
    
    /**
     * Get product data
     *
     * @return \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\ProductDataInterface[]|null
     */
    public function getProductData();
    
    /**
     * Set product data
     *
     * @param \Appseconnect\B2BMage\Api\CustomerTierPrice\Data\ProductDataInterface[] $productData
     * @return $this
     */
    public function setProductData($productData);
}
