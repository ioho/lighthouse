<?php
namespace Appseconnect\B2BMage\Api\CategoryDiscount;

use Appseconnect\B2BMage\Api\CategoryDiscount\Data\CategoryDiscountInterface;
use Appseconnect\B2BMage\Api\CategoryDiscount\Data\CategoryDiscountDataInterface;

/**
 * Interface for managing customer specific category discount.
 * @api
 */
interface CustomerCategoryDiscountRepositoryInterface
{

    /**
     * Create customer specific category discount.
     *
     * @param CategoryDiscountInterface $categoryDiscount
     * @param string $customerId
     * @return \Appseconnect\B2BMage\Api\CategoryDiscount\Data\CategoryDiscountInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createCustomerCategoryDiscount(CategoryDiscountInterface $categoryDiscount);

    /**
     * Get customer specific category discounts.
     *
     * @param int $customerId
     * @return \Appseconnect\B2BMage\Api\CategoryDiscount\Data\CategoryDiscountDataInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomerCategoryDiscount($customerId);

    /**
     * Update customer specific category discount.
     *
     * @param CategoryDiscountInterface $categoryDiscount
     * @param string $customerId
     * @return \Appseconnect\B2BMage\Api\CategoryDiscount\Data\CategoryDiscountInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function updateCustomerCategoryDiscount(CategoryDiscountInterface $categoryDiscount);
}
