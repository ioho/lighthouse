<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Appseconnect\B2BMage\Api\CategoryDiscount\Data;

/**
 * Customer interface.
 * @api
 */
interface CategoryDiscountDataInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants defined for keys of the data array.
     * Identical to the name of the getter in snake case
     */
    const CATEGORYDISCOUNT_ID = 'categorydiscount_id';

    const CATEGORY_ID = 'category_id';

    const DISCOUNT_FACTOR = 'discount_factor';

    const IS_ACTIVE = 'is_active';

    /**
     * #@-
     */
    
    /**
     * Get category discount id
     *
     * @return int|null
     */
    public function getCategorydiscountId();

    /**
     * Set category discount id
     *
     * @param int $categoryDiscountId
     * @return $this
     */
    public function setCategorydiscountId($categoryDiscountId);

    /**
     * Get category id
     *
     * @return int|null
     */
    public function getCategoryId();

    /**
     * Set category id
     *
     * @param int $categoryId
     * @return $this
     */
    public function setCategoryId($categoryId);

    /**
     * Get discount factor
     *
     * @return string|null
     */
    public function getDiscountFactor();

    /**
     * Set discount factor
     *
     * @param string $discountFactor
     * @return $this
     */
    public function setDiscountFactor($discountFactor);

    /**
     * Get is active
     *
     * @return int|null
     */
    public function getIsActive();

    /**
     * Set is active
     *
     * @param int $isActive
     * @return $this
     */
    public function setIsActive($isActive);
}
