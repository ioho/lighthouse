<?php
namespace Appseconnect\B2BMage\Api\Pricelist;

use Appseconnect\B2BMage\Api\Pricelist\Data\PricelistInterface;
use Appseconnect\B2BMage\Api\Pricelist\Data\ProductAssignInterface;
use Appseconnect\B2BMage\Api\Pricelist\Data\PricelistAssignInterface;

/**
 * Pricelist repository interface.
 *
 * Pricelist is one of the pricerule that can be applied to products and assigned to customers.
 * @api
 */
interface PricelistRepositoryInterface
{

    /**
     * Loads a customer specified pricelist.
     *
     * @param int $customerId
     * @return \Appseconnect\B2BMage\Api\Pricelist\Data\PricelistInterface
     */
    public function get($customerId);

    /**
     * Performs upadate operations for a specified pricelist.
     *
     * @param PricelistInterface[] $pricelist
     * @return \Appseconnect\B2BMage\Api\Pricelist\Data\PricelistInterface
     */
    public function update(PricelistInterface $pricelist);

    /**
     * Performs assign operation of pricelist to a customer.
     *
     * @param PricelistAssignInterface $entity
     * @return \Appseconnect\B2BMage\Api\Pricelist\Data\PricelistAssignInterface
     */
    public function assignPricelist(PricelistAssignInterface $entity);

    /**
     * Performs persist operations for a specified pricelist.
     *
     * @param PricelistInterface[] $pricelist
     * @return \Appseconnect\B2BMage\Api\Pricelist\Data\PricelistInterface
     */
    public function create(PricelistInterface $pricelist);

    /**
     * Performs assign operations for products in pricelist.
     *
     * @param ProductAssignInterface[] $product
     * @param bool $isAdmin
     * @return \Appseconnect\B2BMage\Api\Pricelist\Data\ProductAssignInterface
     */
    public function assignProducts(ProductAssignInterface $product, $isAdmin = false);
}
