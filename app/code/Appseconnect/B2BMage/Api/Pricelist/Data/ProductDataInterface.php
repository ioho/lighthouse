<?php
namespace Appseconnect\B2BMage\Api\Pricelist\Data;

/**
 * Product Data Interface.
 * @api
 */
interface ProductDataInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants defined for keys of the data array.
     * Identical to the name of the getter in snake case
     */
    const SKU = 'sku';

    const PRICE = 'price';

    /**
     * #@-
     */
    
    /**
     * Get Sku
     *
     * @return string|null
     */
    public function getSku();

    /**
     * Set Sku
     *
     * @param int $sku
     * @return $this
     */
    public function setSku($sku);

    /**
     * Get Price
     *
     * @return double|null
     */
    public function getPrice();

    /**
     * Set Price
     *
     * @param double $price
     * @return $this
     */
    public function setPrice($price);
}
