<?php
namespace Appseconnect\B2BMage\Api\Pricelist\Data;

/**
 * Pricelist Assign interface.
 *
 * @api
 */
interface PricelistAssignInterface
{

    /**
     * #@+
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case
     */
    
    /**
     * Customer ID
     */
    const CUSTOMER_ID = 'customer_id';

    /**
     * Pricelist ID.
     */
    const PRICELIST_ID = 'pricelist_id';

    /**
     * Get Customer Id
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Get Pricelist Id
     *
     * @return int|null
     */
    public function getPricelistId();

    /**
     * Set Customer Id
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * Set Pricelist Id
     *
     * @param int $pricelistId
     * @return $this
     */
    public function setPricelistId($pricelistId);
}
