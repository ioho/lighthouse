<?php
namespace Appseconnect\B2BMage\Api\Pricelist\Data;

/**
 * Product Assign Interface.
 * @api
 */
interface ProductAssignInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants defined for keys of the data array.
     * Identical to the name of the getter in snake case
     */
    const PRICELIST_ID = 'pricelist_id';

    const PRODUCT_DATA = 'product_data';

    const ERROR = 'error';

    /**
     * #@-
     */
    
    /**
     * Get Pricelist Id
     *
     * @return int|null
     */
    public function getPricelistId();

    /**
     * Set Pricelist Id
     *
     * @param int $pricelistId
     * @return $this
     */
    public function setPricelistId($pricelistId);

    /**
     * Get product data
     *
     * @return \Appseconnect\B2BMage\Api\Pricelist\Data\ProductDataInterface[]|null
     */
    public function getProductData();

    /**
     * Set product data
     *
     * @param \Appseconnect\B2BMage\Api\Pricelist\Data\ProductDataInterface[] $productData
     * @return $this
     */
    public function setProductData($productData);

    /**
     * Get Error
     *
     * @return string|null
     */
    public function getError();

    /**
     * Set Error
     *
     * @param string $error
     * @return $this
     */
    public function setError($error);
}
