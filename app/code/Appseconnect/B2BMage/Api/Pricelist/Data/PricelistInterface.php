<?php
namespace Appseconnect\B2BMage\Api\Pricelist\Data;

/**
 * Pricelist interface.
 * @api
 */
interface PricelistInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case
     */
    
    /**
     * Pricelist ID
     */
    const ID = 'id';

    /**
     * Website ID.
     */
    const WEBSITE_ID = 'website_id';

    /**
     * Pricelist Name.
     */
    const PRICELIST_NAME = 'pricelist_name';

    /**
     * Factor.
     */
    const FACTOR = 'factor';

    /**
     * Status.
     */
    const IS_ACTIVE = 'is_active';

    /**
     * Gets the id for pricelist.
     *
     * @return int|null Pricelist Id.
     */
    public function getId();

    /**
     * Gets the website id for pricelist.
     *
     * @return int|null Website Id.
     */
    public function getWebsiteId();

    /**
     * Gets pricelist name.
     *
     * @return string|null Pricelist Name.
     */
    public function getPricelistName();

    /**
     * Gets the factor.
     *
     * @return float|null Factor.
     */
    public function getFactor();

    /**
     * Gets the is active.
     *
     * @return int|null Is Active.
     */
    public function getIsActive();

    /**
     * Sets Pricelist ID.
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Sets website ID.
     *
     * @param int $entityId
     * @return $this
     */
    public function setWebsiteId($websiteId);

    /**
     * Sets price name.
     *
     * @param string $pricelistName
     * @return $this
     */
    public function setPricelistName($pricelistName);

    /**
     * Sets factor
     *
     * @param float $factor
     * @return $this
     */
    public function setFactor($factor);

    /**
     * Sets Is Active.
     *
     * @param int $isActive
     * @return $this
     */
    public function setIsActive($isActive);
}
