<?php
namespace Appseconnect\B2BMage\Api\Pricelist\Data;

/**
 * Update Pricelist interface.
 * @api
 */
interface UpdatePricelistInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case
     */
    
    /**
     * Pricelist ID
     */
    const ID = 'id';

    /**
     * Website ID.
     */
    const WEBSITE_ID = 'website_id';

    /**
     * Pricelist Name.
     */
    const PRICELIST_NAME = 'pricelist_name';

    /**
     * Parent Pricelist ID.
     */
    const PARENT_ID = 'parent_id';

    /**
     * Discount Factor.
     */
    const DISCOUNT_FACTOR = 'discount_factor';

    /**
     * Status.
     */
    const IS_ACTIVE = 'is_active';

    /**
     * Product Sku.
     */
    const PRODUCT_SKUS = 'product_skus';

    /**
     * Gets the id for pricelist.
     *
     * @return int|null Pricelist Id.
     */
    public function getId();

    /**
     * Gets the website id for pricelist.
     *
     * @return int|null Website Id.
     */
    public function getWebsiteId();

    /**
     * Gets pricelist name.
     *
     * @return string|null Pricelist Name.
     */
    public function getPricelistName();

    /**
     * Gets the parent id.
     *
     * @return string|null Parent Id.
     */
    public function getParentId();

    /**
     * Gets the discount factor.
     *
     * @return float|null Discount Factor.
     */
    public function getDiscountFactor();

    /**
     * Gets the is active.
     *
     * @return string|null Is Active.
     */
    public function getIsActive();

    /**
     * Get Product Sku.
     *
     * @return string[]|null
     */
    public function getProductSkus();

    /**
     * Sets Pricelist ID.
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Sets website ID.
     *
     * @param int $entityId
     * @return $this
     */
    public function setWebsiteId($websiteId);

    /**
     * Sets price name.
     *
     * @param string $pricelistName
     * @return $this
     */
    public function setPricelistName($pricelistName);

    /**
     * Sets parent ID.
     *
     * @param string $parentId
     * @return $this
     */
    public function setParentId($parentId);

    /**
     * Sets discount factor
     *
     * @param float $discountFactor
     * @return $this
     */
    public function setDiscountFactor($discountFactor);

    /**
     * Sets Is Active.
     *
     * @param string $isActive
     * @return $this
     */
    public function setIsActive($isActive);

    /**
     * Sets Product Sku.
     *
     * @param string[] $productSku
     * @return $this
     */
    public function setProductSkus(array $productSku = null);
}
