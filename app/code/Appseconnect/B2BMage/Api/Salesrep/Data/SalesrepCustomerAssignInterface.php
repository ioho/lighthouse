<?php
/**
 *
 * Copyright © 2017 Appseconnect. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Appseconnect\B2BMage\Api\Salesrep\Data;

/**
 * Salesrep Customer Assign interface.
 * @api
 */
interface SalesrepCustomerAssignInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**#@+
	 * Constants defined for keys of the data array. Identical to the name of the getter in snake case
	 */
    const SALESREP_ID = 'salesrep_id';
    const CUSTOMER_IDS = 'customer_ids';
    /**#@-*/

    /**
     * Get salesrep id
     *
     * @return int|null
     */
    public function getSalesrepId();

    /**
     * Set salesrep id
     *
     * @param int $salesrepId
     * @return $this
     */
    public function setSalesrepId($salesrepId);
    /**
     * Get customer ids
     *
     * @return string[]|null
     */
    public function getCustomerIds();

    /**
     * Set customer ids
     *
     * @param string[] $customerIds
     * @return $this
     */
    public function setCustomerIds(array $customerIds);
}
