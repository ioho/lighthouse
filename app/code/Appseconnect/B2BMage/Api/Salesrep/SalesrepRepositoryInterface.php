<?php
/**
 *
 * Copyright © 2017 Appseconnect. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Appseconnect\B2BMage\Api\Salesrep;

/**
 * Salesrep CRUD interface.
 * @api
 */
interface SalesrepRepositoryInterface
{
    
    /**
     * Create salesrep account. Perform necessary business operations like sending email.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $salesrepData
     * @param string $password
     * @param string $redirectUrl
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createAccount(
        \Magento\Customer\Api\Data\CustomerInterface $salesrepData,
        $password = null,
        $redirectUrl = ''
    );
    
    /**
     * Update a salesrep.
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $salesrepData
     * @param string $passwordHash
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\InputException If bad input is provided
     * @throws \Magento\Framework\Exception\State\InputMismatchException If the provided email is already used
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Magento\Customer\Api\Data\CustomerInterface $salesrepData, $passwordHash = null);
    
    /**
     * Assign a salesrep to customers.
     *
     * @param \Appseconnect\B2BMage\Api\Salesrep\Data\SalesrepCustomerAssignInterface $entity
     * @return \Appseconnect\B2BMage\Api\Salesrep\Data\SalesrepCustomerAssignInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function assignCustomer(\Appseconnect\B2BMage\Api\Salesrep\Data\SalesrepCustomerAssignInterface $entity);
    
    /**
     * Retrieve customers which match a specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Customer\Api\Data\CustomerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomerData(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
