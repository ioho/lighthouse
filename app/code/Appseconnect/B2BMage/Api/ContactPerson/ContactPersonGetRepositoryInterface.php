<?php
/**
 *
 * Copyright � 2016 Appseconnect. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Appseconnect\B2BMage\Api\ContactPerson;

/**
 * Interface for managing customers accounts.
 * @api
 */
interface ContactPersonGetRepositoryInterface
{

    /**
     * Retrieve customers which match a specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Customer\Api\Data\CustomerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getContactPersonData(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}
