<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Appseconnect\B2BMage\Api\ContactPerson\Data;

/**
 * Customer interface.
 * @api
 */
interface ContactPersonInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants defined for keys of the data array.
     * Identical to the name of the getter in snake case
     */
    const CONTACTPERSON = 'contactperson';

    /**
     * #@-
     */
    
    /**
     * Get Contactperson.
     *
     * @return \Appseconnect\B2BMage\Api\ContactPerson\Data\CustomerExtendInterface[]|null
     */
    public function getContactperson();

    /**
     * Set Contactperson.
     *
     * @param \Appseconnect\B2BMage\Api\ContactPerson\Data\CustomerExtendInterface[] $contactperson
     * @return $this
     */
    public function setContactperson(array $contactperson = null);
}
