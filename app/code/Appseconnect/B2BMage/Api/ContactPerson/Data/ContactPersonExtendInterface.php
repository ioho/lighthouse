<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Appseconnect\B2BMage\Api\ContactPerson\Data;

/**
 * Customer extend interface.
 * @api
 */
interface ContactPersonExtendInterface extends \Magento\Customer\Api\Data\CustomerInterface
{

    /**
     * #@+
     * Constants defined for keys of the data array.
     * Identical to the name of the getter in snake case
     */
    const CUSTOMER_ID = 'customer_id';

    const CONTACTPERSON_ID = 'contactperson_id';

    /**
     * #@-
     */
    
    /**
     * Get Customer id
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set Customer id
     *
     * @param
     *            $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * Get Contact Person id
     *
     * @return int|null
     */
    public function getContactPersonId();

    /**
     * Set Contact Person id
     *
     * @param
     *            $contactPersonId
     * @return $this
     */
    public function setContactPersonId($contactPersonId);
}
