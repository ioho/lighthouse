<?php
namespace Appseconnect\B2BMage\Api\ContactPerson;

use Appseconnect\B2BMage\Api\ContactPerson\Data\ContactPersonExtendInterface;

/**
 * Interface for managing customers accounts.
 * @api
 */
interface ContactPersonRepositoryInterface
{

    /**
     * Create customer account.
     * Perform necessary business operations like sending email.
     *
     * @param ContactPersonExtendInterface $contactPerson
     * @param string $password
     * @param string $redirectUrl
     * @return \Appseconnect\B2BMage\Api\ContactPerson\Data\ContactPersonExtendInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createContactPerson(
        ContactPersonExtendInterface $contactPerson
    );
}
