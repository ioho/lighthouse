<?php
namespace Appseconnect\B2BMage\Api\ErpDocs\Data;

/**
 * Pdf Interface.
 * @api
 */
interface PdfInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{

    /**
     * #@+
     * Constants defined for keys of the data array.
     * Identical to the name of the getter in snake case
     */
    const PDF_DATA = 'pdf_data';

    const INVOICE = 'invoice_increment_id';

    const ORDER_ID = 'order_id';

    /**
     * get order Id
     *
     * @return int|null
     */
    public function getOrderId();

    /**
     * Set order Id
     *
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * get invoice
     *
     * @return string|null
     */
    public function getInvoiceIncrementId();

    /**
     * Set invoice
     *
     * @param string $invoice
     * @return $this
     */
    public function setInvoiceIncrementId($invoice);

    /**
     * get Base64 data of pdf
     *
     * @return string|null
     */
    public function getPdfData();

    /**
     * Set Base64 data of pdf
     *
     * @param string $pdfData
     * @return $this
     */
    public function setPdfData($pdfData);
}
