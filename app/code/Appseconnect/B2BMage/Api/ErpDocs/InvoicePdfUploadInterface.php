<?php
/**
 *
 * Copyright Appseconnect. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Appseconnect\B2BMage\Api\ErpDocs;

/**
 * @api
 */
interface InvoicePdfUploadInterface
{
    /**
     * Upload invoice
     *
     * @param \Appseconnect\B2BMage\Api\ErpDocs\Data\PdfInterface $pdf
     * @return \Appseconnect\B2BMage\Api\ErpDocs\Data\PdfInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function upload(\Appseconnect\B2BMage\Api\ErpDocs\Data\PdfInterface $pdf);
}
