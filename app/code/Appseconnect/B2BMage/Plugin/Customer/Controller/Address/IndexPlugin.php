<?php
namespace Appseconnect\B2BMage\Plugin\Customer\Controller\Address;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\Session;

class IndexPlugin
{
    
    /**
     * Customer repository.
     *
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    public $customerRepository;

    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;
    
    /**
     *
     * @var Session
     */
    public $customerSession;

    /**
     *
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    public $resultRedirectFactory;
    
    /**
     *
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    public $redirect;

    /**
     * Helper Contact Person.
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     *
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param Session $customerSession
     * @param \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        Session $customerSession,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
    
        $this->resultPageFactory = $resultPageFactory;
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->redirect = $redirect;
        $this->helperContactPerson = $helperContactPerson;
        $this->customerFactory = $customerFactory;
    }

    /**
     * @param \Magento\Customer\Controller\Address\Index $subject
     * @param \Closure $proceed
     * @return \Magento\Framework\View\Result\Page|mixed
     */
    public function aroundExecute(
        \Magento\Customer\Controller\Address\Index $subject,
        \Closure $proceed
    ) {
    
        $customerId = $this->customerSession->getCustomerId();
        if ($this->helperContactPerson->isContactPerson($this->customerFactory->create()
            ->load($customerId))) {
            $parentCustomerMapData = $this->helperContactPerson->getCustomerId($customerId);
            $customerId = $parentCustomerMapData ?
            $parentCustomerMapData['customer_id'] :
            $customerId;
        }
        $addresses = $this->customerRepository->getById($customerId)->getAddresses();
        if ($addresses) {
            /** @var \Magento\Framework\View\Result\Page $resultPage */
            $resultPage = $this->resultPageFactory->create();
            $block = $resultPage->getLayout()->getBlock('address_book');
            if ($block) {
                $block->setRefererUrl($this->redirect->getRefererUrl());
            }
            return $resultPage;
        } else {
            return $this->resultRedirectFactory->create()->setPath('*/*/new');
        }
    }
}
