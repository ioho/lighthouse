<?php
namespace Appseconnect\B2BMage\Plugin\Customer\Helper\Session;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Customer\Api\Data\AddressInterface;

class CurrentCustomerAddressPlugin
{

    /**
     *
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    public $currentCustomer;

    /**
     *
     * @var AccountManagementInterface
     */
    public $accountManagement;

    /**
     * Helper Contact Person.
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     *
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     *
     * @param CurrentCustomer $currentCustomer
     * @param AccountManagementInterface $accountManagement
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        CurrentCustomer $currentCustomer,
        AccountManagementInterface $accountManagement,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
    
        $this->currentCustomer = $currentCustomer;
        $this->accountManagement = $accountManagement;
        $this->helperContactPerson = $helperContactPerson;
        $this->customerFactory = $customerFactory;
    }

    /**
     *
     * @param \Magento\Customer\Helper\Session\CurrentCustomerAddress $subject
     * @param \Closure $proceed
     * @return \Magento\Framework\View\Result\Page|mixed
     */
    public function aroundGetDefaultBillingAddress(
        \Magento\Customer\Helper\Session\CurrentCustomerAddress $subject,
        \Closure $proceed
    ) {
    
        $customerId = $this->currentCustomer->getCustomerId();
        if ($this->helperContactPerson->isContactPerson($this->customerFactory->create()
            ->load($customerId))) {
            $parentCustomerMapData = $this->helperContactPerson->getCustomerId($customerId);
            $customerId = $parentCustomerMapData ? $parentCustomerMapData['customer_id'] : $customerId;
        }
        return $this->accountManagement->getDefaultBillingAddress($customerId);
    }

    /**
     *
     * @param \Magento\Customer\Helper\Session\CurrentCustomerAddress $subject
     * @param \Closure $proceed
     * @return \Magento\Framework\View\Result\Page|mixed
     */
    public function aroundGetDefaultShippingAddress(
        \Magento\Customer\Helper\Session\CurrentCustomerAddress $subject,
        \Closure $proceed
    ) {
    
        $customerId = $this->currentCustomer->getCustomerId();
        if ($this->helperContactPerson->isContactPerson($this->customerFactory->create()
            ->load($customerId))) {
            $parentCustomerMapData = $this->helperContactPerson->getCustomerId($customerId);
            $customerId = $parentCustomerMapData ? $parentCustomerMapData['customer_id'] : $customerId;
        }
        return $this->accountManagement->getDefaultShippingAddress($customerId);
    }
}
