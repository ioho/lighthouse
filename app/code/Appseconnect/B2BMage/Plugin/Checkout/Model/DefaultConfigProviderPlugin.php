<?php
namespace Appseconnect\B2BMage\Plugin\Checkout\Model;

use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Context as CustomerContext;
use Magento\Framework\App\Http\Context as HttpContext;

class DefaultConfigProviderPlugin
{
    /**
     *
     * @var \Magento\Customer\Model\Address\Mapper
     */
    public $addressMapper;

    /**
     *
     * @var \Magento\Customer\Model\Address\Config
     */
    public $addressConfig;

    /**
     *
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     *
     * @var Session
     */
    private $customerSession;
    
    /**
     *
     * @var HttpContext
     */
    public $httpContext;
    
    /**
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     *
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @param \Magento\Customer\Model\Address\Mapper $addressMapper
     * @param \Magento\Customer\Model\Address\Config $addressConfig
     * @param HttpContext $httpContext
     * @param CustomerRepository $customerRepository
     * @param Session $customerSession
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        \Magento\Customer\Model\Address\Mapper $addressMapper,
        \Magento\Customer\Model\Address\Config $addressConfig,
        HttpContext $httpContext,
        CustomerRepository $customerRepository,
        Session $customerSession,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
    
        $this->addressMapper = $addressMapper;
        $this->addressConfig = $addressConfig;
        $this->httpContext = $httpContext;
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
        $this->helperContactPerson = $helperContactPerson;
        $this->customerFactory = $customerFactory;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     */
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, $result)
    {
        if (isset($result) && $result) {
            $result['customerData'] = $this->getCustomerData();
        }
        return $result;
    }

    /**
     * Retrieve customer data
     *
     * @return array
     */
    private function getCustomerData()
    {
        $customerData = [];
        if ($this->isCustomerLoggedIn()) {
            $customerId = $this->customerSession->getCustomerId();
            if ($this->helperContactPerson->isContactPerson($this->customerSession->getCustomer())) {
                $parentCustomerMapData = $this->helperContactPerson->getCustomerId($customerId);
                $customerId = $parentCustomerMapData ? $parentCustomerMapData['customer_id'] : $customerId; // B2B
            }
            
            $customer = $this->customerRepository->getById($customerId);
            $customerData = $customer->__toArray();
            foreach ($customer->getAddresses() as $key => $address) {
                $customerData['addresses'][$key]['inline'] = $this->getCustomerAddressInline($address);
            }
        }
        return $customerData;
    }

    /**
     * Set additional customer address data
     *
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     * @return string
     */
    private function getCustomerAddressInline($address)
    {
        $builtOutputAddressData = $this->addressMapper->toFlatArray($address);
        return $this->addressConfig->getFormatByCode(\Magento\Customer\Model\Address\Config::DEFAULT_ADDRESS_FORMAT)
            ->getRenderer()
            ->renderArray($builtOutputAddressData);
    }

    /**
     * Check if customer is logged in
     *
     * @return bool @codeCoverageIgnore
     */
    private function isCustomerLoggedIn()
    {
        return (bool) $this->httpContext->getValue(CustomerContext::CONTEXT_AUTH);
    }
}
