<?php
namespace Appseconnect\B2BMage\Plugin\Quote\Model\Quote\Address\Total;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\Session;

class GrandPlugin
{
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     *
     * @param Session $customerSession
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     */
    public function __construct(
        Session $customerSession,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    ) {
    
        $this->customerSession = $customerSession;
        $this->helperContactPerson = $helperContactPerson;
        $this->customerFactory = $customerFactory;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\Total\Grand $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return void
     */
    public function aroundCollect(
        \Magento\Quote\Model\Quote\Address\Total\Grand $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
    
        $customerSpecificDiscount = 0.00;
        $discount = 0.00;
        $grandTotal = $total->getGrandTotal();
        $baseGrandTotal = $total->getBaseGrandTotal();
        $totals = array_sum($total->getAllTotalAmounts());
        $baseTotals = array_sum($total->getAllBaseTotalAmounts());
        
        $customerId = $this->customerSession->getCustomer()->getId();
        if ($customerId) {
            $customerType = $this->customerSession->getCustomer()->getCustomerType();
            if ($customerType == 1) {
                $customerSpecificDiscount = $this->customerSession->getCustomer()->getCustomerSpecificDiscount();
            }
            if ($customerType == 3) {
                $customerDetail = $this->helperContactPerson->getCustomerId($customerId);
                $customerCollection = $this->customerFactory->create()->load($customerDetail['customer_id']);
                $customerSpecificDiscount = $customerCollection->getCustomerSpecificDiscount();
            }
            if ($customerType == 2) {
                $customerSpecificDiscount = 0;
            }
            
            if ($customerSpecificDiscount != 0) {
                $discount = $total->getSubtotal() * ($customerSpecificDiscount / 100);
            }
            
            $total->setGrandTotal($grandTotal + $totals - $discount);
            $total->setBaseGrandTotal($baseGrandTotal + $baseTotals - $discount);
            
            $result = $proceed($quote,$shippingAssignment,$total);
            return $result;
        }else{
            $result = $proceed($quote,$shippingAssignment,$total);
            return $result;
        }
    }
}
