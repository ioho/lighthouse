<?php
namespace Appseconnect\B2BMage\Plugin\Sales\Block\Order;

use Magento\Customer\Model\Session;

class History
{

    /**
     *
     * @var \Magento\Sales\Model\Order\Config
     */
    public $orderConfig;

    /**
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public $orders;
    
    /**
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    public $orderCollectionFactory;
    
    /**
     *
     * @var Session
     */
    public $customerSession;

    /**
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param Session $customerSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     */
    public function __construct(
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        Session $customerSession,
        \Magento\Sales\Model\Order\Config $orderConfig
    ) {
    
        $this->helperContactPerson = $helperContactPerson;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderConfig = $orderConfig;
        $this->customerSession = $customerSession;
    }

    /**
     * @param \Magento\Sales\Block\Order\History $subject
     * @param \Closure $proceed
     * @return boolean|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function aroundGetOrders(\Magento\Sales\Block\Order\History $subject, \Closure $proceed)
    {
        if (! ($customerId = $this->customerSession->getCustomerId())) {
            return false;
        }
        $contactPersonRole = null;
        $salesrepId = null;
        $contactId = [];
        $customerType = $this->customerSession->getCustomer()->getCustomerType();
        if ($customerType == 2) {
            $salesrepId = $customerId;
        } elseif ($customerType == 3) {
            $contactPersonId = $this->customerSession->getCustomerId();
            $contactPersonRole = $this->customerSession->getCustomer()->getContactpersonRole();
            $parentCustomerMapData = $this->helperContactPerson->getCustomerId($contactPersonId);
            $contactId[] = $parentCustomerMapData['customer_id'];
        }
        
        $proceed();
        if (! $this->orders) {
            if ($contactPersonRole == 2) {
                $orderCollection = $this->orderCollectionFactory->create()
                ->addFieldToFilter('contact_person_id', $contactPersonId);
            } elseif ($salesrepId) {
                $orderCollection = $this->orderCollectionFactory->create()
                ->addFieldToFilter('salesrep_id', $salesrepId);
            } elseif ($contactId) {
                $orderCollection = $this->orderCollectionFactory->create()
                ->addFieldToFilter('customer_id', [
                    'in' => $contactId
                ]);
            } else {
                $orderCollection = $this->orderCollectionFactory->create()
                ->addFieldToFilter('customer_id', $customerId);
            }
            $this->orders = $orderCollection->addFieldToSelect('*')
                ->addFieldToFilter('status', [
                'in' => $this->orderConfig->getVisibleOnFrontStatuses()
                ])
                ->setOrder('created_at', 'desc');
        }
        
        return $this->orders;
    }
}
