<?php

namespace Appseconnect\B2BMage\Plugin\Sales\Model;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemExtensionInterfaceFactory;
use Magento\Sales\Api\Data\OrderSearchResultInterface;

class OrderRepository
{
    /**
     * @var OrderExtensionFactory
     */
    protected $orderExtensionFactory;
    /**
     * @var OrderItemExtensionInterfaceFactory
     */
    private $orderItemExtensionInterfaceFactory;

    /**
     * OrderRepository constructor.
     * @param OrderExtensionFactory $orderExtensionFactory
     */
    public function __construct(
        OrderExtensionFactory $orderExtensionFactory
    )
    {
        $this->orderExtensionFactory = $orderExtensionFactory;
    }

    /**
     * load entity
     * @param OrderInterface $order
     * @param \Magento\Sales\Model\OrderRepository $subject ,
     * @return OrderInterface
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function afterGet(\Magento\Sales\Model\OrderRepository $subject, $order)
    {
        $extensionAttributes = $order->getExtensionAttributes();
        if (!$extensionAttributes) $extensionAttributes = $this->getOrderExtensionDependency();
        $extensionAttributes->setContactPersonId($order->getContactPersonId());
        $extensionAttributes->setIsPlacedbySalesrep($order->getIsPlacedbySalesrep());
        $extensionAttributes->setSalesrepId($order->getSalesrepId());
        $order->setExtensionAttributes($extensionAttributes);

        $orderItems = $order->getItems();
        foreach ($orderItems as $orderItemsVal) {
            $extensionAttributes = $orderItemsVal->getExtensionAttributes();
            if (!$extensionAttributes) $extensionAttributes = $this->getOrderExtensionDependency();
            $extensionAttributes->setInsyncSconto($orderItemsVal->getInsyncSconto());
            $orderItemsVal->setExtensionAttributes($extensionAttributes);
        }
        return $order;
    }

    /**
     * @return OrderExtensionFactory
     */
    private function getOrderExtensionDependency()
    {
        return $this->orderExtensionFactory->create();
    }

    /**
     * Find entities by criteria
     *
     * @param OrderSearchResultInterface $orderList
     * @param \Magento\Sales\Model\OrderRepository $subject
     * @return OrderSearchResultInterface
     */
    public function afterGetList(\Magento\Sales\Model\OrderRepository $subject, $orderList)
    {
        $orderItems = $orderList->getItems();
        foreach ($orderItems as $orderListVal) {
            $extensionAttributes = $orderListVal->getExtensionAttributes();
            if (!$extensionAttributes) $extensionAttributes = $this->getOrderExtensionDependency();
            $extensionAttributes->setContactPersonId($orderListVal->getContactPersonId());
            $extensionAttributes->setIsPlacedbySalesrep($orderListVal->getIsPlacedbySalesrep());
            $extensionAttributes->setSalesrepId($orderListVal->getSalesrepId());
            $orderListVal->setExtensionAttributes($extensionAttributes);
            $items = $orderListVal->getItems();
            foreach ($items as &$item) {
                $extensionAttributes = $item->getExtensionAttributes();
                if (!$extensionAttributes) $extensionAttributes = $this->getOrderExtensionDependency();
                $extensionAttributes->setInsyncSconto($item->getInsyncSconto());
                $item->setExtensionAttributes($extensionAttributes);
            }
        }

        return $orderList;
    }


}
