<?php
namespace Appseconnect\B2BMage\Plugin\Sales\Controller\AbstractController;

use Magento\Framework\App\RequestInterface;
use Magento\Sales\Controller\AbstractController\OrderViewAuthorizationInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Registry;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\Result\RedirectFactory;

class OrderLoaderPlugin
{
    
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    public $orderFactory;
    
    /**
     * @var OrderViewAuthorizationInterface
     */
    public $orderAuthorization;
    
    /**
     * @var Registry
     */
    public $registry;
    
    /**
     * @var \Magento\Framework\UrlInterface
     */
    public $url;
    
    /**
     * @var ForwardFactory
     */
    public $resultForwardFactory;
    
    /**
     * @var RedirectFactory
     */
    public $redirectFactory;
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\Sales\Data
     */
    public $helperSales;

    /**
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param OrderViewAuthorizationInterface $orderAuthorization
     * @param Registry $registry
     * @param \Magento\Framework\UrlInterface $url
     * @param ForwardFactory $resultForwardFactory
     * @param RedirectFactory $redirectFactory
     * @param Session $customerSession
     * @param \Appseconnect\B2BMage\Helper\Sales\Data $helperSales
     */
    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderFactory,
        OrderViewAuthorizationInterface $orderAuthorization,
        Registry $registry,
        \Magento\Framework\UrlInterface $url,
        ForwardFactory $resultForwardFactory,
        RedirectFactory $redirectFactory,
        Session $customerSession,
        \Appseconnect\B2BMage\Helper\Sales\Data $helperSales
    ) {
    
        $this->orderFactory = $orderFactory;
        $this->orderAuthorization = $orderAuthorization;
        $this->registry = $registry;
        $this->url = $url;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->redirectFactory = $redirectFactory;
        $this->customerSession = $customerSession;
        $this->helperSales = $helperSales;
    }

    /**
     * @param \Magento\Sales\Controller\AbstractController\OrderLoader $subject
     * @param \Closure $proceed
     * @param RequestInterface $request
     * @return mixed|boolean
     */
    public function aroundLoad(
        \Magento\Sales\Controller\AbstractController\OrderLoader $subject,
        \Closure $proceed,
        RequestInterface $request
    ) {
    
        $flag = false;
        $orderId = (int) $request->getParam('order_id');
        $parentObject = $subject;
        
        if (! $orderId) {
            /** @var \Magento\Framework\Controller\Result\Forward $resultForward */
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('noroute');
        }
        
        $order = $this->orderFactory->create()->load($orderId);
        $customerId = $this->customerSession->getCustomer()->getId();
        $isApprove = false;
        if ($order->getStatus() == 'holded' && $order->getCustomerId() != $customerId) {
            $flag = true;
            $isApprove = $this->helperSales->isOrderApprover($order->getIncrementId(), $customerId);
        }
        
        if (!$flag) {
            $result = $proceed($request);
            return $result;
        }
        
        if ($this->orderAuthorization->canView($order) || $isApprove) {
            $this->registry->register('current_order', $order);
            return true;
        }
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->redirectFactory->create();
        return $resultRedirect->setUrl($this->url->getUrl('*/*/history'));
    }
}
