<?php
namespace Appseconnect\B2BMage\Plugin\Sales\Controller\AbstractController;

use Magento\Customer\Model\Session;

class OrderViewAuthorizationPlugin
{

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;

    /**
     *
     * @var \Magento\Sales\Model\Order\Config
     */
    public $orderConfig;

    /**
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     *
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     */
    public function __construct(
        Session $customerSession,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        \Magento\Sales\Model\Order\Config $orderConfig
    ) {
    
        $this->customerSession = $customerSession;
        $this->helperContactPerson = $helperContactPerson;
        $this->orderConfig = $orderConfig;
    }

    /**
     * @param \Magento\Sales\Controller\AbstractController\OrderViewAuthorization $subject
     * @param \Closure $proceed
     * @param \Magento\Sales\Model\Order $order
     * @return boolean
     */
    public function aroundCanView(
        \Magento\Sales\Controller\AbstractController\OrderViewAuthorization $subject,
        \Closure $proceed,
        \Magento\Sales\Model\Order $order
    ) {
    
        $parentCustomerId = null;
        $isAllowed = false;
        $customerId = $this->customerSession->getCustomerId();
        $availableStatuses = $this->orderConfig->getVisibleOnFrontStatuses();
        $customer = $this->customerSession->getCustomer();
        
        $isAdministrator = $customer->getContactpersonRole() == 1 ? true : false;
        $customerType = $customer->getCustomerType();
        
        if ($customerType == 3) {
            if ($isAdministrator) {
                $parentCustomerMapData = $this->helperContactPerson->getCustomerId($customerId);
                $parentCustomerId = $parentCustomerMapData['customer_id'];
                if ($order->getCustomerId() == $parentCustomerId) {
                    $isAllowed = true;
                }
            } elseif ($order->getContactPersonId() == $customerId) {
                $isAllowed = true;
            }
        } elseif ($customerType == 2) {
            if ($order->getSalesrepId() == $customerId) {
                $isAllowed = true;
            }
        }
        
        if ($order->getId() &&
            $order->getCustomerId() &&
            $isAllowed &&
            in_array($order->getStatus(), $availableStatuses, true)) {
            return true;
        }
        
        if ($order->getId() &&
            $order->getCustomerId() &&
            $order->getCustomerId() == $customerId &&
            in_array($order->getStatus(), $availableStatuses, true)) {
            return true;
        }
        return false;
    }
}
