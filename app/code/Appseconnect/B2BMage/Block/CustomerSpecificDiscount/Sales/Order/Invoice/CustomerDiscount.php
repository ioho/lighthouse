<?php
namespace Appseconnect\B2BMage\Block\CustomerSpecificDiscount\Sales\Order\Invoice;

use Magento\Sales\Model\Order;

class CustomerDiscount extends \Magento\Sales\Block\Order\Totals
{

    /**
     * @var \Magento\Framework\DataObject\Factory
     */
    private $dataObjectFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\DataObject\Factory $dataObjectFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\DataObject\Factory $dataObjectFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->_isScopePrivate = true;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * @var Order|null
     */
    protected $_invoice = null;

    /**
     * @return Order
     */
    public function getInvoice()
    {
        $currentInvoice = $this->_coreRegistry->registry('current_invoice');
        if ($this->_invoice === null) {
            if ($this->hasData('invoice')) {
                $this->_invoice = $this->_getData('invoice');
            } elseif ($currentInvoice) {
                $this->_invoice = $currentInvoice;
            } elseif ($this->getParentBlock()->getInvoice()) {
                $this->_invoice = $this->getParentBlock()->getInvoice();
            }
        }
        return $this->_invoice;
    }

    /**
     * @param Order $invoiceData
     * @return $this
     */
    public function setInvoice($invoiceData)
    {
        $this->_invoice = $invoiceData;
        return $this;
    }

    /**
     * Get totals source object
     *
     * @return Order
     */
    public function getSource()
    {
        $invoice = $this->getInvoice();
        return $invoice;
    }

    /**
     * Initialize order totals array
     *
     * @return $this
     */
    protected function _initTotals()
    {
        $baseGrandTotalKey = 'base_grandtotal';
        parent::_initTotals();
        $this->removeTotal($baseGrandTotalKey);
        $this->removeTotal('grand_total');
        if ($this->getSource()->getOrder()->getCustomerDiscount() > 0) {
            $this->_totals['customer_discount'] = $this->dataObjectFactory->create(
                [
                    'code' => 'customer_discount',
                    'value' => $this->getSource()->getSubtotal() * ( $this->getSource()->getOrder()->getCustomerDiscount() / 100 ),
                    'base_value' => $this->getSource()->getBaseSubtotal() * ( $this->getSource()->getOrder()->getCustomerDiscount() / 100 ),
                    'label' => 'Customer Discount( '.$this->getSource()->getOrder()->getCustomerDiscount().'%)',
                ]
            );
        }
        $this->_totals['grand_total'] = $this->dataObjectFactory->create(
            [
                'code' => 'grand_total',
                'field' => 'grand_total',
                'strong' => true,
                'value' => $this->getSource()->getGrandTotal(),
                'label' => __('Grand Total'),
            ]
        );

        return $this;
    }
}
