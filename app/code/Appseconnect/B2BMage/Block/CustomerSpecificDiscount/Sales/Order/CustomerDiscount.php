<?php
namespace Appseconnect\B2BMage\Block\CustomerSpecificDiscount\Sales\Order;

class CustomerDiscount extends \Magento\Framework\View\Element\Template
{

    /**
     * Tax configuration model
     *
     * @var \Magento\Tax\Model\Config
     */
    public $config;

    /**
     * @var Order
     */
    public $order;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    public $dataObjectFactory;

    /**
     *
     * @var \Magento\Framework\DataObject
     */
    public $source;

    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\DataObject\Factory $dataObjectFactory,
        \Magento\Tax\Model\Config $taxConfig,
        array $data = []
    ) {
        $this->config = $taxConfig;
        $this->dataObjectFactory = $dataObjectFactory;
        parent::__construct($context, $data);
    }

    /**
     * Check if we need display full tax total info
     *
     * @return bool
     */
    public function displayFullSummary()
    {
        return true;
    }

    /**
     * Get data (totals) source model
     *
     * @return \Magento\Framework\DataObject
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return $this
     */
    public function getStore()
    {
        return $this->order->getStore();
    }

    /**
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     *
     * @return array
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     *
     * @return array
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    /**
     * Initialize all order totals relates with tax
     *
     * @return \Magento\Tax\Block\Sales\Order\Tax
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->order = $parent->getOrder();
        $this->source = $parent->getSource();
        
        $store = $this->getStore();
        if ($this->source->getCustomerDiscount() > 0) {
            $amount = $this->dataObjectFactory->create([
                'code' => 'customer_discount',
                'strong' => false,
                'value' => $this->source->getCustomerDiscountAmount(),
                'label' => __('Customer Discount( ' . $this->source->getCustomerDiscount() . '% )')
            ]);
            
            $parent->addTotal($amount, 'customer_discount');
            $parent->addTotal($amount, 'customer_discount');
        }
        
        return $this;
    }
}
