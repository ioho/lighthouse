<?php
namespace Appseconnect\B2BMage\Block\Customer;

class CustomerSessionDetails extends \Magento\Framework\View\Element\Template
{
    protected $_customerSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    )
    {
        $this->httpContext = $httpContext;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }
    public function getLogin() {
        return $this->_customerSession->isLoggedIn();
    }
    public function getCustomerData()
    {
        $customerData = $this->_customerSession->getCustomer();
        return $customerData;
    }
}
