<?php
namespace Appseconnect\B2BMage\Block\CreditLimit\Account\Dashboard;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Ui\Component\Layout\Tabs\TabInterface;
use Magento\Customer\Model\Session;

/**
 * Credit Transaction Block for Customer
 */
class Credit extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Appseconnect\B2BMage\Helper\CreditLimit\Data
     */
    public $creditLimit;

    /**
     * @var Session
     */
    public $customerSession;

    /**
     * @var  \Magento\Directory\Model\CurrencyFactory
     */
    public $currencyFactory;

    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Appseconnect\B2BMage\Helper\CreditLimit\Data $creditLimit
     * @param Session $session
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Appseconnect\B2BMage\Helper\CreditLimit\Data $creditLimit,
        Session $session,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        array $data = []
    ) {
        $this->creditLimit = $creditLimit;
        $this->customerSession = $session;
        $this->currencyFactory = $currencyFactory;
        $this->helperContactPerson = $helperContactPerson;
        parent::__construct($context, $data);
    }

    /**
     * @return float
     */
    public function getCredit()
    {
        $customerId = $this->customerSession->getCustomer()->getId();
        $customerType = $this->customerSession->getCustomer()->getCustomerType();
        $price = null;
        if ($customerType == 3) {
            $customerDetail = $this->helperContactPerson->getCustomerId($customerId);
            $customerId = $customerDetail['customer_id'];
            $customerCreditBalance = $this->creditLimit->getCustomerCreditData($customerId);
            if ($customerCreditBalance['available_balance']) {
                $currency = $this->currencyFactory->create();
                $creditBalance = number_format($customerCreditBalance['available_balance'], 2);
                $price = $currency->getCurrencySymbol() . $creditBalance;
            }
        }
        return $price;
    }
}
