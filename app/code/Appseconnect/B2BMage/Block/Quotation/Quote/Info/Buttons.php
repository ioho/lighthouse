<?php
namespace Appseconnect\B2BMage\Block\Quotation\Quote\Info;

use Magento\Customer\Model\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Customer\Model\Session;

class Buttons extends \Magento\Framework\View\Element\Template
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;
    
    /** @var \Magento\Catalog\Model\Session */
    public $catalogSession;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    public $httpContext;

    /**
     * @var Session
     */
    public $session;
    
    /**
     * @param Session $customerSession
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param array $data
     */
    public function __construct(
        Session $customerSession,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Http\Context $httpContext,
        array $data = []
    ) {
        $this->session = $customerSession;
        $this->coreRegistry = $registry;
        $this->httpContext = $httpContext;
        parent::__construct($context, $data);
    }

    /**
     * @return $this
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::quotation/quote/info/buttons.phtml");
        
        return parent::_toHtml();
    }
    
    /**
     * @return \Magento\Catalog\Model\Session
     */
    private function getCatalogSession()
    {
        $this->catalogSession = ObjectManager::getInstance()->get(
            \Magento\Catalog\Model\Session::class
        );
        
        return $this->catalogSession;
    }

    /**
     * Retrieve current quote model instance
     *
     * @return \Appseconnect\B2BMage\Model\Quote
     */
    public function getQuote()
    {
        return $this->coreRegistry->registry('insync_current_customer_quote');
    }

    /**
     * Get url for printing order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return string
     */
    public function getPrintUrl($order)
    {
        if (! $this->httpContext->getValue(Context::CONTEXT_AUTH)) {
            return $this->getUrl('sales/guest/print', [
                'order_id' => $order->getId()
            ]);
        }
        return $this->getUrl('sales/order/print', [
            'order_id' => $order->getId()
        ]);
    }

    /**
     * @param \Appseconnect\B2BMage\Model\Quote $quote
     * @return string
     */
    public function getSubmitUrl($quote)
    {
        return $this->getUrl('b2bmage/quotation/index_submit', [
            'quote_id' => $quote->getId()
        ]);
    }

    /**
     * @return NULL|int
     */
    public function getSalesrepId()
    {
        return $this->getCatalogSession()->getSalesrepId() ? $this->getCatalogSession()->getSalesrepId() : null;
    }

    /**
     * Get url for reorder action
     *
     * @param \Magento\Sales\Model\Order $order
     * @return string
     */
    public function getReorderUrl($order)
    {
        if (! $this->httpContext->getValue(Context::CONTEXT_AUTH)) {
            return $this->getUrl('sales/guest/reorder', [
                'order_id' => $order->getId()
            ]);
        }
        return $this->getUrl('sales/order/reorder', [
            'order_id' => $order->getId()
        ]);
    }
}
