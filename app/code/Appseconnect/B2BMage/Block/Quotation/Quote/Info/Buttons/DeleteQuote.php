<?php
namespace Appseconnect\B2BMage\Block\Quotation\Quote\Info\Buttons;

class DeleteQuote extends \Magento\Framework\View\Element\Template
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;

    /**
     *
     * @var \Magento\Framework\App\Http\Context
     */
    public $httpContext;

    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Http\Context $httpContext,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->httpContext = $httpContext;
        parent::__construct($context, $data);
    }
    
    /**
     * @return $this
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::quotation/quote/info/buttons/delete_quote.phtml");
        
        return parent::_toHtml();
    }
    
    /**
     * Retrieve current quote model instance
     *
     * @return \Appseconnect\B2BMage\Model\Quote
     */
    public function getQuote()
    {
        return $this->coreRegistry->registry('insync_current_customer_quote');
    }

    public function getClearQuoteUrl($quote)
    {
        return $this->getUrl('b2bmage/quotation/index_deleteQuote', [
            'quote_id' => $quote->getId()
        ]);
    }
}
