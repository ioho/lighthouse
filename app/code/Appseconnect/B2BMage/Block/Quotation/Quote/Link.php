<?php
namespace Appseconnect\B2BMage\Block\Quotation\Quote;

class Link extends \Magento\Framework\View\Element\Html\Link\Current
{

    /**
     *
     * @var \Magento\Framework\Registry
     */
    public $registry;

    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\DefaultPathInterface $defaultPath
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\DefaultPathInterface $defaultPath,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $defaultPath, $data);
        $this->registry = $registry;
    }

    /**
     * Retrieve current quote model instance
     *
     * @return \Appseconnect\B2BMage\Model\Quote
     */
    private function getQuote()
    {
        return $this->registry->registry('insync_current_customer_quote');
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getHref()
    {
        return $this->getUrl($this->getPath(), [
            'quote_id' => $this->getQuote()
                ->getId()
        ]);
    }

    /**
     * @inheritdoc
     *
     * @return string
     */
    public function _toHtml()
    {
        if ($this->hasKey()
            && method_exists($this->getQuote(), 'has' . $this->getKey())
            && ! $this->getQuote()->{'has' . $this->getKey()}()) {
            return '';
        }
        return parent::_toHtml();
    }
}
