<?php
namespace Appseconnect\B2BMage\Block\Quotation\Quote\Item\Renderer;

use Magento\Sales\Model\Order\CreditMemo\Item as CreditMemoItem;
use Magento\Framework\App\ObjectManager;
use Magento\Sales\Model\Order\Invoice\Item as InvoiceItem;
use Appseconnect\B2BMage\Model\QuoteProduct as QuoteItem;
use Magento\Customer\Model\Session;

class DefaultRenderer extends \Magento\Framework\View\Element\Template
{

    /**
     * Magento string lib
     *
     * @var \Magento\Framework\Stdlib\StringUtils
     */
    public $string;
    
    /** @var \Magento\Catalog\Model\Session */
    public $catalogSession;
    
    /**
     * @var Session
     */
    public $session;
    
    /**
     * @var \Appseconnect\B2BMage\Helper\Quotation\Data
     */
    public $helperQuote;
    
    /**
     *
     * @var \Magento\Catalog\Model\Product\OptionFactory
     */
    public $productOptionFactory;

    /**
     * @param Session $customerSession
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Appseconnect\B2BMage\Helper\Quotation\Data $helperQuote
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Model\Product\OptionFactory $productOptionFactory
     * @param array $data
     */
    public function __construct(
        Session $customerSession,
        \Magento\Framework\View\Element\Template\Context $context,
        \Appseconnect\B2BMage\Helper\Quotation\Data $helperQuote,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Model\Product\OptionFactory $productOptionFactory,
        array $data = []
    ) {
        $this->string = $string;
        $this->session = $customerSession;
        $this->helperQuote = $helperQuote;
        $this->productOptionFactory = $productOptionFactory;
        parent::__construct($context, $data);
    }

    /**
     *
     * @param \Magento\Framework\DataObject $item
     * @return $this
     */
    public function setItem(\Magento\Framework\DataObject $item)
    {
        $this->setData('item', $item);
        return $this;
    }
    
    /**
     * @return \Magento\Catalog\Model\Session
     */
    private function getCatalogSession()
    {
        $this->catalogSession = ObjectManager::getInstance()->get(
            \Magento\Catalog\Model\Session::class
        );
        return $this->catalogSession;
    }

    /**
     *
     * @return array|null
     */
    public function getItem()
    {
        return $this->_getData('item');
    }

    /**
     * Retrieve current quote model instance
     *
     * @return \Appseconnect\B2BMage\Model\Quote
     */
    public function getQuote()
    {
        return $this->getQuoteItem()->getQuote();
    }

    /**
     *
     * @return array|null
     */
    public function getQuoteItem()
    {
        if ($this->getItem() instanceof \Appseconnect\B2BMage\Model\QuoteProduct) {
            return $this->getItem();
        } else {
            return $this->getItem()->getQuoteItem();
        }
    }

    /**
     *
     * @return array
     */
    public function getItemOptions()
    {
        $result = [];
		$orderItem = $this->getOrderItem();
        $options = $orderItem->getProductOptions();
        if ($options) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }
        return $result;
    }

    /**
     * Return sku of quote item.
     *
     * @return string
     */
    public function getSku()
    {
        return $this->getItem()->getProductSku();
    }

    /**
     * Return product additional information block
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    public function getProductAdditionalInformationBlock()
    {
		$additionalProductInfoBlock = $this->getLayout()->getBlock('additional.product.info');
        return $additionalProductInfoBlock;
    }

    /**
     * Prepare SKU
     *
     * @param string $sku
     * @return string
     */
    public function prepareSku($sku)
    {
		$formattedSku = $this->escapeHtml($this->string->splitInjection($sku));
        return $formattedSku;
    }

    /**
     * Return item unit price html
     *
     * @param OrderItem|InvoiceItem|CreditmemoItem $item
     *            child item in case of bundle product
     * @return string
     */
    public function getItemPriceHtml($item = null)
    {
        $priceBlock = $this->getLayout()->getBlock('item_unit_price');
        if (! $item) {
            $item = $this->getItem();
        }
        $priceBlock->setItem($item);
        return $priceBlock->toHtml();
    }

    /**
     * Return item row total html
     *
     * @param QuoteItem $item
     *            child item in case of bundle product
     * @return string
     */
    public function getItemRowTotalHtml($item = null)
    {
        $rowBlock = $this->getLayout()->getBlock('item_row_total');
        if (! $item) {
            $item = $this->getItem();
        }
        $rowBlock->setItem($item);
        return $rowBlock->toHtml();
    }

    /**
     * Return the total amount minus discount
     *
     * @param QuoteItem $item
     * @return mixed
     */
    public function getTotalAmount($item)
    {
        $totalAmount = ($item->getRowTotal()
                        + $item->getTaxAmount()
                        + $item->getDiscountTaxCompensationAmount()
                        + $item->getWeeeTaxAppliedRowAmount()
                        - $item->getDiscountAmount());
        
        return $totalAmount;
    }

    /**
     * @param \Appseconnect\B2BMage\Model\QuoteProduct $item
     * @return string
     */
    public function getRemoveUrl($item)
    {
		$itemId = $item->getId();
        return $this->getUrl('b2bmage/quotation/index_delete', [
            'item_id' => $itemId
        ]);
    }

    /**
     * Return HTML for item total after discount
     *
     * @param QuoteItem $item
     *            child item in case of bundle product
     * @return string
     */
    public function getItemRowTotalAfterDiscountHtml($item = null)
    {
        $discountBlock = $this->getLayout()->getBlock('item_row_total_after_discount');
        if (! $item) {
            $item = $this->getItem();
        }
        $discountBlock->setItem($item);
        return $discountBlock->toHtml();
    }

    /**
     * @return NULL|int
     */
    public function getSalesrepId()
    {
		$salesrepId = $this->getCatalogSession()->getSalesrepId();
        return $salesrepId ? $salesrepId : null;
    }

   /**
    * @param array $item
    * @return string
    */
    public function getDeletePostJson($item)
    {
        return $this->helperQuote->getDeletePostJson($item);
    }
}
