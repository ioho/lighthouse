<?php
namespace Appseconnect\B2BMage\Block\Quotation\Quote;

use Magento\Framework\App\ObjectManager;
use Appseconnect\B2BMage\Model\ResourceModel\Quote\CollectionFactoryInterface;
use Magento\Customer\Model\Session;

class History extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;

    /**
     * @var \Appseconnect\B2BMage\Model\ResourceModel\Quote\Collection
     */
    public $quotes;

    /**
     * @var CollectionFactoryInterface
     */
    private $quoteCollectionFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    private $customerFactory;

    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    private $helperContactPerson;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        Session $customerSession,
        array $data = []
    ) {
        $this->customerFactory = $customerFactory;
        $this->helperContactPerson = $helperContactPerson;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Quotes'));
    }

    /**
     * @return $this
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::quotation/quote/history.phtml");
        
        return parent::_toHtml();
    }

    /**
     *
     * @return CollectionFactoryInterface
     *
     * @deprecated 100.1.1
     */
    private function getQuoteCollectionFactory()
    {
        if ($this->quoteCollectionFactory === null) {
            $this->quoteCollectionFactory = ObjectManager::getInstance()->get(CollectionFactoryInterface::class);
        }
        return $this->quoteCollectionFactory;
    }

    /**
     *
     * @return bool|\Appseconnect\B2BMage\Model\ResourceModel\Quote\Collection
     */
    public function getQuotes()
    {
        $customerId = $this->customerSession->getCustomerId();
        
        $parentCustomerId = null;
        if ($customerId && $this->customerSession->getCustomer()->getCustomerType() == 3) {
            $parentCustomer = $this->helperContactPerson->getCustomerId($customerId);
            $parentCustomerId = $parentCustomer['customer_id'];
        }
        
        if (! $customerId || ! $parentCustomerId) {
            return false;
        }
        
        if (! $this->quotes) {
            $this->quotes = $this->getQuoteCollectionFactory()
                ->create($parentCustomerId, $customerId)
                ->addFieldToSelect('*')
                ->addFieldToFilter('status', array('neq' => 'closed'))
                ->setOrder('created_at', 'desc');
        }
        return $this->quotes;
    }

    /**
     *
     * @return $this
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getQuotes()) {
            $pager = $this->getLayout()
                ->createBlock(\Magento\Theme\Block\Html\Pager::class, 'sales.order.history.pager')
                ->setCollection($this->getQuotes());
            $this->setChild('pager', $pager);
            $this->getQuotes()->load();
        }
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     *
     * @param object $quote
     * @return string
     */
    public function getEditUrl($quote)
    {
        return $this->getUrl('b2bmage/quotation/index_edit', [
            'quote_id' => $quote->getId()
        ]);
    }

    /**
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
    
    /**
     * @param int $id
     * @return string
     */
    public function getCustomerName($id)
    {
        $name = null;
        $model = $this->customerFactory->create();
        $customer = $model->load($id);
        $name = $customer->getName();
        return $name;
    }
}
