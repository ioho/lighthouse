<?php
namespace Appseconnect\B2BMage\Block\Quotation\Quote;

use Magento\Sales\Model\Order\Address;
use Magento\Framework\App\ObjectManager;
use Magento\Customer\Model\Session;
use Magento\Customer\Block\Address\Book as AddressBook;
use Magento\Customer\Model\Customer;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;

class Info extends \Magento\Framework\View\Element\Template
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;
    
    /**
     * @var \Magento\Catalog\Model\Session
     */
    public $catalogSession;

    /**
     *
     * @var \Magento\Payment\Helper\Data
     */
    public $paymentHelper;

    /**
     * @var AddressRenderer
     */
    public $addressRenderer;

    /**
     * @var Session
     */
    public $session;

    /**
     * @var \Appseconnect\B2BMage\Helper\Quotation\Data
     */
    public $helperQuote;

    /**
     * @var Customer
     */
    public $customerModel;

    /**
     * @var boolean
     */
    public $isScopePrivate;

    /**
     * @param TemplateContext $context
     * @param Session $customerSession
     * @param \Appseconnect\B2BMage\Helper\Quotation\Data $helperQuote
     * @param AddressBook $addressBook
     * @param Customer $customerModel
     * @param Registry $registry
     * @param PaymentHelper $paymentHelper
     * @param AddressRenderer $addressRenderer
     * @param array $data
     */
    public function __construct(
        TemplateContext $context,
        Session $customerSession,
        \Appseconnect\B2BMage\Helper\Quotation\Data $helperQuote,
        AddressBook $addressBook,
        Customer $customerModel,
        Registry $registry,
        PaymentHelper $paymentHelper,
        AddressRenderer $addressRenderer,
        array $data = []
    ) {
        $this->addressRenderer = $addressRenderer;
        $this->session = $customerSession;
        $this->helperQuote = $helperQuote;
        $this->addressBook = $addressBook;
        $this->customerModel = $customerModel;
        $this->paymentHelper = $paymentHelper;
        $this->coreRegistry = $registry;
        $this->isScopePrivate = true;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return void
     */
    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Quote #%1', $this->getQuote()
            ->getId()));
    }
    
    /**
     * @return \Magento\Catalog\Model\Session
     */
    private function getCatalogSession()
    {
        $this->catalogSession = ObjectManager::getInstance()->get(
            \Magento\Catalog\Model\Session::class
        );
        return $this->catalogSession;
    }

    /**
     *
     * @return string
     */
    public function getPaymentInfoHtml()
    {
        return $this->getChildHtml('payment_info');
    }

    /**
     * Retrieve current quote model instance
     *
     * @return \Appseconnect\B2BMage\Model\Quote
     */
    public function getQuote()
    {
        return $this->coreRegistry->registry('insync_current_customer_quote');
    }

    /**
     * Returns string with formatted address
     *
     * @param Address $address
     * @return null|string
     */
    public function getFormattedAddress(Address $address)
    {
        return $this->addressRenderer->format($address, 'html');
    }

    /**
     * @return NULL|int
     */
    public function getDefaultShipping()
    {
        return $this->customerModel->load($this->getQuote()
            ->getContactId())
            ->getDefaultShipping() ? $this->customerModel->load($this->getQuote()
            ->getContactId())
            ->getDefaultShipping() : null;
    }

    /**
     * @param int $defaultShippingId
     * @return mixed
     */
    public function getAddressHtml($defaultShippingId)
    {
        return $this->addressBook->getAddressHtml(
            $this->addressBook->getAddressById($defaultShippingId)
        );
    }

    /**
     * @return string
     */
    public function getCheckoutPostJson()
    {
        return $this->helperQuote->getCheckoutPostJson($this->getQuote());
    }

    /**
     * @return NULL|int
     */
    public function getSalesrepId()
    {
        return $this->getCatalogSession()->getSalesrepId() ?
        $this->getCatalogSession()->getSalesrepId() : null;
    }
}
