<?php
namespace Appseconnect\B2BMage\Block\Quotation\Header\Quote;

class Link extends \Magento\Framework\View\Element\Html\Link
{

    /**
     *
     * @var \Magento\Customer\Model\Url
     */
    public $customerUrl;

    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Url $customerUrl,
        array $data = []
    ) {
        $this->customerUrl = $customerUrl;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return string
     */
    public function getHref()
    {
        return $this->getUrl('b2bmage/quotation/index_history');
    }
}
