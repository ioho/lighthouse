<?php
namespace Appseconnect\B2BMage\Block\ContactPerson\Account;

class AuthorizationLink extends \Magento\Customer\Block\Account\AuthorizationLink
{

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::contactperson/account/link/authorization.phtml");
        
        return parent::_toHtml();
    }
}
