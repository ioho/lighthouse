<?php
namespace Appseconnect\B2BMage\Block\ContactPerson\Account;

use Magento\Customer\Model\Context;

/**
 * Customer register link
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class RegisterLink extends \Magento\Framework\View\Element\Html\Link
{

    /**
     * Customer session
     *
     * @var \Magento\Framework\App\Http\Context
     */
    private $httpContext;

    /**
     *
     * @var \Magento\Customer\Model\Registration
     */
    private $registration;

    /**
     *
     * @var \Magento\Customer\Model\Url
     */
    private $customerUrl;
    
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Customer\Model\Registration $registration
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Registration $registration,
        \Magento\Customer\Model\Url $customerUrl,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->scopeConfig = $scopeConfig;
        $this->httpContext = $httpContext;
        $this->registration = $registration;
        $this->customerUrl = $customerUrl;
    }

    /**
     *
     * @return string
     */
    public function getHref()
    {
        return $this->customerUrl->getRegisterUrl();
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        $canRegister = $this->scopeConfig->getValue('insync_account/create/type', 'store');
        if (! $canRegister) {
            return '';
        }
        if (! $this->registration->isAllowed() || $this->httpContext->getValue(Context::CONTEXT_AUTH)) {
            return '';
        }
        return parent::_toHtml();
    }
}
