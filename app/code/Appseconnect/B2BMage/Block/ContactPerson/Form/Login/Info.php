<?php
namespace Appseconnect\B2BMage\Block\ContactPerson\Form\Login;

/**
 * Customer login info block
 */
class Info extends \Magento\Customer\Block\Form\Login\Info
{
    
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     *
     */
    public $scopeConfig;

    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Registration $registration
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param \Magento\Checkout\Helper\Data $checkoutData
     * @param \Magento\Framework\Url\Helper\Data $coreUrl
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Registration $registration,
        \Magento\Customer\Model\Url $customerUrl,
        \Magento\Checkout\Helper\Data $checkoutData,
        \Magento\Framework\Url\Helper\Data $coreUrl,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registration,
            $customerUrl,
            $checkoutData,
            $coreUrl,
            $data
        );
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string|NULL
     */
    public function _toHtml()
    {
        $canRegister = $this->scopeConfig->getValue('insync_account/create/type', 'store');
        if (! $canRegister) {
            return '';
        }
        return parent::_toHtml();
    }
}
