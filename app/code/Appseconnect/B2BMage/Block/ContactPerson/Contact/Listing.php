<?php
namespace Appseconnect\B2BMage\Block\ContactPerson\Contact;

use Magento\Customer\Model\Session;
use Appseconnect\B2BMage\Model\ResourceModel\Contact\CollectionFactory as ContactCollectionFactory;

class Listing extends \Magento\Framework\View\Element\Template
{

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     *
     * @var \Magento\Customer\Model\ResourceModel\Customer\Collection
     */
    private $customers;
    
    /**
     *
     * @var ContactCollectionFactory
     */
    public $contactCollectionFactory;
    
    /**
     *
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     *
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param ContactCollectionFactory $contactCollectionFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param Session $customerSession
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        ContactCollectionFactory $contactCollectionFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        array $data = []
    ) {
        $this->contactCollectionFactory = $contactCollectionFactory;
        $this->customerSession = $customerSession;
        $this->helperContactPerson = $helperContactPerson;
        $this->customerFactory = $customerFactory;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return $this
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'simplenews.news.list.pager'
        );
        $pager->setLimit(10)
            ->setShowAmounts(true)
            ->setCollection($this->getContactPersons());
        $this->setChild('pager', $pager);
        $this->getContactPersons()->getData();
        
        return $this;
    }

    /**
     * @return boolean|\Magento\Customer\Model\ResourceModel\Customer\Collection
     */
    public function getContactPersons()
    {
        if (! ($contactPersonId = $this->customerSession->getCustomerId())) {
            return false;
        }
        $customerData = $this->helperContactPerson->getCustomerId($contactPersonId);
        $customerId = $customerData['customer_id'];
        if (! $this->customers) {
            $this->customers = $this->customerFactory->create()->getCollection();
            $contactFactory = $this->contactCollectionFactory->create();
            $contactCollection = $contactFactory->addFieldToSelect('contactperson_id')
                                                ->addFieldToFilter('customer_id', $customerId);
            $contactIds = [];
            foreach ($contactCollection as $data) {
                if ($data['contactperson_id'] != $contactPersonId) {
                    $contactIds[] = $data['contactperson_id'];
                }
            }
            $this->customers->addExpressionAttributeToSelect('name', '(CONCAT({{firstname}},"  ",{{lastname}}))', [
                'entity_id',
                'firstname',
                'lastname',
                'customer_status',
                'contactperson_role'
            ])->addFieldToFilter('entity_id', [
                'in' => $contactIds
            ]);
        }
        return $this->customers;
    }

    /**
     * @return string
     */
    public function getContactPersonAddUrl()
    {
        return $this->getUrl('b2bmage/contact/index_add/');
    }

    /**
     * @param int $customrId
     * @return string
     */
    public function getContactPersonEditUrl($customrId)
    {
        return $this->getUrl('b2bmage/salesrep/customer_view/', [
            'customer_id' => base64_encode($customrId['entity_id'])
        ]);
    }

    /**
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return boolean
     */
    public function canShowTab()
    {
        return false;
    }
}
