<?php
namespace Appseconnect\B2BMage\Block\ContactPerson\Contact;

use Magento\Customer\Model\Session;

class Edit extends \Magento\Framework\View\Element\Template
{

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;
    
    /**
     *
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param Session $customerSession
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        array $data = []
    ) {
    
        $this->customerSession = $customerSession;
        $this->customerFactory = $customerFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param int $customrId
     * @return string
     */
    public function getContactPersonUrl($customrId)
    {
        return $this->getUrl('b2bmage/salesrep/customer_view/', [
            'customer_id' => base64_encode($customrId['entity_id'])
        ]);
    }

    /**
     * @param int $id
     * @return \Magento\Customer\Model\Customer|NULL
     */
    public function getContactPerson($id)
    {
        if ($id) {
            $model = $this->customerFactory->create()->load($id);
            return $model;
        }
        return null;
    }
}
