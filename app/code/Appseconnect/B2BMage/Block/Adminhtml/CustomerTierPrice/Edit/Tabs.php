<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\CustomerTierPrice\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{

    /**
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setId('post_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Product Tier Price Information'));
    }

    /**
     * Prepare Layout
     *
     * @return $this
     */
    public function _beforeToHtml()
    {
        if ($this->getRequest()->getParam('id')) {
            $this->addTab('product_tab', [
                'label' => __('Product Details'),
                'url' => $this->getUrl('*/*/productgrid', [
                    '_current' => true
                ]),
                'class' => 'ajax'
            ]);
            
            $this->_updateActiveTab();
        }
        return parent::_beforeToHtml();
    }

    /**
     * @return void
     */
    public function _updateActiveTab()
    {
        $tabId = $this->getRequest()->getParam('tab');
        if ($tabId) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if ($tabId) {
                $this->setActiveTab($tabId);
            }
        }
    }
}
