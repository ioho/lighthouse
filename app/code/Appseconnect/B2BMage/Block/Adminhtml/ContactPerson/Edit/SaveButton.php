<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\ContactPerson\Edit;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveButton extends GenericButton implements ButtonProviderInterface
{

    /**
     *
     * @var AccountManagementInterface
     */
    public $customerAccountManagement;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param AccountManagementInterface $customerAccountManagement
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        AccountManagementInterface $customerAccountManagement
    ) {
        parent::__construct($context, $registry);
        $this->customerAccountManagement = $customerAccountManagement;
    }

    /**
     *
     * @return array
     */
    public function getButtonData()
    {
        $customerId = $this->getCustomerId();
        $canModify = ! $customerId || ! $this->customerAccountManagement->isReadonly($this->getCustomerId());
        $data = [];
        if ($canModify) {
            $data = [
                'label' => __('Save Contact Person'),
                'class' => 'primary',
                'on_click' => 'setLocation("' . $this->getCustomerSaveUrl() . '")'
            ];
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getCustomerSaveUrl()
    {
        return $this->getUrl('b2bmage/contact/save', []);
    }
}
