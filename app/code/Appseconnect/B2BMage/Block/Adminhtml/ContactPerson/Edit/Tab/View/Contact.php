<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\ContactPerson\Edit\Tab\View;

use Magento\Store\Model\Store;
use Appseconnect\B2BMage\Model\ResourceModel\ContactFactory;

class Contact extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry|null
     */
    public $moduleManager;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory]
     */
    public $setsFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    public $productFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Type
     */
    public $type;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    public $status;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    public $visibility;

    /**
     * @var \Magento\Store\Model\WebsiteFactory
     */
    public $websiteFactory;

    /**
     * @var ContactFactory
     */
    public $contactResourceFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\ContactFactory
     */
    public $contactFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param ContactFactory $contactResourceFactory
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Store\Model\WebsiteFactory $websiteFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setsFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Appseconnect\B2BMage\Model\ContactFactory $contactFactory
     * @param \Magento\Catalog\Model\Product\Type $type
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $status
     * @param \Magento\Catalog\Model\Product\Visibility $visibility
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        ContactFactory $contactResourceFactory,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setsFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Appseconnect\B2BMage\Model\ContactFactory $contactFactory,
        \Magento\Catalog\Model\Product\Type $type,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $status,
        \Magento\Catalog\Model\Product\Visibility $visibility,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->setsFactory = $setsFactory;
        $this->productFactory = $productFactory;
        $this->contactResourceFactory = $contactResourceFactory;
        $this->contactFactory = $contactFactory;
        $this->type = $type;
        $this->status = $status;
        $this->visibility = $visibility;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    public function _prepareGrid()
    {
        $this->setId('comparedproduct_view_compared_grid' . $this->getCustomerId());
        parent::_prepareGrid();
    }

    public function _preparePage()
    {
        $this->getCollection()
            ->setPageSize(20)
            ->setCurPage(1);
    }

    /**
     *
     * @return $this
     */
    public function _prepareCollection()
    {
        $contactResourceModel = $this->contactResourceFactory->create();
        $collection = $this->contactFactory->create()->getCollection();
        $collection = $contactResourceModel->getContactCollection($collection);
        $collection->addFieldToFilter('customer_id', $pricelist_id = $this->getRequest()
            ->getParam('id'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     *
     * @return $this @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function _prepareColumns()
    {
        $this->addColumn('entity_id', [
            'header' => __('ID'),
            'width' => '15',
            'type' => 'number',
            'index' => 'entity_id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        $this->addColumn('firstname', [
            'header' => __('First Name'),
            'type' => 'text',
            'index' => 'firstname',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        $this->addColumn('lastname', [
            'header' => __('Last Name'),
            'width' => 15,
            'type' => 'text',
            'index' => 'lastname',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        $this->addColumn('contact_email', [
            'header' => __('Email'),
            'type' => 'text',
            'index' => 'email',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        
        $this->addColumn('action_edit1', [
            'header' => __('Action'),
            'index' => 'image',
            'filter' => false,
            'renderer' => 'Appseconnect\B2BMage\Block\Adminhtml\ContactPerson\Renderer\Action'
        ]);
        
        return parent::_prepareColumns();
    }

    /**
     * Get headers visibility
     *
     * @return bool @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getHeadersVisibility()
    {
        return $this->getCollection()->getSize() >= 0;
    }
}
