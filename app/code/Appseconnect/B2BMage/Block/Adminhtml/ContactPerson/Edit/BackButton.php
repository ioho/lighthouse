<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\ContactPerson\Edit;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Catalog\Model\Session;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class BackButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * @var AccountManagementInterface
     */
    public $customerAccountManagement;

    /**
     * @var Session
     */
    public $catalogSession;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param Session $catalogSession
     * @param \Magento\Framework\Registry $registry
     * @param AccountManagementInterface $customerAccountManagement
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        Session $catalogSession,
        \Magento\Framework\Registry $registry,
        AccountManagementInterface $customerAccountManagement
    ) {
        parent::__construct($context, $registry);
        $this->catalogSession = $catalogSession;
        $this->customerAccountManagement = $customerAccountManagement;
    }

    /**
     *
     * @return array
     */
    public function getButtonData()
    {
        $data = [
            'label' => __('Back'),
            'class' => 'back',
            'on_click' => 'setLocation("' . $this->getDeleteUrl() . '")'
        ];
        return $data;
    }

    /**
     * Generate redirect url
     */
    public function getDeleteUrl()
    {
        $parentCustomerId = $this->catalogSession->getParentCustomerId();
        return $this->getUrl('customer/index/edit/', [
            'id' => $parentCustomerId
        ]);
    }
}
