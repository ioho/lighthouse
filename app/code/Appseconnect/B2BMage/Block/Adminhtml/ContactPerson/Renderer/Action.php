<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\ContactPerson\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Appseconnect\B2BMage\Model\ResourceModel\ContactFactory;
use Magento\Framework\DataObject;

class Action extends AbstractRenderer
{
    
    /**
     * @var ContactFactory
     */
    public $contactResourceFactory;
    
    /**
     * @param \Magento\Backend\Block\Context $context
     * @param ContactFactory $contactResourceFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        ContactFactory $contactResourceFactory,
        array $data = []
    ) {
    
        $this->contactResourceFactory = $contactResourceFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $contactResourceModel = $this->contactResourceFactory->create();
        $result = $contactResourceModel->getRowData($row->getId());
        
        $url = $this->getUrl('b2bmage/contact/edit', [
            'id' => $result['contactperson_id'],
            'contactperson_id' => $row->getId(),
            'customer_id' => $this->getRequest()
                ->getParam('id'),
            'is_contactperson' => 1,
            '_current' => false
        ]);
        return '<a href="' . $url . '">Edit</a>';
    }
}
