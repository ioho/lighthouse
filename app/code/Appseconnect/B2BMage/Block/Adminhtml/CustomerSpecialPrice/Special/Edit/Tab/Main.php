<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\CustomerSpecialPrice\Special\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    public $systemStore;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    public $customerCollectionFactory;

    /**
     * @var \Magento\Store\Model\ResourceModel\Website\CollectionFactory
     */
    public $websiteCollectionFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory
     */
    public $pricelistFactory;

    /**
     * @var \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data
     */
    public $helper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory
     * @param \Magento\Store\Model\ResourceModel\Website\CollectionFactory $websiteCollectionFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory $pricelistFactory
     * @param \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Store\Model\ResourceModel\Website\CollectionFactory $websiteCollectionFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Appseconnect\B2BMage\Model\ResourceModel\Price\CollectionFactory $pricelistFactory,
        \Appseconnect\B2BMage\Helper\CustomerSpecialPrice\Data $helper,
        array $data = []
    ) {
        $this->systemStore = $systemStore;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->websiteCollectionFactory = $websiteCollectionFactory;
        $this->pricelistFactory = $pricelistFactory;
        $this->helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('insync_pricelist');
        
        $isElementDisabled = false;
        
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        
        $form->setHtmlIdPrefix('page_');
        
        $fieldset = $form->addFieldset('base_fieldset', [
            'legend' => __('Customer Information')
        ]);
        
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', [
                'name' => 'id'
            ]);
        }
        
        $fieldset->addField('website_id', 'select', [
            'name' => 'website_id',
            'label' => __('Associated to Website'),
            'title' => __('Associated to Website'),
            'required' => true,
            'options' => $this->getWebsiteId(),
            'disabled' => $isElementDisabled
        ]);
        
        $fieldset->addField('customer_id', 'select', [
            'name' => 'customer_id',
            'label' => __('B2B Customer'),
            'title' => __('B2B Customer'),
            'required' => true,
            'options' => $this->getCustomerId()
        ]);
        
        $fieldset->addField('pricelist_id', 'select', [
            'name' => 'pricelist_id',
            'label' => __('Pricelist'),
            'title' => __('Pricelist'),
            'required' => false,
            'options' => $this->getPricelistId(),
            'disabled' => $isElementDisabled
        ]);
        
        $fieldset->addField('start_date', 'date', [
            'name' => 'start_date',
            'label' => __('Special Price Start Date'),
            'title' => __('Special Price Start Date'),
            'required' => true,
            'class' => '',
            'singleClick' => true,
            'date_format' => 'yyyy-MM-dd',
            'time' => false
        ]);
        
        $fieldset->addField('end_date', 'date', [
            'name' => 'end_date',
            'label' => __('Special Price End Date'),
            'title' => __('Special Price End Date'),
            'required' => true,
            'class' => '',
            'singleClick' => true,
            'date_format' => 'yyyy-MM-dd',
            'time' => false
        ]);
        
        $fieldset->addField('discount_type', 'select', [
            'name' => 'discount_type',
            'label' => __('Discount Type'),
            'title' => __('Discount Type'),
            'required' => false,
            'options' => [
                '0' => 'By Fixed Price',
                '1' => 'By Percentage'
            ],
            'disabled' => $isElementDisabled
        ]);
        
        $fieldset->addField('is_active', 'select', [
            'name' => 'is_active',
            'label' => __('Is Active'),
            'title' => __('Is Active'),
            'required' => false,
            'options' => [
                '0' => 'No',
                '1' => 'Yes'
            ],
            'disabled' => $isElementDisabled
        ]);

        
        $form->setValues($model->getData());
        $this->setForm($form);
        
        return parent::_prepareForm();
    }

    /**
     * @return string[]|array
     */
    public function getPricelistId()
    {
        $priceList = $this->pricelistFactory->create();
        $result = [];
        $result[0] = "Base Price";
        foreach ($priceList->getData() as $val) {
            $result[$val['id']] = $val['pricelist_name'];
        }
        
        return $result;
    }

    /**
     * @return array
     */
    public function getCustomerId()
    {
        $customerCollection = $this->customerCollectionFactory->create()
            ->addNameToSelect()
            ->addFieldToFilter('customer_type', 4);
        $customerData = $customerCollection->getData();
        $result = [];
        $result[null] = 'Please Select Customer';
        foreach ($customerData as $value) {
            $result[$value['entity_id']] = $value['name'];
        }
        
        return $result;
    }

    /**
     * @return array
     */
    public function getWebsiteId()
    {
        $website = $this->websiteCollectionFactory->create();
        $output = $website->getData();
        foreach ($output as $val) {
            $result[$val["website_id"]] = $val['name'];
        }
        
        return $result;
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Customer Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Customer Information');
    }

    /**
     *
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     *
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    public function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
