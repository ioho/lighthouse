<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\CustomerSpecialPrice\Special\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{

    /**
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setId('post_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Customer Special Price Information'));
    }

    /**
     * Prepare Layout
     *
     * @return $this
     */
    public function _beforeToHtml()
    {
        if ($this->getRequest()->getParam('id')) {
            $this->addTab('product_tab', [
                'label' => __('Products'),
                'title' => __('Products'),
                'content' => $this->getLayout()
                    ->createBlock('Appseconnect\B2BMage\Block\Adminhtml\CustomerSpecialPrice\Edit\Tab\View\GridInit')
                    ->toHtml()
            ]);
            
            $this->_updateActiveTab();
        }
        return parent::_beforeToHtml();
    }

    /**
     * @return void
     */
    public function _updateActiveTab()
    {
        $tabId = $this->getRequest()->getParam('tab');
        if ($tabId) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if ($tabId) {
                $this->setActiveTab($tabId);
            }
        }
    }
}
