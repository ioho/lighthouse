<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\Salesrep\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Appseconnect\B2BMage\Model\ResourceModel\Salesrep\CollectionFactory;
use Magento\Framework\DataObject;

class Status extends AbstractRenderer
{

    /**
     * @var \Appseconnect\B2BMage\Helper\Salesrep\Data
     */
    public $salesrepHelper;
    
    /**
     * @var CollectionFactory
     */
    public $salesrepCollectionFactory;
    
    /**
     * @param \Magento\Backend\Block\Context $context
     * @param \Appseconnect\B2BMage\Helper\Salesrep\Data $salesrepHelper
     * @param CollectionFactory $salesrepCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Appseconnect\B2BMage\Helper\Salesrep\Data $salesrepHelper,
        CollectionFactory $salesrepCollectionFactory,
        array $data = []
    ) {
        $this->salesrepHelper = $salesrepHelper;
        $this->salesrepCollectionFactory = $salesrepCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $selsrepData = $this->salesrepHelper->isSalesrep($this->getRequest()
            ->getParam('id'), true);
        $salesrepId = $selsrepData[0]['id'];
        $customer = $this->salesrepCollectionFactory->create();
        $customer->addFieldToFilter('customer_id', $row->getId());
        $customer->addFieldToFilter('salesrep_id', $salesrepId);
        $output = $customer->getData();
        $result = [];
        if (! empty($output)) {
            return 'Assigned';
        } else {
            return "Unassigned";
        }
    }
}
