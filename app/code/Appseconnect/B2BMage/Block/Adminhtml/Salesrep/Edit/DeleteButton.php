<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\Salesrep\Edit;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class DeleteButton
 *
 * @package Magento\Customer\Block\Adminhtml\Edit
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * @var AccountManagementInterface
     */
    public $customerAccountManagement;

    /**
     * @var Http
     */
    public $httpRequest;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param Http $httpRequest,
     * @param \Magento\Framework\Registry $registry
     * @param AccountManagementInterface $customerAccountManagement
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        Http $httpRequest,
        \Magento\Framework\Registry $registry,
        AccountManagementInterface $customerAccountManagement
    ) {
        parent::__construct($context, $registry);
        $this->httpRequest = $httpRequest;
        $this->customerAccountManagement = $customerAccountManagement;
    }

    /**
     *
     * @return array
     */
    public function getButtonData()
    {
        $salesrepId = $this->httpRequest->getParam('salesrep_id');
        $data = [];
        if ($salesrepId) {
            $data = [
                'label' => __('Delete Sales Representative'),
                'class' => '',
                'on_click' => 'setLocation("' . $this->getDeleteUrl() . '")'
            ];
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        $salesrepId = $this->httpRequest->getParam('salesrep_id');
        return $this->getUrl('b2bmage/salesrep/delete', [
            'id' => $salesrepId
        ]);
    }
}
