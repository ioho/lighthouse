<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\Pricelist\Edit;

use Magento\Backend\Model\Auth\Session;

class View extends \Magento\Backend\Block\Widget\Tabs
{

    /**
     * Constructor
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setId('pricelist_price_edit_view');
        $this->setDestElementId('pricelist_price_edit');
        $this->setTitle(__('Pricelist View'));
    }
}
