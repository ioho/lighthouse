<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\CreditLimit\Edit\Tab\View;

use Magento\Store\Model\Store;
use Magento\Sales\Model\OrderFactory;

/**
 * Credit Card Transaction Grid Admin
 */
class CreditTransaction extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry|null
     */
    public $moduleManager;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory]
     */
    public $setsFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    public $productFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Type
     */
    public $type;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    public $status;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    public $visibility;

    /**
     * @var \Magento\Store\Model\WebsiteFactory
     */
    public $websiteFactory;

    /**
     * @var OrderFactory
     */
    public $orderFactory;

    /**
     * @var \Appseconnect\B2BMage\Model\CreditFactory
     */
    public $creditFactory;
    
    /**
     * @var  \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param OrderFactory $orderFactory
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Store\Model\WebsiteFactory $websiteFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setsFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Appseconnect\B2BMage\Model\CreditFactory $creditFactory
     * @param \Magento\Catalog\Model\Product\Type $type
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $status
     * @param \Magento\Catalog\Model\Product\Visibility $visibility
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        OrderFactory $orderFactory,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $setsFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Appseconnect\B2BMage\Model\CreditFactory $creditFactory,
        \Magento\Catalog\Model\Product\Type $type,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $status,
        \Magento\Catalog\Model\Product\Visibility $visibility,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->orderFactory = $orderFactory;
        $this->setsFactory = $setsFactory;
        $this->productFactory = $productFactory;
        $this->creditFactory = $creditFactory;
        $this->customerFactory = $customerFactory;
        $this->type = $type;
        $this->status = $status;
        $this->visibility = $visibility;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return $this
     */
    public function _prepareGrid()
    {
        $this->setId('comparedproduct_view_compared_grid' . $this->getCustomerId());
        parent::_prepareGrid();
    }
    
    /**
     * @return void
     */
    public function _preparePage()
    {
        $this->getCollection()
            ->setPageSize(20)
            ->setCurPage(1);
    }

    /**
     * @return $this
     */
    public function _prepareCollection()
    {
        $customerId = $this->getRequest()->getParam('id');
        $collection = $this->creditFactory->create()->getCollection();
        $collection->addFieldToFilter('customer_id', $customerId);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     *
     * @return $this @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function _prepareColumns()
    {
        $this->addColumn('id', [
            'header' => __('ID'),
            'width' => '15',
            'type' => 'number',
            'index' => 'id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        $this->addColumn('increment_id', [
            'header' => __('Increment ID'),
            'width' => 15,
            'type' => 'text',
            'index' => 'increment_id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        $this->addColumn('debit_amount', [
            'header' => __('Debit Amount'),
            'width' => 15,
            'type' => 'text',
            'index' => 'debit_amount',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        $this->addColumn('credit_amount', [
            'header' => __('Credit Amount'),
            'width' => 15,
            'type' => 'text',
            'index' => 'credit_amount',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        
        $this->addColumn('available_balance', [
            'header' => __('Available Balance'),
            'width' => 15,
            'type' => 'text',
            'index' => 'available_balance',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        $this->addColumn('credit_limit', [
            'header' => __('Credit Limit'),
            'type' => 'text',
            'index' => 'credit_limit',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
        ]);
        
        return parent::_prepareColumns();
    }

    /**
     * Retrieve the Url for a specified sales order row.
     *
     * @param \Appseconnect\B2BMage\Model\CreditFactory|\Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row)
    {
        if ($row->getIncrementId()) {
            $order = $this->orderFactory->create()->loadByIncrementId($row->getIncrementId());
            return $this->getUrl('sales/order/view', [
                'order_id' => $order->getId(),
                '_current' => false
            ]);
        }
        return false;
    }

    /**
     * Get headers visibility
     *
     * @return bool @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getHeadersVisibility()
    {
        return $this->getCollection()->getSize() >= 0;
    }
}
