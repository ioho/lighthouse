<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\CreditLimit\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Customer\Model\CustomerFactory;
use Magento\Ui\Component\Layout\Tabs\TabInterface;

/**
 * Credit Transaction Block for Customer
 */
class CreditLimit extends \Magento\Backend\Block\Template implements TabInterface
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry;

    /**
     * @var CustomerFactory
     */
    public $customerFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param CustomerFactory $customerFactory
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        CustomerFactory $customerFactory,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->customerFactory = $customerFactory;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Credit Transaction');
    }

    /**
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Credit Transaction');
    }

    /**
     *
     * @return bool
     */
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            $customerDetail = $this->customerFactory->create()->load($this->getCustomerId());
            $customerType = $customerDetail->getData('customer_type');
            return ($customerType == 4) ? true : false;
        }
        return false;
    }

    /**
     *
     * @return bool
     */
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return false;
        }
        return true;
    }

    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('b2bmage/credit/listing', [
            '_current' => true
        ]);
    }

    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return true;
    }
}
