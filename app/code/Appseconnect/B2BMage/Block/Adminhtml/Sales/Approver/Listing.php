<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\Sales\Approver;

class Listing extends Group\AbstractGroup
{

    /**
     * Retrieve list of initial customer groups
     *
     * @return array
     */
    public function _getInitialCustomerGroups()
    {
        return [
            $this->_groupManagement->getAllCustomersGroup()->getId() => __('ALL GROUPS')
        ];
    }

    /**
     * @return string
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::sales/approver/listing.phtml");
        
        return parent::_toHtml();
    }

    /**
     * @param int $customerId
     * @param boolean $type
     * @return array
     */
    public function getContactpersonList($customerId = null, $type = false)
    {
        $approverResourceModel = $this->approverResourceFactory->create();
        $customerCollection = $this->customerFactory->create()->getCollection();
        $collection = $approverResourceModel->getContacts($customerId, $customerCollection);
        $collection->addExpressionAttributeToSelect('name', '(CONCAT({{firstname}},"  ",{{lastname}}))', [
            'firstname',
            'lastname'
        ]);
        
        if ($type) {
            $contactPersonIds = $this->getApprover($customerId);
            $idList = [];
            if ($contactPersonIds) {
                foreach ($contactPersonIds as $val) {
                    $idList[] = $val['contact_person_id'];
                }
                $collection->addAttributeToFilter('entity_id', [
                    'nin' => $idList
                ]);
            }
        }
        
        return $collection->getData();
    }

    /**
     * @param int $customerId
     * @return array
     */
    public function getApprover($customerId)
    {
        $approverData = $this->approverCollectionFactory->create()
                        ->addFieldToFilter('customer_id', $customerId);
        return $approverData->getData();
    }
}
