<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\Quotation\Quote\Item\Search\Grid\Renderer;

class Qty extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Input
{

    /**
     * Type config
     *
     * @var \Magento\Catalog\Model\ProductTypes\ConfigInterface
     */
    public $typeConfig;
    
    /**
     * @var string
     */
    public $disabled;
    
    /**
     * @var string
     */
    public $addClass;
    
    /**
     * @var string
     */
    public $html;

    /**
     *
     * @param \Magento\Backend\Block\Context $context
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $typeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $typeConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->typeConfig = $typeConfig;
    }

    /**
     * Returns whether this qty field must be inactive
     *
     * @param \Magento\Framework\DataObject $row
     * @return bool
     */
    public function isRowActive($row)
    {
        $typeId = $row->getTypeId();
        return $this->typeConfig->isProductSet($typeId);
    }

    /**
     * Render product qty field
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $disabledIndicator = '';
        $addClassIndicator = '';
        
        if ($this->_isInactive($row)) {
            $qty = '';
            $disabledIndicator = 'disabled="disabled" ';
            $addClassIndicator = ' input-inactive';
        } else {
            $qty = $row->getData($this->getColumn()
                ->getIndex());
            $qty *= 1;
            if (! $qty) {
                $qty = '';
            }
        }
        $columnId = $this->getColumn()->getId();
        // Compose html
        $html = '<input type="text" ';
        $html .= 'name="' . $columnId . '" ';
        $html .= 'value="' . $qty . '" ' . $disabledIndicator;
        $html .= 'class="input-text admin__control-text '
                . $this->getColumn()->getInlineCss() . $addClassIndicator . '" />';
        return $html;
    }
}
