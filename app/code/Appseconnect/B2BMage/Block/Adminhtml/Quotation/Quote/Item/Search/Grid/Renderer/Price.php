<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\Quotation\Quote\Item\Search\Grid\Renderer;

/**
 * Adminhtml sales create order product search grid price column renderer
 *
 * @author Magento Core Team <core@magentocommerce.com>
 */
class Price extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Price
{

    /**
     * Render minimal price for downloadable products
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        if ($row->getTypeId() == 'downloadable') {
            $row->setPrice($row->getPrice());
        }
        return parent::render($row);
    }
}
