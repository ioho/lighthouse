<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\Quotation\Quote\View;

use Magento\Framework\View\Element\Message\InterpretationStrategyInterface;
use Appseconnect\B2BMage\Model\Quote;

class Messages extends \Magento\Framework\View\Element\Messages
{

    /**
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;

    /**
     * @var \Appseconnect\B2BMage\Model\QuoteProduct
     */
    public $quoteItems;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Appseconnect\B2BMage\Model\QuoteProduct $quoteItems
     * @param \Magento\Framework\Message\Factory $messageFactory
     * @param \Magento\Framework\Message\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param InterpretationStrategyInterface $interpretationStrategy
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Appseconnect\B2BMage\Model\QuoteProduct $quoteItems,
        \Magento\Framework\Message\Factory $messageFactory,
        \Magento\Framework\Message\CollectionFactory $collectionFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        InterpretationStrategyInterface $interpretationStrategy,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $messageFactory,
            $collectionFactory,
            $messageManager,
            $interpretationStrategy,
            $data
        );
        $this->quoteItems = $quoteItems;
        $this->coreRegistry = $registry;
    }

    /**
     * Retrieve quote model instance
     *
     * @return Quote
     */
    public function _getQuote()
    {
        return $this->coreRegistry->registry('insync_customer_quote');
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    public function _prepareLayout()
    {
        /**
         * Check Item products existing
         */
        $productIds = [];
        $quoteItems = $this->quoteItems->getCollection()
            ->addFieldToFilter('quote_id', $this->_getQuote()
            ->getId())
            ->getData();
        foreach ($quoteItems as $item) {
            $productIds[] = $item['product_id'];
        }
        
        return parent::_prepareLayout();
    }
}
