<?php
namespace Appseconnect\B2BMage\Block\Adminhtml\Quotation\Quote\View;

class Form extends \Magento\Backend\Block\Template
{

    /**
     * @return string
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::quotation/quote/view/form.phtml");
        
        return parent::_toHtml();
    }
}
