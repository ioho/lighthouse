<?php
namespace Appseconnect\B2BMage\Block\Salesrep\Html;

use Magento\Catalog\Model\Session;

/**
 * Html page header block
 */
class Header extends \Magento\Theme\Block\Html\Header
{
    
    /**
     * @var Session
     */
    public $customerSession;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param Session $catalogSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Session $catalogSession,
        array $data = []
    ) {
    
        $this->catalogSession = $catalogSession;
        parent::__construct($context, $data);
    }
    
    /**
     * @return $this
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::salesrep/html/header.phtml");
        
        return parent::_toHtml();
    }
    
    /**
     * @return Session
     */
    public function getMessage()
    {
        return $this->catalogSession->getSalesrepMessage();
    }
}
