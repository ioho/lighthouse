<?php
namespace Appseconnect\B2BMage\Block\Salesrep\Customer;

use Magento\Customer\Model\Session;
use Appseconnect\B2BMage\Model\ResourceModel\SalesrepgridFactory;

class Listing extends \Magento\Framework\View\Element\Template
{

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public $customers;

    /**
     * @var SalesrepgridFactory
     */
    public $salesRepResourceFactory;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param SalesrepgridFactory $salesRepResourceFactory
     * @param Session $customerSession
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        SalesrepgridFactory $salesRepResourceFactory,
        Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        array $data = []
    ) {
    
        $this->customerSession = $customerSession;
        $this->salesRepResourceFactory = $salesRepResourceFactory;
        $this->customerFactory = $customerFactory;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return $this
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'simplenews.news.list.pager'
        );
        $pager->setLimit(10)
            ->setShowAmounts(true)
            ->setCollection($this->getCustomer());
        $this->setChild('pager', $pager);
        $this->getCustomer()->getData();
        
        return $this;
    }

    /**
     * @return $this
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::salesrep/customer/listing.phtml");
        
        return parent::_toHtml();
    }

    /**
     * @return boolean|\Magento\Customer\Model\CustomerFactory
     */
    public function getCustomer()
    {
        if (! ($customerId = $this->customerSession->getCustomerId())) {
            return false;
        }
        
        if (! $this->customers) {
            $this->customers = $this->customerFactory->create()->getCollection();
            $salesRepResourceModel = $this->salesRepResourceFactory->create();
            
            $this->customers = $salesRepResourceModel->getSalesRepCustomers(
                $this->customers,
                $customerId
            );
            $this->customers->addExpressionAttributeToSelect(
                'name',
                '(CONCAT({{firstname}},"  ",{{lastname}}))',
                [
                'firstname',
                'lastname',
                'customer_status'
                ]
            )->addFieldToFilter('customer_type', array('in' => array(4,1)));
        }
        
        return $this->customers;
    }

    /**
     * @param int $customerId
     * @return string
     */
    public function getContactPersonUrl($customerId)
    {
        return $this->getUrl('b2bmage/salesrep/customer_view/', [
            'customer_id' => $customerId['entity_id']
        ]);
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return boolean
     */
    public function canShowTab()
    {
        return false;
    }
}
