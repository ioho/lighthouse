<?php
namespace Appseconnect\B2BMage\Block\Salesrep\Customer;

use Magento\Customer\Model\Session;
use Appseconnect\B2BMage\Model\ResourceModel\ApproverFactory;

/**
 * Salesrep View block
 */
class View extends \Magento\Framework\View\Element\Template
{

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Collection */
    public $customers;
    
    /**
     * @var SalesrepgridFactory
     */
    public $salesRepResourceFactory;
    
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    public $customerFactory;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param ApproverFactory $approverResourceFactory
     * @param Session $customerSession
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        ApproverFactory $approverResourceFactory,
        Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        array $data = []
    ) {
    
        $this->customerSession = $customerSession;
        $this->approverResourceFactory = $approverResourceFactory;
        $this->customerFactory = $customerFactory;
        parent::__construct($context, $data);
    }
    
    /**
     * @return $this
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::salesrep/customer/view.phtml");
        
        return parent::_toHtml();
    }

    /**
     *
     * @return $this
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'simplenews.news.list.pager'
        );
        $pager->setLimit(10)
            ->setShowAmounts(true)
            ->setCollection($this->getCustomer());
        $this->setChild('pager', $pager);
        $this->getCustomer()->getData();
        
        return $this;
    }
    
    /**
     * @return boolean|\Magento\Customer\Model\CustomerFactory
     */
    public function getCustomer()
    {
        if (! ($customerSessionId = $this->customerSession->getCustomerId())) {
            return false;
        }
        
        if (! $this->customers) {
            $approverResourceModel = $this->approverResourceFactory->create();
            $customerId = $this->getRequest()->getParam('customer_id');
            $this->customers = $this->customerFactory->create()->getCollection();
            
            $this->customers = $approverResourceModel->getContacts($customerId, $this->customers);
            $this->customers->addExpressionAttributeToSelect(
                'name',
                '(CONCAT({{firstname}},"  ",{{lastname}}))',
                [
                'firstname',
                'lastname',
                'customer_status'
                ]
            );
        }
        
        return $this->customers;
    }
    
    /**
     * @param int $customerId
     * @return string
     */
    public function getContactPersonLogin($customrId)
    {
        return $this->getUrl('b2bmage/salesrep/customer_login/', [
            'customer_id' => $customrId['entity_id']
        ]);
    }

    /**
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Return back url for logged in and guest users
     *
     * @return string
     */
    public function getBackUrl()
    {
        if ($this->httpContext->getValue(Context::CONTEXT_AUTH)) {
            return $this->getUrl('*/*/history');
        }
        return $this->getUrl('*/*/form');
    }
}
