<?php

namespace Appseconnect\B2BMage\Block\Sales;

use Magento\Customer\Model\Session;

class CustomerAccount extends \Magento\Framework\View\Element\Template
{
    
    /**
     * @var \Magento\Framework\App\DefaultPathInterface
     */
    public $customerSession;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param Session $customerSession
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Session $customerSession
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
    }
    /**
     * @return boolean
     */
    public function isCustomerLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }
}
