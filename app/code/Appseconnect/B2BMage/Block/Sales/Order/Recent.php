<?php
namespace Appseconnect\B2BMage\Block\Sales\Order;

use Magento\Customer\Model\Session;

/**
 * Sales order history block
 */
class Recent extends \Magento\Sales\Block\Order\Recent
{
    
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    public $helperContactPerson;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param Session $customerSession
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        Session $customerSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $helperContactPerson,
        array $data = []
    ) {
    
        $this->helperContactPerson = $helperContactPerson;
        parent::__construct(
            $context,
            $orderCollectionFactory,
            $customerSession,
            $orderConfig,
            $data
        );
    }

    /**
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $filterField = "customer_id";
        $customerId = $this->_customerSession->getCustomerId();
        $customerType = $this->_customerSession->getCustomer()->getCustomerType();
        $salesrepId = null;
        $contactId = null;
        if ($customerType == 2) {
            $filterField = "salesrep_id";
        } elseif ($customerType == 3) {
            $contactId = $customerId;
            $parentCustomerMapData = $this->helperContactPerson->getCustomerId($contactId);
            $customerId = $parentCustomerMapData['customer_id'];
        }
        
        $orders = $this->_orderCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter($filterField, $customerId)
            ->addAttributeToFilter('status', [
            'in' => $this->_orderConfig->getVisibleOnFrontStatuses()
            ])
            ->addAttributeToSort('created_at', 'desc')
            ->setPageSize('5');
        if ($contactId) {
            $orders->addFieldToFilter('contact_person_id', $contactId);
        }
        $orders->load();
        $this->setOrders($orders);
    }
}
