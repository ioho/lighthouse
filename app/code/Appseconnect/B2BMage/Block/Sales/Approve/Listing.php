<?php
namespace Appseconnect\B2BMage\Block\Sales\Approve;

use Magento\Customer\Model\Session;
use Appseconnect\B2BMage\Model\ResourceModel\OrderApproverFactory;

class Listing extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    public $orderCollectionFactory;

    /**
     *
     * @var \Magento\Customer\Model\Session
     */
    public $customerSession;

    /**
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public $orders;

    /**
     *
     * @var \Magento\Sales\Model\Order\Config
     */
    public $orderConfig;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public $customers;

    /**
     * @var OrderApproverFactory
     */
    public $orderApproverResourceFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param OrderApproverFactory $orderApproverResourceFactory
     * @param Session $customerSession
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        OrderApproverFactory $orderApproverResourceFactory,
        Session $customerSession,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\Order\Config $orderConfig,
        array $data = []
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderApproverResourceFactory = $orderApproverResourceFactory;
        $this->orderConfig = $orderConfig;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return $this
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::sales/approve/listing.phtml");
        
        return parent::_toHtml();
    }

    /**
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     *
     * @return $this
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'simplenews.news.list.pager'
        );
        $pager->setLimit(10)
            ->setShowAmounts(true)
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function canShowTab()
    {
        return false;
    }

    /**
     *
     * @return bool|\Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getOrders()
    {
        if (! ($customerId = $this->customerSession->getCustomerId())) {
            return false;
        }
        if (! $this->orders) {
            $orderApproverResource = $this->orderApproverResourceFactory->create();
            $this->orders = $this->orderCollectionFactory->create()
                ->addFieldToSelect('*')
                ->addFieldToFilter('main_table.status', [
                'in' => $this->orderConfig->getVisibleOnFrontStatuses()
                ])
                ->setOrder('main_table.created_at', 'desc');
            
            $approvalOrders = $orderApproverResource->getApprovalOrders(
                $customerId,
                $this->orders
            );
            $approvalOrders->addFieldToFilter('main_table.status', 'holded');
            $this->orders = $approvalOrders;
        }
        return $this->orders;
    }

    /**
     * @param id $id
     * @return static
     */
    public function getOrderEditUrl($id)
    {
        return $this->getUrl('b2bmage/sales/order_edit/', [
            'order_id' => $id,
            'approver' => 'yes'
        ]);
    }
    
    /**
     * @param id $id
     * @return static
     */
    public function getViewUrl($id)
    {
        return $this->getUrl('sales/order/view/', [
            'order_id' => $id,
            'approver' => 'yes'
        ]);
    }
    
    /**
     * @return static
     */
    public function getActionUrl()
    {
        return $this->getUrl('*/*/approve_order/', []);
    }
}
