<?php
namespace Appseconnect\B2BMage\Block\QuickOrder\Cart;

class ProductListing extends Group\AbstractGroup
{

    /**
     * @return string
     */
    public function _tohtml()
    {
        $this->setTemplate("Appseconnect_B2BMage::quickorder/cart/productlisting.phtml");
        
        return parent::_toHtml();
    }
    
    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }
    
    /**
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}
