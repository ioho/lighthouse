<?php
namespace Appseconnect\B2BMage\Block\Wishlist;

use Magento\Customer\Model\Session;

class Link extends \Magento\Framework\View\Element\Html\Link
{
    
    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    public $wishlistHelper;
    
    /**
     * @var Session
     */
    public $customerSession;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Wishlist\Helper\Data $wishlistHelper
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        Session $customerSession,
        array $data = []
    ) {
        $this->wishlistHelper = $wishlistHelper;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function _toHtml()
    {
        $customerType = $this->customerSession->getCustomer()->getCustomerType();
        if ($this->wishlistHelper->isAllow() && $customerType != 2) {
            $this->setTemplate("Magento_Wishlist::link.phtml");
            return parent::_toHtml();
        }
        return '';
    }

    /**
     * @return string
     */
    public function getHref()
    {
        return $this->getUrl('wishlist');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('My Wish List');
    }
}
