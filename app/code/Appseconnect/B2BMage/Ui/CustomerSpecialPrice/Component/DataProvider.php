<?php
namespace Appseconnect\B2BMage\Ui\CustomerSpecialPrice\Component;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;

class DataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{

    /**
     *
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = parent::getData();
        return $data;
    }
}
