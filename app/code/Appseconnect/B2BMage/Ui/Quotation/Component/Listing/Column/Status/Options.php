<?php
namespace Appseconnect\B2BMage\Ui\Quotation\Component\Listing\Column\Status;

use Magento\Framework\Data\OptionSourceInterface;
use Appseconnect\B2BMage\Model\ResourceModel\QuoteStatus\CollectionFactory;

class Options implements OptionSourceInterface
{

    /**
     *
     * @var array
     */
    public $options;

    /**
     *
     * @var CollectionFactory
     */
    public $collectionFactory;

    /**
     * Constructor
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = $this->collectionFactory->create()->toOptionArray();
        }
        return $this->options;
    }
}
