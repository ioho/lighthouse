/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* global $, $H */

define([
    'mage/adminhtml/grid'
], function () {
    'use strict';

    return function (config) {
        var selectedProducts = config.selectedProducts,
            pricelistProducts = $H(selectedProducts),
            gridJsObject = window[config.gridJsObjectName],
            tabIndex = 1000;

        $('in_pricelist_products').value = Object.toJSON(pricelistProducts);

        /**
         * Register Pricelist Product
         *
         * @param {Object} grid
         * @param {Object} element
         * @param {Boolean} checked
         */
        function registerPricelistProduct(grid, element, checked)
        {
            if (checked) {
                if (element.finalPriceElement) {
                    element.finalPriceElement.disabled = false;
                    pricelistProducts.set(element.value, element.finalPriceElement.value);
                }
            } else {
                if (element.finalPriceElement) {
                    element.finalPriceElement.disabled = true;
                }
                pricelistProducts.unset(element.value);
            }
            $('in_pricelist_products').value = Object.toJSON(pricelistProducts);
            grid.reloadParams = {
                'products[]': pricelistProducts.keys()
            };
        }

        /**
         * Click on product row
         *
         * @param {Object} grid
         * @param {String} event
         */
        function pricelistProductRowClick(grid, event)
        {
            var trElement = Event.findElement(event, 'tr'),
                isInput = Event.element(event).tagName === 'INPUT',
                checked = false,
                checkbox = null;

            if (trElement) {
                checkbox = Element.getElementsBySelector(trElement, 'input');

                if (checkbox[0]) {
                    checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                    gridJsObject.setCheckboxChecked(checkbox[0], checked);
                }
            }
        }

        /**
         * Change product finalPrice
         *
         * @param {String} event
         */
        function finalPriceChange(event)
        {
            var element = Event.element(event);

            if (element && element.checkboxElement && element.checkboxElement.checked) {
                pricelistProducts.set(element.checkboxElement.value, element.value);
                $('in_pricelist_products').value = Object.toJSON(pricelistProducts);
            }
        }

        /**
         * Initialize pricelist product row
         *
         * @param {Object} grid
         * @param {String} row
         */
        function pricelistProductRowInit(grid, row)
        {
            var checkbox = $(row).getElementsByClassName('checkbox')[0],
                finalPrice = $(row).getElementsByClassName('input-text')[0];

            if (checkbox && finalPrice) {
                if (pricelistProducts.get(checkbox.value)) {
                    finalPrice.value = pricelistProducts.get(checkbox.value);
                }
                checkbox.finalPriceElement = finalPrice;
                finalPrice.checkboxElement = checkbox;
                finalPrice.disabled = !checkbox.checked;
                finalPrice.tabIndex = tabIndex++;
                Event.observe(finalPrice, 'keyup', finalPriceChange);
            }
        }

        gridJsObject.rowClickCallback = pricelistProductRowClick;
        gridJsObject.initRowCallback = pricelistProductRowInit;
        gridJsObject.checkboxCheckCallback = registerPricelistProduct;

        if (gridJsObject.rows) {
            gridJsObject.rows.each(function (row) {
                pricelistProductRowInit(gridJsObject, row);
            });
        }
    };
});

