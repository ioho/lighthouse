<?php
namespace Appseconnect\LightechDiscounts\Api;

/**
 * Interface for creating Lightech Discounts.
 * @api
 */
interface LightechDiscountsRepositryInterface
{
    
    /**
     * Create Sconto Discount.
     *
     * @param \Appseconnect\LightechDiscounts\Api\Data\LightechDiscountsInterface $discountInfo
     * @return \Appseconnect\LightechDiscounts\Api\Data\LightechDiscountsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createDiscount(\Appseconnect\LightechDiscounts\Api\Data\LightechDiscountsInterface $discountInfo);
}
