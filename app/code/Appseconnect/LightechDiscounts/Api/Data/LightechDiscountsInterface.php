<?php
namespace Appseconnect\LightechDiscounts\Api\Data;

/**
 * Update Lightech Discounts Interface.
 * @api
 */
interface LightechDiscountsInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    
    
    /*
     * Discount ID
     */
    const ENTITY_ID = 'entity_id';
    
    /*
     * Customer ID
     */
    const CUSTOMER_ID = 'customer_id';

    /*
     * Class ID
     */
    const CLASSE_ID = 'classe_id';

    /*
    * Product SKU
    */
    const SKU = 'sku';
    /*
     * Product ID
     */
    const PRODUCT_ID = 'product_id';
    /*
     * Discount Percent.
     */
    const SCONTO = 'sconto';

    /*
     * Customer Group
     */
    const SCONTO1 = 'sconto1';

    /*
     * Type of customer for
     */
    const SCONTO2 = 'sconto2';

    /*
     * Item Property
     */
    const SCONTO3 = 'sconto3';
    
    /*
     * Is Active.
     */
    const IS_ACTIVE = 'is_active';
    
    /*
     * Website Id.
     */
    const WEBSITE_ID = 'website_id';

    /**
     * Gets the Row ID.
     *
     * @return int|null Discount Id.
     */
    public function getEntityId();
    
    /**
     * Gets the Customer ID
     *
     * @return int|null .
     */
    public function getCustomerId();

    /**
     * Gets the Classe ID
     *
     * @return int|null .
     */
    public function getClasseId();

    /**
     * Gets the product sku.
     *
     * @return string|null .
     */
    public function getSku();

    /**
     * Gets the Product ID
     *
     * @return int|null .
     */
    public function getProductId();
    
    /**
     * Gets the discount sconto.
     *
     * @return string|null .
     */
    public function getSconto();

    /**
     * Gets the discount sconto1.
     *
     * @return string|null .
     */
    public function getSconto1();
    /**
     * Gets the discount sconto2.
     *
     * @return string|null .
     */
    public function getSconto2();
    /**
     * Gets the discount sconto3.
     *
     * @return string|null .
     */
    public function getSconto3();
    /**
     * Get Is Active
     *
     * @return int|null
     */
    public function getIsActive();
    
    /**
     * Get Website Id
     *
     * @return int|null
     */
    public function getWebsiteId();
    
    /**
     * Sets the SCONTO Entity ID.
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Sets the Customer ID.
     *
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * Sets the Class ID.
     *
     * @param int $classId
     * @return $this
     */
    public function setClasseId($classId);

    /**
     * Set Sku
     *
     * @param $sku
     *
     * @return $this
     */
    public function setSku($sku);

    /**
     * Sets the Product ID.
     *
     * @param int $productId
     * @return $this
     */
    public function setProductId($productId);

    /**
     * Set Sconto
     *
     * @param $sconto
     *
     * @return $this
     */
    public function setSconto($sconto);

    /**
     * Set Sconto1
     *
     * @param $sconto1
     *
     * @return $this
     */
    public function setSconto1($sconto1);
    /**
     * Set Sconto2
     *
     * @param $sconto2
     *
     * @return $this
     */
    public function setSconto2($sconto2);
    /**
     * Set Sconto3
     *
     * @param $sconto3
     *
     * @return $this
     */
    public function setSconto3($sconto3);
    
    /**
     * Set Is Active
     *
     * @param $isActive
     *
     * @return $this
     */
    public function setIsActive($isActive);
    
    /**
     * Set Website Id
     *
     * @param $websiteId
     *
     * @return $this 
     */
    public function setWebsiteId($websiteId);

}
