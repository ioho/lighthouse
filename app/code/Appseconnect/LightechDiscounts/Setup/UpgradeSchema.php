<?php
namespace Appseconnect\LightechDiscounts\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            $installer = $setup;
            $installer->startSetup();
            // Get insync_lightechdiscounts table
            $tableName = $installer->getTable('insync_lightechdiscounts');
            // Check if the table already exists
            if ($installer->getConnection()->isTableExists($tableName) == true) {
                // Create insync_lightechdiscounts table
                $setup->getConnection()->addColumn(
                    $tableName,
                    'sku', [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 128,
                    'nullable' => false,
                    'comment' => 'Product SKU'
                ]);
            }
            $installer->endSetup();
        }


        if (version_compare($context->getVersion(), '0.0.3') < 0) {
            $installer = $setup;
            $installer->startSetup();
            // Get insync_lightechdiscounts table
            $tableName = $installer->getTable('sales_order_item');
            // Check if the table already exists
            if ($installer->getConnection()->isTableExists($tableName) == true) {
                // Create insync_lightechdiscounts table
                $setup->getConnection()->addColumn(
                    $tableName,
                    'insync_sconto', [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 128,
                    'nullable' => true,
                    'comment' => 'Lightech Sconto'
                ]);
            }
            $installer->endSetup();
        }

        if (version_compare($context->getVersion(), '0.0.4') < 0) {
            $installer = $setup;
            $installer->startSetup();
            // Get insync_lightechdiscounts table
            $tableName = $installer->getTable('insync_lightechdiscounts');
            // Check if the table already exists
            if ($installer->getConnection()->isTableExists($tableName) == true) {
                // Create insync_lightechdiscounts table
                $setup->getConnection()->addColumn(
                    $tableName,
                    'classe_id', [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 11,
                    'nullable' => false,
                    'comment' => 'Product Classe'
                ]);
            }
            $installer->endSetup();
        }
    }
}
