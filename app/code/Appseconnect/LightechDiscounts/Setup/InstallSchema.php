<?php
namespace Appseconnect\LightechDiscounts\Setup;
 
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
 
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
 
        // Get insync_lightechdiscounts table
        $tableName = $installer->getTable('insync_lightechdiscounts');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            // Create insync_lightechdiscounts table
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true,
                        'auto_increment' => true
                    ],
                    'Entity ID'
                )
                ->addColumn(
                    'customer_id',
                    Table::TYPE_INTEGER,
                    10,
                    [
                        'nullable' => false,
                        'unsigned' => true
                    ],
                    'Customer ID'
                )
                ->addColumn(
                    'sconto',
                    Table::TYPE_TEXT,
                    10,
                    ['nullable' => false],
                    'Sconto'
                    )
                ->addColumn(
                    'sconto1',
                    Table::TYPE_TEXT,
                    10,
                    ['nullable' => false],
                    'Sconto 1'
                )
                ->addColumn(
                    'sconto2',
                    Table::TYPE_TEXT,
                    10,
                    ['nullable' => false],
                    'Sconto 2'
                )
                ->addColumn(
                    'sconto3',
                    Table::TYPE_TEXT,
                    10,
                    ['nullable' => false],
                    'Sconto 3'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Is Active'
                )
                ->addColumn(
                    'website_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Associated Website Id'
                )
                ->addColumn(
                    'product_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Product ID'
                )
                ->setComment('Appseconnect Lightech Discounts')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
