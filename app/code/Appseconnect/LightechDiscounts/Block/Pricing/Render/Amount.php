<?php
namespace Appseconnect\LightechDiscounts\Block\Pricing\Render;

use Magento\Framework\Pricing\Amount\AmountInterface;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template;

class Amount extends \Magento\Framework\Pricing\Render\Amount
{
    /**
     * @var \Appseconnect\LightechDiscounts\Helper\Data
     */
    public $lighTechDiscountHelper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Amount constructor.
     * @param Template\Context $context
     * @param AmountInterface $amount
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\Pricing\Render\RendererPool $rendererPool
     * @param SaleableInterface|null $saleableItem
     * @param PriceInterface|null $price
     * @param \Appseconnect\LightechDiscounts\Helper\Data $lighTechDiscountHelper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        AmountInterface $amount,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\Pricing\Render\RendererPool $rendererPool,
        SaleableInterface $saleableItem = null,
        PriceInterface $price = null,
        \Appseconnect\LightechDiscounts\Helper\Data $lighTechDiscountHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        parent::__construct($context, $amount, $priceCurrency, $rendererPool, $saleableItem, $price, $data);
        $this->lighTechDiscountHelper = $lighTechDiscountHelper;
        $this->productRepository = $productRepository;
    }

    public function getDisplayValue()
    {
        $writer = new \Zend\Log\Writer\Stream (BP."/var/log/TISU.log");
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        if($this->getZone() == "item_view" && $this->getSaleableItem()->getTypeId() == 'simple') {
            $product = $this->productRepository->getById($this->getSaleableItem()->getId());
            $logger->info('data='.print_r($product->getFinalPrice(),true));

            return $this->lighTechDiscountHelper->getScontoData($this->getSaleableItem()->getId(), '', 1, $product->getFinalPrice());
        }
        elseif($this->getSaleableItem()->getTypeId() == 'simple') {
            return $this->getSaleableItem()->getPrice();
        } 
        elseif($this->getSaleableItem()->getTypeId() == 'configurable') {
            if($this->getSaleableItem()->getId() !== null && $this->getSaleableItem()->getId() > 0) {
                $childs = $this->lighTechDiscountHelper->getConfigChildProductIds($this->getSaleableItem()->getId());
                if($childs[0]->getData('base_cost') != null && 
                    $childs[0]->getData('price') != null && 
                    $childs[0]->getData('base_cost') > $childs[0]->getData('price')) {

                    return $childs[0]->getData('price');
                } elseif($childs[0]->getData('base_cost') != null) { 

                    return $childs[0]->getData('base_cost');
                } else {
                    return $this->getSaleableItem()->getMinimalPrice();
                }
            } else {
                return $this->getSaleableItem()->getMinimalPrice();
            }
        }
    }

    public function getCustomPriceDiscount() 
    {
        if($this->getZone() == "item_view" && $this->getSaleableItem()->getTypeId() == 'simple') {
            return $this->lighTechDiscountHelper->getScontoData($this->getSaleableItem()->getId(), '', 1, $this->getSaleableItem()->getPrice());
        }
        elseif($this->getSaleableItem()->getTypeId() == 'simple') {
            return $this->getSaleableItem()->getPrice();
        }
        elseif($this->getSaleableItem()->getTypeId() == 'configurable') {
            if($this->getSaleableItem()->getId() !== null && $this->getSaleableItem()->getId() > 0) {
                $childs = $this->lighTechDiscountHelper->getConfigChildProductIds($this->getSaleableItem()->getId());
                if($childs[0]->getData('base_cost') != null) {
                    return $childs[0]->getData('base_cost');
                } else {
                    return $this->getSaleableItem()->getMinimalPrice();
                }
            } else {
                return $this->getSaleableItem()->getMinimalPrice();
            }
        } 
        else {
            return $this->getSaleableItem()->getMinimalPrice();
        }
    }
}
