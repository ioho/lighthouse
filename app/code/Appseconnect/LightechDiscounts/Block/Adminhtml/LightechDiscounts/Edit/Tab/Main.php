<?php

namespace Appseconnect\LightechDiscounts\Block\Adminhtml\LightechDiscounts\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;

class Main extends Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    /**
     * Form constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Eav\Api\AttributeRepositoryInterface $eavAttributeRepository,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_systemStore = $systemStore;
        $this->eavAttributeRepository = $eavAttributeRepository;
    }

    /**
     * @return Generic
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        /** @var $model \Appseconnect\LightechDiscounts\Model\LightechDiscounts */
        $model = $this->_coreRegistry->registry('insync_lightech_discounts');

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => 'Lightech Discounts']
        );

        if (!empty($model) && $model->getEntityId()) {
            $fieldset->addField(
                'entity_id',
                'hidden',
                ['name' => 'entity_id']
            );
        }
        $fieldset->addField(
            'is_active',
            'select',
            [
                'name'      => 'is_active',
                'label'     => __('Is Active'),
                'options'   => array('1' => 'Yes', '0' => 'No')
            ]
        );
        $fieldset->addField(
            'website_id',
            'select',
            [
                'name' => 'website_id',
                'label' => __('Associate to Website'),
                'title' => __('Associate to Website'),
//                'required' => true,
                'values' => $this->_systemStore->getWebsiteValuesForForm(),

            ]
        );
        $fieldset->addField(
            'customer_id',
            'text',
            [
                'name'        => 'customer_id',
                'label'    => __('Customer_id'),
                'required'     => true
            ]
        );

        $attribute = $this->eavAttributeRepository
            ->get(\Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE,'classe');
        $options = $attribute->getSource()->getAllOptions(false);
        //print_r($options);
        $fieldset->addField(
            'classe_id',
            'select',
            [
                'name' => 'classe_id',
                'label' => __('Product Classe'),
                'title' => __('Product Classe'),
                'required' => true,
                'values' => $options,

            ]
        );
//        $fieldset->addField(
//            'sku',
//            'text',
//            [
//                'name'        => 'sku',
//                'label'    => __('SKU'),
//                'required'     => true
//            ]
//        );
        $fieldset->addField(
            'sconto1',
            'text',
            [
                'name'        => 'sconto1',
                'label'    => __('Sconto 1'),
                'required'     => true
            ]
        );
        $fieldset->addField(
            'sconto2',
            'text',
            [
                'name'        => 'sconto2',
                'label'    => __('Sconto 2'),
                'required'     => true
            ]
        );
        $fieldset->addField(
            'sconto3',
            'text',
            [
                'name'        => 'sconto3',
                'label'    => __('Sconto 3'),
                'required'     => true
            ]
        );
        $data ="";
        if(!empty($model)) {
            $data = $model->getData();
        }
        $form->setValues($data);
        $this->setForm($form);
        return parent::_prepareForm();
    }


    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Edit Lightech Discount');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Edit Lightech Discount');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
    public function _isAllowed(){
        return true;
    }
}
