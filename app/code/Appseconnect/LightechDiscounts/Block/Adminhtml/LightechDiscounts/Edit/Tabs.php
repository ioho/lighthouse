<?php
namespace Appseconnect\LightechDiscounts\Block\Adminhtml\LightechDiscounts\Edit;
use Magento\Backend\Block\Widget\Tabs as WidgetTabs;
class Tabs extends WidgetTabs
{
    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('discount_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Discount Information'));
    }

    /**
     * @return \Magento\Backend\Block\Widget
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'discount_info',
            [
                'label' => __('General'),
                'title' => __('General'),
                'content' => $this->getLayout()->createBlock(
                    'Appseconnect\LightechDiscounts\Block\Adminhtml\LightechDiscounts\Edit\Tab\Main'
                )->toHtml(),
                'active' => true
            ]
        );

        return parent::_beforeToHtml();
    }
}
