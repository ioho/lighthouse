<?php

namespace Appseconnect\LightechDiscounts\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\State;
use Appseconnect\LightechDiscounts\Helper\Data as DiscountHelper;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\Session\SessionManagerInterface;

class AdminCheckoutSubmitAfter implements ObserverInterface
{
    /**
     * @var State
     */
    protected $_state;

    /**
     * AdminCustomPrice constructor.
     * @param State $state
     * @param DiscountHelper $discountHelper
     */
    public function __construct(
        State $state,
        DiscountHelper $discountHelper,
        Quote $sessionQuote,
        SessionManagerInterface $coreSession
    ){
        $this->_state=$state;
        $this->discountHelper = $discountHelper;
        $this->sessionQuote = $sessionQuote;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        if($this->_state->getAreaCode()=='adminhtml'){
//            $items =  $observer->getEvent()->getData('items');
//            $cId = $this->sessionQuote->getCustomerId();
            $subtotal= 0;
//            foreach($items as $item) {
//                $subtotal += $item->getBaseCost();
//                $class = $item->getProduct()->getClasse();
//                $dCollection = $this->discountHelper->getDiscountCollection($cId, $class);
//                if (!empty($dCollection)) {
//                    $price = $item->getBaseCost() * $dCollection[0]['sconto'];
//                    $item->setCustomPrice($price);
//                    $item->setInsyncSconto($dCollection[0]['sconto']);
//                    $item->setOriginalCustomPrice($price);
//                    $item->getProduct()->setIsSuperMode(true);
//                }
//            }
//            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/testrepo.log');
//            $logger = new \Zend\Log\Logger();
//            $logger->addWriter($writer);
//            $val=json_encode($observer->getEvent()->getData());
//            $logger->info("=====");
//            $logger->info("$val");
//            $logger->info("=====");



        }
    }

}