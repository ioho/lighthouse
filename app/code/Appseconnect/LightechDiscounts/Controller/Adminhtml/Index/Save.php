<?php
namespace Appseconnect\LightechDiscounts\Controller\Adminhtml\Index;

use Appseconnect\LightechDiscounts\Model\LightechDiscountsFactory;
use Magento\Backend\App\Action;
use Appseconnect\LightechDiscounts\Model\LightechDiscountsRepositry;
use Magento\Backend\Model\Session;
use Magento\Framework\Controller\ResultFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var LightechDiscountsFactory
     */
    protected $lightechDiscountsFactory;

    /**
     * @var Session
     */
    public $session;
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultRedirect;

    /**
     * Save constructor.
     * @param LightechDiscountsFactory $lightechDiscountsModel
     * @param Session $session
     * @param Action\Context $context
     * @param LightechDiscountsRepositry $reprositry
     */
    public function __construct(
        LightechDiscountsFactory $lightechDiscountsModel,
        Session $session,
        Action\Context $context,
        \Appseconnect\LightechDiscounts\Model\LightechDiscountsRepositry $reprositry
    )
    {
        $this->lightechDiscountsFactory = $lightechDiscountsModel;
        $this->session = $session;
        $this->reprositry = $reprositry;
        parent::__construct($context);

    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\InputException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $discountModel = $this->lightechDiscountsFactory->create();
        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
            if ($id) {
                $discountModel->load($id);
            }
            $pid = $this->reprositry->productValidator($data['classe_id']);
            $validate = $this->reprositry->discountDataValidator($data);
            $customerExist = $this->reprositry->isCustomerExist($data['customer_id']);
            if(isset($data['entity_id']) && (!$validate || !$customerExist || !$pid)){
                return $resultRedirect->setPath('*/*/edit', [
                    'id' => $data['entity_id'],
                    '_current' => true
                ]);
            }elseif(!$validate || !$customerExist || !$pid){
                return $resultRedirect->setPath('*/*/edit');
            }
            try {
                $existingDiscount = $this->reprositry->isDiscountExist($data);
                if(isset($existingDiscount["entity_id"]) && ($existingDiscount["entity_id"] !="" || $existingDiscount["entity_id"] == null)){
                    $discountModel->load($existingDiscount["entity_id"]);
                    $data['entity_id'] = $existingDiscount["entity_id"];
                }
                $data['sconto'] = $this->reprositry->getSconto($data['sconto1'],$data['sconto2'],$data['sconto3']);
                $discountModel->setData($data);
                $discountModel->save();
                $resultRedirect->setPath('*/*/edit');
                $this->messageManager->addSuccess(__('The discount has been saved.'));
                $this->session->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', [
                        'id' => $discountModel->getEntityId(),
                        '_current' => true
                    ]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __('Something went wrong while saving the discount.')
                );
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', [
                'id' => $this->getRequest()
                    ->getParam('id')
            ]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
