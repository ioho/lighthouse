<?php
namespace Appseconnect\LightechDiscounts\Controller\Adminhtml\Index;
use Appseconnect\LightechDiscounts\Model\LightechDiscounts;
use Magento\Backend\App\Action;

class Delete extends Action
{
    public function execute()
    {
        $entityId = $this->getRequest()->getParam('id');

        if (!($lightechDiscounts = $this->_objectManager->create(LightechDiscounts::class)->load($entityId))) {
            $this->messageManager->addError(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
        try{
            $lightechDiscounts->delete();
            $this->messageManager->addSuccess(__('Your lightech discounts has been deleted !'));
        } catch (Exception $e) {
            $this->messageManager->addError(__('Error while trying to delete lightech discounts: '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }
}