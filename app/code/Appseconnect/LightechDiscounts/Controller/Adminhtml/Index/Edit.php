<?php
namespace Appseconnect\LightechDiscounts\Controller\Adminhtml\Index;


use Magento\Backend\App\Action;
use Appseconnect\LightechDiscounts\Model\LightechDiscountsFactory;
use Magento\Backend\Model\Session;

class Edit extends Action
{

    /**
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public $resultPageFactory;

    /**
     * @var LightechDiscountFactory
     */
    public $lightechDiscountFactory;

    /**
     * @var Session
     */
    public $session;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param LightechDiscountFactory $lightechDiscountFactory
     * @param Session $session
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        LightechDiscountsFactory $lightechDiscountFactory,
        Session $session,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {

        $this->lightechDiscountFactory = $lightechDiscountFactory;
        $this->session = $session;
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    private function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Appseconnect_LightechDiscounts::lightech_discounts')
            ->addBreadcrumb(__('Lightech Discount'), __('Lightech Discount'))
            ->addBreadcrumb(__('Manage Lightech Discount'), __('Manage Lightech Discount'));
        return $resultPage;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->lightechDiscountFactory->create();

        if ($id) {
            $model->load($id);
            if (! $model->getEntityId()) {
                $this->messageManager->addError(__('This discount no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->session->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }

        $this->coreRegistry->register('insync_lightech_discounts', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb($id ? __('Edit Lightech Dscount') : __('New Lightech Dscount'), $id ?__('Edit Lightech Dscount') :
            __('New Lightech Dscount'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend(__('Lightech Dscounts'));
        $resultPage->getConfig()
            ->getTitle()
            ->prepend($id ?
                __('Edit Lightech Dscount') :
                __('New Lightech Dscount'));

        return $resultPage;
    }
}