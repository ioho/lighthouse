<?php
namespace Appseconnect\LightechDiscounts\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Appseconnect\LightechDiscounts\Model\ResourceModel\LightechDiscounts\Collection;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{

    /**
     *
     * @var Filter
     */
    protected $filter;

    /**
     *
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var \Appseconnect\LightechDiscounts\Model\LightechDiscounts
     */
    protected $lightechDiscounts;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param \Appseconnect\LightechDiscounts\Model\LightechDiscounts $lightechDiscounts
     * @param Filter $filter
     * @param Collection $collectionFactory
     */
    public function __construct(
        Context $context,
        \Appseconnect\LightechDiscounts\Model\LightechDiscountsRepositry $lightechDiscounts,
        Filter $filter,
        Collection $collectionFactory)
    {
        $this->filter = $filter;
        $this->lightechDiscounts = $lightechDiscounts;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $selectedIds = $this->getRequest()->getParam('selected');
        $excludedStatus = $this->getRequest()->getParam('excluded');
        if ($this->getRequest()->getParam('excluded') && $excludedStatus == 'false') {
            $discountCollection = $this->collectionFactory->getData();
            $deletedCount = count($discountCollection);
            
            foreach ($discountCollection as $discount) {
                $this->lightechDiscounts->deleteById($discount['entity_id']);
            }
        } elseif ($this->getRequest()->getParam('selected') && $selectedIds) {
            $deletedCount = count($selectedIds);
            foreach ($selectedIds as $discountId) {
                $this->lightechDiscounts->deleteById($discountId);
            }
        }
        
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $deletedCount));
        
        /* @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}
