<?php
namespace Appseconnect\LightechDiscounts\Controller\Adminhtml\Index;

class Create extends \Magento\Backend\App\Action {

    public function __construct(
        \Magento\Backend\App\Action\Context $context
        ) {

        parent::__construct($context);
    }
    /**
     * Create new news action
     *
     * @return void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}
