<?php
namespace Appseconnect\LightechDiscounts\Plugin;

use Magento\Sales\Api\Data\OrderItemExtensionFactory;
use Magento\Sales\Api\Data\OrderItemExtensionInterface;
//use Magento\Sales\Api\Data\OrderItemInterface;
//use Magento\Sales\Api\Data\OrderSearchResultInterface;
//use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Model\Order;
    /**
     * Class OrderRepositoryPlugin
     */
class OrderItemRepository
{

    public function __construct(
        OrderItemExtensionFactory $extensionFactory,
        Order $order,
        \Appseconnect\LightechDiscounts\Helper\Data $lightechDiscountHelper
    )
    {
        $this->extensionFactory = $extensionFactory;
        $this->order = $order;
        $this->lightechDiscountHelper = $lightechDiscountHelper;
    }

    public function afterGet(\Magento\Sales\Api\OrderRepositoryInterface $subject, $entity) {
        $orderData = $this->order->load($entity->getId());
        $items = $orderData->getAllItems();

        $itemsData = array();
        foreach ($items as &$item) {
//            if(empty($item->getData('base_cost'))){
                $item->setBaseCost($this->lightechDiscountHelper->getProductBaseCostBySku($item->getSku()));
//            }
            $customerFeedback = $item->getData('insync_sconto');
            $extensionAttributes = $item->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();
            $extensionAttributes->setInsyncSconto($customerFeedback);
            $item->setExtensionAttributes($extensionAttributes);
            $itemsData[] = $item;
        }
        return $entity;
    }
}