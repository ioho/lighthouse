<?php
namespace Appseconnect\LightechDiscounts\Plugin;

use Appseconnect\LightechDiscounts\Helper\Data as LightechDiscountHelper;

/**
 * Class ItemUpdater
 */
class ItemUpdater
{
    public function __construct(
        LightechDiscountHelper $lightechDiscountHelper
    )
    {
        $this->lightechDiscountHelper = $lightechDiscountHelper;
    }

    public function beforeUpdate(\Magento\Quote\Model\Quote\Item\Updater $subject, $item, array $info)
    {
        $discountedPrice = $this->lightechDiscountHelper->getDiscountCollection($this->lightechDiscountHelper->getAdminOrderCreateCustomerId(), $this->lightechDiscountHelper->getProductClass($item->getProductId()));
        if (!empty($discountedPrice)) {
            $price = $item->getBaseCost()*$discountedPrice[0]['sconto'];
            $info['custom_price'] = $price;
        }

        return [$item, $info];
    }
}