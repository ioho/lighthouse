<?php
namespace Appseconnect\LightechDiscounts\Plugin;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

class Add
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Configurable
     */
    private $configurable;

    /**
     * Add constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param ProductRepositoryInterface $productRepository
     * @param Configurable $configurable
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ProductRepositoryInterface $productRepository,
        Configurable $configurable
    ) {
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->configurable = $configurable;
    }

    public function aroundExecute(
        \Magento\Checkout\Controller\Cart\Add $subject,
        \Closure $proceed
    ) {
        $productId = (int)$subject->getRequest()->getParam('product');
        if ($product = $this->initProduct($productId)) {

//            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/super_attribute.log');
//            $logger = new \Zend\Log\Logger();
//            $logger->addWriter($writer);


            if ($product->getTypeId() == Configurable::TYPE_CODE) {

                $params = $subject->getRequest()->getParams();
//                $var = json_encode($params);
//                $logger->info($var);
                if(isset($params['super_attribute']) && !isset($params['amconfigurable-option'])){
                    $childProduct = $this->configurable->getProductByAttributes($params['super_attribute'], $product);
                    if ($childProduct->getId()) {
                        $params['product'] = $childProduct->getId();
                        $params['item'] = null;
                        $params['super_attribute'] = null;
                        $params['product_page'] = false;
                        $subject->getRequest()->setParams($params);
                    }
                }

            }
        }

        return $proceed();
    }

    /**
     * Initialize product instance from request data
     *
     * @return \Magento\Catalog\Model\Product|false
     */
    protected function initProduct($productId)
    {
        if ($productId) {
            $storeId = $this->storeManager->getStore()->getId();
            try {
                return $this->productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }
}
