<?php
namespace Appseconnect\LightechDiscounts\Plugin;

use Appseconnect\LightechDiscounts\Helper\Data as LitechDiscountHelper;
use Magento\Framework\App\State;

class SaveSconto {

    /**
     * @var LitechDiscountHelper
     */
    protected $lightechDiscountHelper;
    /**
     * @var State
     */
    protected $_state;

    public function __construct(
        LitechDiscountHelper $lightechDiscountHelper,
        State $state
    ) {
        $this->lightechDiscountHelper = $lightechDiscountHelper;
        $this->_state=$state;
    }

    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {
        $discountStatus = null;
        /** @var $orderItem Item */
        $orderItem = $proceed($item, $additional);
        if(empty($item->getData('base_cost'))){
            $orderItem->setBaseCost($this->lightechDiscountHelper->getProductBaseCost($orderItem->getProductId()));
        }
        if ($this->_state->getAreaCode() == 'adminhtml') {
            $discountCollection = $this->lightechDiscountHelper->getDiscountCollection($this->lightechDiscountHelper->getAdminOrderCreateCustomerId(),$this->lightechDiscountHelper->getProductClass($orderItem->getProductId()));

            if (!empty($discountCollection)) {
                $orderItem->setInsyncSconto($discountCollection[0]['sconto'] * 100);
            }
        }else {
            $discountCollection = $this->lightechDiscountHelper->getScontoData($orderItem->getProductId());
            if ($discountCollection) {
                $orderItem->setInsyncSconto($discountCollection['sconto'] * 100);
            }
        }
        return $orderItem;
    }
}
