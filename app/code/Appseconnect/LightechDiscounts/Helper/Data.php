<?php
namespace Appseconnect\LightechDiscounts\Helper;

use Appseconnect\LightechDiscounts\Model\ResourceModel\LightechDiscounts\CollectionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Checkout\Model\Cart;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Backend\Model\Session\Quote as AdminQuote;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var LightechDiscounts
     */
    protected $lightechDiscounts;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $productModel;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var \Appseconnect\B2BMage\Helper\ContactPerson\Data
     */
    protected $contactPersonhelper;

    /**
     * @var CheckoutSession
     */
    protected $cart;

    /**
     * @var QuoteAdmin
     */
    protected $quoteAdmin;

    /**
     * Data constructor.
     * @param Context $context
     * @param CollectionFactory $lightechDiscountCollection
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ProductFactory $product
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Appseconnect\B2BMage\Helper\ContactPerson\Data $contactPersonhelper
     * @param Cart $cart
     * @param \Magento\Framework\Pricing\Helper\Data $formatPrice
     * @param QuoteAdmin $quoteAdmin
     */
    public function __construct(
        Context $context,
        CollectionFactory $lightechDiscountCollection,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Appseconnect\B2BMage\Helper\ContactPerson\Data $contactPersonhelper,
        Cart $cart,
        \Magento\Framework\Pricing\Helper\Data $formatPrice,
        AdminQuote $adminQuote,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper

    )
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->lightechDiscountCollectionFactory = $lightechDiscountCollection;
        $this->storeManager = $storeManager;
        $this->productModel = $product;
        $this->customerFactory = $customerFactory;
        $this->contactPersonhelper = $contactPersonhelper;
        $this->cart = $cart;
        $this->formatPrice = $formatPrice;
//        $this->coreSession = $coreSession;
        $this->adminQuote = $adminQuote;
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * @return mixed
     */
    public function getCustomerId(){
        return $this->customerSession->getCustomer()->getId();
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCurrentWebsiteId(){
        return $this->storeManager->getStore()->getWebsiteId();
    }

    /**
     * @param $proId
     * @param string $sku
     * @param int $qty
     * @param int $finalprice
     * @return float|int
     */
    public function getScontoData($proId,$sku="",$qty=1,$finalprice = 0){
        $customerId = $this->getCustomerId();
//        if(!empty($sku)){
//            $product = $this->productModel->create()->load($sku);
//        }else {
            $product = $this->productModel->create()->load($proId);
//        }
        $classeId = $product->getData('classe');
        if(!empty($customerId) && !empty($classeId)){
            $discountCollection = $this->getDiscountCollection($customerId,$classeId);
            if(empty($discountCollection)) {
                $discountCollection = $this->checkDiscountCollection($customerId, $classeId);
            }

            if(empty($discountCollection)){
                return $finalprice ? $finalprice: 0;
            }
            $priceData = $discountCollection[0];
            $discountedPrice = $finalprice ? ($finalprice - ($finalprice * $priceData['sconto'])) : $priceData;
            return $discountedPrice;
        }
        return $finalprice;
    }

    /**
     * @param $customerId
     * @param $classeId
     * @return mixed
     */
    public function checkDiscountCollection($customerId, $classeId){
        if ($b2bId = $this->checkContactPerson($customerId)) {
            $discountCollection = $this->getDiscountCollection($b2bId, $classeId);
            if (empty($discountCollection)) {
                return $this->getDiscountCollection($customerId, $classeId);
            }
            return $discountCollection;
        }
        return null;
    }

    // Check B2B customer discount
    /**
     * @param $id
     * @return int
     */
    public function checkContactPerson($id){
        $customer = $this->customerFactory->create()->load(intval($id));
        if($customer->getCustomerType() == 3){
            return $this->contactPersonhelper->getCustomerId($customer->getId());
        }
        return 0;
    }

    /**
     * @param $customerId
     * @param $classeId
     * @return mixed
     */
    public function getDiscountCollection($customerId, $classeId){
        return $this->lightechDiscountCollectionFactory->create()
            ->addFieldToFilter('customer_id', $customerId)
            ->addFieldToFilter('classe_id', $classeId)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('website_id', $this->getCurrentWebsiteId())
            ->getData();
    }

    /**
     * @param $proId
     * @return mixed
     */
    public function getProductClass($proId){
        $product = $this->productModel->create()->load($proId);
        return $product->getData('classe');
    }
    /**
     * @param $proId
     * @return mixed
     */
    public function getProductBaseCost($proId){
        $product = $this->productModel->create()->load($proId);
        return $product->getData('price');
    }
    /**
     * @param $sku
     * @return mixed
     */
    public function getProductBaseCostBySku($sku){
        return $this->productRepositoryInterface->get($sku)->getPrice();
    }

    public function getSubtotal(){
        $items = $this->cart->getQuote()->getAllVisibleItems();
        $total = 0;
        foreach ($items as $item){
            $item['base_cost'] = $this->getProductBaseCostBySku($item['sku']);
            $total += $item['base_cost'] * $item['qty'];
        }
        return $total;
    }

    public function getRowDetails(){
        $items = $this->cart->getQuote()->getAllVisibleItems();
        $products = array();
        $details = array();
        foreach ($items as $item){
            $item['base_cost'] = $this->getProductBaseCostBySku($item['sku']);
            $sconto = null;
            $details['subtotal'] = 0;
            if($item['base_cost'] != $item['price']){
                $details['subtotal'] = $this->formatPrice->currency($item['base_cost'] * $item['qty'],true,false);
                $customerId = $this->getCustomerId();
                $class = $this->getProductClass($item['product_id']);
                $sconto = $this->checkDiscountCollection($customerId, $class);
                if(empty($sconto) && $id = $this->checkContactPerson($customerId)){
                    $sconto = $this->checkDiscountCollection($id, $class);
                }
            }
            $details['discount_percent'] = empty($sconto)?0:$sconto[0]['sconto'];
            $products[$item['item_id']] = $details;
        }
        return $products;
    }

    public function getAdminOrderItemsD(){
        $this->coreSession->start();
        return  $this->coreSession->getCustomSubTotal();
    }

    public function getFormatPrice($item, $qty=1){
        return $this->formatPrice->currency($item * $qty,true,false);
    }

    public function getAdminOrderSubtotal(){
        $items = $this->adminQuote->getQuote()->getAllVisibleItems();
        $subtotal = 0;
        foreach($items as $item) {
            if(empty($item["base_cost"])){
                $item['base_cost'] = $this->getProductBaseCost($item['product_id']);
            }
            $subtotal += $item['base_cost']*$item->getQty();
        }
        return $subtotal;
    }

    public function getAdminOrderCreateCustomerId(){
        return $this->adminQuote->getCustomerId();
    }

    public function getConfigChildProductIds($id){
        $attributesIds = array("price","base_cost");
        $product = array();
        if(is_numeric($id)){           
            $product = $this->productRepositoryInterface->getById($id); 
        }else{
            return;
        } 

        if(!isset($product)){
            return;
        }

        if ($product->getTypeId() != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            return [];
        }

        $storeId = $this->storeManager->getStore()->getId();

        $productTypeInstance = $product->getTypeInstance();
        $productTypeInstance->setStoreFilter($storeId, $product);
        return $productTypeInstance->getUsedProducts($product,$attributesIds);
    }
}
