<?php
namespace Appseconnect\LightechDiscounts\Model;

class LightechDiscounts extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Appseconnect\LightechDiscounts\Model\ResourceModel\LightechDiscounts');
    }
}
