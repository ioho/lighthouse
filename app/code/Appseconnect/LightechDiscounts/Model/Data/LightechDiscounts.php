<?php
namespace Appseconnect\LightechDiscounts\Model\Data;

use \Magento\Framework\Api\AttributeValueFactory;

/**
 * Class LightechDiscounts
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class LightechDiscounts extends \Magento\Framework\Api\AbstractExtensibleObject implements \Appseconnect\LightechDiscounts\Api\Data\LightechDiscountsInterface
{

    /**
     * Get Discount Row ID
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return $this->_get(self::ENTITY_ID);
    }

    /**
     * Set Discount Row ID
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Set Customer ID
     *
     * @param int|null $customerId
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get Classe ID
     *
     * @return int|null
     */
    public function getClasseId()
    {
        return $this->_get(self::CLASSE_ID);
    }

    /**
     * Set Classe ID
     *
     * @param int|null $classeId
     * @return $this
     */
    public function setClasseId($classeId)
    {
        return $this->setData(self::CLASSE_ID, $classeId);
    }

    /**
     * Get Product ID
     *
     * @return int|null
     */
    public function getProductId()
    {
        return $this->_get(self::PRODUCT_ID);
    }

    /**
     * Set Customer ID
     *
     * @param int|null $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * Get Sku
     * @return string|null
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * Set sku
     *
     * @param string|null $sku
     * @return $this
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * Get Sconto
     * @return string|null
     */
    public function getSconto()
    {
        return $this->_get(self::SCONTO);
    }

    /**
     * Set sconto
     *
     * @param string|null $sconto
     * @return $this
     */
    public function setSconto($sconto)
    {
        return $this->setData(self::SCONTO, $sconto);
    }
    /**
     * Get Sconto1
     * @return string|null
     */
    public function getSconto1()
    {
        return $this->_get(self::SCONTO1);
    }

    /**
     * Set sconto1
     *
     * @param string|null $sconto1
     * @return $this
     */
    public function setSconto1($sconto1)
    {
        return $this->setData(self::SCONTO1, $sconto1);
    }
    /**
     * Get Sconto2
     * @return string|null
     */
    public function getSconto2()
    {
        return $this->_get(self::SCONTO2);
    }

    /**
     * Set sconto2
     *
     * @param string|null $sconto2
     * @return $this
     */
    public function setSconto2($sconto2)
    {
        return $this->setData(self::SCONTO2, $sconto2);
    }
    /**
     * Get Sconto3
     * @return string|null
     */
    public function getSconto3()
    {
        return $this->_get(self::SCONTO3);
    }

    /**
     * Set sconto3
     *
     * @param string|null $sconto
     * @return $this
     */
    public function setSconto3($sconto3)
    {
        return $this->setData(self::SCONTO3, $sconto3);
    }

    /**
     *
     * @return int|null
     */
    public function getIsActive()
    {
        return $this->_get(self::IS_ACTIVE);
    }

    /**
     * Set Is Active
     *
     * @param int $isActive
     * @return $this
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     *
     * @return int|null
     */
    public function getWebsiteId()
    {
        return $this->_get(self::WEBSITE_ID);
    }

    /**
     * Set Website Id
     *
     * @param int $websiteId
     * @return $this
     */
    public function setWebsiteId($websiteId)
    {
        return $this->setData(self::WEBSITE_ID, $websiteId);
    }
}
