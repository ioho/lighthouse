<?php
namespace Appseconnect\LightechDiscounts\Model\ResourceModel;

class LightechDiscounts extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('insync_lightechdiscounts', 'entity_id');
    }
}
