<?php
namespace Appseconnect\LightechDiscounts\Model\ResourceModel\LightechDiscounts;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Appseconnect\LightechDiscounts\Model\LightechDiscounts', 'Appseconnect\LightechDiscounts\Model\ResourceModel\LightechDiscounts');
        $this->_map['fields']['entity_id'] = 'main_table.entity_id';
    }

    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
}
