<?php
namespace Appseconnect\LightechDiscounts\Model;

use Appseconnect\LightechDiscounts\Api\LightechDiscountsRepositryInterface;

/**
 * Handle various actions
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)createDiscount
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class LightechDiscountsRepositry extends \Magento\Customer\Model\AccountManagement implements LightechDiscountsRepositryInterface
{
    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customerModel;

    /**
     * @var LightechDiscounts
     */
    protected $discountModel;

    /**
     * @var ResourceModel\LightechDiscounts\CollectionFactory
     */
    protected $lightechDiscountsCollection;

    /**
     * @var LightechDiscountsFactory
     */
    protected $lightechDiscountsFactory;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $productModel;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * LightechDiscountsRepositry constructor.
     * @param \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Magento\Customer\Model\Customer $customerModel
     * @param LightechDiscounts $discountModel
     * @param ResourceModel\LightechDiscounts\CollectionFactory $lightechDiscountsCollection
     * @param LightechDiscountsFactory $lightechDiscountsFactory
     * @param \Magento\Catalog\Model\Product $productModel
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        \Magento\Customer\Model\Customer $customerModel,
        \Appseconnect\LightechDiscounts\Model\LightechDiscounts $discountModel,
        \Appseconnect\LightechDiscounts\Model\ResourceModel\LightechDiscounts\CollectionFactory  $lightechDiscountsCollection,
        \Appseconnect\LightechDiscounts\Model\LightechDiscountsFactory $lightechDiscountsFactory,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->productModel = $productModel;
        $this->customerModel = $customerModel;
        $this->discountModel = $discountModel;
        $this->lightechDiscountsCollection = $lightechDiscountsCollection;
        $this->lightechDiscountsFactory = $lightechDiscountsFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * @param \Appseconnect\LightechDiscounts\Api\Data\LightechDiscountsInterface $discountInfo
     * @return \Appseconnect\LightechDiscounts\Api\Data\LightechDiscountsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createDiscount(\Appseconnect\LightechDiscounts\Api\Data\LightechDiscountsInterface $discountInfo)
    {
        $discountData = $this->extensibleDataObjectConverter->toNestedArray($discountInfo, [], '\Appseconnect\LightechDiscounts\Api\Data\LightechDiscountsInterface');
        if ($this->discountDataValidator($discountData) && $this->isCustomerExist($discountData['customer_id'])) {
            $saveData = $this->saveData($discountData);
            if($saveData) {
                $discountId = $saveData->getId();
                $discountInfo->setEntityId($discountId);
            }else{
                return $this->messageManager->getMessages(true)->getLastAddedMessage()->getText();
            }
        }else{
            return $this->messageManager->getMessages(true)->getLastAddedMessage()->getText();
        }
        return $discountInfo;
    }


    /**
     * @param $discountData
     * @return int
     * @throws \Magento\Framework\Exception\InputException
     */
    public function discountDataValidator($discountData)
    {
        $requiredFields[] = 'customer_id';
        $requiredFields[] = 'sku';
        $requiredFields[] = 'is_active';
        $requiredFields[] = 'sconto1';
        $requiredFields[] = 'sconto2';
        $requiredFields[] = 'sconto3';
        if($this->checkRequiredFields($requiredFields, $discountData)){
            return 0;
        }

        if ($discountData['is_active']) {
            if ($discountData['is_active'] != 0 && $discountData['is_active'] != 1)
                $this->messageManager->addError(__('[is_active] should have binary values.'));
        }
        if (isset($discountData['sconto1']) && isset($discountData['sconto2']) && isset($discountData['sconto3'])) {
            if ($discountData['sconto1'] < 0 || !is_numeric($discountData['sconto1'])) {
                $this->messageManager->addError(__('Inavlid [sconto1] value provided.'));
                return 0;
            }
            if ($discountData['sconto2'] < 0 || !is_numeric($discountData['sconto2'])) {
                $this->messageManager->addError(__('Inavlid [sconto3] value provided.'));
                return 0;
            }
            if ($discountData['sconto3'] < 0 || !is_numeric($discountData['sconto3'])) {
                $this->messageManager->addError(__('Inavlid [sconto3] value provided.'));
                return 0;
            }
        }
        return 1;
    }


    /**
     * @param $requiredFields
     * @param $discountData
     * @return int
     */
    protected function checkRequiredFields($requiredFields, $discountData)
    {
        foreach ($requiredFields as $requiredValues) {
            if (isset($discountData[$requiredValues]) && trim($discountData[$requiredValues]) == '') {
                $this->messageManager->addError(__('The [' . $requiredValues . '] can not be empty.'));
                break;
                return 1;
            }
            if (! isset($discountData[$requiredValues])) {
                $this->messageManager->addError(__('Field [' . $requiredValues . '] is required.'));
                break;
                return 1;
            }
        }
        return 0;
    }

    /**
     * @param int $id
     * @return void
     */
    public function deleteById($id){
        $model = $this->lightechDiscountsFactory->create();
        $model->load($id);
        $model->delete();
    }

    /**
     * @param $customerId
     * @return int
     */
    public function isCustomerExist($customerId){
        $customer = $this->customerModel->load($customerId);
        if(empty($customer->getId())){
            $this->messageManager->addError(__('Customer Id Not Found.'));
            return 0;
        }
        return 1;
    }

    /**
     * @param $sku
     * @return int
     */
    public function productValidator($sku){
        $pId = $this->productModel->getIdBySku($sku);
        if(empty($pId)){
            $this->messageManager->addError(__('Product Not Found.'));
            return 0;
        }
        return $pId;
    }

    /**
     * @param $discountData
     * @return mixed
     */
    public function isDiscountExist($discountData){
        $lightechDiscountsCollection = $this->lightechDiscountsCollection->create()
            ->addFieldToFilter('customer_id', $discountData['customer_id'])
            ->addFieldToFilter('product_id', $discountData['product_id'])
            ->addFieldToFilter('sku', $discountData['sku'])
//            ->addFieldToFilter('sconto', $discountData['sconto'])
//            ->addFieldToFilter('sconto2', $discountData['sconto2'])
//            ->addFieldToFilter('sconto3', $discountData['sconto3'])
            ->getFirstItem();
        if($lightechDiscountsCollection->getData()){
            $discountData["entity_id"] = $lightechDiscountsCollection->getData("entity_id");
        }
        return $discountData;
    }

    /**
     * @param $discountData
     * @return LightechDiscounts|int
     */
    public function saveData($discountData){
        try{
            $id = $this->productValidator($discountData['sku']);
            if(!$id){
                return 0;
            }
            $discountData['product_id'] = $id;
            $discountData['sconto'] = $this->getSconto($discountData['sconto1'],$discountData['sconto2'],$discountData['sconto3']);
            $discountData = $this->isDiscountExist($discountData);
            $discountDetails = $this->discountModel->setData($discountData)->save();
        }catch(\Exception $e){
            $this->messageManager->addError($e);
            return 0;
        }
        return $discountDetails;
    }

    /**
     * @param $sconto1
     * @param $sconto2
     * @param $sconto3
     * @return float|int
     */
    public function getSconto($sconto1,$sconto2,$sconto3){
        $sconto1 = 1 - (100 - $sconto1)/100;
        $sconto2 = 1 - (100 - $sconto2)/100;
        $sconto3 = 1 - (100 - $sconto3)/100;
        return number_format($sconto3 * $sconto2 * $sconto1,6);
    }
}
