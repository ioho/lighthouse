<?php
namespace Appseconnect\LightechDiscounts\Model\Page\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */
class IsActive implements OptionSourceInterface {
	/**
	 *
	 * @var \Magento\Cms\Model\Page
	 */
	protected $cmsPage;
	
	/**
	 * Constructor
	 *
	 * @param \Magento\Cms\Model\Page $cmsPage        	
	 */
	public function __construct(\Magento\Cms\Model\Page $cmsPage) {
		$this->cmsPage = $cmsPage;
	}
	
	/**
	 * Get options
	 *
	 * @return array
	 */
	public function toOptionArray() {
		$options = array ();
		
		$options [] = array (
				'label' => 'No',
				'value' => 0 
		);
		$options [] = array (
				'label' => 'Yes',
				'value' => 1 
		);
		
		return $options;
	}
}
