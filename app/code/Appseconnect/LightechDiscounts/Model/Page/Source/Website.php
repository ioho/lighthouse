<?php
namespace Appseconnect\LightechDiscounts\Model\Page\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Website
 */
class Website implements OptionSourceInterface {
	/**
	 *
	 * @var \Magento\Cms\Model\Page
	 */
	protected $cmsPage;
	
	/**
	 * Constructor
	 *
	 * @param \Magento\Cms\Model\Page $cmsPage        	
	 */
	public function __construct(\Magento\Cms\Model\Page $cmsPage) {
		$this->cmsPage = $cmsPage;
	}
	
	/**
	 * Get options
	 *
	 * @return array
	 */
	public function toOptionArray() {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance ();
		$websiteCollection = $objectManager->create ( 'Magento\Store\Model\ResourceModel\Website\CollectionFactory' )->create ();
		$output = $websiteCollection->getData ();
		$result = array ();
	
		foreach ( $output as $val ) {
			$result [] = array (
					'label' => $val ['name'],
					'value' => $val ["website_id"] 
			);
		}
		return $result;
	}
}
