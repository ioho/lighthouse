<?php
namespace Appseconnect\LightechDiscounts\Model\Page\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Website
 */
class Classe implements OptionSourceInterface {
	/**
	 *
	 * @var \Magento\Cms\Model\Page
	 */
	protected $eavAttributeRepository;
	
	/**
	 * Constructor
	 *
	 * @param \Magento\Cms\Model\Page $cmsPage        	
	 */
	public function __construct(\Magento\Eav\Api\AttributeRepositoryInterface $eavAttributeRepository) {
		$this->eavAttributeRepository = $eavAttributeRepository;
	}
	
	/**
	 * Get options
	 *
	 * @return array
	 */
	public function toOptionArray() {
        $attribute = $this->eavAttributeRepository
            ->get(\Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE,'classe');
        $output = $attribute->getSource()->getAllOptions(false);
		$result = array ();
	
//		foreach ( $output as $val ) {
//			$result [] = array (
//					'label' => $val ['name'],
//					'value' => $val ["website_id"]
//			);
//		}
		return $output;
	}
}
