<?php

namespace AmastyExtension\ProductAttachmentExtension\Model\File;

use Amasty\ProductAttachment\Api\Data\FileScopeInterface;
use Amasty\ProductAttachment\Controller\Adminhtml\RegistryConstants;
use Amasty\ProductAttachment\Model\ConfigProvider;
use Amasty\ProductAttachment\Model\File\FileScope\DataProviders\Product as ProductDataProvider;
use Amasty\ProductAttachment\Model\File\FileScope\DataProviders\ProductCategories;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Amasty\ProductAttachment\Model\File\Repository;

class ProductOverride extends \Amasty\ProductAttachment\Model\File\FileScope\DataProviders\Frontend\Product
{

    /**
     * @var Repository
     */
    private $fileRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var  \Amasty\ProductAttachment\Model\File\FileScope\DataProviders\Frontend\File
     */
    private $fileDataProvider;

    /**
     * @var ProductCategories
     */
    private $productCategoriesDataProvider;

    /**
     * @var ProductDataProvider
     */
    private $productDataProvider;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(
        Repository\Proxy $fileRepository,
        \Amasty\ProductAttachment\Model\File\FileScope\DataProviders\Frontend\File $fileDataProvider,
        ProductRepositoryInterface $productRepository,
        ProductCategories\Proxy $productCategoriesDataProvider,
        ConfigProvider $configProvider,
        ProductDataProvider $productDataProvider
    ) {

        $this->fileRepository = $fileRepository;
        $this->fileDataProvider = $fileDataProvider;
        $this->productRepository = $productRepository;
        $this->productCategoriesDataProvider = $productCategoriesDataProvider;
        $this->configProvider = $configProvider;
        $this->productDataProvider = $productDataProvider;
    }

    /**
     * @inheritdoc
     */
    public function aroundExecute(\Amasty\ProductAttachment\Model\File\FileScope\DataProviders\Frontend\Product $subject,
                                  \Closure $proceed, $params)
    {
        $result = [];
        $fileIds = [];
        if ($productFiles = $this->productDataProvider->execute($params)) {
            foreach ($productFiles as $productFile) {
//START MODIFICA CORE - catch exception
                try{
                    $file = $this->fileRepository->getById($productFile[FileScopeInterface::FILE_ID]);
                    $file->addData($productFile);
                    $fileIds[] = $file->getFileId();
                    if ($file = $this->fileDataProvider->processFileParams($file, $params)) {
                        $result[] = $file;
                    }
                }catch (\Exception $ex){

                }
//END MODIFICA CORE - catch exception
            }
        }

        if ($this->configProvider->addCategoriesFilesToProducts()) {
            $params[RegistryConstants::EXCLUDE_FILES] = $fileIds;
            if ($product = $this->productRepository->getById($params[RegistryConstants::PRODUCT])) {
                $params[RegistryConstants::PRODUCT_CATEGORIES] = $product->getCategoryIds();
                if ($productCategoriesFiles = $this->productCategoriesDataProvider->execute($params)) {
                    foreach ($productCategoriesFiles as $productCategoryFile) {
//START MODIFICA CORE - catch exception
                        try{
                            $file = $this->fileRepository->getById($productCategoryFile[FileScopeInterface::FILE_ID]);
                            $file->addData($productCategoryFile);
                            if ($file = $this->fileDataProvider->processFileParams($file, $params)) {
                                $result[] = $file;
                            }
                        }catch (\Exception $ex){

                        }
//END MODIFICA CORE - catch exception
                    }
                }
            }
        }

        return $result;
    }
}
