define([
    'jquery',
    'mage/url',
    'domReady!',
    'jquery/jquery.cookie'
], function ($,urlBuilder) {

    'use strict';

    return function () {



        var linkUrl = urlBuilder.build('retrievecustomer/index/index');



        $.ajax({
            showLoader: true,
            url: linkUrl,

            type: "POST",
            dataType: 'json'

        }).done(function (data) {

            console.log(data);
            if(data!=true)
            {
                $('#product-options-wrapper').hide();
            }

            $('#new_pr_cry').html(data);
        });


    }
});




