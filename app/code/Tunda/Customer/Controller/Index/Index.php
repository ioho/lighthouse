<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Tunda\Customer\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    protected $session;

    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context              $context,
        \Magento\Framework\View\Result\PageFactory         $resultPageFactory,
        \Magento\Framework\Json\Helper\Data                $jsonHelper,
        \Magento\Customer\Model\Session                    $session



    )
    {
        $this->resultPageFactory = $resultPageFactory;



        $this->jsonHelper = $jsonHelper;
        $this->session = $session;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {


            //$logger->info($discount." disc");


            return $this->jsonResponse($this->isCustomerLoggedIn());


            return $this->jsonResponse($molt);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {

            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }

    public function isCustomerLoggedIn()
    {
        return $this->session->isLoggedIn();
    }
}
