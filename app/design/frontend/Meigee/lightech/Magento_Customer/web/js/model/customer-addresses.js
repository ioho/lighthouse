/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define([
    'jquery',
    'ko',
    './customer/address'
], function ($, ko, Address) {
    'use strict';

    var isLoggedIn = ko.observable(window.isCustomerLoggedIn);

    return {
        /**
         * @return {Array}
         */
        getAddressItems: function () {
            var items = [],
                customerData = window.customerData;
            

            if (isLoggedIn()) {

                if (Object.keys(customerData).length) {

                    $.each(customerData.addresses, function (key, item) {
                        var match = false;
                    if ( typeof item['custom_attributes'] !== "undefined" && item['custom_attributes']) {
                        $.each(item['custom_attributes'], function( index, value ){

                            if (index == 'address_type'){

                                $.each(value, function(index2, value2){
                                    if (index2 != undefined && index2 != null && index2 == 'value'
                                        &&
                                        value2 != undefined && value2 != null && value2.trim().toUpperCase() == 'S')
                                    {
                                        match = true;
                                    }
                                });
                            }
                        });
                    }else{
                         match = true;
                    }

                        if (match){
                            items.push(new Address(item));
                        }
                    });
                }
            }

            return items;
        }
    };
});
