define( ['jquery', 'Magento_Ui/js/modal/modal', 'mage/cookies' ], function( $, modal, cookies ) {
	return function( config, element ) {
		'use strict';

		var options = {
				type: 'popup',
				responsive: true,
				innerScroll: true,
				modalClass: 'popup-block-modal modal-popup--subscribe',
				buttons: false
			},
			popupBlock = $(element),
			subscribeFlag = $.mage.cookies.get( 'coccinellePopup' ),
			contentWrapper = popupBlock.find('.popup-content-wrapper'),
			bgImg = contentWrapper.data('bgimg'),
			showCheckbox = $(config.checkboxClass);

		$( document ).ready( function() {

			modal( options, popupBlock );

			popupBlock.find('.action.subscribe').on( 'click', function() {

				if ( !popupBlock.find('.mage-error').length && !$('#subscribecheck').attr('aria-invalid') ) {

					$.mage.cookies.set( 'coccinellePopupFlag', 'true', {
						expires: new Date( new Date().getTime() + 30 * 1000 * 60 * 60 * 24 ),
						domain: window.location.host,
						path: '/'
					} );

				} else {

					$.mage.cookies.clear( 'coccinellePopupFlag' );

				}

			} );

			if ( !subscribeFlag && !$.mage.cookies.get( 'coccinellePopupFlag' ) ) {
				
				if( !($('body').hasClass('weltpixel_quickview-catalog_product-view') || $('body').hasClass('weltpixel-quickview-catalog-product-view')) ){
					popupBlock.modal('openModal');
				}

			} else {

				$.mage.cookies.clear( 'coccinellePopup' );
				subsSetCookie();

			}

			popupBlock.closest('body').css( {
				'padding': 0,
				'overflow': 'visible'
			} );

			popupBlock.find(showCheckbox).on( 'click', function() {

				$.mage.cookies.clear( 'coccinellePopup' )
				if ( $(this).prop('checked') ) subsSetCookie();

			} );

			if ( bgImg ) contentWrapper.attr('style', bgImg);

		} );

		function subsSetCookie() {
			var cookieExpires = Math.abs( parseInt( config.expiresCookie ) );

			// Set expires cookie: if 0 = 10 years, else if undefined = 30 days
			cookieExpires = cookieExpires == 0 ? 3650 : typeof cookieExpires == 'undefined' ? 30 : cookieExpires;

			cookieExpires = new Date( new Date().getTime() + cookieExpires * 1000 * 60 * 60 * 24 );

			$.mage.cookies.set( 'coccinellePopup', 'true', {
				expires: cookieExpires,
				domain: window.location.host,
				path: '/'
			} );

		}

	}

} );
