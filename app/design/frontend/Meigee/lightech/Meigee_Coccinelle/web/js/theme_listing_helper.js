define(['jquery', 'mage/cookies'], function ( $ ) {
	return function( config ) {
		'use strict';

		var listingCustomGrid = config.customSwitcher;

		if ( listingCustomGrid == 1 ) {
			var productList = $('.products.wrapper.products-grid'),
				switchItem = $('.modes .item'),
				columnClasses = [
					'one-column',
					'two-columns',
					'three-columns',
					'four-columns',
					'five-columns',
					'six-columns',
					'seven-columns',
					'eight-columns',
				];

			if ( window.matchMedia('(min-width: 768px)').matches ) switchItem = switchItem.not( $('.mobile') );

			$( window ).on( 'resize', function() {
				moveListingBanner();
			} );

		}

		$( document ).ready( function() {

			if ( listingCustomGrid == 1 ) {

				changeSwitcher();
				loadSwitcherCurrent() ;

			} else {

				listingBanner();

			}

		} );

		function changeSwitcher() {

			switchItem.on( 'click', function() {
				var switchCurrentData = $(this).data('grid-switcher');

				if ( !$(this).hasClass('current') ) {

					switchItem.removeClass('current');
					$(this).addClass('current');
					removeCookie();
					setCookie( switchCurrentData );
					listingGridSwitch( switchCurrentData );
				}

			} );

		}
		function loadSwitcherCurrent() {
			var cookieColumns = getCookie(),
				currentSwitch = $('.modes').find('[data-grid-switcher="'+cookieColumns+'"]');

			switchItem.removeClass('current');

			if ( window.matchMedia('(min-width: 1008px)').matches ) {

				if ( !currentSwitch.length || currentSwitch.is( $('.mobile') ) ) {

					switchItem.first().addClass('current');
					cookieColumns = $('.modes .item.current').not( $('.mobile') ).data('grid-switcher');

				}

			} else if ( window.matchMedia('(min-width: 768px) and (max-width: 1007px').matches ) {

				$('body[class*="page-layout-1column"]').length ? cookieColumns = 'four-columns' : cookieColumns = 'three-columns';

			} else {

				if ( !cookieColumns || cookieColumns == 'undefined' || ( cookieColumns != 'one-column' && cookieColumns != 'two-columns' ) ) cookieColumns = 'two-columns';

			}

			$('.modes').find('[data-grid-switcher="'+cookieColumns+'"]').addClass('current');
			removeCookie();
			setCookie( cookieColumns );
			listingGridSwitch( cookieColumns );

		}
		function removeCookie() {
			$.mage.cookies.clear('coccinelleListingGridSwitcher');
		}
		function getCookie() {
			return $.mage.cookies.get('coccinelleListingGridSwitcher');
		}
		function setCookie( columns ) {
			$.mage.cookies.set('coccinelleListingGridSwitcher', columns, {
				expires: null,
				domain: window.location.host,
				path: '/'
			} );
		}
		function listingGridSwitch( switchCurrentData ) {
			var cookieColumns = getCookie(),
				bodyNode = document.querySelector('body'),
				moreViews = document.querySelectorAll('.more-views');

			$.each( columnClasses, function( index, columns ) {
				productList.removeClass(columns);
			} );

			!cookieColumns ? productList.addClass(switchCurrentData) : productList.addClass(cookieColumns);

			listingBanner();
			moveListingBanner();

			if ( bodyNode.classList.contains('catalog-category-view') && moreViews.length ) setTimeout( reinitCarousel( moreViews ), 500 );

		}
		function reinitCarousel( moreViews ) {

			$('.more-views').trigger('refresh.owl.carousel');

			moreViews.forEach( function( product ) {

				product.style.maxWidth = 'none';

			} );

		}
		function listingBanner() {
			var listingBanner = $('.listing-banner'),
				listingBannerWrapper = $('.listing-banner-wrapper'),
				categoryDescription = $('.category-description');

			if ( categoryDescription.find(listingBanner).length ) {
				var mode = listingBanner.data('mode'),
					position = listingBanner.data('position'),
					rowPosition = listingBanner.data('rowPosition');

				if ( !listingBannerWrapper.length ) {
					listingBanner.wrap('<li class="item listing-banner-wrapper"/>');
					listingBannerWrapper = $('.listing-banner-wrapper');

					if ( !categoryDescription.find(listingBannerWrapper).length ) listingBannerWrapper.appendTo(categoryDescription);

				}

				switch ( mode ) {
					case 'grid':
						var productsGrid = $('.products-grid');

						if ( productsGrid.length ) {

							productsGrid.closest('.main-container').find('.category-description li.item').css('display', 'block');

							if ( rowPosition == 1 ) rowPosition = 2;

							if ( rowPosition ) {
								var columns = 1;

								if ( window.matchMedia('(min-width: 375px) and (max-width: 767px)').matches ) {
									columns = 2;

									if ( productsGrid.hasClass('one-column') ) columns = 1;

								} else if ( window.matchMedia('(max-width: 374px)').matches ) {
									columns = 1;

									if ( productsGrid.hasClass('two-columns' ) ) columns = 2;

								} else if ( window.matchMedia('(min-width: 768px)').matches ) {

									if ( productsGrid.hasClass('eight-columns') ) {
										columns = 8;
									} else if ( productsGrid.hasClass('seven-columns') ) {
										columns = 7;
									} else if ( productsGrid.hasClass('six-columns') ) {
										columns = 6;
									} else if ( productsGrid.hasClass('five-columns') ) {
										columns = 5;
									} else if ( productsGrid.hasClass('four-columns') ) {
										columns = 4;
									} else if ( productsGrid.hasClass('three-columns') ) {
										columns = 3;
									} else if ( productsGrid.hasClass('two-columns') ) {
										columns = 2;
									}

								}

								if ( !productsGrid.find(listingBannerWrapper).length ) {
									productsGrid.find('li.item:nth-of-type(' + columns*(rowPosition-1) + ')').after( $('.category-description .listing-banner-wrapper').addClass('row-banner') );
								}

							} else {

								if ( !productsGrid.find(listingBannerWrapper).length ) {
									productsGrid.find('li.item:nth-of-type(' + (position-1) + ')').after( $('.category-description .listing-banner-wrapper') );
								}

							}
						}

						break;

					case 'list':
						var productsList = $('.products-list');

						if ( productsList.length ) {

							productsList.closest('.main-container').find(categoryDescription).find('li.item').css('display', 'block');

							if ( rowPosition == 1 ) rowPosition = 2;

							if ( rowPosition ) {

								if ( !productsList.find(listingBannerWrapper).length ) {
									productsList.find('li.item:nth-of-type(' + (rowPosition-1) + ')').after($('.category-description .listing-banner-wrapper').addClass('row-banner'));
								}

							} else {

								if ( !productsList.find(listingBannerWrapper).length ) {
									productsList.find('li.item:nth-of-type(' + (position-1) + ')').after(listingBannerWrapper);
								}
							}

						}

						break;

				}
			}
		}
		function moveListingBanner() {
			var productsGrid = $('.products-grid'),
				listingBannerWrapper = $('.listing-banner-wrapper'),
				listingBanner = $('.listing-banner');

			if ( productsGrid.find(listingBannerWrapper).length ) {
				var rowPosition = productsGrid.find(listingBannerWrapper).find(listingBanner).data('rowPosition');

				if ( rowPosition ) {
					var columns = 1;

					if ( window.matchMedia('(min-width: 375px) and (max-width: 767px)').matches ) {
						columns = 2;

						if ( productsGrid.hasClass('one-column') ) columns = 1;

					} else if ( window.matchMedia('(max-width: 374px)').matches ) {
						columns = 1;

						if ( productsGrid.hasClass('two-columns') ) columns = 2;

					} else if ( window.matchMedia('(min-width: 768px)').matches ) {

						if ( productsGrid.hasClass('eight-columns') ) {
							columns = 8;
						} else if ( productsGrid.hasClass('seven-columns') ) {
							columns = 7;
						} else if ( productsGrid.hasClass('six-columns') ) {
							columns = 6;
						} else if ( productsGrid.hasClass('five-columns') ) {
							columns = 5;
						} else if ( productsGrid.hasClass('four-columns') ) {
							columns = 4;
						} else if ( productsGrid.hasClass('three-columns') ) {
							columns = 3;
						} else if ( productsGrid.hasClass('two-columns') ) {
							columns = 2;
						}

					}

					var banner = listingBannerWrapper.remove();

					productsGrid.find('li.item:not(.listing-banner-wrapper):nth-of-type(' + columns*(rowPosition-1) + ')').after(banner);
				}
			}
		}

	}

} );
