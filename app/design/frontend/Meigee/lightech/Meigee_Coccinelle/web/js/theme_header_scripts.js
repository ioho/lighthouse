define(['jquery'], function ( $ ) {
	return function( config ) {
		'use strict';

				var menu_select = $('.megamenu-wrapper .amfinder-select');
				var close_btn = null;

				menu_select.on( 'change', function(){
					console.log('select change');
					if (!$(this).closest('.megamenu-wrapper').hasClass('fixed-position')) {
						$(this).closest('.megamenu-wrapper').addClass('fixed-position').prepend('<span class="megamenu-wrapper-close"></span>');
					}
					close_btn = $(this).closest('.megamenu-wrapper').find('.megamenu-wrapper-close');
					init_close_btn(close_btn);
				});

				function init_close_btn(close_btn) {
						close_btn.on( 'click', function(){
							console.log('btn click');
							$(this).parent().removeClass('fixed-position');
							$(this).closest('.menu-active').removeClass('menu-active');
							$(this).remove();
						});
				}
	}
});
